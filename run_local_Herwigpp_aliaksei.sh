DIR_RUN=Test_Herwigpp
JOB_OPTION=GenerateJetsJO_PY8_EIG.py

# Create working directory
rm -r    ${DIR_RUN}
mkdir -p ${DIR_RUN}

# Copy All needed file
cp *.so          ${DIR_RUN}/.
cp ${JOB_OPTION} ${DIR_RUN}/.

# cd to working directory
cd ${DIR_RUN}

# setup Rivet
source setupRivet.sh
export RIVET_ANALYSIS_PATH=$PWD

# Run Gen_tf.py
Generate_tf.py \
--ecmEnergy=13000 \
--firstEvent=1 \
--randomSeed=123456789 \
--jobConfig=${JOB_OPTION} \
--runNumber=100000 \
--outputEVNTFile=pool.root \
--maxEvents=10 \
--env NPC_JSAMPLE=1 NPC_TUNE=UEEE5MSTW2008 NPC_GEN=Herwigpp NPC_EFFECT=ParticleLVL

#--env NPC_JSAMPLE=1 NPC_TUNE=UEEE5MSTW2008 NPC_GEN=Herwigpp NPC_EFFECT=PartonLVL
#--env NPC_JSAMPLE=1 NPC_TUNE=UEEE5 NPC_GEN=Herwigpp NPC_EFFECT=PartonLVL
#--env NPC_JSAMPLE=1 NPC_TUNE=CTEQEE4 NPC_GEN=Herwigpp NPC_EFFECT=PartonLVL
#--env NPC_JSAMPLE=1 NPC_TUNE=UEEE5CTEQ6L1 NPC_GEN=Herwigpp NPC_EFFECT=PartonLVL
