DIR_RUN=Test_H7_rel23
JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ9plus.py
#JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ4.py
#RIVET_ANALYSIS_OBJ=RivetATLAS_2023_IJXS_IDXS_Rivet_3_1_8.so
RIVET_ANALYSIS_OBJ=RivetATLAS_2023_IJXS_IDXS_Rivet_3_1_8_ROOT.so

#RIVET_FILES=src/ATLAS_2023_IJXS_IDXS_Rivet_3_1_8*

# Create working directory
rm -r    ${DIR_RUN}
mkdir -p ${DIR_RUN}

# Copy All needed file
#cp *.so          ${DIR_RUN}/.
cp ${RIVET_ANALYSIS_OBJ} ${DIR_RUN}/.
cp ${JOB_OPTION}         ${DIR_RUN}/.
#cp ${RIVET_FILES}        ${DIR_RUN}/.

# cd to working directory
cd ${DIR_RUN}

# setup Rivet
setupRivet
export RIVET_ANALYSIS_PATH=$PWD


# Run Gen_tf.py
Gen_tf.py \
--ecmEnergy=13000 \
--firstEvent=1 \
--randomSeed=123456789 \
--jobConfig=$PWD \
--runNumber=100000 \
--outputEVNTFile=pool.root \
--maxEvents=10 \
--rivetAnas=ATLAS_2023_IJXS_IDXS_Rivet_3_1_8 \
--env NPC_JSAMPLE=1 NPC_TUNE=BaryonicReconnectionTune NPC_GEN=Herwig7 NPC_EFFECT=PartonLVL


#--rivetAnas=MC_GENERIC,${RIVET_CC} \
#--extFile=RivetATLAS_2023_IJXS_IDXS_Rivet_3_1_8.so \
#--rivetAnas=RivetATLAS_2023_IJXS_IDXS_Rivet_3_1_8.so \

