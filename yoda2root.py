# Convert yoda to root file
# Taken from  https://gitlab.cern.ch/dta/athena/-/tree/master/Generators/Rivet_i
# In section: Converting YODA files to ROOT format

#print "Before averything"
from array import array
#print "Before ROOT"

import ROOT as rt
#print "Before yoda"

import yoda
#print "Before argparse"

import argparse

outFile_default = "inFile.root"
parser = argparse.ArgumentParser()
parser.add_argument('--inFile',      help='Example: --inFile inFile.yoda (Note conversion works for 1D histograms only, other are skiped)',     type=str,   default="inFile.yoda")
parser.add_argument('--outFile',     help='Example: --outFile outFile.root',      type=str,   default=outFile_default)

args=parser.parse_args()
fName    = args.inFile
fNameOut = args.outFile

print ("fName: ")
print (str(fName))

if args.outFile == outFile_default:
	new_outFileName = fName
	new_outFileName = new_outFileName[:new_outFileName.find('.yoda')] + '.root' #new_outFileName.repace(".yoda", ".root")
	fNameOut = new_outFileName


#help(yoda.read)
#help(yoda.Write)

yodaAOs = yoda.read(fName)
print (yodaAOs)
#rtFile = rt.TFile(fName[:fName.find('.yoda')] + '.root', 'recreate')
rtFile = rt.TFile(fNameOut, 'recreate')

l_absPaths = list()

for name in yodaAOs:
  yodaAO = yodaAOs[name];  rtAO = None
  
  ##############################################
  # Ota make update to consider TDirectories 
  # for different rivet rutines
  print ("\n\n\n")
  print ("Working on: " + name)
  
  # Extract names of subdirectories
  lsubDirs_tmp = name.split('/')
  lsubDirs = [subDir for subDir in lsubDirs_tmp if subDir != ""]
  lsubDirs.pop()
  
  # Create subdirectories - TDirectory structure
  if len(lsubDirs) > 0:
    absPath = ""
    for subDir in lsubDirs:
      absPath += subDir + "/"
      print ("\t Check TDirectory:")
      print ("\t absPath: " + str(absPath))
      print ("\t tFile.GetDirectory(absPath):" + str(rtFile.GetDirectory(absPath)))
      
      # create new TDirectory if does not exist already
      if absPath not in l_absPaths:
        rtFile.mkdir(absPath)
        l_absPaths.append(absPath)
      
      # Move to the TDirectory
      print ("\t cd: " + str(subDir))
      rtFile.cd(subDir)
      
  
  # TH1D histograms
  if 'Histo1D' in str(yodaAO):
    hist_name = lsubDirs_tmp[-1]
    hist_path = name
    hist_path = hist_path.replace(hist_name, "")
    print ("\n\n\n")
    print ("\t hist_path: " + hist_path)
    print ("\t hist_name: " + name)

    rtFile.cd("")
    rtFile.cd(hist_path)
    
    # Ota make update
    # use hist_name variable instead of name variable
    print ("\t convert yoda object to TH1D object")
    rtAO = rt.TH1D(hist_name, '', yodaAO.numBins(), array('d', yodaAO.xEdges()))
    rtAO.Sumw2(); rtErrs = rtAO.GetSumw2()
    for i in range(rtAO.GetNbinsX()):
      rtAO.SetBinContent(i + 1, yodaAO.bin(i).sumW())
      rtErrs.AddAt(yodaAO.bin(i).sumW2(), i+1)
  # 1D Scatter plot
  #elif 'Scatter1D' in str(yodaAO):
  #  hist_name = lsubDirs_tmp[-1]
  #  hist_path = name
  #  hist_path = hist_path.replace(hist_name, "")
  #  print ("\n\n\n")
  #  print ("\t hist_path: " + hist_path)
  #  print ("\t hist_name: " + name)
  #  
  #  rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
  #  for i in range(yodaAO.numPoints()):
  #    x = yodaAO.point(i).x(); 
  #    xLo, xHi = yodaAO.point(i).xErrs()
  #    rtAO.SetPoint(i, x, 0.0)
  #    rtAO.SetPointError(i, xLo, xHi, 0.0, 0.0)
  #
  # TH2D histograms
  #elif 'Scatter2D' in str(yodaAO) or 'Histo2D' in str(yodaAO):
  #  rtAO = rt.TGraphAsymmErrors(yodaAO.numPoints())
  #  for i in range(yodaAO.numPoints()):
  #    x = yodaAO.point(i).x(); y = yodaAO.point(i).y()
  #    xLo, xHi = yodaAO.point(i).xErrs()
  #    yLo, yHi = yodaAO.point(i).yErrs()
  #    rtAO.SetPoint(i, x, y)
  #    rtAO.SetPointError(i, xLo, xHi, yLo, yHi)
  else:
    print ("\t str(yodaAO): " + str(yodaAO))
    print ("\t This object " + name + "is not converted/saved to root file")
    continue
  # Save the object to rootFile to the current TDirectory
  rtAO.Write(hist_name)
  
  # Move to the main directroy in the root-file
  if len(lsubDirs) > 0:
    rtFile.cd("")
  
rtFile.Close()
