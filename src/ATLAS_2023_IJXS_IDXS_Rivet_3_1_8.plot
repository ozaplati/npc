BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/nFiles
Title=Number of files
XLabel=Counter
YLabel=nFiles
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/nEvents
Title=Number of events
XLabel=Counter
YLabel=nEvents
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/nSelectedEvents
Title=Number of selected dijet events
XLabel=Counter
YLabel=nEvents 
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/sumOfWeights
Title=Sum of weight 
XLabel=
YLabel=sumOfWeights 
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/crossSection
Title=Cross-section
XLabel=
YLabel=crossSection 
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/filtEff
Title=Filter efficiency
XLabel=
YLabel=filtEff 
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y0
Title=Inclusive jet cross-section, $0.0 < |y|< 0.5$ 
XLabel=$p_{T}$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y1
Title=Inclusive jet cross-section, $0.5 < |y|< 1.0$
XLabel=$p_T$ [GeV]
YLabel=$d^2  \sigma/dp_T d|y|$
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y2
Title=Inclusive jet cross-section, $1.0 < |y|< 1.5$
XLabel=$p_T$ [GeV]
YLabel=$d^2  \sigma/dp_T d|y|$
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y3
Title=Inclusive jet cross-section, $1.5 < |y|< 2.0$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT

BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y4
Title=Inclusive jet cross-section, $2.0 < |y|< 2.5$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y5
Title=Inclusive jet cross-section, $2.5 < |y|< 3.0$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y6
Title=Inclusive jet cross-section, $3.0 < |y|< 3.5$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y7
Title=Inclusive jet cross-section, $3.5 < |y|< 4.0$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_pt_y8
Title=Inclusive jet cross-section, $4.0 < |y|< 4.5$
XLabel=$p_T$ [GeV]
YLabel=$d^2 \sigma/dp_T d|y|$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar0
Title=Inclusive dijet cross-section, $0.0 <y*< 0.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 sigma/dmjjdy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar1
Title=Inclusive dijet cross-section, $0.5 <y*< 1.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar2
Title=Inclusive dijet cross-section, 1.0 <y*< 1.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar3
Title=Inclusive dijet cross-section, $1.5 <y*< 2.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar4
Title=Inclusive dijet cross-section, $2.0 <y*< 2.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_ystar5
Title=Inclusive dijet cross-section, $2.5 <y*< 3.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy*$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost0
Title=Inclusive dijet cross-section, $0.0 <y_{boost}< 0.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost1
Title=Inclusive dijet cross-section, $0.5 <y_{boost}< 1.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost2
Title=Inclusive dijet cross-section, $1.0 <y_{boost}< 1.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost3
Title=Inclusive dijet cross-section, $1.5 <y_{boost}< 2.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost4
Title=Inclusive dijet cross-section, $2.0 <y_{boost}< 2.5$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT


BEGIN PLOT /ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/h1_mjj_yboost5
Title=Inclusive dijet cross-section, $2.5 <y_{boost}< 3.0$
XLabel=$m_{jj}$ [GeV]
YLabel=$d^2 \sigma/dm_{jj} dy_{boost}$
END PLOT
