// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DirectFinalState.hh"

#include <vector>
#include <filesystem>

/// Root Objects
#include <TH1D.h>
#include <TFile.h>

namespace Rivet {


  /// @brief Add a short analysis description here
  class ATLAS_2023_IJXS_IDXS_Rivet_3_1_8 : public Analysis 
  {
    public:
    
    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(ATLAS_2023_IJXS_IDXS_Rivet_3_1_8);


    /// @name Analysis methods
    /// @{

    /// Book histograms and initialise projections before the run
    void init() {
      if (isDEBUG) cout << "void init() method - begin" << endl;

      // Initialise and register projections

      // The basic final-state projection:
      // all final-state particles within
      // the given eta acceptance
      const FinalState fs(Cuts::abseta < 4.9);

      /// The final-state particles declared above are clustered using FastJet with
      /// the anti-kT algorithm and a jet-radius parameter 0.4
      /// muons and neutrinos are excluded from the clustering
      //FastJets jetfs(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      ///  warning: 'void Rivet::JetFinder::useInvisibles(bool)' is deprecated: make an explicit choice from Invisibles::{NONE,DECAY,ALL}. This boolean call does not allow for ALL [-Wdeprecated-declarations]
      FastJets jetfs(fs, FastJets::ANTIKT, 0.4);
      jetfs.useInvisibles(true);
      
      declare(jetfs, "jets");

      /// FinalState of direct photons and bare muons and electrons in the event
      //DirectFinalState photons(Cuts::abspid == PID::PHOTON);
      //DirectFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON);

      /// Dress the bare direct leptons with direct photons within dR < 0.1,
      /// and apply some fiducial cuts on the dressed leptons
      //Cut lepton_cuts = Cuts::abseta < 2.5 && Cuts::pT > 20*GeV;
      //DressedLeptons dressed_leps(photons, bare_leps, 0.1, lepton_cuts);
      //declare(dressed_leps, "leptons");

      /// Missing momentum
      //declare(MissingMomentum(fs), "MET");

      /// Book histograms
      /// specify custom binning
      //book(_h["XXXX"], "myh1", 20, 0.0, 11000.0);
      
      //book(_h["YYYY"], "myh2", logspace(20, 1e-2, 11e3));
      //book(_h["ZZZZ"], "myh3", {0.0, 1.0, 2.0, 4.0, 8.0, 16.0});
      /// take binning from reference data using HEPData ID (digits in "d01-x01-y01" etc.)
      //book(_h["AAAA"], 1, 1, 1);
      //book(_p["BBBB"], 2, 1, 1);
      //book(_c["CCCC"], 3, 1, 1);
      
      /// Technical histograms
      book(h1_nFiles,                 "nFiles",          1,1,2);
      book(h1_nEvents,                "nEvents",         1,1,2);
      book(h1_nSelectedEvents_jets,   "nSelectedEvents_jets", 1,1,2);
      book(h1_nSelectedEvents_dijets, "nSelectedEvents_dijets", 1,1,2);
      book(h1_sumOfWeights,           "sumOfWeights",    1,1,2);
      book(h1_crossSection,           "crossSection",    1,1,2);
      book(h1_filtEff,                "filtEff",         1,1,2); 


      h_nFiles                 = new TH1D("nFiles", "nFiles",         1, 1.0, 2.0);
      h_nEvents                = new TH1D("nEvents", "nEvents",       1, 1.0, 2.0);
      h_nSelectedEvents_jets   = new TH1D("nSelectedEvents_jets", "nSelectedEvents_jets",     1, 1.0, 2.0);
      h_nSelectedEvents_dijets = new TH1D("nSelectedEvents_dijets", "nSelectedEvents_dijets", 1, 1.0, 2.0);
      h_sumOfWeights           = new TH1D("sumOfWeights", "sumOfWeights",    1, 1.0, 2.0);
      h_crossSection           = new TH1D("crossSection", "crossSection",    1, 1.0, 2.0);
      h_filtEff                = new TH1D("filtEff", "filtEff",              1, 1.0, 2.0); 
      
      
      h_fromLog_filtEff         = new TH1D("fromLog_filterEfficiecy",         "fromLog_filterEfficiecy",           1, 1.0, 2.0);
      h_fromLog_weightedFiltEff = new TH1D("fromLog_weightedFilterEfficiecy", "fromLog_weightedFilterEfficiecy",   1, 1.0, 2.0);
      h_fromLog_xs              = new TH1D("fromLog_crossSection",            "fromLog_crossSection",              1, 1.0, 2.0);

         
      /// Book 1D histograms
      /// Inclusive jet pT vs. y  - 1D histograms    
      book(h1_pt_y0, "h1_pt_y0", 5500, 0.0, 5500.0);
      book(h1_pt_y1, "h1_pt_y1", 5500, 0.0, 5500.0);
      book(h1_pt_y2, "h1_pt_y2", 5500, 0.0, 5500.0);
      book(h1_pt_y3, "h1_pt_y3", 5500, 0.0, 5500.0);
      book(h1_pt_y4, "h1_pt_y4", 5500, 0.0, 5500.0);
      book(h1_pt_y5, "h1_pt_y5", 5500, 0.0, 5500.0);
      book(h1_pt_y6, "h1_pt_y6", 5500, 0.0, 5500.0);
      book(h1_pt_y7, "h1_pt_y7", 5500, 0.0, 5500.0);
      book(h1_pt_y8, "h1_pt_y8", 5500, 0.0, 5500.0);
      
      h_pt_y0 = new TH1D( "h1_pt_y0", "h1_pt_y0", 5500, 0.0, 5500.0);
      h_pt_y1 = new TH1D( "h1_pt_y1", "h1_pt_y1", 5500, 0.0, 5500.0);
      h_pt_y2 = new TH1D( "h1_pt_y2", "h1_pt_y2", 5500, 0.0, 5500.0);
      h_pt_y3 = new TH1D( "h1_pt_y3", "h1_pt_y3", 5500, 0.0, 5500.0);
      h_pt_y4 = new TH1D( "h1_pt_y4", "h1_pt_y4", 5500, 0.0, 5500.0);
      h_pt_y5 = new TH1D( "h1_pt_y5", "h1_pt_y5", 5500, 0.0, 5500.0);
      h_pt_y6 = new TH1D( "h1_pt_y6", "h1_pt_y6", 5500, 0.0, 5500.0);
      h_pt_y7 = new TH1D( "h1_pt_y7", "h1_pt_y7", 5500, 0.0, 5500.0);
      h_pt_y8 = new TH1D( "h1_pt_y8", "h1_pt_y8", 5500, 0.0, 5500.0);
      
      /// Book 1D histograms
      /// Inclusive dijet mjj vs. ystar - 1D histograms
      book(h1_mjj_ystar0, "h1_mjj_ystar0", 11000, 0.0, 11000.0);
      book(h1_mjj_ystar1, "h1_mjj_ystar1", 11000, 0.0, 11000.0);
      book(h1_mjj_ystar2, "h1_mjj_ystar2", 11000, 0.0, 11000.0);
      book(h1_mjj_ystar3, "h1_mjj_ystar3", 11000, 0.0, 11000.0);
      book(h1_mjj_ystar4, "h1_mjj_ystar4", 11000, 0.0, 11000.0);
      book(h1_mjj_ystar5, "h1_mjj_ystar5", 11000, 0.0, 11000.0); 
      
      h_mjj_ystar0 = new TH1D( "h1_mjj_ystar0", "h1_mjj_ystar0", 11000, 0.0, 11000.0);
      h_mjj_ystar1 = new TH1D( "h1_mjj_ystar1", "h1_mjj_ystar1", 11000, 0.0, 11000.0);
      h_mjj_ystar2 = new TH1D( "h1_mjj_ystar2", "h1_mjj_ystar2", 11000, 0.0, 11000.0);
      h_mjj_ystar3 = new TH1D( "h1_mjj_ystar3", "h1_mjj_ystar3", 11000, 0.0, 11000.0);
      h_mjj_ystar4 = new TH1D( "h1_mjj_ystar4", "h1_mjj_ystar4", 11000, 0.0, 11000.0);
      h_mjj_ystar5 = new TH1D( "h1_mjj_ystar5", "h1_mjj_ystar5", 11000, 0.0, 11000.0); 
      
      
      /// Book 1D histograms
      /// Inclusive dijet mjj vs. yboost - 1D histograms 
      book(h1_mjj_yboost0, "h1_mjj_yboost0", 11000, 0.0, 11000.0);
      book(h1_mjj_yboost1, "h1_mjj_yboost1", 11000, 0.0, 11000.0);
      book(h1_mjj_yboost2, "h1_mjj_yboost2", 11000, 0.0, 11000.0);
      book(h1_mjj_yboost3, "h1_mjj_yboost3", 11000, 0.0, 11000.0);
      book(h1_mjj_yboost4, "h1_mjj_yboost4", 11000, 0.0, 11000.0);
      book(h1_mjj_yboost5, "h1_mjj_yboost5", 11000, 0.0, 11000.0);
      
      h_mjj_yboost0 = new TH1D( "h1_mjj_yboost0", "h1_mjj_yboost0", 11000, 0.0, 11000.0);
      h_mjj_yboost1 = new TH1D( "h1_mjj_yboost1", "h1_mjj_yboost1", 11000, 0.0, 11000.0);
      h_mjj_yboost2 = new TH1D( "h1_mjj_yboost2", "h1_mjj_yboost2", 11000, 0.0, 11000.0);
      h_mjj_yboost3 = new TH1D( "h1_mjj_yboost3", "h1_mjj_yboost3", 11000, 0.0, 11000.0);
      h_mjj_yboost4 = new TH1D( "h1_mjj_yboost4", "h1_mjj_yboost4", 11000, 0.0, 11000.0);
      h_mjj_yboost5 = new TH1D( "h1_mjj_yboost5", "h1_mjj_yboost5", 11000, 0.0, 11000.0);
      
      if (isDEBUG) cout << "void init() method - end" << endl;
    }
    
    
    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
      if (isDEBUG)
      {
         cout << "\n\n" << endl;
         cout << "void analyze(const Event& event) method - begin" << endl;
         cout << endl;
       }
      
      /// Retrieve dressed leptons, sorted by pT
      //Particles leptons = apply<FinalState>(event, "leptons").particles();

      /// Retrieve clustered jets, sorted by pT, with a minimum pT cut, with a maximum rapidity
      // Jets jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 30*GeV);
      // Jets kt4Jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 15*GeV && Cuts::absrap < 4.5);
      Jets kt4Jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 15*GeV && Cuts::absrap < 4.5);

      int nJets = kt4Jets.size();
      
      /// Event counter
      ///    Do not save nEvents to histograms in analyze method
      ///    All histograms filled in the analyze method are weighted by sumW() automatically
      ///    You should use event counter and apply fill->(1, nEvents) in finilize method
      ///    Apply the same for selected (di)jet events
      nEvents++;
       
      if (isDEBUG || nEvents%5000 == 0)
      {
        cout << "void analyze(const Event& event) method" << endl;
        cout << "iEvent: " << nEvents << endl;
        cout << endl;
      }
      
      /// Inclusive jet selection
      /// Loop over jets
      if (isDEBUG)
      {
        cout << "void analyze(const Event& event) method" << endl;
        cout << "Inclusive jets" << endl;
        cout << endl;
      }
      
      // Testing event weight effect
      double eventWeight = event.weights()[0];
      
      bool event_passed = false;
      for (int ijet = 0; ijet < nJets; ijet++)
      {
        FourMomentum jet = kt4Jets[ijet].momentum();
        
        const double pt = jet.pt();
        const double absy = jet.absrap();
        
        
        if (ijet == 0 && (isDEBUG || nEvents%5000 == 0))
        {
          cout << "List of jets" << endl;
        }
        
        if (isDEBUG || nEvents%5000 == 0)
        {
          cout << "\t jet - ind/n: " << ijet << "/" << nJets << endl;
          cout << "\t jet - pt:  " << pt  << endl;
          cout << "\t jet - |y|: " << absy << endl;
        }
        
        /// pT and absy selection
        if(jet.pt() > 15.0*GeV && absy < 4.5)
        {
          if (isDEBUG || nEvents%5000 == 0) cout << "\t jet passed selection \n" << endl;
          
          /// At least one jet passed incl. jet selection
          event_passed = true;
          
          if     ( 0.0 <= fabs(absy) && fabs(absy) < 0.5 ){ h1_pt_y0->fill(pt/GeV); h_pt_y0->Fill(pt/GeV, eventWeight); }
          else if( 0.5 <= fabs(absy) && fabs(absy) < 1.0 ){ h1_pt_y1->fill(pt/GeV); h_pt_y1->Fill(pt/GeV, eventWeight); }
          else if( 1.0 <= fabs(absy) && fabs(absy) < 1.5 ){ h1_pt_y2->fill(pt/GeV); h_pt_y2->Fill(pt/GeV, eventWeight); }
          else if( 1.5 <= fabs(absy) && fabs(absy) < 2.0 ){ h1_pt_y3->fill(pt/GeV); h_pt_y3->Fill(pt/GeV, eventWeight); }
          else if( 2.0 <= fabs(absy) && fabs(absy) < 2.5 ){ h1_pt_y4->fill(pt/GeV); h_pt_y4->Fill(pt/GeV, eventWeight); }
          else if( 2.5 <= fabs(absy) && fabs(absy) < 3.0 ){ h1_pt_y5->fill(pt/GeV); h_pt_y5->Fill(pt/GeV, eventWeight); }
          else if( 3.0 <= fabs(absy) && fabs(absy) < 3.5 ){ h1_pt_y6->fill(pt/GeV); h_pt_y6->Fill(pt/GeV, eventWeight); }
          else if( 3.5 <= fabs(absy) && fabs(absy) < 4.0 ){ h1_pt_y7->fill(pt/GeV); h_pt_y7->Fill(pt/GeV, eventWeight); }
          else if( 4.0 <= fabs(absy) && fabs(absy) < 4.5 ){ h1_pt_y8->fill(pt/GeV); h_pt_y8->Fill(pt/GeV, eventWeight); }
        }
        else
        {
           if (isDEBUG || nEvents%5000 == 0) cout << "\t jet did not pass selection \n" << endl;
        }
      }
      
      /// At least one jet passed incl. jet selection
      if ( event_passed == true )
      {
        nSelectedEvents_jets += 1;
      }

      
      
      /// Inclusive dijet selection
      if(nJets >= 2)
      {
        /// Skip events with less than 2 jets
        FourMomentum jet0   = kt4Jets[0].momentum(); 
        FourMomentum jet1   = kt4Jets[1].momentum();
        
        const double pt0   = jet0.pt();
        const double pt1   = jet1.pt();

        const double y0   = jet0.rapidity();
        const double y1   = jet1.rapidity();

        const double ystar  = fabs(y0-y1)/2.0;
        const double yboost = fabs(y0+y1)/2.0;
        
        const double mass  = (jet0 + jet1).mass(); 
        const double HT2   = jet0.pt()+jet1.pt();
        
        
        if (isDEBUG || nEvents%5000 == 0)
        {
          cout << "List of dijets" << endl;
          cout << "\t Dijet -  yStar:  " << ystar  << endl;
          cout << "\t Dijet -  yBoost: " << yboost << endl;
          cout << "\t Dijet -  mjj:    " << mass   << endl;
          cout << "\t Dijet -  HT2:    " << HT2    << endl;
          cout << "\t GeV:             " << GeV    << endl;
          cout << "\t 200*GeV:         " << 200*GeV    << endl;
          cout << "\t mass/GeV:        " << mass/GeV    << endl;
          cout << "\t weight:          " << event.weight()    << endl;          
          cout << "\t std::valarray< double > weights ():    " << event.weights() << endl;
          cout << "\t std::valarray< double > weights ()[0]: " << event.weights()[0] << endl;

          //cout << "w:               " << w << endl;
        }
        
        if( pt0 > 75*GeV && pt1 > 75*GeV && HT2 > 200*GeV && fabs(y0) < 3.0 && fabs(y1) < 3.0 && ystar < 3.0 )
        {
          /// Pass event with:
          ///    HT2     > 200 GeV
          ///    |y0|    < 3.0 
          ///    |y1|    < 3.0 
          ///    |ystar| < 3.0 
          ///    pT0     > 75 GeV
          ///    pT1     > 75 GeV - check this
          ///
          if (isDEBUG || nEvents%5000 == 0) cout << "\t dijet passed selection \n" << endl;
          
          nSelectedEvents_dijets += 1;
          
          /// Fill 1D distribution
          /// Inclusive dijet in ystar bins
          if     ( 0.0 <= fabs(ystar) && fabs(ystar) < 0.5 ){ h1_mjj_ystar0->fill(mass/GeV); h_mjj_ystar0->Fill(mass/GeV, eventWeight); }
          else if( 0.5 <= fabs(ystar) && fabs(ystar) < 1.0 ){ h1_mjj_ystar1->fill(mass/GeV); h_mjj_ystar1->Fill(mass/GeV, eventWeight); }
          else if( 1.0 <= fabs(ystar) && fabs(ystar) < 1.5 ){ h1_mjj_ystar2->fill(mass/GeV); h_mjj_ystar2->Fill(mass/GeV, eventWeight); }
          else if( 1.5 <= fabs(ystar) && fabs(ystar) < 2.0 ){ h1_mjj_ystar3->fill(mass/GeV); h_mjj_ystar3->Fill(mass/GeV, eventWeight); }
          else if( 2.0 <= fabs(ystar) && fabs(ystar) < 2.5 ){ h1_mjj_ystar4->fill(mass/GeV); h_mjj_ystar4->Fill(mass/GeV, eventWeight); }
          else if( 2.5 <= fabs(ystar) && fabs(ystar) < 3.0 ){ h1_mjj_ystar5->fill(mass/GeV); h_mjj_ystar5->Fill(mass/GeV, eventWeight); }
          
          /// Fill 1D distribution
          /// Inclusive dijet in yboost bins
          if     ( 0.0 <= fabs(yboost) && fabs(yboost) < 0.5 ){ h1_mjj_yboost0->fill(mass/GeV); h_mjj_yboost0->Fill(mass/GeV, eventWeight); }
          else if( 0.5 <= fabs(yboost) && fabs(yboost) < 1.0 ){ h1_mjj_yboost1->fill(mass/GeV); h_mjj_yboost1->Fill(mass/GeV, eventWeight); }
          else if( 1.0 <= fabs(yboost) && fabs(yboost) < 1.5 ){ h1_mjj_yboost2->fill(mass/GeV); h_mjj_yboost2->Fill(mass/GeV, eventWeight); }
          else if( 1.5 <= fabs(yboost) && fabs(yboost) < 2.0 ){ h1_mjj_yboost3->fill(mass/GeV); h_mjj_yboost3->Fill(mass/GeV, eventWeight); }
          else if( 2.0 <= fabs(yboost) && fabs(yboost) < 2.5 ){ h1_mjj_yboost4->fill(mass/GeV); h_mjj_yboost4->Fill(mass/GeV, eventWeight); }
          else if( 2.5 <= fabs(yboost) && fabs(yboost) < 3.0 ){ h1_mjj_yboost5->fill(mass/GeV); h_mjj_yboost5->Fill(mass/GeV, eventWeight); }          
        }
      }
      else
      {
        if (isDEBUG || nEvents%5000 == 0) cout << "\t dijet did not pass selection \n" << endl;
      }
      
      
      if (isDEBUG) cout << "void analyze(const Event& event) method - end" << endl;
    }


    /// Normalise histograms etc., after the run
    void finalize() 
    {
      
      if (isDEBUG) cout << "void finilize() method - begin" << endl;
      
      //normalize(_h["XXXX"]);                                  // normalize to unity
      //normalize(_h["YYYY"], crossSection()/picobarn);         // normalize to generated cross-section in pb (no cuts)
      //scale(    _h["ZZZZ"], crossSection()/picobarn/sumW());  // norm to generated cross-section in pb (after cuts)
      h1_nFiles->fill( 1 );  h_nFiles->Fill( 1 );
      h1_filtEff->fill( 1 ); h_filtEff->Fill( 1 );
      
      h1_nEvents->fill( 1, nEvents);                               h_nEvents->Fill( 1, nEvents);
      h1_nSelectedEvents_jets->fill( 1, nSelectedEvents_jets);     h_nSelectedEvents_jets->Fill( 1, nSelectedEvents_jets);
      h1_nSelectedEvents_dijets->fill( 1, nSelectedEvents_dijets); h_nSelectedEvents_dijets->Fill( 1, nSelectedEvents_dijets);
      
      //h1_crossSection->fill(1, getXSfromLog() ); h_crossSection->Fill(1, getXSfromLog() );                       /// Seems to work
      h1_crossSection->fill(1, crossSection() );   h_crossSection->Fill(1, crossSection() );                       /// Seems to work
      h1_sumOfWeights->fill(1, sumW() );           h_sumOfWeights->Fill(1, sumW() );                               /// Seems to work
      
      
      if (isDEBUG) cout << "getXSfromLog()           " << getXSfromLog() << endl;
      cout << "picobarn:                             " << picobarn       << endl;
      cout << "sumW():                               " << sumW()         << endl;
      cout << "crossSection():                       " << crossSection() << endl;
      cout << "crossSection()/picobarn():            " << crossSection()/picobarn        << endl;
      cout << "crossSection()/( picobarn * sumW() ): " << crossSection()/picobarn/sumW() << endl;
      
      
      /// Load values from log.generate
      TString logFile = "log.generate";
      double fromLog_xs                      = GrepFunction(logFile, "INFO MetaData: cross-section",    "="); /// in nb
      double fromLog_filterEfficiecy         = GrepFunction(logFile, "INFO Filter Efficiency",          "=");
      double fromLog_weightedFilterEfficiecy = GrepFunction(logFile, "INFO Weighted Filter Efficiency", "=");
      
      cout << "fromLog_xs:                      " << fromLog_xs << endl;
      cout << "fromLog_filterEfficiecy:         " << fromLog_filterEfficiecy << endl;
      cout << "fromLog_weightedFilterEfficiecy: " << fromLog_weightedFilterEfficiecy << endl;
      
      if (fromLog_xs >= 0.0)                      h_fromLog_xs->Fill(1, fromLog_xs);
      if (fromLog_filterEfficiecy >= 0.0)         h_fromLog_filtEff->Fill(1, fromLog_filterEfficiecy);
      if (fromLog_weightedFilterEfficiecy >= 0.0) h_fromLog_weightedFiltEff->Fill(1, fromLog_weightedFilterEfficiecy);

      
      if (isDEBUG) cout << "void finilize() method - call WriteToRootFile()" << endl;
      WriteToRootFile();
      
      if (isDEBUG) cout << "void finilize() method - end" << endl;

    }
    
    float getXSfromLog()
    {
      cout << "Getting cross-section from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: cross-section (nb) =";
      unsigned int curLine = 0;
      string line;
      while(getline(fileInput, line))  // I changed this, see below
      {
        curLine++;
        if (line.find(search, 0) != string::npos) 
        {
          stringstream ss(line); 
          string buf; 
          vector<string> tokens;
          while (ss >> buf)
          tokens.push_back(buf);
          cout << "found: " << search << "line: " << curLine << endl;
          cout << "XS=" << tokens[tokens.size()-1] <<endl;
          cout << "XS=\"" <<  tokens[tokens.size()-1] << "\"" << endl;
          fileInput.close();
            return atof(tokens[tokens.size()-1].c_str());
        }
      }
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 0;
    }
    
    double GrepFunction(TString _inputFile, TString _keyString, TString _keyTokenInFrontOfTheValue = "=")
    {
        cout << "\n\n\n" << endl;
        cout << "GrepFunction(TString _inputFile, TString _keyString)" << endl;
        cout << "    _inputFile:                 " << _inputFile << endl;
        cout << "    _keyString:                 " << _keyString << endl;
        cout << "    _keyTokenInFrontOfTheValue: " << _keyTokenInFrontOfTheValue << endl;
        
        /// Check it the file exists
        bool b_exists = std::filesystem::exists( _inputFile.Data() );
        if (b_exists == false)
        {
             cout << "   _inputFile: " << _inputFile << " does not exits, return -888 value" << endl;
             return(-888);
        }
        else
        {
            std::ifstream f;
            f.open( _inputFile.Data() );
             
            string line;
            unsigned int iLine = 0;
            
            /// Read File line by line
            while( getline(f, line) )  // I changed this, see below
            {
               iLine++;
               /// Line is with the key
               if (line.find( _keyString.Data(), 0) != string::npos) 
               {
                   stringstream ss(line); 
                   string buf;
                   
                   cout << "    line: " << line << endl;
                    
                   /// Identify a token in front of the values you're looking for         
                   int ind_token      = -999;          
                   
                   vector<TString> v_words;
                   int ind_tmp=0; 
                   while (ss >> buf)
                   {
                        TString tmp(buf);
                        v_words.push_back(tmp);
                        
                        if (tmp == _keyTokenInFrontOfTheValue || tmp.Contains(_keyTokenInFrontOfTheValue))
                        {
                            ind_token = ind_tmp;
                        }
                        
                       ind_tmp += 1;
                   }
                   
                   /// Get the string
                   TString str_val;
                  if (ind_token + 1 <= v_words.size())
                  {
                      str_val= v_words.at(ind_token + 1);
                  } 
                  /// Convert the string to double
                  double      val = atof( str_val.Data() );
                  
                  cout << "    str_val: " << "\"" << str_val << "\"" <<endl;
                  cout << "    val    : " << "\"" << val     << "\"" <<endl;
                  
                  f.close();
                  return (val);
              }
           } /// end of while
           
           f.close();
           cout<< "    Not found, return -999" <<endl;
           return -999;
        } /// end of else
    }




    void WriteToRootFile()
    {
        f_rootFile = new TFile("RivetRootFile.root", "RECREATE");
        f_rootFile->mkdir(m_rivetAnaName);
        f_rootFile->cd(m_rivetAnaName);
        
        vector<TH1D *> v_h = {  /// Technical histograms
                                h_nFiles, h_nEvents, h_sumOfWeights, h_crossSection, h_filtEff,
                                h_nSelectedEvents_jets, h_nSelectedEvents_dijets,
                                h_fromLog_filtEff, h_fromLog_weightedFiltEff, h_fromLog_xs,
                                /// Inclusive jet pt
                                h_pt_y0,       h_pt_y1,       h_pt_y2,       h_pt_y3,       h_pt_y4,       h_pt_y5,      h_pt_y6, h_pt_y7, h_pt_y8,
                                /// Inclusive dijet mass, ystar
                                h_mjj_ystar0,  h_mjj_ystar1,  h_mjj_ystar2,  h_mjj_ystar3,  h_mjj_ystar4,  h_mjj_ystar5,
                                /// Inclusive dijet mass, ystar
                                h_mjj_yboost0, h_mjj_yboost1, h_mjj_yboost2, h_mjj_yboost3, h_mjj_yboost4, h_mjj_yboost5,
                             };
        for (TH1D * h: v_h)
        {
            h->Write();
        }
        
        f_rootFile->Close();
        
        
        /// Clear memory
        for (TH1D * h: v_h)
        {
            delete h;
        }
        
        delete f_rootFile;
    
    }

  private:
  
  
    bool isDEBUG                = false;
    long nEvents                = 0;
    long nSelectedEvents_jets   = 0;
    long nSelectedEvents_dijets = 0;
    
    /// Rivet objects
    // map<string, Histo1DPtr>   _h;
    // map<string, Profile1DPtr> _p;
    // map<string, CounterPtr>   _c;
    Histo1DPtr h1_nFiles, h1_nEvents, h1_sumOfWeights, h1_crossSection, h1_filtEff;
    Histo1DPtr h1_nSelectedEvents_jets, h1_nSelectedEvents_dijets;
    Histo1DPtr h1_pt_y0,       h1_pt_y1,       h1_pt_y2,       h1_pt_y3,       h1_pt_y4,       h1_pt_y5,      h1_pt_y6, h1_pt_y7, h1_pt_y8;
    Histo1DPtr h1_mjj_ystar0,  h1_mjj_ystar1,  h1_mjj_ystar2,  h1_mjj_ystar3,  h1_mjj_ystar4,  h1_mjj_ystar5;
    Histo1DPtr h1_mjj_yboost0, h1_mjj_yboost1, h1_mjj_yboost2, h1_mjj_yboost3, h1_mjj_yboost4, h1_mjj_yboost5;
    
    /// Root objects
    TString m_rivetAnaName = "ATLAS_2023_IJXS_IDXS_Rivet_3_1_8";
    TFile * f_rootFile;
    TH1D  * h_nFiles, *h_nEvents, *h_sumOfWeights, *h_crossSection, *h_filtEff;
    TH1D  * h_nSelectedEvents_jets, *h_nSelectedEvents_dijets;
    TH1D  * h_fromLog_filtEff, * h_fromLog_weightedFiltEff, * h_fromLog_xs;
    TH1D  * h_pt_y0,       *h_pt_y1,       *h_pt_y2,       *h_pt_y3,       *h_pt_y4,       *h_pt_y5,      *h_pt_y6, *h_pt_y7, *h_pt_y8;
    TH1D  * h_mjj_ystar0,  *h_mjj_ystar1,  *h_mjj_ystar2,  *h_mjj_ystar3,  *h_mjj_ystar4,  *h_mjj_ystar5;
    TH1D  * h_mjj_yboost0, *h_mjj_yboost1, *h_mjj_yboost2, *h_mjj_yboost3, *h_mjj_yboost4, *h_mjj_yboost5;

  };


  RIVET_DECLARE_PLUGIN(ATLAS_2023_IJXS_IDXS_Rivet_3_1_8);

}
