// -*- C++ -*-
//#include "Rivet/Analysis.hh"
//#include "Rivet/Projections/FinalState.hh"
//#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Particle.fhh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/JetAlg.hh"
//#include "Rivet/Projections/IdentifiedFinalState.hh"
//#include "Rivet/Projections/LeadingParticlesFinalState.hh"


#include <vector>
#include <string>
#include <sstream>

using namespace std;

namespace Rivet {
  
  /// |y|, ystar, yboost bins   
  const unsigned int nYBins = 9;
  const unsigned int nYBins_dijet = 6;
  
  /// C++ 98 compilator
  /// C++ 98 vector must be initialized by constructor, not by ‘{….}’
  /// std::vector --> array
  const std::string a_ybinname[nYBins]               =  { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  const std::string a_y_dijet_binname[nYBins_dijet]  =  { "0", "1", "2", "3", "4", "5" };
  
  const double a_y      [nYBins + 1]       = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5 };
  const double a_ystar  [nYBins_dijet + 1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
  const double a_yboost [nYBins_dijet + 1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
  
  const unsigned int nMjjBins = 311;
  const double a_mjj[nMjjBins + 1]  = { 40, 110, 240, 250, 270, 280, 290, 300, 320, 330, 340, 360, 370, 380, 390, 400, 410, 420, 440, 450, 460, 470, 480, 490, 500, 510, 530, 540, 550, 570, 580, 590, 600, 610, 630, 640, 650, 660, 670, 680, 690, 700, 730, 740, 750, 760, 770, 780, 790, 810, 820, 830, 840, 860, 880, 890, 910, 930, 940, 950, 960, 990, 1000, 1010, 1020, 1040, 1070, 1080, 1090, 1100, 1110, 1120, 1140, 1150, 1160, 1190, 1200, 1210, 1220, 1240, 1260, 1280, 1290, 1300, 1310, 1330, 1350, 1380, 1390, 1410, 1420, 1430, 1440, 1460, 1480, 1500, 1510, 1520, 1540, 1550, 1560, 1570, 1580, 1590, 1610, 1620, 1640, 1650, 1660, 1670, 1690, 1710, 1720, 1730, 1760, 1790, 1810, 1830, 1840, 1850, 1860, 1890, 1920, 1930, 1940, 1990, 2020, 2030, 2040, 2060, 2090, 2100, 2130, 2140, 2180, 2190, 2200, 2240, 2250, 2280, 2310, 2350, 2360, 2370, 2390, 2420, 2440, 2460, 2470, 2490, 2500, 2540, 2580, 2610, 2620, 2660, 2680, 2700, 2730, 2750, 2790, 2810, 2820, 2830, 2880, 2890, 2930, 2940, 2950, 2980, 3000, 3010, 3020, 3030, 3040, 3080, 3140, 3180, 3190, 3210, 3220, 3240, 3320, 3330, 3360, 3380, 3410, 3450, 3470, 3490, 3510, 3560, 3580, 3600, 3620, 3640, 3650, 3660, 3700, 3720, 3750, 3780, 3820, 3860, 3930, 3950, 3970, 3990, 4000, 4010, 4130, 4160, 4180, 4200, 4230, 4310, 4340, 4370, 4380, 4410, 4470, 4530, 4570, 4610, 4620, 4710, 4720, 4730, 4750, 4770, 4850, 4940, 4960, 4980, 5030, 5040, 5100, 5160, 5200, 5250, 5300, 5350, 5360, 5370, 5390, 5420, 5540, 5620, 5630, 5640, 5650, 5770, 5840, 5880, 5890, 5930, 5970, 6010, 6070, 6140, 6160, 6240, 6260, 6320, 6400, 6420, 6500, 6570, 6670, 6710, 6720, 6870, 6910, 6950, 7010, 7020, 7060, 7250, 7280, 7350, 7410, 7420, 7480, 7570, 7660, 7690, 7710, 7900, 7950, 8070, 8080, 8100, 8250, 8260, 8540, 8650, 8670, 8680, 9040, 9050, 9060, 9070, 9280, 9540, 9650, 9730, 9950, 10000, 10200, 10280, 10800, 11000};
  
  
  //const size_t N = 3;
  //int a[N] = { 4, 3, 5 };
  //std::vector<int> v1( a, a + N );
  
  /// C++98 syntax for vectors
  const vector<double> v_ystar  (a_ystar,  a_ystar  + nYBins_dijet + 1);
  const vector<double> v_yboost (a_yboost, a_yboost + nYBins_dijet + 1);
  const vector<double> v_mjj    (a_mjj,    a_mjj    + nMjjBins + 1);

  //const vector<double> v_yboost { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
  //const vector<double> v_mjj    { 40, 110, 240, 250, 270, 280, 290, 300, 320, 330, 340, 360, 370, 380, 390, 400, 410, 420, 440, 450, 460, 470, 480, 490, 500, 510, 530, 540, 550, 570, 580, 590, 600, 610, 630, 640, 650, 660, 670, 680, 690, 700, 730, 740, 750, 760, 770, 780, 790, 810, 820, 830, 840, 860, 880, 890, 910, 930, 940, 950, 960, 990, 1000, 1010, 1020, 1040, 1070, 1080, 1090, 1100, 1110, 1120, 1140, 1150, 1160, 1190, 1200, 1210, 1220, 1240, 1260, 1280, 1290, 1300, 1310, 1330, 1350, 1380, 1390, 1410, 1420, 1430, 1440, 1460, 1480, 1500, 1510, 1520, 1540, 1550, 1560, 1570, 1580, 1590, 1610, 1620, 1640, 1650, 1660, 1670, 1690, 1710, 1720, 1730, 1760, 1790, 1810, 1830, 1840, 1850, 1860, 1890, 1920, 1930, 1940, 1990, 2020, 2030, 2040, 2060, 2090, 2100, 2130, 2140, 2180, 2190, 2200, 2240, 2250, 2280, 2310, 2350, 2360, 2370, 2390, 2420, 2440, 2460, 2470, 2490, 2500, 2540, 2580, 2610, 2620, 2660, 2680, 2700, 2730, 2750, 2790, 2810, 2820, 2830, 2880, 2890, 2930, 2940, 2950, 2980, 3000, 3010, 3020, 3030, 3040, 3080, 3140, 3180, 3190, 3210, 3220, 3240, 3320, 3330, 3360, 3380, 3410, 3450, 3470, 3490, 3510, 3560, 3580, 3600, 3620, 3640, 3650, 3660, 3700, 3720, 3750, 3780, 3820, 3860, 3930, 3950, 3970, 3990, 4000, 4010, 4130, 4160, 4180, 4200, 4230, 4310, 4340, 4370, 4380, 4410, 4470, 4530, 4570, 4610, 4620, 4710, 4720, 4730, 4750, 4770, 4850, 4940, 4960, 4980, 5030, 5040, 5100, 5160, 5200, 5250, 5300, 5350, 5360, 5370, 5390, 5420, 5540, 5620, 5630, 5640, 5650, 5770, 5840, 5880, 5890, 5930, 5970, 6010, 6070, 6140, 6160, 6240, 6260, 6320, 6400, 6420, 6500, 6570, 6670, 6710, 6720, 6870, 6910, 6950, 7010, 7020, 7060, 7250, 7280, 7350, 7410, 7420, 7480, 7570, 7660, 7690, 7710, 7900, 7950, 8070, 8080, 8100, 8250, 8260, 8540, 8650, 8670, 8680, 9040, 9050, 9060, 9070, 9280, 9540, 9650, 9730, 9950, 10000, 10200, 10280, 10800, 11000};


  ///
  /// Ota's Rivet routine Based on 2018 paper of 13 TeV measurement using 2015 data
  /// re-wrote to be compatible with Rivet 2.2.1
  ///
  class ATLAS_2022_IJXS_IDXS_Rivet_2_2_1 : public Analysis 
  {
  public:

    /// Constructor
    ATLAS_2022_IJXS_IDXS_Rivet_2_2_1() : Analysis("ATLAS_2022_IJXS_IDXS_Rivet_2_2_1") { setNeedsCrossSection(true); }
    
    /// Book histograms and initialise projections before the run
    void init() {
      
      /// Number of events
      nEvents = 0;
      
      /// Differect syntax for FinalState
      const FinalState fs(-5.0, 5.0, 0.);

      /// Different sytax for FastJets
      //FastJets fj04(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      FastJets fj04(fs, FastJets::ANTIKT, 0.4);
      fj04.useInvisibles(true);
      
      ///fj04.useMuons(Muons::ALL); /// not applied by Aliaksie in River 2.2.1 - seems to be available in Rivet 3.1.X not in 2.2.1
      ///                           /// https://rivet.hepforge.org/code/dev/classRivet_1_1JetFinder.html#a0ab3196345b058e5fd1297f8ed4edb38
      
     /// Different sytax for addProjection
      addProjection(fj04, "AntiKT04");
      
      /// Different sytax for histogram inicialization
      h_nFiles                 = bookHisto1D( "nFiles",         1,1,2, "nFiles");
      h_nEvents                = bookHisto1D( "nEvents",        1,1,2, "nEvents");
      h_nSelectedEvents_jets   = bookHisto1D( "nSelectedEvents_jets",  1,1,2, "nSelected_jets");
      h_nSelectedEvents_dijets = bookHisto1D( "nSelectedEvents_dijets",1,1,2, "nSelected_dijets");
      h_sumOfWeights           = bookHisto1D( "sumOfWeights",   1,1,2, "sumOfWeights");
      h_crossSection           = bookHisto1D( "crossSection",   1,1,2, "crossSection");
      h_filtEff                = bookHisto1D( "filtEff",        1,1,2, "filtEff");
      
       
      /// pt vs. y - 1D histograms
      for(unsigned int i = 0; i < nYBins; ++i)
      {
        _pThistograms[i]          = bookHisto1D( "h1_pt_y"       + a_ybinname[i],  5500, 0.0,  5500.0, "h1_pt_y"       + a_ybinname[i]);
      }
      
      /// mjj vs. ystar  - 1D histograms
      /// mjj vs. yBoost - 1D histograms
      for(unsigned int i = 0; i < nYBins_dijet; ++i)
      {
        _mjj_ystar_histograms[i]  = bookHisto1D( "h1_mjj_ystar"  + a_ybinname[i], 11000, 0.0, 11000.0, "h1_mjj_ystar"  + a_ybinname[i]);
        _mjj_yboost_histograms[i] = bookHisto1D( "h1_mjj_yboost" + a_ybinname[i], 11000, 0.0, 11000.0, "h1_mjj_yboost" + a_ybinname[i]);
  
      }
      
      /// Book 2D histograms
      const string h2name_mjj_ystar("h2_mjj_ystar");
      const string h2name_mjj_yboost("h2_mjj_yboost");
      const string xAxis_mjj("Mjj [GeV]");
      const string yAxis_ystar("y* [-]");
      const string yAxis_yboost("y_{boost} [-]");
      const string zAxis(" ");
      
      _h2_mjj_ystar  = bookHisto2D (h2name_mjj_ystar, v_mjj, v_ystar,  h2name_mjj_ystar,  xAxis_mjj, yAxis_ystar,  zAxis);
      _h2_mjj_yboost = bookHisto2D (h2name_mjj_yboost,v_mjj, v_yboost, h2name_mjj_yboost, xAxis_mjj, yAxis_yboost, zAxis);
      
    }
    
    /// Perform the per-event analysis
    void analyze(const Event& event) 
    {
      //const Jets& kt4Jets = applyProjection<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 75*GeV && Cuts::absrap < 3.0);
      const Jets& kt4Jets = applyProjection<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 15*GeV && Cuts::absrap < 4.5);
      
      unsigned int nJets = kt4Jets.size();
      
      nEvents++;
      h_nEvents -> fill ( 1 );
      if ( nEvents%1000 == 0 ) cout << nEvents << endl;

      
      double w = event.weight();
      
      if (nJets > 0)
      { 
        /// all incl jet criteria defined in applyProjection
        h_nSelectedEvents_jets->fill( 1 );
      }
      
      /// Inclusive jet selection
      for(unsigned int ijet = 0; ijet < nJets; ++ijet)   /// loop over jets
      { 
        FourMomentum jet = kt4Jets[ijet].momentum();
        
        /// pT selection
        if(jet.pt()>15.0*GeV)
        {
          const double absy = jet.absrap();
          const double pt = jet.pt();
          
          for ( unsigned int iy = 0; iy < nYBins; iy++ )   /// loop over y bins
          {
             if( fabs(absy) >= a_y[iy] && fabs(absy) < a_y[iy+1] )   /// test y bin
             {
               /// Fill histogram
               _pThistograms[iy] -> fill( pt/GeV, w);       
               
               break;
             }
          }
        }
      }
      
      
      
      /// Dijet selection
      if(nJets > 1)            /// skip events with less than 2 jets passing pT>75GeV and |y|<3.0 cuts
      { 
        FourMomentum jet0   = kt4Jets[0].momentum(); 
        FourMomentum jet1   = kt4Jets[1].momentum();
        
        const double pt0   = jet0.pt();
        const double pt1   = jet1.pt();

        const double rap0   = jet0.rapidity();
        const double rap1   = jet1.rapidity();
        const double ystar  = fabs(rap0-rap1)/2.0;
        const double yboost = fabs(rap0+rap1)/2.0;
        
        const double mass  = (jet0 + jet1).mass(); 
        const double HT2   = jet0.pt()+jet1.pt();
        
        if (isDEBUG || nEvents%1000 == 0)
        {
          cout << "Dijet -  yStar:  " << ystar  << endl;
          cout << "Dijet -  yBoost: " << yboost << endl;
          cout << "Dijet -  mjj:    " << mass   << endl;
          cout << "Dijet -  HT2:    " << HT2    << endl;
          cout << "GeV:             " << GeV    << endl;
          cout << "200*GeV:         " << 200*GeV    << endl;
          cout << "mass/GeV:        " << mass/GeV    << endl;
          cout << "w:               " << w << endl;
        }
        
        if( pt0>75*GeV  && pt1>75*GeV && 
            rap0 < 3.0  && rap1 < 3.0 &&
            HT2>200*GeV &&
            ystar<3.0 )
        {
          h_nSelectedEvents_dijets -> fill( 1 );
          
          for ( unsigned int iy = 0; iy < nYBins_dijet; iy++ )    /// Loop over ystar (yboost) index
          {
             if( fabs(ystar) >= a_ystar[iy] && fabs(ystar) < a_ystar[iy+1] )    /// test ystar
             {
               /// Fill ystar histogram
               _mjj_ystar_histograms[iy] -> fill( mass/GeV, w);       
             }
             
             if( fabs(yboost) >= a_yboost[iy] && fabs(yboost) < a_yboost[iy+1] )  /// test yboost
             {
               /// Fill yboost histogram
               _mjj_yboost_histograms[iy] -> fill( mass/GeV, w);       
             }
          }
          /// Fill 2D histogram
          _h2_mjj_yboost->fill( mass/GeV, yboost, w);
          _h2_mjj_ystar->fill ( mass/GeV, ystar,  w);
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize()
    {
      /// Different sytax for Normalization

      /// Finalize Method from Aliaksei - src/InclusiveJets.cc
      std::cout << " I'm in finalize() function" << std::endl;
      std::cout << "Needs cross-section " << needsCrossSection() <<std::endl;
      std::cout << "SumOfWeights: " << sumOfWeights() << std::endl;
      std::cout << "CrossSection: " << crossSection() << std::endl;
      std::cout << "CrossSectionPerEvent: " << crossSectionPerEvent() << "  " << picobarn << std::endl;
      
      double xs=1.0;
      if( crossSection() == 987654321 ) /// What?
      {
        xs=getXSfromLog();
      }
      else
      {
        xs=crossSection()/picobarn;
      }
      cout << "Using cross section: " << xs << endl;
      
      double genFiltEff=1.0;
      
      // filter eff is printed after the finalize method -> can be found the proper string
      //genFiltEff=getGetGenFiltEffFromLog();
      cout << "Using gen filter efficiency: : " << genFiltEff << endl;
      
      
      h_nFiles -> fill( 1 );
      h_sumOfWeights -> fill( 1, sumOfWeights() );
      h_crossSection -> fill( 1, xs );
      h_filtEff      -> fill( 1, genFiltEff );

    }
    
    
    float getXSfromLog()
    {
      /// Read log.genetare to find information about cross-section
      /// Taken from Aliaksei - src/InclusiveJets.cc
      
      cout << "getting cross-section from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: cross-section (nb)=";
      unsigned int curLine = 0;
      string line;
      
      while(getline(fileInput, line))  // I changed this, see below
      {
          curLine++;
          if (line.find(search, 0) != string::npos) 
          {
              stringstream ss(line); 
              string buf; 
              vector<string> tokens;
              while (ss >> buf)
              tokens.push_back(buf);
              cout << "found: " << search << "line: " << curLine << endl;
              cout << "XS=" << tokens[tokens.size()-1] <<endl;
              fileInput.close();
              return atof(tokens[tokens.size()-1].c_str());
          }
       }
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 0;
    }
    
    
    float getGetGenFiltEffFromLog()
    {
      /// Read log.genetare to find information about filter efficiancy
      /// Taken from Aliaksei - src/InclusiveJets.cc
      /// However it filter efficincy in printed after the rivet finilize method -> Does not work
      /// Furthemore, filter efficiency use to be at value of 1.0
      
      cout << "getting GenFiltEff from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      
      char* search = "MetaData: GenFiltEff =";
      unsigned int curLine = 0;
      string line;
      
      while(getline(fileInput, line)) 
      {
          curLine++;
          if (line.find(search, 0) != string::npos) 
          {
              stringstream ss(line); 
              string buf; 
              vector<string> tokens;
              while (ss >> buf)
              tokens.push_back(buf);
              
              cout << "found: " << search << "line: " << curLine << endl;
              cout << "GenFiltEff=" << tokens[tokens.size()-1] <<endl;
              fileInput.close();
              return atof(tokens[tokens.size()-1].c_str());
          }
      }
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 1.0;
    }
    

  private:
    bool isDEBUG = false;
    long nEvents;
    
    Histo1DPtr h_nFiles;           
    Histo1DPtr h_nEvents;          
    Histo1DPtr h_nSelectedEvents_jets; 
    Histo1DPtr h_nSelectedEvents_dijets; 
    Histo1DPtr h_sumOfWeights;    
    Histo1DPtr h_crossSection;     
    Histo1DPtr h_filtEff;          

    /// The inclusive pT spectrum for akt4 jets
    Histo1DPtr _pThistograms [nYBins];

    /// The dijet mass vs. ystar spectrum for akt4 jets
    Histo1DPtr _mjj_ystar_histograms [nYBins];

    /// The dijet mass vs. yboost  spectrum for akt4 jets
    Histo1DPtr _mjj_yboost_histograms [nYBins];
    
    Histo2DPtr _h2_mjj_ystar;
    Histo2DPtr _h2_mjj_yboost;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(ATLAS_2022_IJXS_IDXS_Rivet_2_2_1);
}
