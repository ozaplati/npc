// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

#include <vector>

namespace Rivet {


  /// brief inclusive jet and dijet at 13 TeV
  class ATLAS_2022_IJXS_IDXS : public Analysis 
  {
  public:

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(ATLAS_2022_IJXS_IDXS);

    /// Book histograms and initialise projections before the run
    void init() {

      const FinalState fs;
      declare(fs,"FinalState");
      FastJets fj04(fs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::DECAY);
      declare(fj04, "AntiKT04");

      /// |y|, ystar, yboost bins      
      std::vector<string> v_ybinname = { "0", "1", "2", "3", "4", "5" };
      std::vector<double> v_y        = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
      std::vector<double> v_ystar    = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
      std::vector<double> v_yboost   = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0 };
      
      /// pt, mjj bins
      std::vector<double> v_ptbins   = { 40, 110, 240, 250, 270, 280, 290, 300, 320, 330, 340, 360, 370, 380, 390, 400, 410, 420, 440, 450, 460, 470, 480, 490, 500, 510, 530, 540, 550, 570, 580, 590, 600, 610, 630, 640, 650, 660, 670, 680, 690, 700, 730, 740, 750, 760, 770, 780, 790, 810, 820, 830, 840, 860, 880, 890, 910, 930, 940, 950, 960, 990, 1000, 1010, 1020, 1040, 1070, 1080, 1090, 1100, 1110, 1120, 1140, 1150, 1160, 1190, 1200, 1210, 1220, 1240, 1260, 1280, 1290, 1300, 1310, 1330, 1350, 1380, 1390, 1410, 1420, 1430, 1440, 1460, 1480, 1500, 1510, 1520, 1540, 1550, 1560, 1570, 1580, 1590, 1610, 1620, 1640, 1650, 1660, 1670, 1690, 1710, 1720, 1730, 1760, 1790, 1810, 1830, 1840, 1850, 1860, 1890, 1920, 1930, 1940, 1990, 2020, 2030, 2040, 2060, 2090, 2100, 2130, 2140, 2180, 2190, 2200, 2240, 2250, 2280, 2310, 2350, 2360, 2370, 2390, 2420, 2440, 2460, 2470, 2490, 2500, 2540, 2580, 2610, 2620, 2660, 2680, 2700, 2730, 2750, 2790, 2810, 2820, 2830, 2880, 2890, 2930, 2940, 2950, 2980, 3000, 3010, 3020, 3030, 3040, 3080, 3140, 3180, 3190, 3210, 3220, 3240, 3320, 3330, 3360, 3380, 3410, 3450, 3470, 3490, 3510, 3560, 3580, 3600, 3620, 3640, 3650, 3660, 3700, 3720, 3750, 3780, 3820, 3860, 3930, 3950, 3970, 3990, 4000, 4010, 4130, 4160, 4180, 4200, 4230, 4310, 4340, 4370, 4380, 4410, 4470, 4530, 4570, 4610, 4620, 4710, 4720, 4730, 4750, 4770, 4850, 4940, 4960, 4980, 5030, 5040, 5100, 5160, 5200, 5250, 5300, 5350, 5360, 5370, 5390, 5420, 5540, 5620, 5630, 5640, 5650, 5770, 5840, 5880, 5890, 5930, 5970, 6010, 6070, 6140, 6160, 6240, 6260, 6320, 6400, 6420, 6500, 6570, 6670, 6710, 6720, 6870, 6910, 6950, 7010, 7020, 7060, 7250, 7280, 7350, 7410, 7420, 7480, 7570, 7660, 7690, 7710, 7900, 7950, 8070, 8080, 8100, 8250, 8260, 8540, 8650, 8670, 8680, 9040, 9050, 9060, 9070, 9280, 9540, 9650, 9730, 9950, 10000, 10200, 10280, 10800, 11000};
      std::vector<double> v_mjjbins  = { 40, 110, 240, 250, 270, 280, 290, 300, 320, 330, 340, 360, 370, 380, 390, 400, 410, 420, 440, 450, 460, 470, 480, 490, 500, 510, 530, 540, 550, 570, 580, 590, 600, 610, 630, 640, 650, 660, 670, 680, 690, 700, 730, 740, 750, 760, 770, 780, 790, 810, 820, 830, 840, 860, 880, 890, 910, 930, 940, 950, 960, 990, 1000, 1010, 1020, 1040, 1070, 1080, 1090, 1100, 1110, 1120, 1140, 1150, 1160, 1190, 1200, 1210, 1220, 1240, 1260, 1280, 1290, 1300, 1310, 1330, 1350, 1380, 1390, 1410, 1420, 1430, 1440, 1460, 1480, 1500, 1510, 1520, 1540, 1550, 1560, 1570, 1580, 1590, 1610, 1620, 1640, 1650, 1660, 1670, 1690, 1710, 1720, 1730, 1760, 1790, 1810, 1830, 1840, 1850, 1860, 1890, 1920, 1930, 1940, 1990, 2020, 2030, 2040, 2060, 2090, 2100, 2130, 2140, 2180, 2190, 2200, 2240, 2250, 2280, 2310, 2350, 2360, 2370, 2390, 2420, 2440, 2460, 2470, 2490, 2500, 2540, 2580, 2610, 2620, 2660, 2680, 2700, 2730, 2750, 2790, 2810, 2820, 2830, 2880, 2890, 2930, 2940, 2950, 2980, 3000, 3010, 3020, 3030, 3040, 3080, 3140, 3180, 3190, 3210, 3220, 3240, 3320, 3330, 3360, 3380, 3410, 3450, 3470, 3490, 3510, 3560, 3580, 3600, 3620, 3640, 3650, 3660, 3700, 3720, 3750, 3780, 3820, 3860, 3930, 3950, 3970, 3990, 4000, 4010, 4130, 4160, 4180, 4200, 4230, 4310, 4340, 4370, 4380, 4410, 4470, 4530, 4570, 4610, 4620, 4710, 4720, 4730, 4750, 4770, 4850, 4940, 4960, 4980, 5030, 5040, 5100, 5160, 5200, 5250, 5300, 5350, 5360, 5370, 5390, 5420, 5540, 5620, 5630, 5640, 5650, 5770, 5840, 5880, 5890, 5930, 5970, 6010, 6070, 6140, 6160, 6240, 6260, 6320, 6400, 6420, 6500, 6570, 6670, 6710, 6720, 6870, 6910, 6950, 7010, 7020, 7060, 7250, 7280, 7350, 7410, 7420, 7480, 7570, 7660, 7690, 7710, 7900, 7950, 8070, 8080, 8100, 8250, 8260, 8540, 8650, 8670, 8680, 9040, 9050, 9060, 9070, 9280, 9540, 9650, 9730, 9950, 10000, 10200, 10280, 10800, 11000};
      
      /// Different sytax for histogram inicialization
      book( h_nFiles,          "nFiles",         1,1,2);
      book( h_nEvents,         "nEvents",        1,1,2);
      book( h_nSelectedEvents, "nSelectedEvents",1,1,2);
      book( h_sumOfWeights,    "sumOfWeights",   1,1,2);
      book( h_crossSection,    "crossSection",   1,1,2);
      book( h_filtEff,         "filtEff",        1,1,2);
      
      
      //const std::vector<double> g_vec_binning_new_yStar0 = {40, 240, 270, 300, 340, 380, 420, 460, 500, 540, 580, 630, 680, 730, 780, 830, 890, 950, 1010, 1070, 1140, 1210, 1280, 1350, 1430, 1510, 1590, 1670, 1760, 1850, 1940, 2040, 2140, 2240, 2350, 2460, 2580, 2700, 2820, 2950, 3080, 3220, 3360, 3510, 3660, 3820, 3990, 4160, 4340, 4530, 4730, 4940, 5160, 5390, 5630, 5880, 6140, 6420, 6710, 7020, 7350, 7710, 8100, 8540, 9050, 9730, 11000};
      //const std::vector<double> g_vec_binning_new_yStar1 = {110, 280, 320, 360, 400, 450, 500, 550, 600, 650, 700, 760, 820, 880, 940, 1010, 1080, 1150, 1220, 1300, 1380, 1460, 1550, 1640, 1730, 1830, 1930, 2030, 2140, 2250, 2370, 2490, 2620, 2750, 2890, 3030, 3180, 3330, 3490, 3650, 3820, 4000, 4180, 4370, 4570, 4770, 4980, 5200, 5420, 5650, 5890, 6140, 6400, 6670, 6950, 7250, 7570, 7900, 8260, 8650, 9060, 9540, 10200, 11000};
      //const std::vector<double> g_vec_binning_new_yStar2 = {250, 390, 450, 510, 570, 630, 700, 770, 840, 910, 990, 1070, 1150, 1240, 1330, 1420, 1520, 1620, 1720, 1830, 1940, 2060, 2180, 2310, 2440, 2580, 2730, 2880, 3040, 3210, 3380, 3560, 3750, 3950, 4160, 4380, 4610, 4850, 5100, 5360, 5640, 5930, 6240, 6570, 6910, 7280, 7660, 8080, 8540, 9070, 9650, 10800};
      //const std::vector<double> g_vec_binning_new_yStar3 = {450, 600, 690, 790, 890, 990, 1090, 1200, 1310, 1420, 1540, 1660, 1790, 1920, 2060, 2200, 2350, 2500, 2660, 2830, 3010, 3190, 3380, 3580, 3780, 4000, 4230, 4470, 4720, 4980, 5250, 5540, 5840, 6160, 6500, 6870, 7280, 7690, 8070, 8670, 9280, 10280};
      //const std::vector<double> g_vec_binning_new_yStar4 = {730, 960, 1110, 1260, 1410, 1560, 1720, 1890, 2060, 2240, 2420, 2610, 2810, 3020, 3240, 3470, 3700, 3950, 4200, 4470, 4750, 5040, 5350, 5650, 5970, 6320, 6720, 7060, 7410, 7950, 8540};
      //const std::vector<double> g_vec_binning_new_yStar5 = {1220, 1570, 1830, 2100, 2390, 2680, 3000, 3320, 3640, 3970, 4340, 4710, 5160, 5620, 6070, 6570, 7420, 8680, 9950};
      
      /// updated new_yBoost binning with mcEventWeight->at(0) weights
      /// using fine 2D RooUnfildResponse
      /// InputFile: /mnt/nfs19/zaplatilek/IJXS/OUTPUT/mc/IJXSv21EM/NominalPythia_FullRun2__19_05_2022_1DMjj_2DMjjYBoost_fineBinning/y_range_00_30_00_30/MCFullRun2_normalized.root
      /// _RefPutity = 0.5, _RefStability = 0.5, _unc = 0.0
      //const std::vector<double> g_vec_binning_new_yBoost0 = {40, 240, 280, 320, 370, 420, 480, 540, 610, 690, 770, 860, 960, 1070, 1190, 1330, 1480, 1650, 1840, 2040, 2250, 2470, 2700, 2940, 3190, 3450, 3720, 4010, 4310, 4620, 4960, 5300, 5650, 6010, 6400, 6910, 7480, 8250, 9040, 10000, 11000};
      //const std::vector<double> g_vec_binning_new_yBoost1 = {40, 240, 280, 320, 370, 420, 470, 530, 590, 660, 740, 830, 930, 1040, 1160, 1290, 1430, 1570, 1710, 1860, 2020, 2190, 2360, 2540, 2730, 2930, 3140, 3360, 3600, 3860, 4130, 4410, 4720, 5030, 5370, 5770, 6260, 6710, 7010, 11000};
      //const std::vector<double> g_vec_binning_new_yBoost2 = {40, 250, 290, 330, 370, 420, 470, 530, 600, 670, 750, 830, 910, 1000, 1090, 1190, 1290, 1390, 1500, 1610, 1730, 1860, 1990, 2130, 2280, 2440, 2610, 2790, 2980, 3190, 3410, 3620, 3930, 11000};
      //const std::vector<double> g_vec_binning_new_yBoost3 = {40, 240, 280, 320, 360, 410, 460, 510, 570, 630, 690, 750, 820, 890, 960, 1040, 1120, 1200, 1290, 1380, 1480, 1580, 1690, 1810, 1940, 2090, 2250, 2470, 11000}; 
      //const std::vector<double> g_vec_binning_new_yBoost4 = {40, 240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 690, 750, 810, 880, 950, 1020, 1100, 1190, 1290, 1440, 11000};
      //const std::vector<double> g_vec_binning_new_yBoost5 = {40, 240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 700, 760, 830, 11000};
      
      /// Book 1D histograms
      /// pT vs. y  - 1D histograms
      for(size_t i=0;i<v_y.size() - 1;++i){// loop over |y| bins
        {Histo1DPtr tmp; _pThistograms.add(v_y.at(i), v_y.at(i+1),book(tmp, "h1_pt_y" + v_ybinname.at(i), 5500, 0.0, 5500.0));}
        //{Histo1DPtr tmp; _pThistograms.add(v_y.at(i), v_y.at(i+1),book(tmp, "h1_pt_y" + v_ybinname.at(i), v_mjjbins));}
      }
      /// mjj vs. ystar - 1D histograms
      for(size_t i=0;i<v_ystar.size() - 1;++i)
      {
        Histo1DPtr tmp; 
        _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), 11000, 0.0, 11000.0));
        
        //if      (i == 0) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar0));
        //else if (i == 1) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar1));
        //else if (i == 2) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar2));
        //else if (i == 3) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar3));
        //else if (i == 4) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar4));
        //else if (i == 5) _mjj_ystar_histograms.add(v_ystar.at(i), v_ystar.at(i+1), book(tmp, "h1_mjj_ystar" + v_ybinname.at(i), g_vec_binning_new_yStar5));
      }
      /// mjj vs. yboost - 1D histograms
      for(size_t i=0;i<v_yboost.size() - 1;++i)
      {
        Histo1DPtr tmp; 
        _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), 11000, 0.0, 11000.0));

        //if      (i == 0) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost0));
        //else if (i == 1) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost1));
        //else if (i == 2) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost2));
        //else if (i == 3) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost3));
        //else if (i == 4) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost4));
        //else if (i == 5) _mjj_yboost_histograms.add(v_yboost.at(i), v_yboost.at(i+1), book(tmp, "h1_mjj_yboost" + v_ybinname.at(i), g_vec_binning_new_yBoost5));
      }
      
      /// Book 2D histograms
      book ( _h2_mjj_ystar,  "h2_mjj_ystar",  v_mjjbins, v_ystar);
      book ( _h2_mjj_yboost, "h2_mjj_yboost", v_mjjbins, v_yboost);
    }
    
    /// Perform the per-event analysis
    void analyze(const Event& event) {

      const Jets& kt4Jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 75*GeV && Cuts::absrap < 3.0);

      int nJets = kt4Jets.size();

      nEvents++;
      h_nEvents -> fill ( 1 );
      if ( nEvents%1000 == 0 ) cout << nEvents << endl;
      
      // Inclusive jet selection
      for(int ijet=0;ijet<nJets;++ijet){ // loop over jets
        FourMomentum jet = kt4Jets[ijet].momentum();
        // pT selection
        if(jet.pt()>100.0*GeV){
          // Fill distribution
          const double absy = jet.absrap();
          _pThistograms.fill(absy,jet.pt()/GeV);
        }
      }

      // Dijet selection
      if(nJets > 1){ // skip events with less than 2 jets passing pT>75GeV and |y|<3.0 cuts
        FourMomentum jet0   = kt4Jets[0].momentum(); 
        FourMomentum jet1   = kt4Jets[1].momentum();
        const double rap0   = jet0.rapidity();
        const double rap1   = jet1.rapidity();
        const double ystar  = fabs(rap0-rap1)/2.0;
        const double yboost = fabs(rap0+rap1)/2.0;
        
        const double mass  = (jet0 + jet1).mass(); 
        const double HT2   = jet0.pt()+jet1.pt();
        
        if(HT2>200*GeV && ystar<3.0){
          
          h_nSelectedEvents -> fill( 1 );
          
          /// Fill 1D distribution
          _mjj_ystar_histograms.fill(ystar,mass/GeV);
          _mjj_yboost_histograms.fill(yboost,mass/GeV);
          
          /// Fill 2D distribution
          _h2_mjj_ystar->fill(mass/GeV, ystar);
          _h2_mjj_yboost->fill(mass/GeV, yboost);
          
        }
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double xs_pb( crossSection() / picobarn );
      const double sumW( sumOfWeights() );
      const double xs_norm_factor( 0.5*xs_pb / sumW );
     
      MSG_DEBUG( "Cross-Section/pb     : " << xs_pb       );
      MSG_DEBUG( "ZH                   : " << crossSectionPerEvent()/ picobarn);
      MSG_DEBUG( "Sum of weights       : " << sumW        );
      MSG_DEBUG( "nEvents              : " << numEvents() );
      _pThistograms.scale(xs_norm_factor, this);
      _mjj_ystar_histograms.scale(crossSectionPerEvent()/picobarn, this);
      _mjj_yboost_histograms.scale(crossSectionPerEvent()/picobarn, this);
      
      //_h2_mjj_ystar->scale(crossSectionPerEvent()/picobarn, this);
      //_h2_mjj_yboost->scale(crossSectionPerEvent()/picobarn, this);
      scale(_h2_mjj_ystar,  crossSectionPerEvent()/picobarn);
      scale(_h2_mjj_yboost, crossSectionPerEvent()/picobarn);


      double xs=1.0;
      if( crossSection() == 987654321 ) /// What?
      {
        xs=getXSfromLog();
      }
      else
      {
        xs=crossSection()/picobarn;
      }
      cout << "Using cross section: " << xs << endl;
      
      double genFiltEff=1.0;
      
      // filter eff is printed after the finalize method -> can be found the proper string
      //genFiltEff=getGetGenFiltEffFromLog();
      cout << "Using gen filter efficiency: : " << genFiltEff << endl;
      
      
      h_nFiles -> fill( 1 );
      h_sumOfWeights -> fill( 1, sumOfWeights() );
      h_crossSection -> fill( 1, xs );
      h_filtEff      -> fill( 1, genFiltEff );
    }

    float getXSfromLog()
    {
      /// Read log.genetare to find information about cross-section
      /// Taken from Aliaksei - src/InclusiveJets.cc
      
      cout << "getting cross-section from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: cross-section (nb)=";
      unsigned int curLine = 0;
      string line;
      
      while(getline(fileInput, line))  // I changed this, see below
      {
          curLine++;
          if (line.find(search, 0) != string::npos) 
          {
              stringstream ss(line); 
              string buf; 
              vector<string> tokens;
              while (ss >> buf)
              tokens.push_back(buf);
              cout << "found: " << search << "line: " << curLine << endl;
              cout << "XS=" << tokens[tokens.size()-1] <<endl;
              fileInput.close();
              return atof(tokens[tokens.size()-1].c_str());
          }
       }
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 0;
    }
    
    /*
    float getGetGenFiltEffFromLog()
    {
      /// Read log.genetare to find information about filter efficiancy
      /// Taken from Aliaksei - src/InclusiveJets.cc
      /// However it filter efficincy in printed after the rivet finilize method -> Does not work
      /// Furthemore, filter efficiency use to be at value of 1.0
      
      cout << "getting GenFiltEff from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      
      char* search = "MetaData: GenFiltEff =";
      unsigned int curLine = 0;
      string line;
      
      while(getline(fileInput, line)) 
      {
          curLine++;
          if (line.find(search, 0) != string::npos) 
          {
              stringstream ss(line); 
              string buf; 
              vector<string> tokens;
              while (ss >> buf)
              tokens.push_back(buf);
              
              cout << "found: " << search << "line: " << curLine << endl;
              cout << "GenFiltEff=" << tokens[tokens.size()-1] <<endl;
              fileInput.close();
              return atof(tokens[tokens.size()-1].c_str());
          }
      }
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 1.0;
    }
    */
    
  private:
    bool isDEBUG = false;
    long nEvents;
    
    Histo1DPtr h_nFiles;           
    Histo1DPtr h_nEvents;          
    Histo1DPtr h_nSelectedEvents; 
    Histo1DPtr h_sumOfWeights;    
    Histo1DPtr h_crossSection;     
    Histo1DPtr h_filtEff;     
    
    // The inclusive pT spectrum for akt4 jets
    BinnedHistogram _pThistograms;
    // The dijet mass vs. ystar spectrum for akt4 jets
    BinnedHistogram _mjj_ystar_histograms;
    // The dijet mass vs. yboost  spectrum for akt4 jets
    BinnedHistogram _mjj_yboost_histograms;
    // The dijet mass vs. ystar vs. yboost for akt4 jets
    BinnedHistogram _mjj_star_yboost_histograms;
    
    Histo2DPtr _h2_mjj_ystar;
    Histo2DPtr _h2_mjj_yboost;
  };

  // The hook for the plugin system
  RIVET_DECLARE_PLUGIN(ATLAS_2022_IJXS_IDXS);
  //DECLARE_RIVET_PLUGIN(ATLAS_2022_IJXS_IDXS);

}
