/// Rivet migration v2 --> v3 based on:
///    https://gitlab.com/hepcedar/rivet/-/blob/release-3-1-x/doc/tutorials/mig2to3.md
///

// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Particle.fhh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/JetAlg.hh"
//#include "Rivet/Projections/IdentifiedFinalState.hh"
//#include "Rivet/Projections/LeadingParticlesFinalState.hh"

#include <fstream>
#include <vector>
#include <sstream>
#include <string> 

namespace Rivet
{

	//const int nAlg = 5;
	//const string AlgName[nAlg] = { "AKT02", "AKT04", "AKT06", "AKT08", "AKT10" };
	//const double R      [nAlg] = { 0.2, 0.4, 0.6, 0.8, 1.0 };
	
	const int nAlg = 1;
	const string AlgName[nAlg] = { "AKT04" };
	const double R      [nAlg] = { 0.4 };

	const int nybins = 9;
	const string ybinsNames[nybins]   = { "0", "1",  "2",  "3",  "4",  "5",  "6", "7", "8" };
	const double ybinsEdges[nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5 };
	const double ystarbins [nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5 };
	
	class ATLAS_2018_IJXS_IDXS_Alieksei : public Analysis 
	{
		public:
			/// Constructor
			//JETS() : Analysis("InclusiveJets") {setNeedsCrossSection(true); }
			RIVET_DEFAULT_ANALYSIS_CTOR(ATLAS_2018_IJXS_IDXS_Alieksei);
		
		private:
			long evt;
			double sumw, sumw2;
			
			// Histograms
			Histo1DPtr h_nFiles;
			Histo1DPtr h_nEvents;
			Histo1DPtr h_nSelectedEvents;
			Histo1DPtr h_sumOfWeights;
			Histo1DPtr h_crossSection;
			Histo1DPtr h_filtEff;
			
			Histo1DPtr h_jet_n        [nAlg];
			Histo1DPtr h_jet_pt       [nAlg];
			Histo1DPtr h_jet_E        [nAlg];
			Histo1DPtr h_jet_eta      [nAlg];
			Histo1DPtr h_jet_y        [nAlg];
			Histo1DPtr h_jet_phi      [nAlg];
			Histo1DPtr h_jet_mass     [nAlg];
			Histo1DPtr h_dijet_pt     [nAlg];
			Histo1DPtr h_dijet_E      [nAlg];
			Histo1DPtr h_dijet_eta    [nAlg];
			Histo1DPtr h_dijet_y      [nAlg];
			Histo1DPtr h_dijet_ystar  [nAlg];
			Histo1DPtr h_dijet_phi    [nAlg];
			Histo1DPtr h_dijet_mass   [nAlg];
			Histo1DPtr h_dijet_invmass[nAlg];
			
			Histo1DPtr h_jet_pt_y           [nAlg][nybins];
			Histo1DPtr h_dijet_pt_y         [nAlg][nybins];
			Histo1DPtr h_jet_mass_y         [nAlg][nybins];
			Histo1DPtr h_dijet_mass_y       [nAlg][nybins];
			Histo1DPtr h_dijet_invmass_ystar[nAlg][nybins];
			
		public:
		
		float getXSfromLog()
		{
			cout << "getting cross-section from log" << endl;
			string cfilename="log.generate";
			std::ifstream fileInput;
			fileInput.open(cfilename.c_str());
			char* search = "MetaData: cross-section (nb)=";
			unsigned int curLine = 0;
			string line;
			while(getline(fileInput, line))  // I changed this, see below
			{
				curLine++;
				if (line.find(search, 0) != string::npos) 
				{
					stringstream ss(line); 
					string buf; 
					vector<string> tokens;
					while (ss >> buf)
						tokens.push_back(buf);
					cout << "found: " << search << "line: " << curLine << endl;
					cout << "XS=" << tokens[tokens.size()-1] <<endl;
					fileInput.close();
					return atof(tokens[tokens.size()-1].c_str());
				}
			}
			fileInput.close();
			cout<< "not found :(" <<endl;
			return 0;
		}
		
		
		float getGetGenFiltEffFromLog()
		{
			cout << "getting GenFiltEff from log" << endl;
			string cfilename="log.generate";
			std::ifstream fileInput;
			fileInput.open(cfilename.c_str());
			char* search = "MetaData: GenFiltEff =";
			unsigned int curLine = 0;
			string line;
			while(getline(fileInput, line)) 
			{
				curLine++;
				if (line.find(search, 0) != string::npos) 
				{
					stringstream ss(line); 
					string buf; 
					vector<string> tokens;
					while (ss >> buf)
						tokens.push_back(buf);
					cout << "found: " << search << "line: " << curLine << endl;
					cout << "GenFiltEff=" << tokens[tokens.size()-1] <<endl;
					fileInput.close();
					return atof(tokens[tokens.size()-1].c_str());
				}
			}
			
			fileInput.close();
			cout<< "not found :(" <<endl;
			return 1.0;
		}
		
		void init() 
		{
			std::cout << "**** I'm in init() Rivet function" << std::endl;
			
			/// Old syntax
			///const FinalState fs(-5.0, 5.0, 0.);
			FinalState fs(Cuts::abseta < 4.5); 
			
			for(int iAlg=0; iAlg<nAlg; iAlg++) 
			{
				FastJets j( fs, FastJets::ANTIKT, R[iAlg] );
				j.useInvisibles(true);
				/// Old syntax
				/// addProjection(j,AlgName[iAlg]);
				declare(j,AlgName[iAlg]);
			}
			
			/// Old syntax
			/// h_nFiles            = bookHisto1D( "nFiles",         1,1,2, "nFiles");
			/// h_nEvents           = bookHisto1D( "nEvents",        1,1,2, "nEvents");
			/// h_nSelectedEvents   = bookHisto1D( "nSelectedEvents",1,1,2, "nSelected");
			/// h_sumOfWeights      = bookHisto1D( "sumOfWeights",   1,1,2, "sumOfWeights");
			/// h_crossSection      = bookHisto1D( "crossSection",   1,1,2, "crossSection");
			/// h_filtEff           = bookHisto1D( "filtEff",        1,1,2, "filtEff");
			  
			book( h_nFiles,          "nFiles",         1,1,2);
			book( h_nEvents,         "nEvents",        1,1,2);
			book( h_nSelectedEvents, "nSelectedEvents",1,1,2);
			book( h_sumOfWeights,    "sumOfWeights",   1,1,2);
			book( h_crossSection,    "crossSection",   1,1,2);
			book( h_filtEff,         "filtEff",        1,1,2);
			
			for(int iAlg=0; iAlg<nAlg; iAlg++) 
			{
				/// Old syntax:
				///h_jet_n        [iAlg] = bookHisto1D("jet_n_"         +AlgName[iAlg], 30,0-0.5,30-0.5, "jet_n_"         +AlgName[iAlg]);
				///h_jet_pt       [iAlg] = bookHisto1D("jet_pt_"        +AlgName[iAlg], 2800,0,14000,   "jet_pt_"        +AlgName[iAlg]);
				///h_dijet_pt     [iAlg] = bookHisto1D("dijet_pt_"      +AlgName[iAlg], 2800,0,14000,   "dijet_pt_"      +AlgName[iAlg]);
				///h_jet_E        [iAlg] = bookHisto1D("jet_E_"         +AlgName[iAlg], 2800,0,14000,   "jet_E_"         +AlgName[iAlg]);
				///h_dijet_E      [iAlg] = bookHisto1D("dijet_E_"       +AlgName[iAlg], 2800,0,14000,   "dijet_E_"       +AlgName[iAlg]);
				///h_jet_eta      [iAlg] = bookHisto1D("jet_eta_"       +AlgName[iAlg], 90,-4.5,4.5,     "jet_eta_"       +AlgName[iAlg]);
				///h_dijet_eta    [iAlg] = bookHisto1D("dijet_eta_"     +AlgName[iAlg], 90,-4.5,4.5,     "dijet_eta_"     +AlgName[iAlg]);
				///h_jet_y        [iAlg] = bookHisto1D("jet_y_"         +AlgName[iAlg], 90,-4.5,4.5,     "jet_y_"         +AlgName[iAlg]);
				///h_dijet_ystar  [iAlg] = bookHisto1D("dijet_ystar"    +AlgName[iAlg], 90,-4.5,4.5,     "dijet_ystar"    +AlgName[iAlg]);
				///h_dijet_y      [iAlg] = bookHisto1D("dijet_y_"       +AlgName[iAlg], 90,-4.5,4.5,     "dijet_y_"       +AlgName[iAlg]);
				///h_jet_phi      [iAlg] = bookHisto1D("jet_phi_"       +AlgName[iAlg], 90,-180,180,     "jet_phi_"       +AlgName[iAlg]);
				///h_dijet_phi    [iAlg] = bookHisto1D("dijet_phi_"     +AlgName[iAlg], 90,-180,180,     "dijet_phi_"     +AlgName[iAlg]);
				///h_dijet_mass   [iAlg] = bookHisto1D("dijet_mass_"    +AlgName[iAlg], 2800,0,14000,   "dijet_mass_"    +AlgName[iAlg]);
				///h_dijet_invmass[iAlg] = bookHisto1D("dijet_invmass_" +AlgName[iAlg], 2800,0,14000,   "dijet_invmass_" +AlgName[iAlg]);
				
				book( h_jet_n    [iAlg], "jet_n_"         +AlgName[iAlg], 30,0-0.5,30-0.5);
				book( h_jet_pt   [iAlg], "jet_pt_"        +AlgName[iAlg], 2800,0,14000);
				book( h_dijet_pt [iAlg], "dijet_pt_"      +AlgName[iAlg], 2800,0,14000);
				book( h_jet_E    [iAlg], "jet_E_"         +AlgName[iAlg], 2800,0,14000);
				book( h_dijet_E  [iAlg], "dijet_E_"       +AlgName[iAlg], 2800,0,14000);
				book( h_jet_eta  [iAlg], "jet_eta_"       +AlgName[iAlg], 90,-4.5,4.5);
				book(h_dijet_eta [iAlg], "dijet_eta_"     +AlgName[iAlg], 90,-4.5,4.5);
				book( h_jet_y        [iAlg], "jet_y_"         +AlgName[iAlg], 90,-4.5,4.5);
				book( h_dijet_ystar  [iAlg], "dijet_ystar"    +AlgName[iAlg], 90,-4.5,4.5);
				book( h_dijet_y      [iAlg], "dijet_y_"       +AlgName[iAlg], 90,-4.5,4.5);
				book( h_jet_phi      [iAlg], "jet_phi_"       +AlgName[iAlg], 90,-180,180);
				book( h_dijet_phi    [iAlg], "dijet_phi_"     +AlgName[iAlg], 90,-180,180);
				book( h_dijet_mass   [iAlg], "dijet_mass_"    +AlgName[iAlg], 11000,0,11000);
				book( h_dijet_invmass[iAlg], "dijet_invmass_" +AlgName[iAlg], 11000,0,11000);
				
				for(int iy=0; iy<nybins;iy++)
				{
					book( h_jet_pt_y           [iAlg][iy], "jet_pt_y"           + ybinsNames[iy] +"_" +AlgName[iAlg], 2800,0,14000);
					book( h_jet_mass_y         [iAlg][iy], "jet_mass_y"         + ybinsNames[iy] +"_" +AlgName[iAlg], 2800,0,14000);
					book( h_dijet_pt_y         [iAlg][iy], "dijet_pt_y"         + ybinsNames[iy] +"_" +AlgName[iAlg], 2800,0,14000);
					book( h_dijet_mass_y       [iAlg][iy], "dijet_mass_y"       + ybinsNames[iy] +"_" +AlgName[iAlg], 11000,0,11000);
					book( h_dijet_invmass_ystar[iAlg][iy], "dijet_invmass_ystar"+ ybinsNames[iy] +"_" +AlgName[iAlg], 11000,0.0,11000);
				}
			}
			
			evt=0;
			sumw=0;
			sumw2=0;
			
			std::cout << "**** Finished with init()" << std::endl;
		}
		
		
		
		void analyze(const Event& event) 
		{
			//std::cout << "**** I'm in analyze(...) function: Event " << evt << std::endl;
			
			double cut_jet_pt = 25;
			double cut_jet_y  = 4.4;
			
			evt++;
			
			h_nEvents -> fill ( 1 );
			
			if ( evt%1000 == 0 ) cout<<evt<<endl;
			
			const double w = event.weight();
			
			Jets jets[nAlg];
			for( int iAlg=0; iAlg < nAlg; iAlg++ ) 
			{
				///froeach
				  //const Jets& kt4Jets = apply<FastJets>(event, "AntiKT04").jetsByPt(Cuts::pT > 75*GeV && Cuts::absrap < 3.0);
				
				const Jets & ktXJets = apply<FastJets>(event, AlgName[iAlg]).jetsByPt(cut_jet_pt*GeV);
				int nJets = ktXJets.size();
				
				for(int ijet=0;ijet<nJets;++ijet)
				{
					// loop over jets
					//for_each(const Jet& jet, apply<FastJets>(event, AlgName[iAlg]).jetsByPt(cut_jet_pt*GeV)) 
					//for_each(Jet& jet, apply<FastJets>(event, AlgName[iAlg]).jetsByPt(cut_jet_pt*GeV)) 
					{
						//FourMomentum jmom = jet.momentum();
						FourMomentum jmom = ktXJets[ijet].momentum();
						if ( fabs(jmom.rapidity()) < cut_jet_y ) 
						{
							jets[iAlg].push_back(ktXJets[ijet]);
						}
					}
				}
			}
			vector<FourMomentum> leadjets;
			for(int iAlg=0; iAlg<nAlg; iAlg++) 
			{
				if (jets[iAlg].size() < 1 ) continue;
				
				// std::cout << "njets = " << jets[iAlg].size() << std::endl;
				h_jet_n[iAlg]->fill( jets[iAlg].size(), w );
				for (unsigned int iJet=0; iJet < jets[iAlg].size();iJet++)
				{
					FourMomentum jmom = jets[iAlg][iJet].momentum();
					double jet_pt   = jmom.pT() / GeV;
					// std::cout <<"jet_pt = "<< jet_pt << std::endl;
					double jet_e    = jmom.E()  / GeV;
					// std::cout <<"jet_E = "<< jet_e << std::endl;
					double jet_eta  = jmom.eta();
					// std::cout <<"jet_eta = "<< jet_eta << std::endl;
					double jet_rap  = jmom.rapidity();
					// std::cout <<"jet_rap = "<< jet_rap << std::endl;
					double jet_phi  = (jmom.phi() - PI) * 180 / PI;
					// std::cout <<"jet_phi = "<< jet_phi << std::endl;
					
					if (jets[iAlg].size() >= 2)
					{
						if ( (iJet == 0) || (iJet==1) ) 
						{
							leadjets.push_back(jmom);
							if (iJet==1)
							{
								h_nSelectedEvents -> fill( 1 );
							}
						}
					}
					h_jet_pt  [iAlg] -> fill( jet_pt,   w );
					h_jet_E   [iAlg] -> fill( jet_e,    w );
					h_jet_eta [iAlg] -> fill( jet_eta,  w );
					h_jet_y   [iAlg] -> fill( jet_rap,  w );
					h_jet_phi [iAlg] -> fill( jet_phi,  w );
					
					for(int iy=0; iy<nybins;iy++)
					{
						if( fabs(jet_rap) >= ybinsEdges[iy] && fabs(jet_rap) < ybinsEdges[iy+1] )
						{
							const double m = jmom.mass() / GeV;
							// std::cout << "jet_mass = " << m << "***********" << std::endl;
							h_jet_mass_y[iAlg][iy] -> fill( m, w);
							h_jet_pt_y[iAlg][iy]   -> fill( jet_pt, w);
							break;
						}
					}
				}
				if (!leadjets.empty())
				{
					const double y1 = leadjets[0].rapidity();
					const double y2 = leadjets[1].rapidity();
					const double ystar = fabs(y1-y2)/2.;
					//std::cout << "********* " << ystar << "***********" << std::endl;
					
					double dijet_pt  = (leadjets[0]+leadjets[1]).pT() / GeV;
					//std::cout << "----dijet_pt = " << dijet_pt << "-------------" << std::endl;
					
					double dijet_e   = (leadjets[0]+leadjets[1]).E() / GeV;
					//std::cout << "----dijet_e = " << dijet_e << "-------------" << std::endl;
					
					double dijet_eta = leadjets[0].eta() ;
					//std::cout << "----dijet_eta = " << dijet_eta << "-------------" << std::endl;
					
					double dijet_rap = leadjets[0].rapidity();
					//std::cout << "----dijet_rap = " << dijet_rap << "-------------" << std::endl;
					
					double dijet_phi = (leadjets[0].phi()  - PI) * 180 / PI;
					//std::cout << "----dijet_phi = " << dijet_phi << "-------------" << std::endl;
					
					double dijet_invmass = (leadjets[0]+leadjets[1]).mass() / GeV;
					double dijet_mass = leadjets[0].mass() / GeV + leadjets[1].mass() / GeV; 
					//std::cout << "----dijet_mass = " << dijet_mass << "-------------" << std::endl;
					
					h_dijet_pt     [iAlg] -> fill( dijet_pt,      w );
					h_dijet_E      [iAlg] -> fill( dijet_e,       w );
					h_dijet_eta    [iAlg] -> fill( dijet_eta,     w );
					h_dijet_y      [iAlg] -> fill( dijet_rap,     w );
					h_dijet_ystar  [iAlg] -> fill( ystar,         w );
					h_dijet_phi    [iAlg] -> fill( dijet_phi,     w );
					h_dijet_mass   [iAlg] -> fill( dijet_mass,    w );
					h_dijet_invmass[iAlg] -> fill( dijet_invmass, w );
					
					for(int iy=0; iy<nybins;iy++)
					{
						if( fabs(ystar) >= ystarbins[iy] && fabs(ystar) < ystarbins[iy+1] )
						{
							//const double m = (leadjets[0] + leadjets[1]).mass() / GeV;                                                            
							h_dijet_invmass_ystar[iAlg][iy]   -> fill( dijet_invmass, w);	        
							break;
						}
					}
					for(int iy=0; iy<nybins;iy++)
					{
						if( fabs(dijet_rap) >= ybinsEdges[iy] && fabs(dijet_rap) < ybinsEdges[iy+1] )
						{
							h_dijet_mass_y[iAlg][iy] -> fill( dijet_mass, w );
							h_dijet_pt_y  [iAlg][iy] -> fill( dijet_pt,   w );
							break;
						}
					}
					leadjets.clear();
				}
			}
		}
		
		
		void finalize() 
		{
			
			std::cout << " I'm in finalize() function" << std::endl;
			///std::cout << "Needs cross-section " << needsCrossSection() <<std::endl;
			std::cout << "SumOfWeights: " << sumOfWeights() << std::endl;
			std::cout << "CrossSection: " << crossSection() << std::endl;
			//std::cout << "CrossSectionPerEvent: " << crossSectionPerEvent() << "  " << picobarn << std::endl;
			//std::cout << "Cross section" <<crossSection()/picobarn << "\t Cross section per event" << crossSectionPerEvent()/picobarn << std::endl;
			//double xs = 1.0; // = crossSection()/picobarn;
			
			// This part does not work in pythia 6 <= 428.2
			//const double xs = crossSectionPerEvent()/picobarn;
			double xs=1.0;
			if( crossSection() == 987654321 )
			{
				xs=getXSfromLog();
			}
			else
			{
				xs=crossSection()/picobarn;
			}
			
			cout << "Using cross section: " << xs << endl;
			
			double genFiltEff=1.0;
			//genFiltEff=getGetGenFiltEffFromLog();
			cout << "Using gen filter efficiency: : " << genFiltEff << endl;
			
			h_nFiles -> fill( 1 );
			h_sumOfWeights -> fill( 1, sumOfWeights() );
			h_crossSection -> fill( 1, xs );
			h_filtEff      -> fill( 1, genFiltEff );
		}
	
	
	
	};
	
	
	// The hook for the plugin system
	DECLARE_RIVET_PLUGIN(ATLAS_2018_IJXS_IDXS_Alieksei);
}

