import os

b_rm_root = True
b_rm_yoda = True
b_merge_yoda = True
b_yodaToRoot = True
b_mkhtml = True

path_HTCondor = "/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/Fork/npc/HTCondor_Run_5"

rootdir = path_HTCondor
for rootdir, dirs, files in os.walk(rootdir):
	for subdir in dirs:
		
		#print(os.path.join(rootdir, subdir))
		#print subdir
		#print "\n"
		if subdir == "BatchOutput":
			# river-merge
			path = os.path.join(rootdir, subdir)
			inFiles    = path + "/*.yoda.gz"
			mergedFile = path + "/MERGED.yoda.gz"
			mergedRoot = mergedFile
			mergedRoot = mergedRoot.replace(".yoda.gz", ".root")
			
			if b_rm_root:
				cmd_rm_root = "rm " + mergedRoot
				print (cmd_rm_root)
				os.system(cmd_rm_root)
			
			if b_rm_yoda:
				cmd_rm_yoda = "rm " + mergedFile
				print (cmd_rm_yoda)
				os.system(cmd_rm_yoda)
			
			if b_merge_yoda:
				cmd_river_merge = "rivet-merge -o " + mergedFile + " " + inFiles
				print cmd_river_merge
				os.system(cmd_river_merge)
			
			if b_yodaToRoot == True:
				# yoda to root
				cmd_yodaToRoot = "python yoda2root.py --inFile " + mergedFile
				print cmd_yodaToRoot
				os.system(cmd_yodaToRoot)
			
			if b_mkhtml:
				# mkhtml
				cmd_mkhtml = "rivet-mkhtml --errs -o " + path + "/HTML_MERGED " + mergedFile
				print cmd_mkhtml
				os.system(cmd_mkhtml)


