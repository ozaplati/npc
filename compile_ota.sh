ANA_PATH=src

#ARR_ANA_NAME=( ATLAS_2018_IJXS_IDXS_Alieksei ATLAS_2022_IJXS_IDXS )
#ARR_ANA_NAME=( ATLAS_2022_IJXS_IDXS )
ARR_ANA_NAME=( ATLAS_2023_IJXS_IDXS_Rivet_3_1_8 ATLAS_2023_IJXS_IDXS_Rivet_3_1_8_ROOT )
#ARR_ANA_NAME=( ATLAS_2023_IJXS_IDXS_Rivet_3_1_8_ROOT )


for ANA_NAME in ${ARR_ANA_NAME[@]}; do
	#rivet-build Rivet${ANA_NAME}.so ${ANA_PATH}/${ANA_NAME}.cc 
	rivet-build --with-root Rivet${ANA_NAME}.so ${ANA_PATH}/${ANA_NAME}.cc 
	
	#rivet-buildplugin ${ANA_PATH}/${ANA_NAME}.cc 
 done

