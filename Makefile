ANAPATH=src
#ANANAME=InclusiveJets
ANANAME=ATLAS_2022_IJXS_IDXS_Rivet_2_2_1

all: 
	rivet-buildplugin --with-root Rivet${ANANAME}.so ${ANAPATH}/${ANANAME}.cc

## In case one has 'permission denied' problem when using rivet-buildplugin, do
## 'make all-plug-local-setup'
## 'make all-plug-local'
all-plug-local-setup:
	cat `which rivet-buildplugin` | sed "s#/afs/.cern.ch#${SITEROOT}#g" | sed "s#/afs/cern.ch#${SITEROOT}#g" | sed "s#/yoda/1.3.1#/yoda/1.3.0#g" > rivet-buildplugin_local
	chmod 755 rivet-buildplugin_local
all-plug-local:
	./rivet-buildplugin_local --with-root Rivet${ANANAME}.so ${ANAPATH}/${ANANAME}.cc



test:
	mkdir -p Test_Aliaksei
	cd Test_Aliaksei
	Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=123456789 --jobConfig=../FromAliaksei/jobOptions/GenerateJetsJO_PY8_EIG.py --runNumber=100000 --outputEVNTFile=pool.root --maxEvents=100 --env NPC_JSAMPLE=1 NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL



clean:
	rm -rf *.dec *.out *.dat *.DEC G4particle_whitelist.txt *.xml *.xml.BAK *.pickle jobReport.json jobReport.txt ntuple.pmon.gz pdt.table pool.root pool.root*.log runargs.generate.py runwrapper.generate.sh inclusiveP8DsDPlus.pdt log.generate PDGTABLE.MeV TestHepMC.root _joproxy15 Process *.txt

cleanall: clean
	rm *.so *.yoda


