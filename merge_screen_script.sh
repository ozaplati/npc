#!bin/bash
# Export ATLAS paths
JSAMPLE=${1} 
IN_PATH=${2}

DESC=`basename ${IN_PATH}`
PATH_CODE=/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/Fork/npc/

echo "JSAMPLE:    " ${JSAMPLE}
echo "IN_PATH:    " ${IN_PATH}
echo "PATH_CODE:  " ${PATH_CODE} 
echo ""
echo ""
echo "setupATLAS"
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

echo "setupAthena"
asetup 21.6.94,AthGeneration     # Above version have a problem with rivet, keep this version

# Rivet
echo "Setup Rivet"
source setupRivet.sh
export RIVET_ANALYSIS_PATH=$PWD


python2  ${PATH_CODE}/merge_python2.py --inputPath ${IN_PATH}  --jSampleMin ${JSAMPLE} --jSampleMax ${JSAMPLE} --convertToRoot True --mergeSeeds False  --scale False  --mergeScaled False 2>&1 | tee log.${DESC}_JSample${JSAMPLE}
#python3 ${PATH_CODE}/merge_python3.py --inputPath ${IN_PATH}  --jSampleMin ${JSAMPLE} --jSampleMax ${JSAMPLE} --convertToRoot True --mergeSeeds False  --scale False  --mergeScaled False 2>&1 | tee log.${DESC}_JSample${JSAMPLE}
