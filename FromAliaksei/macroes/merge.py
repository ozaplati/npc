import os
from ROOT import *
from array import array

### first run testFile=True, others True as well. This will check if all files from event_seq exist. If no ERRORs, than it is ready for matching.
### then switch testFile to False to run Merging.

#### These switched for matching Trees.
testFiles=False
mergeRepsMatch = True
normJsamMatch  = True
#merjeJsam: eff=UEOnly; gen=Pythia8_ATLASA14NNPDF; hadd merged_${gen}_${eff}.root reps_merged/merged_10000*_${gen}_${eff}.root

genNames=["Pythia8_ATLASA14NNPDF"]
#genNames=["Pythia8_4C"]
runNumbers=["100001","100002","100003","100004","100005","100006","100007","100008","100009"]
effects = ["PartonLVL","HadOnly","UEOnly","ParticleLVL"]

runtag="v2"

indir  = "../batch_results/%s" % runtag
outdir = "Merge/%s" % runtag

ANALYSIS="JetTree"


#### These switches for matching histograms. Not for trees
mergeReps = False
normJsam  = False
mergeJsam = False
jSamMin=2
jSamMax=4

runs=[
    "result_100003_Pythia8_ATLASA14NNPDF_PartonLVL"
    #"result_ECM13000_ATLASA14NNPDFEIG1p_Run100000_Events100000",
    ]





mRepsDir="reps_merged"
mNormDir="jsam_norm"
mJsamDir="jsam_merged"


hists=[
    "jet_pt_AKT04","jet_n_AKT04","jet_eta_AKT04","jet_phi_AKT04",
    "jet_pt_y0_AKT04","jet_pt_y1_AKT04","jet_pt_y2_AKT04","jet_pt_y3_AKT04","jet_pt_y4_AKT04","jet_pt_y5_AKT04","jet_pt_y6_AKT04"
    ]


if mergeRepsMatch:

    savedirReps=outdir+"/"+mRepsDir
    os.system("mkdir -p "+savedirReps)

    # So far we make the list of first and last events manually to ensure correct event sequence for matching.
    event_seq = ["1_100000", 
                 "100001_200000", 
                 "200001_300000", 
                 "300001_400000",
                 "400001_500000",
                 "500001_600000",
                 "600001_700000"
    ]

    ## First we check that all files of same type do exist.
    ## If you see error, than remove line from event_seq.
    for iGen in genNames:
        for iRun in runNumbers:
            for iEff in effects:

                listFiles=[]

                for event in event_seq:
                    fileName = "result_%s_%s_%s_%s.root" % (iRun, iGen, iEff,event)
                    fileNameFull = "%s/%s" % (indir,fileName)

                    if not os.path.isfile(fileNameFull):
                        print "ERROR: file does not exist: %s" % fileNameFull
                        exit()

                    
                    ifile=TFile(fileNameFull,"read")
                    if not ifile or ifile.IsZombie() or not ifile.Get("xs"):
                        print "ERROR: zombie file: %s" % fileNameFull
                        exit()

                    if normJsamMatch and not testFiles:
                        savedirNorm=outdir+"/"+mNormDir
                        os.system("mkdir -p "+savedirNorm)
                        
                        fileNameFullNorm = "%s/%s" % (savedirNorm,fileName)
                        
                        command = "cp %s %s" % ( fileNameFull,fileNameFullNorm) 
                        
                        os.system( command )

                        fileNameFull = fileNameFullNorm
                        
                        print "Updating file: %s" % fileNameFull
                        fileUpd = TFile(fileNameFull,"update")
                        inTree=fileUpd.JetTree
                        outTree=TTree("xsTree","xsTree")
                        
                        ew = array('f',[0.0])
                        outTree.Branch("xs",ew,"xs")

                        xs      = fileUpd.Get("xs").GetBinContent(1)
                        nFiles  = fileUpd.Get("nFiles").GetBinContent(1)
                        sumW    = fileUpd.Get("sumOfWeights").GetBinContent(1)

                        iEvent=0
                        for e in inTree:    
                            iEvent=iEvent+1
                            if not iEvent % 10000:
                                print iEvent
                            #print e.eventNumber, e.eventWeight
                            ew[0]=xs / nFiles / sumW
                            outTree.Fill()

                        inTree.AddFriend(outTree)
                        fileUpd.Write()

                        
                    listFiles.append(fileNameFull)
                    
                if listFiles:
                    #print listFiles
                    command = "hadd -v 0 %s/merged_%s_%s_%s.root %s" % (savedirReps, iRun, iGen, iEff,' '.join(listFiles))
                    if not testFiles:
                        print command
                        os.system( command )


##########################################
####
####
if mergeReps:
    savedir=outdir+"/"+mRepsDir
    os.system("mkdir -p "+savedir)
    for run in runs:
        print "Merging",run
        for sam in range(jSamMin,jSamMax+1):
            idir="%s/" % (indir)
            listFiles=[]
            for filename in os.listdir(idir):                
                if run+"_JS"+str(sam) in filename and filename.endswith(".root"):
                    #print filename
                    ifile=TFile(idir+filename,"read")
                    if ifile and not ifile.IsZombie() and ifile.Get(ANALYSIS+"/crossSection"):
                        listFiles.append(idir+filename)
                    else:
                        print "Zombie:",idir+filename
            if listFiles:
                command = "hadd -v 0 %s/%s_J%d.root %s" % (savedir,run,sam, ' '.join(listFiles))
                print command
                #os.system( command )
                


##########################################
####
####
if normJsam:
    savedir=outdir+"/"+mNormDir
    os.system("rm -rf "+savedir)
    os.system("mkdir -p "+savedir)
    for run in runs:
        for sam in range(jSamMin,jSamMax+1):
            fileName="%s/%s/%s_J%d.root" %(outdir,mRepsDir,run,sam)
            fj=TFile(fileName,"read")
            xs      = fj.Get(ANALYSIS+"/crossSection").GetBinContent(1)
            nFiles  = fj.Get(ANALYSIS+"/nFiles").GetBinContent(1)
            sumW    = fj.Get(ANALYSIS+"/sumOfWeights").GetBinContent(1)
            nEvents = fj.Get(ANALYSIS+"/nEvents").GetBinContent(1)
            nSelEvents = fj.Get(ANALYSIS+"/nSelectedEvents").GetBinContent(1)
            filtEff = fj.Get(ANALYSIS+"/filtEff").GetBinContent(1)
            sf = xs/nFiles / sumW
            #print nEvents, nSelEvents, sumW

            fileName="%s/%s_J%d.root" % (savedir,run,sam)
            fnew=TFile(fileName,"recreate")
            dd=gDirectory.mkdir(ANALYSIS)
            dd.cd()
            for hist in hists:
                histName  = "%s/%s" % (ANALYSIS,hist)
                htmp=fj.Get( histName)
                #print run,sam,hist
                htmp.Scale(sf)
                htmp.Write(hist)
            fnew.Close()


##########################################
####
####
if mergeJsam:
    savedir=outdir+"/"+mJsamDir
    os.system("rm -rf  "+savedir)
    os.system("mkdir -p "+savedir)
    for run in runs:
        command = "hadd -v 0 %s/%s.root %s/%s/%s_J*.root" %(savedir,run,outdir,mNormDir,run)
        print command
        os.system(command)
