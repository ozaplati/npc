#!/bin/bash
###(to prevent from starving in queue)
###$ -R y
# (only accept jobs with correct resources)
#$ -w e
#
######3# (stderr and stdout are merged together to stdout)
#$ -j y
#
# (send mail on job's end and abort)
###$ -m ae -M karnem@mail.desy.de
#
# (put log files into current working directory)
#$ -cwd
#
# (use ATLAS project)
###$ -P atlas
#
# (choose memory)
#$ -l s_vmem=8G
#$ -l h_vmem=8G
###$ -l h_stack=10M
#
# (choose time)
#$ -l h_rt=23:59:00
###$ -l h_rt=7:59:00
###$ -l h_rt=01:00:00
###$ -l h_cpu=00:59:00
#
#$ -l h_cpu=02:58:00
# (choose operation system)
#$ -l os=sld6
###$ -l arch=x86
echo "----------------------------------------------------------------"
echo "Started at:"
date
hostname
echo "----------------------------------------------------------------"

#RUNNUMBER=100003;
#shift;
#EVENTS=1000;
#shift;
#JSAMP=6;
#REPEAT=2;
#shift;
#TUNE=MONASH;
#REPEAT=${SGE_TASK_ID}

echo "Working with " ${EVENTS} ${fEvent} ${JSAMP} ${TUNE} ${GEN} ${EFFECT}
echo ${EVENTS} ${fEvent} ${JSAMP} ${TUNE} ${GEN} ${EFFECT}
echo "----------------------------------------------------------------"

#exit();
#TMPDIR=$(mktemp -d -p /lustre/grid/atlas/store/user/smihalcov/batch/TempDir)
#TMPDIR=$(mktemp -d -p $HOME)
#echo "echo TMPDIR: "$TMPDIR
#echo "ls tmpdir: "$(ls -d $TMPDIR)
#cd $TMPDIR

#WORKDIRNEW=/lustre/grid/atlas/store/user/smihalcov/workdir/Gen_${GEN}_Tune_${TUNE}_J${JSAMP}_EFFECT_${EFFECT}_fEvent${fEvent}
WORKDIRNEW=$PWD/Gen_${GEN}_Tune_${TUNE}_J${JSAMP}_EFFECT_${EFFECT}_fEvent${fEvent}

mkdir -p ${WORKDIRNEW}

cd ${WORKDIRNEW}

echo "Working in dir: "${WORKDIRNEW}
echo "----------------------------------------------------------------"

ANALYSISDIR=/lustre/grid/atlas/store/user/smihalcov/NPC__/

#ANALYSISDIR=$HOME/NPC__/
#cd ${ANALYSISDYR}

SAVEDIR=/lustre/grid/atlas/store/user/smihalcov/batch_new/results_J${JSAMP}_Tune_${TUNE}_GEN_${GEN}_EFFECT_${EFFECT}
 
#SAVEDIR=$HOME/public/Batch/results_run${RUNNUMBER}"_J"${JSAMP}"_Tune_"${TUNE}"_GEN_"${GEN} 

mkdir -p ${SAVEDIR}
echo ${SAVEDIR}"------------"

echo ${ANALYSISDIR}"------------"
cd ${ANALYSISDYR}

cp ${ANALYSISDIR}setup.sh ${WORKDIRNEW}
cp ${ANALYSISDIR}LeadingJets.cc ${WORKDIRNEW}
cp ${ANALYSISDIR}GenerateJO_PY8_EIG.py ${WORKDIRNEW}
cp ${ANALYSISDIR}RivetJETS.so ${WORKDIRNEW}
#cp ${ANALYSISDIR}yoda2root.py ${WORKDIRNEW}

if [[ $GEN = "Sherpa" ]]; then                                                 
#    echo "Rabotaet"
    cp -r ${ANALYSISDIR}share ${WORKDIRNEW}
    cp -r ${ANALYSISDIR}MC15JobOptions ${WORKDIRNEW}
fi                    

echo "Copy completed"
ls -lsa
echo "----------------------------------------------------------------"

#cd ${ANALYSISDIR}
#if [ $RUNNUMBER == 100000 -o $RUNNUMBER == 100001 -o  $RUNNUMBER == 100002 -o $RUNNUMBER == 100003 ]; then
#source setupCT10.sh
#else
#source setup.sh
#rivet-buildplugin RivetJETS.so JETS.cc
#    export RIVET_ANALYSIS_PATH=$PWD
#fi

#export RIVET_ANALYSIS_PATH=$PWD

cd ${WORKDIRNEW}
source setup.sh
echo "Now in dir: "$PWD
echo "----------------------------------------------------------------"

Generate_tf.py --ecmEnergy=13000 --runNumber=100000 --firstEvent=${fEvent} --randomSeed=1234 --jobConfig=GenerateJO_PY8_EIG.py --outputEVNTFile= --maxEvents=${EVENTS} --env NPC_JSAMPLE=${JSAMP} NPC_EFFECT=${EFFECT} NPC_TUNE=${TUNE} NPC_GEN=${GEN} 


#Generate_tf.py --ecmEnergy=14000 --firstEvent=1 --randomSeed=12346 --jobConfig=GenerateJO_PY8_EIG.py --runNumber=100001 --outputEVNTFile=pool.root --maxEvents=1000 --env NPC_JSAMPLE=5 NPC_TUNE=LOEE4 GEN=Herwigpp


echo "----------------------------------------------------------------"
echo "Check"
ls -las
echo "----------------------------------------------------------------"
echo "Printing log:"
cat log.generate
echo "----------------------------------------------------------------"
#echo "Doing root file in: "${PWD}
#yoda2root Rivet.yoda
echo "----------------------------------------------------------------"
ls -las
#echo "----------------------------------------------------------------"
#echo "Filling filter efficiency from log file"
#root -l -b -q setFilterEfficiency.C

echo "----------------------------------------------------------------"
echo "Moving file to destination"
mkdir -p ${SAVEDIR}
mv "${WORKDIRNEW}/TreeJets.root" "${SAVEDIR}/result_${EVENTS}_J${JSAMP}_TU_${TUNE}_fEvent_${fEvent}_GEN_${GEN}.root"

echo "Removing WORKDIRNEW"
rm -rf ${WORKDIRNEW}
echo "----------------------------------------------------------------"
#ls -lsa ${TMPDIR}/
echo "----------------------------------------------------------------"

echo "----------------------------------------------------------------"
echo "Ended at:"
date
echo "----------------------------------------------------------------"






