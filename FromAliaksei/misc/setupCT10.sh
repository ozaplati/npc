#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
  
#asetup 17.2.11.14,AtlasProduction,64,slc5
#asetup 19.2.0
#asetup 19.3.0
#asetup 19.2.5.1,here
#asetup 19.2.4.12.1,MCProd,64,here # Sherpa 2.2.0
asetup 19.2.1.6,64 # Sherpa 2.1.1
#asetup 19.2.4,AtlasSimulation,64
#asetup 16.6.9.6
###On lxplus
#setupATLAS
#asetup 19.2.0

source /afs/.cern.ch/sw/lcg/external/MCGenerators_lcgcmt67b/rivet/2.2.1/x86_64-slc6-gcc47-opt/rivetenv.sh

#export LHAPATH=/afs/.cern.ch/sw/lcg/external/MCGenerators_lcgcmt67b/lhapdf/5.9.1/share/lhapdf/PDFsets

