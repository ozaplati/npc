import os
from ROOT import *

ver="v3"
INDIR="../BatchOutput/"
OUTDIR=INDIR+"Results_"+ver

Events = 100000
ECM    = 13000

JS = [2,3,4]

tunes  = ["ATLASA14NNPDFEIG1n", "ATLASA14NNPDFEIG1p", 
          "ATLASA14NNPDFEIG2n", "ATLASA14NNPDFEIG2p", 
          "ATLASA14NNPDFEIG3an","ATLASA14NNPDFEIG3ap",
          "ATLASA14NNPDFEIG3bn","ATLASA14NNPDFEIG3bp",
          "ATLASA14NNPDFEIG3cn","ATLASA14NNPDFEIG3cp"]
runs   = [ 100000, 100001 ]

command = "mkdir -p %s" % OUTDIR
os.system( command )


for iJS in range(len(JS)):
    for itune in range(len(tunes)):
        for irun in range(len(runs)):
            mask="result_ECM%d_%s_Run%d_Events%d_JS%d" %(ECM,tunes[itune],runs[irun],Events,JS[iJS])
            command = "hadd %s/%s.root %s/%s_*.root" % (OUTDIR,mask,INDIR,mask)
            os.system( command )




