import os
from ROOT import *

path="../Results/v03/jsam_merged"
hName="JETS/jet_pt_y0_AKT04"

JS     = 2
Events = 100000
ECM    = 13000

tunes  = ["ATLASA14NNPDFEIG1n", "ATLASA14NNPDFEIG1p", 
          "ATLASA14NNPDFEIG2n", "ATLASA14NNPDFEIG2p", 
          "ATLASA14NNPDFEIG3an","ATLASA14NNPDFEIG3ap",
          "ATLASA14NNPDFEIG3bn","ATLASA14NNPDFEIG3bp",
          "ATLASA14NNPDFEIG3cn","ATLASA14NNPDFEIG3cp"]
colors  = [kRed+1,kRed+1,kBlue+1,kBlue+1,kOrange+1,kOrange+1,kGreen+2,kGreen+2,kCyan+1,kCyan+1]
markers = [22,23,22,23,22,23,22,23,22,23]

runs   = [ 100000, 100001 ]

f = []
h = []

leg = TLegend(0.7,0.1,1.0,0.9)

for itune in range(len(tunes)):
    f.append([])
    h.append([])
    for irun in range(len(runs)):
        #mask  = "%s/result_ECM%d_%s_Run%d_Events%d_JS%.root" %(path,ECM,tunes[itune],runs[irun],Events,JS)
        mask  = "%s/result_ECM%d_%s_Run%d_Events%d.root" %(path,ECM,tunes[itune],runs[irun],Events)
        f[itune].append(TFile(mask,"read")) 
        h[itune].append( f[itune][irun].Get(hName) )

        h[itune][irun].Rebin(2)
        h[itune][irun].SetLineColor  (colors [itune])
        h[itune][irun].SetMarkerColor(colors [itune])
        h[itune][irun].SetMarkerStyle(markers[itune] )
        h[itune][irun].SetMarkerSize(1.5)

    h[itune][1].Divide(h[0][0])
    leg.AddEntry(h[itune][1],tunes[itune],"pl")


for itune in range(len(tunes)):
    h[itune][1].Draw("same p")

h[0][1].GetXaxis().SetRangeUser(20,200)
h[0][1].GetYaxis().SetRangeUser(0.81,1.29)

h[0][1].GetYaxis().SetTitle("NPC")
h[0][1].GetXaxis().SetTitle("p_{T}^{jet} [GeV]")

leg.Draw()

tl = TLatex()
tl.SetNDC()
tl.SetTextFont(42)
tl.SetTextSize(0.04)
tl.DrawLatex(0.2,0.85, "Inclusive jets")
tl.DrawLatex(0.2,0.80,"|y_{jet}|<0.5")


raw_input()
