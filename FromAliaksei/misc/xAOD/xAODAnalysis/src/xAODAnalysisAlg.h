#ifndef XAODANALYSIS_XAODANALYSISALG_H
#define XAODANALYSIS_XAODANALYSISALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

#include <iostream>
#include <typeinfo>
#include <vector>

#include "TH1D.h"
#include "TTree.h"
#include "TFile.h"

using namespace std;



class xAODAnalysisAlg: public ::AthAnalysisAlgorithm { 
 public: 
  xAODAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~xAODAnalysisAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   //TTree* m_myTree = 0;
  
  TH1D* h_nEvents = 0;
  TH1D* h_xs = 0;
  TH1D* h_sumOfWeights = 0;
  TH1D* h_nFiles = 0;

  TTree* m_myTree = 0;

  int*    eventNumber;
  //float* event_weight;

  int* jet_N;
  vector<double>* jet_E;
  vector<double>* jet_Pt;
  vector<double>* jet_M;
  vector<double>* jet_Rap;
  vector<double>* jet_Eta;
  vector<double>* jet_Phi;
}; 

#endif //> !XAODANALYSIS_XAODANALYSISALG_H
