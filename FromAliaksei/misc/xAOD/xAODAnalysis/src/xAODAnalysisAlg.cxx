// xAODAnalysis includes
#include "xAODAnalysisAlg.h"
#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthParticle.h"





xAODAnalysisAlg::xAODAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


xAODAnalysisAlg::~xAODAnalysisAlg() {}


StatusCode xAODAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  h_nEvents = new TH1D ("nEvents","Number of Events", 1, 0, 2);
  h_xs      = new TH1D ("xs","Total cross section", 1, 0, 2);
  h_sumOfWeights = new TH1D ("sumOfWeights", "Sum of event weights", 1, 0, 2);
  h_nFiles  = new TH1D ("nFiles", "Number of files", 1, 0, 1);
  
  CHECK( histSvc()->regHist("/MYSTREAM/h_nEvents", h_nEvents) );
  CHECK( histSvc()->regHist("/MYSTREAM/h_xs", h_xs) );
  CHECK( histSvc()->regHist("/MYSTREAM/h_sumOfWeights", h_sumOfWeights) );
  CHECK( histSvc()->regHist("/MYSTREAM/h_nFiles", h_nFiles) );

  m_myTree = new TTree("myTree","myTree");
  CHECK( histSvc()->regTree("/MYSTREAM/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  jet_N   = new int;
  jet_E   = new vector<double>;
  jet_Pt  = new vector<double>;
  jet_M   = new vector<double>;                                                                                                         
  jet_Rap = new vector<double>;                                                                                            
  jet_Eta = new vector<double>;                                          
  jet_Phi = new vector<double>;


  //m_myTree->Branch("eventNumber",&eventNumber);
  //m_myTree->Branch("eventWeight",&eventWeight);

  //m_myTree->Branch( (string("event_weight")). c_str(), &event_weight);
  m_myTree->Branch( (string("jet_N")).        c_str(), &jet_N       );
  m_myTree->Branch( (string("jet_E")).        c_str(), &jet_E       );
  m_myTree->Branch( (string("jet_M")).        c_str(), &jet_M       );
  m_myTree->Branch( (string("jet_Pt")).       c_str(), &jet_Pt      );
  m_myTree->Branch( (string("jet_Rap")).      c_str(), &jet_Rap     );
  m_myTree->Branch( (string("jet_Eta")).      c_str(), &jet_Eta     );
  m_myTree->Branch( (string("jet_Phi")).      c_str(), &jet_Phi     );

  return StatusCode::SUCCESS;
}

StatusCode xAODAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode xAODAnalysisAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed


  //event_weight = 0;
  jet_N  = 0;
  jet_E  ->clear();
  jet_M  ->clear();
  jet_Pt ->clear();
  jet_Rap->clear();
  jet_Eta->clear();
  jet_Phi->clear();

  //const xAOD::TruthEvent* te = 0;
  //CHECK( evtStore()->retrieve( te , "TruthEvent" ) );
  const xAOD::EventInfo* ei = 0;
  CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  ATH_MSG_VERBOSE( "Event = " << *ei );
  ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  ATH_MSG_INFO("eventWight="  << ei->mcEventWeight(0) );
  ATH_MSG_INFO("==================================");


  //event_weight = ei->mcEventWeight(0);

  //ATH_MSG_INFO("xs  =" << ei->mcEventCrossSection());
  //for (int i=0;i<28;i++) {
  //  ATH_MSG_INFO("eventWeight["<< i << "]  =" << ei->mcEventWeight(i));///ei->eventNumber()));
  //}
  //if (ei->eventNumber()<9) {    
  //ATH_MSG_INFO("eventWeight=" << ei->mcEventWeight(0));
 
  const xAOD::TruthParticleContainer* tp = NULL;
  CHECK( evtStore()->retrieve( tp, "TruthParticles"));
  
  //for (const xAOD::TruthParticle *particles : *tp) {
  //cout << tp -> size() << endl;
  
  
  const xAOD::JetContainer* jets = nullptr;
  //CHECK( evtStore()->retrieve( jets, "TruthJets" ) );
  CHECK( evtStore()->retrieve( jets, "AntiKt4TruthDressedWZJets" ) );
  ATH_MSG_INFO("execute(): number of jets = " << jets->size());
  
  //jet_N = jets->size();
  //cout << "execute(): number of jets = " << jets->size() << endl;
  // loop over the jets in the container
  for (const xAOD::Jet *jet : *jets) {
    ATH_MSG_INFO(TString::Format("execute(): jet pt = %f GeV, y = %f, phi = %f", jet->pt() * 0.001, jet->rapidity(), jet->phi()));
    jet_Pt->push_back(jet->pt() * 0.001);
    //ATH_MSG_INFO("execute(): jet y   = " << (jet->rapidity()));
    jet_Rap->push_back(jet->rapidity());
    //ATH_MSG_INFO("execute(): jet eta = " << (jet->eta()));
    jet_Eta->push_back(jet->eta());
    //ATH_MSG_INFO("execute(): jet phi = " << (jet->phi())); 
    jet_Phi->push_back(jet->phi());
    //ATH_MSG_INFO("execute(): jet E   = " << (jet->e() * 0.001) << " GeV");
    jet_E  ->push_back(jet->e());
    //ATH_MSG_INFO("execute(): jet m   = " << (jet->m() * 0.001) << " GeV"); 
    jet_M  ->push_back(jet->m());
  }
  //ATH_MSG_INFO("eventWeight[0]  =" << ei->mcEventWeight(0));///ei->eventNumber()));
  //ATH_MSG_INFO("eventWeight[1]  =" << ei->mcEventWeight(1));///ei->eventNumber()));
  //ATH_MSG_INFO("eventWeight[10] =" << ei->mcEventWeight(10));///ei->eventNumber()));
  //ATH_MSG_INFO("eventWeight[27] =" << ei->mcEventWeight(27));///ei->eventNumber()));
  
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram
  m_myTree->Fill();



  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode xAODAnalysisAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


