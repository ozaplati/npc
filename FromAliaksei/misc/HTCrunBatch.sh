#!/bin/bash

echo "----------------------------------------------------------------"
echo "Start: "$(date)
echo "on host: "${HOSTNAME}
echo "----------------------------------------------------------------"

echo ${PWD}
ls -lsa

GEN=${1}
SEED=${2}
JO=${3}
RUN=${4}
EVENTS=${5}
RUNDIR=${6}

let SEED=${SEED}+1

echo "GEN="${GEN}
echo "SEED="${SEED}
echo "JO="${JO}
echo "RUN="${RUN}
echo "EVENTS="${EVENTS}
echo "NPC_TUNE="${NPC_TUNE}
echo "JSAMPLE="${NPC_JSAMPLE}
echo "rundir="${RUNDIR}

source setup.sh
Generate_tf.py --GEN=${GEN} --firstEvent=1 --randomSeed=${SEED} --jobConfig=${JO} --runNumber=${RUN} --outputEVNTFile=pool.root --maxEvents=${EVENTS} > log_Shower.txt

if [ -f Rivet.yoda ]; then 
    echo "yoda2root"
    yoda2root Rivet.yoda
fi

# if [ -f Rivet.root ]; then
#     echo "Moving output"
#     OUTPUTDIR=${RUNDIR}/Results
#     mkdir -p ${OUTPUTDIR}
#     mv Rivet.root ${OUTPUTDIR}/result_ECM${ECM}_Seed${SEED}_Run${RUN}_Events${EVENTS}.root
# fi


ls -lsa


echo "----------------------------------------------------------------"
echo "Finish: "$(date)
echo "on host: "${HOSTNAME}
echo "----------------------------------------------------------------"