[[_TOC_]]

# Description

The package is intended for studies of non-perturbative corrections in ATLAS.
It provides a jobOption for the `Generate_tf.py` that runs multiple MC generators.
The events are then selected and analysed using Rivet. 
To study the non-perturbative correction, one should run the generation twice: with non-perturbative effects turned on and off. 
The ratio of resulting spectra gives the non-perturbative correction.

The package runs the MC generator setup specified in the jobOption, makes the event selection and fills the histograms using RIVET.

The jobOption example is in `jobOptions/GenerateJetsJO_PY8_EIG.py`. The current jobOption sets up the pp->jets production. 
By default it is Pythia8 (`HardQCD:all = on`). 
Other JO options:
- Generator can be chosen by `NPC_GEN` option (e.g.`NPC_GEN=Pythia8`).
- In order to provide sufficient statistics in wide pT range, the jobOption limits the hardest parton pT range 
(i.e. providing the so called JSamples using `PhaseSpace:pTHatMin` and `PhaseSpace:pTHatMax` options for Pythia8. These options are different for other generators.).
The JSample is setup using the `runNumber` option (e.g. `--runNumber=100000`).
Note, it might be different for generators other then Pythia8, so check the jobOption.
- In order to derive NPC the MC generator must run twice: with underlying events and hadronisation turned on (`NPC_EFFECT=ParticleLVL`) and off (`NPC_EFFECT=PartonLVL`).
In additon there are options `NPC_EFFECT=HadOnly` and `NPC_EFFECT=UEOnly` to study the contribution of each non-perturbative effect sepearately.
- Sometime it makes sense to specify the same random generator seed for `NPC_EFFECT=ParticleLVL` and `NPC_EFFECT=PartonLVL` 
productions in order to reduce the fluctuations when doing the ratio of two spectra. 
It also allows to make the results reproducible. 
This can be done using `--randomSeed=123456789` option.
One should be careful when using seeds, as they should be unique for jobs with same setup, i.e. in case of large scale production on batch or grid. 
- There are several predefined MC generator tunes in order to study the uncertainty on underlying event model (e.g. `NPC_TUNE=ATLASA14NNPDF`).
Note, the tune names are unique for each generator, so one must look ot jobOption for the available tunes or introduces the new ones.
- Different generators can be used to study the uncertainty on hadronisation model 
(i.e. Pythia8 runs Lund-string model by default, Herwig++ runs cluster model, Sherpa can run any model specified by jobOption. The first two should work in this package out-of-the-box, but the Sherpa setup is not provided.)

Examples of RIVET routines are `src/InclusiveJets.cc` for the RIVET style histograms and `src/LeadingJets.cc` for ROOT style histograms.
If the otput is provided in `*.yoda` format, the conversion can be performed using `youda2root` command, which should be available by default when the Athena and Rivet are up.
The RIVET routine is attached to the MC generator using the `rivet.Analyses = ...` line at the end of jobOption.
This works when `rivet-buildplugin ...` is executed (see Makefile).



# Run

Environment is initialised using
```
source setup.sh
```
Current `setup.sh` does

```
asetup 19.2.4.12.1,MCProd,64
```

which contains next generator versions:


- Pythia8 v.210
- Herwig++ v.2.7.1
- Sherpa v.2.2.0 NNPDF
- MadGraph v.2.2.3

The generator versions can be found in the following twiki: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McGeneratorsForAtlas.

To compile analysis do
```
make
```
it executes the following command (see Makefile)
```
 rivet-buildplugin --with-root Rivet${ANANAME}.so ${ANAPATH}/${ANANAME}.cc
```
In case of error, try the recipes from "Issues with rivet-buildplugin" section below.



To run an example do
```
make test
```
it executes the following command (see Makefile)
```
Generate_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=123456789 --jobConfig=jobOptions/GenerateJetsJO_PY8_EIG.py --runNumber=100000 --outputEVNTFile=pool.root --maxEvents=100 --env NPC_JSAMPLE=1 NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL
```

After that you should see the Rivet.yoda file with histograms filled rivet analysis in text. You can do `yoda2root Rivet.yoda` to make root file.

# MISCELLANEOUS

## Extra environment
The following extra environment could be required for some setups,
e.g. in case of lhapdf complaints or for ATLAS default jobOptions use.
```
export RIVET_ANALYSIS_PATH=$PWD
export LHAPATH=/afs/.cern.ch/sw/lcg/external/MCGenerators_lcgcmt67b/lhapdf/5.9.1/share/lhapdf/PDFsets
export JOBOPTSEARCHPATH=/cvmfs/atlas.cern.ch/repo/sw/Generators/MC12JobOptions/latest/common:$JOBOPTSEARCHPATH
```


## Issues with rivet-buildplugin

In case one has 'permission denied' problem when using rivet-buildplugin, do the following

```
make all-plug-local-setup
make all-plug-local
```
This uses rivet-buildplugin_local to compile your Rivet plugins instead of the supplied rivet-buildplugin

This is taken from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RivetForAtlas#Running_Rivet_on_the_GRID_on_CVM.

Notice, `make all-plug-local-setup` introduces some updates to the `rivet-buildplugin_local`. Although, it might need more modifications. 
E.g. in case rivet-buildplugin-local can't find YODA, update all YODA paths inside the rivet-buildplugin-local wherever it is mentioned to the one that is available for the current athena release.
One can start to look for availble one using the partial path of yoda variable in rivet-buildplugin_local.
For the `asetup 19.2.4.12.1,MCProd,64` the following path works:
```
/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc47-opt/19.2.4/sw/lcg/external/MCGenerators_lcgcmt67b/yoda/1.3.0/x86_64-slc6-gcc47-opt/include/
``` 




## Pythia8

### Random seeds

To have the same event sequence each time user runs Pythia8 same random generator seeds must be used.
In current configuration Pythia uses Athena's random generator to set seeds. 
The seeds are updated when `--randomSeed` and `--runNumber` options are changed. 

User can see the current seeds in the log:
```
 generate 23:20:55 AtRndmGenSvc         INFO  Stream =  PYTHIA8, Seed1 =  270431325, Seed2 = 1876767256
 generate 23:20:55 AtRndmGenSvc         INFO  Stream = PYTHIA8_INIT, Seed1 =  361051830, Seed2 = 1507852127
```

Note, same events at parton-level and particle-level are expected 
when user sets same `--randomSeed` and `--runNumber` options.
At least at tree level events look similar, but showering might be different.
To ensure that same seeds are used user should see same seeds in `PYTHIA8_INIT` stream, 
but not in `PYTHIA8` stream. This is due to compicated mechanics of `AtRndmGenSvc`.

## Sherpa

To run Sherpa v.2.1.1 CT10 the setupCT10.sh should be used instead

## MadGraph


For MadGraph you need MadGraphControl (Not yet available)

```
pkgco.py MadGraphControl-00-05-33
cd Generators/MadGraphControl/cmt
cmt make
```


## xAOD

There might be a reason to switch from using Rivet to another method. 
In this case one can do the following xAOD analysis chain: generate EVNT -> do the derivation, e.g. TRUTH1 -> run xAOD analysis.
This is a short working example how to run this chain.

1) Remove everything connected to Rivet from jobOption. Run `Generate_tf.py` as usual. This will produce pool.root file that you should feed to `Derivation Framework`.

2) Setup Athena with Derivation Framework in clean environment:

```
asetup 21.2.86.0,AthDerivation
```

Run

```
Reco_tf.py --inputEVNTFile results/pythia8_AH_JS9.root --outputDAODFile pythia8_AH_JS9.root --reductionConf=TRUTH1
```

3) Create new xAOD analysis in new environment (example is in https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake). Copy `xAODAnalysis` from `misc/xAOD/` folder into `source` directory of your new analysis, compile and run.
