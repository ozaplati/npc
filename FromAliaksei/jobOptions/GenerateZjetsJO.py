## Sherpa config with CT10 PDF
## include("MC15JobOptions/Sherpa_Base_Fragment.py")
## Base config for Sherpa

import os
JSAMPLE=int(os.environ['JSAMPLE'])
print 'JSAMPLE=',JSAMPLE

GEN  = 'Sherpa' 
TYPE = 'NS'
TU = 'CT10'

jetPtMin=20
jetPtMax=8000

if JSAMPLE==1:
   jetPtMin=20
   jetPtMax=50
elif JSAMPLE==2:
   jetPtMin=50
   jetPtMax=100
elif JSAMPLE==3:
   jetPtMin=100
   jetPtMax=200
elif JSAMPLE==4:
   jetPtMin=200
   jetPtMax=300
elif JSAMPLE==5:
   jetPtMin=300
   jetPtMax=400
elif JSAMPLE==6:
   jetPtMin=400
   jetPtMax=8000

#############################
#### SHERPA CT10 defs
if runArgs.runNumber==100000:
   GEN  = 'Sherpa' 
   TYPE = 'NS'
   TU = 'CT10'
elif runArgs.runNumber==100001:
   GEN  = 'Sherpa' 
   TYPE = 'AH'
   TU = 'CT10'
elif runArgs.runNumber==100002:
   GEN  = 'Sherpa' 
   TYPE = 'AS'
   TU = 'CT10'
elif runArgs.runNumber==100003:
   GEN  = 'Sherpa' 
   TYPE = 'NH'
   TU = 'CT10'

#############################
#### SHERPA NNPDF30NNLO defs
elif runArgs.runNumber==100100:
   GEN  = 'Sherpa' 
   TYPE = 'NS'
   TU = 'NNPDF30NNLO'
elif runArgs.runNumber==100101:
   GEN  = 'Sherpa' 
   TYPE = 'AH'
   TU = 'NNPDF30NNLO'
elif runArgs.runNumber==100102:
   GEN  = 'Sherpa' 
   TYPE = 'AS'
   TU = 'NNPDF30NNLO'
elif runArgs.runNumber==100103:
   GEN  = 'Sherpa' 
   TYPE = 'NH'
   TU = 'NNPDF30NNLO'

#############################
#### MadGraph defs
elif runArgs.runNumber==200000:
   GEN  = 'MadGraph' 
   TYPE = 'NS'
elif runArgs.runNumber==200001:
   GEN  = 'MadGraph' 
   TYPE = 'AH'
elif runArgs.runNumber==200002:
   GEN  = 'MadGraph' 
   TYPE = 'AS'
elif runArgs.runNumber==200003:
   GEN  = 'MadGraph' 
   TYPE = 'NH'

#############################
#### Herwigpp defs
elif runArgs.runNumber==300000:
   GEN  = 'Herwigpp' 
   TYPE = 'NS'
elif runArgs.runNumber==300001:
   GEN  = 'Herwigpp' 
   TYPE = 'AH'
elif runArgs.runNumber==300002:
   GEN  = 'Herwigpp' 
   TYPE = 'AS'
elif runArgs.runNumber==300003:
   GEN  = 'Herwigpp' 
   TYPE = 'NH'

#############################
#### Pythia8 defs
elif runArgs.runNumber==400000:
   GEN  = 'Pythia8' 
   TYPE = 'NS'
elif runArgs.runNumber==400001:
   GEN  = 'Pythia8' 
   TYPE = 'AH'
elif runArgs.runNumber==400002:
   GEN  = 'Pythia8' 
   TYPE = 'AS'
elif runArgs.runNumber==400003:
   GEN  = 'Pythia8' 
   TYPE = 'NH'
else:
   print 'ERROR: Wrong run number!'
   exit()


print "Working with ", GEN, TYPE


if GEN=='Sherpa':
   evgenConfig.generators  = ["Sherpa"]
   evgenConfig.auxfiles    = []
   evgenConfig.description = "Sherpa Z/gamma* -> e e + jet"
   evgenConfig.keywords    = ["SM","Z", "electron", "jets" ]
   evgenConfig.contact     = [ "aliaksei.hrynevich@cern.ch" ]
   evgenConfig.minevents   = runArgs.maxEvents


   if JSAMPLE==1:
      runArgs.jobConfig = [ "share/sherpa/sherpa20_50.py"]
   if JSAMPLE==2:
      runArgs.jobConfig = [ "share/sherpa/sherpa50_100.py"]
   if JSAMPLE==3:
      runArgs.jobConfig = [ "share/sherpa/sherpa100_200.py"]
   if JSAMPLE==4:
      runArgs.jobConfig = [ "share/sherpa/sherpa200_300.py"]
   if JSAMPLE==5:
      runArgs.jobConfig = [ "share/sherpa/sherpa300_400.py"]
   if JSAMPLE==6:
      runArgs.jobConfig = [ "share/sherpa/sherpa400_E_CMS.py"]

   if TU=="NNPDF30NNLO":
      include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")
   elif TU=="CT10":
      #include("MC12JobOption/Sherpa_CT10_Common.py")
      #include("MC15JobOptions/Sherpa_2.2.0_Base_Fragment.py")
      #evgenConfig.tune = "CT10"
      include("MC15JobOptions/Sherpa_CT10_Common.py")
      #fixSeq.FixHepMC.LoopsByBarcode = False
      if hasattr(testSeq, "TestHepMC"):
         testSeq.remove(TestHepMC())
   else:
      print 'ERROR: Wrong type ID!'
      exit()   

   if TYPE=='AH':
      pass
   elif TYPE=='NH':
      genSeq.Sherpa_i.Parameters += [
         "MI_HANDLER=None"
         ]
   elif TYPE=='AS':
      genSeq.Sherpa_i.Parameters += [
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         ]
   elif TYPE=='NN':
      genSeq.Sherpa_i.Parameters += [
         "MI_HANDLER=None",
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         ]
   elif TYPE=='NS':
      genSeq.Sherpa_i.Parameters += [
      #topAlg.Sherpa_i.Parameters += [
         "MI_HANDLER=None",
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         #"??? BeamRemnants:primordialKT ???? = off",
         ]
   else:
      print 'ERROR: Wrong type ID!'
      exit()



if GEN=='MadGraph':
    ### 
    ### use no more than 100k events for MadGraph ME, otherwise the output EVNT file is too large
    ###
   
    from MadGraphControl.MadGraphUtils import *
# General settings
    nevents=runArgs.maxEvents
    mode=0
    nJobs=1
    gridpack_dir=None
    gridpack_mode=False
    cluster_type=None
    cluster_queue=None
    
# MG Particle cuts
    mllcut=60
    mllcutmax=120
# Merging settings
    maxjetflavor=5
    ickkw=0
    nJetMax=1
    ktdurham=30      # 25ns change 30 -> 20
    dparameter=0.2   # 25ns change 0.4 -> 0.1
    #ihtmin=20
    ihtmax=-1
    
### Electrons
    mgproc="""
generate p p > e+ e- j@1
"""
#generate p p > e+ e- @0
#add process p p > e+ e- j @1


        #    mgproc="""
#generate p p > e+ e- @0
#add process p p > e+ e- j @1
#add process p p > e+ e- j j @2
#add process p p > e+ e- j j j @3
#add process p p > e+ e- j j j j @4
#"""
    name='Zee_NpX'
    process="pp>e+e-"
    
    #nevents=nevents_forHT[HTrange]
    #nevents=nevents
    gridpack_mode=False
    gridpack_dir='madevent/'
    
    mode=0
    cluster_type='pbs'
    cluster_queue='medium'
    nJobs=20

    stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)


    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
"""+mgproc+"""
output -f
""")
    fcard.close()


    beamEnergy=-999
    if hasattr(runArgs,'ecmEnergy'):
        beamEnergy = runArgs.ecmEnergy / 2.
    else: 
        raise RuntimeError("No center of mass energy found.")
    
#Fetch default LO run_card.dat and set parameters
    extras = { 'lhe_version'    : '3.0',
               'cut_decays'     : 'F', 
               'pdlabel'        : "'lhapdf'",
               'lhaid'          : 260000,  
               'maxjetflavor'   : maxjetflavor,
               'asrwgtflavor'   : maxjetflavor,
               'ickkw'          : 0,
               'ptj'            : jetPtMin,      
               'ptjmax'         : jetPtMax,      
               'ptb'            : 0.,
               'ptl'            : 20,
#               'ptl'            : jetPtMin,
               'ptlmax'         : -1,
#               'ptlmax'         : jetPtMax,
               'mmll'           : mllcut,
               'mmllmax'        : mllcutmax,
#               'mmll'           : 40.,
#               'mmllmax'        : -1,
               'ihtmin'         : 0.,
               'ihtmax'         : -1,
               #'ihtmin'         : jetPtMin,
               #'ihtmax'         : jetPtMax,
               'mmjj'           : 0.,
               'drjj'           : 0.,
               'drll'           : 0.,
               'drjl'           : 0.,       
               'etal'           : 2.8,
               'etab'           : 6,
               'etaj'           : 6,
               'ktdurham'       : ktdurham,    
               'dparameter'     : dparameter
               }
##########################################################################################
    process_dir = new_process(grid_pack=gridpack_dir)
    build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat', 
                   nevts=nevents*5,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0.,
                   extras=extras)

    print_cards()
    generate(run_card_loc='run_card.dat',param_card_loc=None,mode=mode,njobs=nJobs,
             proc_dir=process_dir, grid_pack=gridpack_mode,gridpack_dir=gridpack_dir,
             cluster_type=cluster_type,cluster_queue=cluster_queue,
             nevents=nevents,random_seed=runArgs.randomSeed)
    
    arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',run_name=str(nJobs))
    #### Shower
    
    #evgenConfig.generators += ["MadGraph", "Pythia8"]
    evgenConfig.description = 'MadGraph_'+str(name)
    evgenConfig.keywords   += ['Z','electron','jets','drellYan']
    evgenConfig.inputfilecheck = stringy
    evgenConfig.minevents = nevents
    runArgs.inputGeneratorFile = stringy+'._00001.events.tar.gz'

    if hasattr(testSeq, "TestHepMC"):
        testSeq.remove(TestHepMC())

    include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")    
    include("MC15JobOptions/Pythia8_MadGraph.py")
    
    PYTHIA8_nJetMax=nJetMax
    PYTHIA8_TMS=ktdurham
    PYTHIA8_Dparameter=dparameter
    PYTHIA8_Process=process 
    PYTHIA8_nQuarksMerge=maxjetflavor
    include("MC15JobOptions/Pythia8_CKKWL_kTMerge.py")   

    genSeq.Pythia8.Commands += ["Merging:unorderedASscalePrescrip = 0"]
    testSeq.TestHepMC.MaxNonG4Energy=14000000.

    #genSeq.Pythia8.Commands += ["HadronLevel:all = off"]
    #genSeq.Pythia8.Commands += ["PartonLevel:MPI = off"]
    if TYPE=='AH':
        pass
    elif TYPE=='NH':
        genSeq.Pythia8.Commands += [
            "PartonLevel:MPI = off",
            #"HadronLevel:all = on"
            ]
    elif TYPE=='AS':
        genSeq.Pythia8.Commands += [
            #"PartonLevel:MPI = on",
            "HadronLevel:all = off",
            "Check:event = off"
            ]
    elif TYPE=='NN':
        genSeq.Pythia8.Commands += [
            "PartonLevel:MPI = off",
            "HadronLevel:all = off",
            "Check:event = off"
            ]
    elif TYPE=='NS':
        genSeq.Pythia8.Commands += [
            "PartonLevel:MPI = off",
            "HadronLevel:all = off",
            "BeamRemnants:primordialKT = off",
            "Check:event = off"
            ]
    else:
        print 'ERROR: Wrong type ID!'
        exit()



if GEN=='Herwigpp':
   evgenConfig.description = "Herwig++ Zee+jet sample with CTEQ6L1 PDF and UE-EE5 tune"
   evgenConfig.keywords = ["electron", "Z","jets"]
   evgenConfig.contact = ["Aliaksei Hrynevich"]

   #include("MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")
   from Herwigpp_i.Herwigpp_iConf import Herwigpp
   genSeq += Herwigpp()
   evgenConfig.generators += ["Herwigpp"]
   
   genSeq.Herwigpp.Commands += ["set /Herwig/Model:EW/Sin2ThetaW 0.23113"]

   from Herwigpp_i import config as hw
   cmds = hw.energy_cmds(int(runArgs.ecmEnergy)) + hw.base_cmds() \
       + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
       + hw.ue_tune_cmds("UE-EE-5-CTEQ6L1")

   # We'll turn UE/HAD off as indicated in tune
   if TYPE=='AH':
      pass
   elif TYPE=='AS':
      cmds += """
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   elif TYPE=='NH':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
"""
   elif TYPE=='NS':
      cmds += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
   else:
      print 'ERROR: Wrong type ID!'
      exit()

   cmds += """
#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2gZ2ff
#set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Electron
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEZJet 5
#set /Herwig/Cuts/LeptonKtCut:MinKT 200*GeV
#set /Herwig/Cuts/EECuts:MHatMin 400*GeV
#set /Herwig/Cuts/QCDCuts:ScaleMin 400000*GeV
#set /Herwig/Cuts/JetKtCut:MinKT 20*GeV
set /Herwig/Cuts/JetKtCut:MinKT %d*GeV
set /Herwig/Cuts/JetKtCut:MaxKT %d*GeV
""" % ( jetPtMin,jetPtMax )


   genSeq.Herwigpp.Commands += cmds.splitlines()
   del cmds
   
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())
   
   evgenConfig.minevents = runArgs.maxEvents




######################################################
#### Pythia8
######################################################

if GEN=='Pythia8':
   evgenConfig.description = "Pythia8 Zee+jet sample with NNPDF23LO PDF and A14 tune"
   evgenConfig.keywords = ["electron","Z","jets"]
   evgenConfig.contact  = ["Aliaksei Hrynevich"]
   
   #!!!!# START include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
   #!!!!## START include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")
   #!!!!### START include("MC15JobOptions/Pythia8_Base_Fragment.py")
   from Pythia8_i.Pythia8_iConf import Pythia8_i
   genSeq += Pythia8_i("Pythia8")
   evgenConfig.generators += ["Pythia8"]
   
   genSeq.Pythia8.Commands += [
      "Main:timesAllowErrors = 500",
      "6:m0 = 172.5",
      "23:m0 = 91.1876",
      "23:mWidth = 2.4952",
      "24:m0 = 80.399",
      "24:mWidth = 2.085",
      "StandardModel:sin2thetaW = 0.23113",
      "StandardModel:sin2thetaWbar = 0.23146",
      "ParticleDecays:limitTau0 = on",
      "ParticleDecays:tau0Max = 10.0"]
   #!!!!### END include("MC15JobOptions/Pythia8_Base_Fragment.py")  
   genSeq.Pythia8.Commands += [
      "Tune:ee = 7", 
      "Tune:pp = 14",
      "PDF:useLHAPDF = on",
      "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
      "SpaceShower:rapidityOrder = on",
      "SigmaProcess:alphaSvalue = 0.140",
      "SpaceShower:pT0Ref = 1.56",
      "SpaceShower:pTmaxFudge = 0.91",
      "SpaceShower:pTdampFudge = 1.05",
      "SpaceShower:alphaSvalue = 0.127",
      "TimeShower:alphaSvalue = 0.127",
      "BeamRemnants:primordialKThard = 1.88",
      "MultipartonInteractions:pT0Ref = 2.09",
      "MultipartonInteractions:alphaSvalue = 0.126",
      "BeamRemnants:reconnectRange  = 1.71"]   
   evgenConfig.tune = "A14 NNPDF23LO"
   #!!!!## END include("MC15JobOptions/nonStandard/Pythia8_A14_NNPDF23LO_Common.py")   
   #!!!!## START include("MC15JobOptions/Pythia8_EvtGen.py")
   assert hasattr(genSeq, "Pythia8")
   #!!!!### START include("MC15JobOptions/EvtGen_Fragment.py")
   evgenConfig.generators += ["EvtGen"]
   evgenConfig.auxfiles += ['2014Inclusive.dec']
   
   from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
   genSeq += EvtInclusiveDecay()
   genSeq.EvtInclusiveDecay.OutputLevel = INFO
   genSeq.EvtInclusiveDecay.decayFile = "2014Inclusive.dec"
   genSeq.EvtInclusiveDecay.allowAllKnownDecays=False
   genSeq.EvtInclusiveDecay.whiteList+=[-411, -421, -10411, -10421, -413, -423,
                                         -10413, -10423, -20413, -20423, -415, -425, -431, -10431, -433, -10433, -20433,
                                         -435, -511, -521, -10511, -10521, -513, -523, -10513, -10523, -20513, -20523,
                                         -515, -525, -531, -10531, -533, -10533, -20533, -535, -541, -10541, -543,
                                         -10543, -20543, -545, -441, -10441, -100441, -443, -10443, -20443, -100443,
                                         -30443, -9000443, -9010443, -9020443, -445, -100445, -551, -10551, -100551,
                                         -110551, -200551, -210551, -553, -10553, -20553, -30553, -100553, -110553,
                                         -120553, -130553, -200553, -210553, -220553, -300553, -9000553, -9010553, -555,
                                         -10555, -20555, -100555, -110555, -120555, -200555, -557, -100557, -4122, -4222,
                                         -4212, -4112, -4224, -4214, -4114, -4232, -4132, -4322, -4312, -4324, -4314,
                                         -4332, -4334, -4412, -4422, -4414, -4424, -4432, -4434, -4444, -5122, -5112,
                                         -5212, -5222, -5114, -5214, -5224, -5132, -5232, -5312, -5322, -5314, -5324,
                                         -5332, -5142, -5242, -5412, -5422, -5414, -5424, -5342, -5432, -5434, -5442,
                                         -5444, -5512, -5522, -5514, -5524, -5532, -5534, -5542, -5544, -5554, -204126,
                                         -104312, -104322, -105122, -105312, -105322, -104124, -104314, -104324, 411,
                                         421, 10411, 10421, 413, 423, 10413, 10423, 20413, 20423, 415, 425, 431, 10431,
                                         433, 10433, 20433, 435, 511, 521, 10511, 10521, 513, 523, 10513, 10523, 20513,
                                         20523, 515, 525, 531, 10531, 533, 10533, 20533, 535, 541, 10541, 543, 10543,
                                         20543, 545, 441, 10441, 100441, 443, 10443, 20443, 100443, 30443, 9000443,
                                         9010443, 9020443, 445, 100445, 551, 10551, 100551, 110551, 200551, 210551, 553,
                                         10553, 20553, 30553, 100553, 110553, 120553, 130553, 200553, 210553, 220553,
                                         300553, 9000553, 9010553, 555, 10555, 20555, 100555, 110555, 120555, 200555,
                                         557, 100557, 4122, 4222, 4212, 4112, 4224, 4214, 4114, 4232, 4132, 4322, 4312,
                                         4324, 4314, 4332, 4334, 4412, 4422, 4414, 4424, 4432, 4434, 4444, 5122, 5112,
                                         5212, 5222, 5114, 5214, 5224, 5132, 5232, 5312, 5322, 5314, 5324, 5332, 5142,
                                         5242, 5412, 5422, 5414, 5424, 5342, 5432, 5434, 5442, 5444, 5512, 5522, 5514,
                                         5524, 5532, 5534, 5542, 5544, 5554, 204126, 104312, 104322, 105122, 105312,
                                         105322, 104124, 104314, 104324 ]
   #!!!!### END include("MC15JobOptions/EvtGen_Fragment.py")   
   evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
   genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
   genSeq.EvtInclusiveDecay.whiteList+=[-5334, 5334]
   #!!!!## END include("MC15JobOptions/Pythia8_EvtGen.py")
   #!!!!# END include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")


   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   if TYPE=='AH':
      pass
   elif TYPE=='NH':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         ]
   elif TYPE=='AS':
      genSeq.Pythia8.Commands += [
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NN':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NS':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "BeamRemnants:primordialKT = off",
         "Check:event = off"
         ]
   else:
      print 'ERROR: Wrong type ID!'
      exit()
   genSeq.Pythia8.Commands += [
      "WeakBosonAndParton:qqbar2gmZg = on",   # q qbar -> gamma^*/Z^0 g
      "WeakBosonAndParton:qg2gmZq    = on",   # q g -> gamma^*/Z^0 q
      "23:onMode = off",                      # switch off Z decays
      "23:onIfMatch = 11 -11",                # switch on Z->ee
      "23:mMin = 60.0",                       # Z Mass Min
      "23:mMax = 120.0",                      # Z Mass Max
      "PhaseSpace:pTHatMin = %d" % jetPtMin,  # The minimum invariant pT
      "PhaseSpace:pTHatMax = %d" % jetPtMax,  # The minimum invariant pT
      ]
   genSeq.Pythia8.CollisionEnergy = float(runArgs.ecmEnergy)

   #include("MC15JobOptions/LeptonFilter.py")
   evgenConfig.minevents = runArgs.maxEvents



ANALYSES = ['ZJETS']

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
rivet.AnalysisPath = os.getenv("PWD")
if GEN=='Sherpa':
    rivet.CrossSection = 987654321 ## Set cross section manually, since Rivet can not catch 
                                   ## Sherpa cross section on fly. 
rivet.OutputLevel = INFO
rivet.Analyses = ANALYSES
genSeq += rivet
