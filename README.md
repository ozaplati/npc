# Introduction notes
 - Ideology of original Aliaksei's code for Non-perturbative corrections is described in `FromAliaksei/README.md`
 - All the Aliaksei's code is moved to `FromAliaksei` directory
 - The main directory includes only updated code considering:
   - `asf` --> `cvmfs` migration 
   - `19.2.4.12.1,MCProd,64` --> `AthGeneration 21.6.9X`
   - `Rivet v2.2` --> `Rivet v2.3` (includes `lhapdf v6`)
   - `Pythia v8.1` --> `Pythia v8.3`,  (`v8.2` resp. it depends on version of `AthGeneration`)
   - `Herwigpp` --> `Herwig7 (v7.2)`
   - JO, Rivet routine, running, drawing, ...
 - Some useful notes about Ota's updates and changes can be found in `NOTES` in the main directory

# Get and Run the code
```
mkdir Fork
cd Fork

git clone https://gitlab.cern.ch/ozaplati/npc.git
cd ncp
```
## Before First Setup
 - Centos7 is not supported - typically after `asetup XX.X.XX,AthGeneration` you will see `!!!ERROR!!! No matched release is found` error msg - you probably need to run the code in ATLAS container `setupATLAS -c centos7 <ARGS>`
 - The container is required for Compilation, Local tests, and Grid submition (potencialy for merging and scaling step via `merge_python3.py`)
 - Update you `~/.bashrc` to define alias `setupATLAS -c centos7 <ARGS>` (just modify the `--pwd` and `--mount` paths):
```
alias setupATLAS_NPC="setupATLAS -c centos7 --batch  --mount=/mnt/nfs19/zaplatilek/ --pwd=/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/Fork/npc "
```
- Re-run your `~/.bashrc`
```
source ~/.bashrc
```
- Note, ATLAS container breaks x-windows -> graphics windows are not supported in ATLAS container -> use visual studio with ssh protocol, or use more terminal windows for code running/code editation/`TBrowser`...
- Create new file `~/.bash_profile_container` to define an alias path variable `NON_PERTURBATIVE_CORRECTIONS`, this valiable is used few `.sh` scripts (just update it to your path)
```
export NON_PERTURBATIVE_CORRECTIONS='/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/'

```

## Setup
### Original setup
- This Original setup probably do not work to you since Centos7 is not supported anymore
```
cd $NON_PERTURBATIVE_CORRECTIONS
cd Fork/npc
source setup_ota.sh 
```
### Setup in ATLAS Container
```
setupATLAS_NPC
source setup_ota_in_container.sh
```

## Compile rivet rutine
```
source compile_ota.sh
```

## Run local test
- local test for Pythia8, output file can be found in `Test_Py8` directory
```
source run_local_Py8.sh
```

- local test for Herwig7, output file can be found in `Test_H7` directory
```
source run_local_H7.sh
```
- local test for Herwig7 in rel.23 , output file can be found in `Test_H7_rel23` directory
```
source run_local_H7_rel23.sh
```
- note, some versions of AthGeneration have a problem with Rivet routines
- current version (rel. 21) of AthGeneration is on blacklist for MC Pythia8 production, but local tests run smoothly (even for Rivet) despite of other versions of AthGeneration

## Run HTCondor
- HTCondor was used for rel. 19 and rel. 21 production
- Do not use HTCondor in rel.23 anymore - code was not optimized/tested in rel.23 -> You should rather use grid resources in rel. 23 as described in next section.

 Until now, only Pythia8 and Herwig7 were used for HTCondor production
 
 - have a look on `myjob.submit`
 - have a look on `HTCrunBatch.sh`
 - have a look on `run_condor.sh`
   - the script submits the jobs to HTCondor system
   - the script make a copy of `myjob.submit`, it modifies the `GEN`, `TUNE` and the `EFFECT`
   - update setup in `run_condor.sh` if needed

### Common setup for `run_condor.sh` 
 - `WORKING_DIR` as output directory with your results
 - `GEN` as generator to be considered 
```
GEN="Pythia8"                                                      # for MC Pythia8
GEN="Herwig7"                                                      # for MC Herwig7
```
 - `arr_TUNES` as an array of all `TUNE`s to be considered
```
arr_TUNES=( "AU2CT10" "AU2CTEQ" "ATLASA14NNPDF" "ATLASA14CTEQL1" ) # for Pythia8
arr_TUNES=( "DEFAULT" )                                            # for Herwig7
```
  - update `arr_EFFECTS` as an array of all `EFFECT`s to be considered
```
arr_EFFECTS=( "PartonLVL" "ParticleLVL" "UEOnly" "HadOnly" )       # for all possible EFFECTs
arr_EFFECTS=( "PartonLVL" "ParticleLVL" )                          # for non-perturbative corrections only (PartonLVL/ParticleLVL)
```

### Submit HTCondor jobs   
```
voms-proxy-init -voms atlas
source run_condor.sh
```

## Submit to Grid
- Please read `GRID/README.md` first

```
cd GRID
```
  - read GRID/README first and follow the instructions to update submition script
  - run the grid submition script as follows

### Original submit
 - open new terminal
```
voms-proxy-init -voms atlas --vomslife 96:00 --valid 96:00

setupATLAS
lsetup panda
cd $NON_PERTURBATIVE_CORRECTIONS
cd Fork/npc/GRID

# submit the jobs in screen section
screen -S Grid_Submit_NPC
source prun_rel23_withROOT.sh 
#source prun_aliaksei_test.sh

# close the screen
ctrl + A + D
```
-  Monitor your jobs at panda web page
```
https://bigpanda.cern.ch/user/
```

### In ATLAS Container
 - open new terminal
```
voms-proxy-init -voms atlas --vomslife 96:00 --valid 96:00
setupATLAS_NPC
lsetup panda
cd GRID

# submit the jobs in screen section
screen -S Grid_Submit_NPC
source prun_rel23_withROOT.sh 
#source prun_aliaksei_test.sh

# close the screen
ctrl + A + D
```
-  Monitor your jobs at panda web page
```
https://bigpanda.cern.ch/user/
```

#### Download yoda containers from rucio
```
cd GRID/DIDS
```
  - create `.txt` file of you yoda DIDS as save it in the GRID/DIDS directory
    - use one DID per line
    - you can use `*` symbol
  - check `rucio_download.sh` script 
    - setup `INFILE_DIDS` variable with your `.txt` file
  - setup proxy with `vomslife` and `valid` flags
```
  voms-proxy-init -voms atlas --vomslife 96:0 --valid 96:0
```
  - run the script - the script creates N screen section, 1 section for one line in your `.txt` file to download the DIDS
  
```
  source rucio_download.sh
```

#### Convert yoda files to root
 - this step not needed for rel 23 with `src/ATLAS_2023_IJXS_IDXS_Rivet_3_1_8_ROOT.cc` which produce RivetRootFile.root directly
 - if you remain with older versions of the code then convert yoda files using one of following scriptes:
   - `merge_python2.py` implemended in Python2 which is compatible with rel19 and rel21 setup
   - `merge_python3.py` implemended in Python3 which is compatible with rel23 setup
```
source setup_ota.sh
python<X> merge_python<X>.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/Herwigpp_PartonLVL_UEEE5CTEQ6L1_20221105__rel19  --jSampleMin 4 --jSampleMax 4 --convertToRoot True --mergeSeeds False  --scale False  --mergeScaled False 2>&1 | tee log.JSample4
```

  - it is better to run in screen section, it take few hours
    - you can use `merge_screen_script.sh` script
    - need to check the flags
```
screen -S YodaToRoot_JSAMPLE_4
merge_screen_script.sh 4  /mnt/nfs19/zaplatilek/IJXS/Rivet/Herwigpp_PartonLVL_UEEE5CTEQ6L1_20221105__rel19 
```
  - do the same for all your JSAMPLE at Parton and Particle level 
  - then it's done merge all the files
```
python<X> merge_python<X>.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/Herwigpp_PartonLVL_UEEE5CTEQ6L1_20221105__rel19  --jSampleMin 2 --jSampleMax 9 --convertToRoot False --mergeSeeds False  --scale False  --mergeScaled True
```


#### Combine and merge the rootFiles
  - convert JSAMPLE individualy
  - use `merge_python<X>.py` script with defined flags e.q.:
```
source setup_ota.sh
#python<X> merge_python<X>.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/Herwigpp_PartonLVL_UEEE5CTEQ6L1_20221105__rel19  --jSampleMin 4 --jSampleMax 4 --convertToRoot False --mergeSeeds True  --scale True  --scaleWithFilterEfficiency True --scaleWithNFilesFromAllSteps True --mergeScaled False 2>&1 | tee log.JSample4
python3 merge_python3.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7 --inputSubDir RUCIO_XXXSteps  --jSampleMin 1 --jSampleMax 9 --convertToRoot False --mergeSeeds True  --scale True  --scaleWithFilterEfficiency True --scaleWithNFilesFromAllSteps True --mergeScaled False 2>&1 | tee log.v7_reprocessing_SoftTune_Patilecle_merge
```

#### Check cross-section before merging the rootFiles
  - additional cross-check for large production, some Herwigpp files suffer from large flustuations, one more idea is exclude particlular rootFiles if it's cross-section is too far from mean value of all files in given directory
  - for this cross-check can be applied `RootMacro/Check_XS.cxx` code comparison cross-section in `i`th file as follows:
  - `abs(mean_crossSection - crossSection_i) < ARG_2 * sigma`  what is equivalent to `abs(mean_crossSection - crossSection_i)/sigma  < ARG_2`, where
    - `crossSection_i` is a cross-section in `i`th rootFile`
    - `mean_crossSection` is mean cross-section of all rootFiles in given sub-directory
    - `sigma` is a sigma associated to the `mean_crossSection` as `sigma = sqrt(sum_of_squares / n - mean_crossSection * mean_crossSection)` 
  - the code have several possible arguments
  - `<ARG_1>` ... PATH_TO_RUCIO directory - the cross-check will be done for each sub-directory (typically different directory for each slice, for each step of grid production) which ends with following string `"Rivet.root"` (in case rel19 production) or `RivetRootFile.root"` (in case rel23 production)
  - `<ARG_2>` ... int number, usually `3` or `5`
 ```
cd RootMacro
make Check_XS
./Check_XS </mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_PartonLVL_Default_rel23_withROOT_v5/RUCIO> 3 
```
  
# Work with Rivet
## Standard Rivet commands
### Merge files
```
rivet-merge -o OUTPUT_FILE.yoda.gz INPUT_FILE_1.yoda.gz INPUT_FILE_2.yoda.gz INPUT_FILE_3.yoda.gz
rivet-merge -o OUTPUT_FILE.yoda.gz INPUTS_*.yoda.gz
```
### Make html page with plots
```
rivet-mkhtml FILE.yoda.gz 
rivet-mkhtml --errs -o my_plots prediction1.yoda.gz:"Title=MC 1" prediction2.yoda.gz:"Title=MC 2"
```
### Convert yoda to root
```
python yoda2root.py --help
python yoda2root.py --inFile INPUT_FILE.yoda.gz
```
## Scripts for rivet
 you can use `yoda2root_loop.py` script:
  - to merge yoda files from HTCondor
  - to convert merged yoda files
  - to mkhtml of merged yoda files
```
  python yoda2root_loop.py
```
 then you can evaluate non-perturbative corrections as a ratio `PartonLVL/ParticleLVL` using histograms of 1D diff. cross-section, which is stored in the merged root-files
 
# Ploting
 check drawing script `RootMacro/draw.C`
  - setup input paths and `HTCondor_Run` variable
```
 TString HTCondor_Run = "HTCondor_Run_2";
```
  - compile the code
```
cd RootMacro
source setup_root.sh
source compile.sh
```
- run the code
```
./draw
```

# Work with original rel. 19
original code of Aliaksei is based on: `19.2.4.12.1,MCProd,64`

## setup
```
source setup_aliaksei.sh
```

## compile
```
source compile_aliaksei.sh
```

## local run 
- local test for `Pythia v8.210`
```
source run_local_Py8_aliaksei.sh
```
- local test for `Herwig++  v2.7.1`
- Herwig++ does not work, some paths point to `asf`, which do not exist - can be fixed?
```
source run_local_Herwigpp_aliaksei.sh
# ...
# ...
# ...
# when you comment all tunes and special set herwig commands for npc, then error:
# ERROR Could not open file '/afs/cern.ch/sw/lcg/external/MCGenerators_lcgcmt67c/herwig++/2.7.1/x86_64-slc6-gcc47-opt/share/Herwig++/PDF/diffraction/2006/h12006jetspdf_singlet_fitA.data' in PomeronPDF::loadTables()
#
# when you keep tunes and comment only special herwig commands for npc, then LHCHandler error:
# ERROR Exception in ThePEG: The event handler 'LHCHandler' cannot be initialized because there are no selected subprocesses
``` 
