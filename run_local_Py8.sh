DIR_RUN=Test_Py8
JOB_OPTION=mc.Py8_ATLASA14NNPDF.py

# Create working directory
rm -r    ${DIR_RUN}
mkdir -p ${DIR_RUN}

# Copy All needed file
cp *.so          ${DIR_RUN}/.
cp ${JOB_OPTION} ${DIR_RUN}/.

# cd to working directory
cd ${DIR_RUN}

# setup Rivet
source setupRivet.sh
export RIVET_ANALYSIS_PATH=$PWD

# Run Gen_tf.py
Gen_tf.py \
--ecmEnergy=13000 \
--firstEvent=1 \
--randomSeed=123456789 \
--jobConfig=$PWD \
--runNumber=100000 \
--outputEVNTFile=pool.root \
--maxEvents=100 \
--env NPC_JSAMPLE=9 NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL
