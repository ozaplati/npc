import os,binascii
########################################################################
# New updated and more clean JobOption for Herwig7 (7.2.2)
# Original Aliaksei's JobOption:
#        FromAliaksei/jobOptions/GenerateJetsJO_PY8_EIG.py
#    includes old Herwig++ (Herwigpp)
#    
print "----------------------------------------------------------------"
print "JobOption - Begin"
print "----------------------------------------------------------------"

########################################################################
# Load NPC_TUNE variable
#    For now - the is only DEFAULT tune for Herwig7
#    Athena try protect changes of the tunes and pgf in Herwig7
#    Interface exists, but non-DEFAULT values cause to exceptions
#        https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L371
#        https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L341

print "----------------------------------------------------------------"
print "JobOption - Tune"
print "----------------------------------------------------------------"

TUNE=""
try:
   TUNE=str(os.environ['NPC_TUNE'])
except KeyError:
   print "WARNING: using default tune"
   TUNE="DEFAULT"
   print "ERROR: Wrong tune set in environment"
print 'TUNE =', TUNE

########################################################################
# Load NPC_EFFECT variable

print "----------------------------------------------------------------"
print "JobOption - EFFECT"
print "----------------------------------------------------------------"
print "HERE"
EFFECT=""
try:
  EFFECT=str(os.environ['NPC_EFFECT'])
except KeyError:
  print "WARNING: using default effect"
  EFFECT="PartonLVL"
  print "ERROR: Wrong effect set in environment"
print 'EFFECT =', EFFECT

########################################################################
# Load ENERGY variable 
#    centre-of-mass energy in GeV
print "----------------------------------------------------------------"
print "JobOption - Energy"
print "----------------------------------------------------------------"

ecmEnergy=0
try:
   ecmEnergy=runArgs.ecmEnergy
except KeyError:
   print "WARNING: using default ecmEnergy"
   ecmEnergy=13000

print 'ecmEnergy =',ecmEnergy

########################################################################
# Setup ranges for jetPtMin and jetPtMax
#     as JZX slices
#
print "----------------------------------------------------------------"
print "JobOption - jetPtMin, jetPtMax ranges"
print "----------------------------------------------------------------"

JSAMPLE=0
try:
   JSAMPLE=int(os.environ['NPC_JSAMPLE'])
except KeyError:
   print "WARNING: using default JSAMPLE"
   JSAMPLE=0
   print "ERROR: Wrong NPC_JSAMPLE in environment"
print 'JSAMPLE =', JSAMPLE

jetPtMin=25
jetPtMax=13000

#
#if runArgs.runNumber==100001:
#   jetPtMin=17
#   jetPtMax=25
#elif runArgs.runNumber==100002:
#   jetPtMin=25
#   jetPtMax=70
#elif runArgs.runNumber==100003:
#   jetPtMin=70
#   jetPtMax=140
#elif runArgs.runNumber==100004:
#   jetPtMin=140
#   jetPtMax=280
#elif runArgs.runNumber==100005:
#   jetPtMin=280
#   jetPtMax=560
#elif runArgs.runNumber==100006:
#   jetPtMin=560
#   jetPtMax=1120
#elif runArgs.runNumber==100007:
#   jetPtMin=1120
#   jetPtMax=2240
#elif runArgs.runNumber==100008:
#   jetPtMin=2240
#   jetPtMax=3000
#elif runArgs.runNumber==100009:
#   jetPtMin=3000
#   jetPtMax=5500
#else:
#   jetPtMin=17
#   jetPtMax=14000


print "JSAMPLE - BEFORE SET pt Ranges : %i" % (JSAMPLE)
if JSAMPLE==0:
  jetPtMin=25
  jetPtMax=13000
elif JSAMPLE==1:
  jetPtMin=17
  jetPtMax=25
elif JSAMPLE==2:
  jetPtMin=25
  jetPtMax=70
elif JSAMPLE==3:
  jetPtMin=70
  jetPtMax=140
elif JSAMPLE==4:
  jetPtMin=140
  jetPtMax=280
elif JSAMPLE==5:
  jetPtMin=280
  jetPtMax=560
elif JSAMPLE==6:
  jetPtMin=560
  jetPtMax=1120
elif JSAMPLE==7:
  jetPtMin=1120
  jetPtMax=2240
elif JSAMPLE==8:
  jetPtMin=2240
  jetPtMax=3000
elif JSAMPLE==9:
  jetPtMin=3000
  jetPtMax=5500
  
print "JSAMPLE - AFTER SET pt Ranges : %i" % (JSAMPLE)
print "Using jetPtMin=%d, jetPtMax=%d" % (jetPtMin,jetPtMax)

########################################################################
# Check EFFECT
 
print "----------------------------------------------------------------"
print "JobOption - Check EFFECT"
print "----------------------------------------------------------------"

l_EFFECTS = ["PartonLVL", "ParticleLVL", "UEOnly", "HadOnly"]
if EFFECT not in l_EFFECTS:
  print 'ERROR: Invalid EFFECT of: ' + EFFECT
  print 'valid EFFECT are: '
  print l_EFFECTS
  exit()

print "----------------------------------------------------------------"
print "JobOption - Herwig7"
print "----------------------------------------------------------------"

## Initialise Herwig7 for run with built-in matrix elements
## Taken from
##    https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/share/common/Herwig7_BuiltinME.py
##    also you can include the setup directly as
##        include("Herwig7_i/Herwig7_BuiltinME.py")

from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

genSeq += Herwig7()
Herwig7Config = Hw7ConfigBuiltinME(genSeq, runArgs)


## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "Herwig7 dijet LO"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "ota.zaplatilek@cern.ch" ]
evgenConfig.nEventsPerJob = 1

# Aliaksei applied 2010 PDF my hand
# However, this is implementedy by default
#     https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L168
#     https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L130
#
# Followinf commands not needed
#command = """
#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/Particles/t:NominalMass 172.5*GeV
#set /Herwig/Particles/tbar:NominalMass 172.5*GeV
#set /Herwig/Particles/W+:NominalMass 80.399*GeV
#set /Herwig/Particles/W-:NominalMass 80.399*GeV
#set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
#set /Herwig/Particles/W+:Width 2.085*GeV
#set /Herwig/Particles/W-:Width 2.085*GeV
#set /Herwig/Particles/Z0:Width 2.4952*GeV
#set /Herwig/Model:EW/Sin2ThetaW 0.23113                                                                                                          
#""" 

# ERROR for insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#       generate 11:22:10 Error: There was no object named '/Herwig/MatrixElements/SimpleQCD' in the repository.
#       
#       Solution: 
#           insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
# 
# ERROR for
#      set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#      generate 11:27:11 Error: There was no object named '/Herwig/EventHandlers/LHCHandler' in the repository.
#      
#      Other examples from Athena do not work as well:
#          set /Herwig/EventHandlers/LHEHandler:HadronizationHandler NULL
#          set /Herwig/EventHandlers/FxFxLHEHandler:HadronizationHandler NULL
#       
#       Solution:
#          cd /Herwig/EventHandlers
#          set EventHandler:HadronizationHandler NULL

if hasattr(testSeq, "TestHepMC"):
  testSeq.remove(TestHepMC())

print "----------------------------------------------------------------"
print "JobOption - Herwig7 - Prosess"
print "----------------------------------------------------------------"

# updated process as mentiond above
command = """
# Setup process
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
"""

# not sure about this command - but athena screems without it
command +="""
set /Herwig/Partons/RemnantDecayer:AllowTop Yes
"""
print "----------------------------------------------------------------"
print "JobOption - Herwig7 - apply EFFECT"
print "----------------------------------------------------------------"

########################################################################
# Apply EFFECT

if EFFECT=='ParticleLVL':
  # ParticleLVL means UE+Had
  #
  # Old sytax: 
  #     TYPE == 'AH'
  pass
elif EFFECT=='PartonLVL':
  # Old syntax:
  #     TYPE == 'AS'
  command += """
# Switch Off MPI
set /Herwig/Shower/ShowerHandler:MPIHandler NULL

# Switch Off Hadronization
cd /Herwig/EventHandlers
set EventHandler:HadronizationHandler NULL

set /Herwig/Analysis/Basics:CheckQuark No
"""
elif EFFECT=='UEOnly':
  # Old syntax:
  #    TYPE=='AS'
  command += """
# Switch Off Hadronization
cd /Herwig/EventHandlers
set EventHandler:HadronizationHandler NULL

set /Herwig/Analysis/Basics:CheckQuark No
"""
elif EFFECT=='HadOnly':
  # Old syntax:
  #  TYPE=='NH'
  command += """
# Switch Off MPI
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
"""
else:
  print 'Error: Wrong type ID!'
  exit()

########################################################################
# Setup jetPtMin and jetPtMax
command += """
set /Herwig/Cuts/JetKtCut:MinKT %d*GeV                               
set /Herwig/Cuts/JetKtCut:MaxKT %d*GeV                                        
""" % (jetPtMin,jetPtMax)
         
########################################################################
# Setup tunes 
#    - there is only DEFAULT tune for Herwig7
#    - alternative tunes for Herwig7 are not available in Athana

print "----------------------------------------------------------------"
print "JobOption - Herwig7 - apply TUNE"
print "----------------------------------------------------------------"

if TUNE == "DEFAULT":
  ## Tunes Herwig7
  Herwig7Config.me_pdf_commands(order="NLO", name="MMHT2014nlo68cl")                            # this works
  Herwig7Config.shower_pdf_commands(order="NLO", name="MMHT2014nlo68cl")                        # this works
  Herwig7Config.tune_commands(ps_tune_name = "H7-PS-MMHT2014LO", ue_tune_name = "H7.1-Default") # this works
  
  # non-default arguments provide an exception as implemented here:
  # https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L371
  # https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L341

print command

########################################################################
# Set herwig commands
Herwig7Config.add_commands(command)

########################################################################
# Run the generator
print "----------------------------------------------------------------"
print "JobOption - Herwig7 - run"
print "----------------------------------------------------------------"

Herwig7Config.run()


########################################################################
# Rivet
# ATLAS_2018_I1634970           ... oficial rivet routine for 2018 measurement
# ATLAS_2022_IJXS_IDXS          ... my rivet routine - need to compile
#                                   based on ATLAS_2018_I1634970
#                                   https://rivet.hepforge.org/analyses/ATLAS_2018_I1634970.html
#
# ATLAS_2018_IJXS_IDXS_Alieksei ... based on Aliaksei's rivet routine with updated syntax - need to compile
#                                   
print "----------------------------------------------------------------"
print "JobOption - Rivet"
print "----------------------------------------------------------------"

ANALYSES = [ 'ATLAS_2018_IJXS_IDXS_Alieksei', 'ATLAS_2022_IJXS_IDXS', 'ATLAS_2018_I1634970' ]

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()

rivet.AnalysisPath = os.getenv("PWD")  
rivet.OutputLevel = INFO
rivet.Analyses = ANALYSES
genSeq += rivet

print "----------------------------------------------------------------"
print "JobOption - End"
print "----------------------------------------------------------------"

