#!/bin/bash

########################################################################
# START
DATA_TAG=`date +%F`
TIME_TAG=`date +%T`

TIME_BEGIN=`date +%s.%N`
echo "================================================================"
echo "======================== START ================================="
echo "==================="
echo "===================" ${DATA_TAG}
echo "===================" ${TIME_TAG}
echo "===================" ${HOSTNAME}
echo "================================================================"

echo "PWD: " ${PWD}
ls -lrtsa

echo " "
echo " "
echo " "
echo "================================================================"
echo "======================== LOAD ARGS ============================="
echo "================================================================"

#ENG=${1}
ENG=13000
JO=${1}
#RUN=${4}
#EVENTS=${5}
#RUNDIR=${6}
EVENTS=${2}
RUNDIR=${3}
STEP=${4}

# SPECIFIC OF RUNNUMBER NEEDED FOR JOB_OPTION
# ptJetMin and ptJetMax is evaluated for
# 100001 - 100009
let RUN=${NPC_JSAMPLE}+100000

# different SEEDs for each jetPtMin jetPtMax ranges (NPC_JSAMPLE)
# SEEDs can be the same for different MC Generator  (NPC_GEN)
# SEEDs can be the same for different TUNEs         (NPC_GEN)
# SEEDs can be the same for different EFFECTs       (NPC_EFFECT)
let SEED=${NPC_JSAMPLE}*100000+${STEP}

echo "ENG=     "${ENG}
echo "SEED=    "${SEED}
echo "JO=      "${JO}
echo "RUN=     "${RUN}
echo "EVENTS=  "${EVENTS}
echo "JSAMPLE= "${NPC_JSAMPLE}
echo "rundir=  "${RUNDIR}

echo "NPC_JSAMPLE= "${NPC_JSAMPLE}
echo "NPC_TUNE=    "${NPC_TUNE}
echo "NPC_GEN=     "${NPC_GEN}
echo "NPC_EFFECT=  "${NPC_EFFECT}

echo " "
echo " "
echo " "
echo "================================================================"
echo "======================== SETUP ================================="
echo "================================================================"

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# Athena
# based on https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware#Versions_release_21_6_AthGenerat
#if   [ ${NPC_GEN} =  "Pythia8" ]; then
#	asetup 21.6.96,AthGeneration
#elif [ ${NPC_GEN} =  "Herwig7" ]; then
#	asetup 21.6.85,AthGeneration
#else
#	echo "CrunBatch.sh - Invalid value of NPC_GEN=${NPC_GEN} - Setup NPC_GEN=Pythia8 or NPC_GEN=Herwig7"
#	echo "return 1"
#	return 1
#fi
# above versions have a problem with rivet in local tests

if   [[ ${JO} =  "mc."* ]]; then
	# AthGeneration - rel. 21 - Ota's setup
	asetup 21.6.94,AthGeneration
	
	# Rivet
	source setupRivet.sh
	export RIVET_ANALYSIS_PATH=$PWD
	echo $RIVET_ANALYSIS_PATH
	
	echo " "
	echo " "
	echo " "
	echo "================================================================"
	echo "==================== RUN Gen_tf.py ============================="
	echo "================================================================"
	
	JOB_CONFIG_TAG=${PWD}
	echo ""
	
	Gen_tf.py \
		--ecmEnergy=${ENG} \
		--firstEvent=1 \
		--randomSeed=${SEED} \
		--jobConfig=${JOB_CONFIG_TAG} \
		--runNumber=${RUN} \
		--outputEVNTFile=pool.root \
		--maxEvents=${EVENTS} \
		--env NPC_JSAMPLE=${NPC_JSAMPLE} NPC_TUNE=${NPC_TUNE} NPC_GEN=${NPC_GEN} NPC_EFFECT=${NPC_EFFECT}
		#--env NPC_JSAMPLE=1 NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL
		
else
	# MCProd - rel. 19 - Aliaksei's setup
	asetup 19.2.4.12.1,MCProd,64
	
	# Rivet
	# source setupRivet.sh
	export RIVET_ANALYSIS_PATH=$PWD
	echo $RIVET_ANALYSIS_PATH
	
	echo " "
	echo " "
	echo " "
	echo "================================================================"
	echo "================= RUN Generate_tf.py ==========================="
	echo "================================================================"
	echo ""
	
	JOB_CONFIG_TAG=${JO}
	
	Generate_tf.py \
		--ecmEnergy=${ENG} \
		--firstEvent=1 \
		--randomSeed=${SEED} \
		--jobConfig=${JOB_CONFIG_TAG} \
		--runNumber=${RUN} \
		--outputEVNTFile=pool.root \
		--maxEvents=${EVENTS} \
		--env NPC_JSAMPLE=${NPC_JSAMPLE} NPC_TUNE=${NPC_TUNE} NPC_GEN=${NPC_GEN} NPC_EFFECT=${NPC_EFFECT}
		#--env NPC_JSAMPLE=1 NPC_TUNE=ATLASA14NNPDF NPC_GEN=Pythia8 NPC_EFFECT=PartonLVL
		
fi

#echo " "
#echo " "
#echo " "
#echo "================================================================"
#echo "==================== YODA TO ROOT  ============================="
#echo "================================================================"
#
#if [ -f Rivet.yoda ]; then 
#    echo "yoda2root"
#    yoda2root Rivet.yoda
#fi

# if [ -f Rivet.root ]; then
#     echo "Moving output"
#     OUTPUTDIR=${RUNDIR}/Results
#     mkdir -p ${OUTPUTDIR}
#     mv Rivet.root ${OUTPUTDIR}/result_ECM${ECM}_Seed${SEED}_Run${RUN}_Events${EVENTS}.root
# fi


ls -lrtsa
du -sh *

TIME_END=`date +%s.%N`
RUNTIME=$( echo "${TIME_END} - ${TIME_BEGIN}" | bc -l )

echo " "
echo " "
echo " "
echo "================================================================"
echo "=======================     END    ============================="
echo "======================= DATE:     $(date)"
echo "======================= HOST:     ${HOSTNAME}"
echo "======================= RUN TIME: ${RUNTIME} sec"
echo "================================================================"
echo "================================================================"
