#!bin/bash

# Export ATLAS paths
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS

# Athena
#asetup 21.6.96,AthGeneration    # For Pythia8 - https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware#Versions_release_21_6_AthGenerat
#asetup 21.6.85,AthGeneration    # For Herwig7 - https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware#Versions_release_21_6_AthGenerat
#asetup 21.6.94,AthGeneration     # Above version have a problem with rivet, keep this version

# Athena for new Herwig7 - recomodation from PMG - Fall 2023
asetup 23.6.18,AthGeneration

# Rivet for rel. 21
#source setupRivet.sh

# River for rel. 23
source setupRivet

export RIVET_ANALYSIS_PATH=$PWD
