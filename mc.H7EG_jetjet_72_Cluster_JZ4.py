# Switchers for local test
# Switchers using system variables
#     ENV_LEVEL = "ParticleLVL" for particle level
#     ENV_LEVEL = "PartonLVL" for parton     level
#     other ENV_LEVEL values are not allowed
#     ENV_TUNE = "Default"
#     ENV_TUNE = "SoftTune"
#     ENV_TUNE = "BaryonicReconnectionTune"
#     other ENV_TUNE values are not allowed
import sys
ENV_LEVEL = os.getenv('NPC_EFFECT')
ENV_TUNE  = os.getenv('NPC_TUNE')

# Map of uderlying event tunes to test
# Until v6 grid production applied "Default": "H7.2-Default" in evgenConfig.tune variable, even for SoftTune and BaryonicReconnectionTune setup
m_tuneNames = {"Default": "H7.2-Default", "SoftTune": "H7.1-SoftTune", "BaryonicReconnectionTune": "H7.1-BaryonicReconnection"}
tuneName    = m_tuneNames.get(ENV_TUNE)

## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py") #TODO can we use same decay table for H72?

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = tuneName
evgenConfig.description = "Herwig7 dijet LO"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "yoran.yeh@cern.ch" ]
#evgenConfig.nEventsPerJob = 100

## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")

include ("GeneratorFilters/FindJets.py")
# CreateJets(prefiltSeq, jetR, mods="")
# https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Generators/GeneratorFilters/share/common/FindJets.py
CreateJets(prefiltSeq, 0.4 )
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4,filtSeq)


## Parton level
## Implementation from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCTuningRecommendations
command_PartonLevelOnly = """
######################################
## Parton level
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
## Switch off Hadronization - TWiki recomedation with typo - Herwig 7 expert (Yoran Yeh, atlas-generators-herwig7-experts) recommeded this:
set /Herwig/EventHandlers/EventHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""


## Default from original steering in Athena
## Do we want 'set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0' for Particle Level?
## Not sure, Ask expert
command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 150.0*GeV

##  - - -   bias to high pt.
create Herwig::MergingReweight MPreWeight HwDipoleShower.so
insert /Herwig/MatrixElements/SubProcess:Preweights 0  MPreWeight
set MPreWeight:MaxPTPower 4
##

"""




## SoftTune
## Recommended by Josu Cantero Garcia from PMG
## Tested, seems to work localy
command_SoftTune = """
######################################
##SoftTune:
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.5
set /Herwig/UnderlyingEvent/MPIHandler:pTmin0 3.502683
set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.402055
set /Herwig/UnderlyingEvent/MPIHandler:Power 0.416852
set /Herwig/Partons/RemnantDecayer:ladderPower -0.08
set /Herwig/Partons/RemnantDecayer:ladderNorm 0.95
"""

## BaryonicReconnectionTune
## Recommended by Josu Cantero Garcia from PMG
## Tested, seems to work localy
command_BaryonicReconnectionTune = """
######################################
#baryonicReconnection:
# Set strange quark mass to 0.45 in order to allow alternative gluon splitting
set /Herwig/Particles/s:ConstituentMass 0.45*GeV
set /Herwig/Particles/sbar:ConstituentMass 0.45*GeV

# Use Baryonic Colour Reconnection Model
set /Herwig/Hadronization/ColourReconnector:Algorithm Baryonic

# Allow alternative gluon splitting
set /Herwig/Hadronization/PartonSplitter:Split uds

# Parameters for the Baryonic Reconnection Model
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbability 0.772606    
set /Herwig/Hadronization/ColourReconnector:ReconnectionProbabilityBaryonic 0.477612
set /Herwig/UnderlyingEvent/MPIHandler:pTmin0 3.053252    
set /Herwig/UnderlyingEvent/MPIHandler:InvRadius 1.282032
set /Herwig/Hadronization/HadronSelector:PwtSquark 0.291717
set /Herwig/Hadronization/PartonSplitter:SplitPwtSquark 0.824135
"""

do_PartonLevel = False
if ENV_LEVEL == "ParticleLVL":
	print ("Apply ParticleLVL")
	do_PartonLevel = False
elif ENV_LEVEL == "PartonLVL":
	print ("Apply PartonLVL")
	do_PartonLevel = True
else:
	msg = "!!!EXCEPTION!!! \n"
	msg += "    Job option for Herwig 7 - Exit the code - Exception Arised - Wrong value for ENV_LEVEL=" + ENV_LEVEL + "\n"
	msg += "                              Only the following values are allowed: \n"
	msg += "                              ENV_LEVEL=ParticleLVL \n"
	msg += "                              ENV_LEVEL=PartonLVL \n"
	raise SystemExit(msg)


do_SoftTune    = False
do_BaryonicReconnectionTune = False
if  ENV_TUNE == "Default":
	print ("Apply Default tune")
	do_SoftTune    = False
	do_BaryonicReconnectionTune = False
elif ENV_TUNE == "SoftTune":
	print ("Apply SoftTune tune")
	do_SoftTune = True
	do_BaryonicReconnectionTune = False
elif  ENV_TUNE == "BaryonicReconnectionTune":
	print ("Apply BaryonicReconnectionTune tune")
	do_SoftTune = False
	do_BaryonicReconnectionTune = True
else:
	msg = "!!!EXCEPTION!!! \n"
	msg += "    Job option for Herwig 7 - Exit the code - Exception Arised - Wrong value for ENV_TUNE=" + ENV_TUNE + "\n"
	msg += "                              Only the following values are allowed: \n"
	msg += "                              ENV_TUNE=Default \n"
	msg += "                              ENV_TUNE=SoftTune \n"
	msg += "                              ENV_TUNE=BaryonicReconnectionTune \n"
	raise SystemExit(msg)


## Default command for all setup
Herwig7Config.add_commands(command)

## Apply Parton Level
if do_PartonLevel == True:
	Herwig7Config.add_commands(command_PartonLevelOnly)
	
	# Fix problem with 0 % Filter efficiency
	# Suggested by Dominic Hirschbuehl
	if hasattr(testSeq, "TestHepMC"):
		testSeq.remove(TestHepMC())

## H7.2-Default Tune
#if do_SoftTune == False and do_BaryonicReconnectionTune == False:
#	print( "Applied default tune: H7.2-Default" )
	
## Apply Soft Tune
if do_SoftTune == True:
	print( "Applied tune: Soft Tune" )
	Herwig7Config.add_commands(command_SoftTune)

## Apply Reconnection Tune
if do_BaryonicReconnectionTune == True:
	print( "Applied tune: BaryonicReconnection Tune" )
	Herwig7Config.add_commands(command_BaryonicReconnectionTune)

## Run Herwig7 generation
Herwig7Config.run()


########################################################################
## Rivet
##
## run Rivet analysis on the fly using:
## Gen_tf.py <args> \
##   --rivetAnas=ATLAS_2023_IJXS_IDXS_Rivet_3_1_8
##
