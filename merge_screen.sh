#!bin/bash
#NPC_SUBDIR=Herwigpp_ParticleLVL_UEEE5MSTW2008_20221104__rel19
NPC_SUBDIR=Test_20221107
PATH_NPC=/mnt/nfs19/zaplatilek/IJXS/Rivet/${NPC_SUBDIR}

#ARR_JSAMPLES=(2 3 4 5 6 7 8 9)
ARR_JSAMPLES=(6 7)
for JSAMPLE in "${ARR_JSAMPLES[@]}"
do
   : 
   echo "working on JSAMPLE"${JSAMPLE}
   #screen -S ${NPC_SUBDIR} -d -m bash -c "source ${PATH_CODE}/setup_ota.sh; python2 ${PATH_CODE}/merge.py --inputPath ${PATH_NPC}  --jSampleMin ${JSAMPLE} --jSampleMax ${JSAMPLE} --mergeSeeds True  --scale False  --mergeScaled False; sleep 60"
   screen -S ${NPC_SUBDIR}_JSAMPLE${JSAMPLE} -d -m bash -c "source merge_screen_script.sh ${JSAMPLE} ${PATH_NPC} 2>&1 | tee log.${NPC_SUBDIR}_JSAMPLE${JSAMPLE}"
   
done
