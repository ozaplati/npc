import os
import argparse
from ROOT import *
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('true', '1'):
        return True
    elif v.lower() in ('false', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser()
parser.add_argument('--inputPath',      help='Input path for the script, MC yoda files are expected in \"BatchOutput\" directory,  example: --inputPath  HTCondor_Aliaksei_Run_1',      type=str,   default="HTCondor_Aliaksei_Run_1")
parser.add_argument('--jSampleMin',     help='Minimal jSample applied for MC generation, example: --jSampleMin 2',      type=int,   default=2)
parser.add_argument('--jSampleMax',     help='Maximal jSample applied for MC generation, example: --jSampleMax 9',      type=int,   default=9)
parser.add_argument('--mergeSeeds',     help='Applied as the First step  - find yoda, convert yoda files to root files, merge the converted root files ( merging applied only for a given jSample, considering various Seeds), example : --jSampleMax True',      type=str2bool,   default=True)
parser.add_argument('--scale',          help='Applied as the Second step - scale jSample root files with MC weight, example: --scale True',   type=str2bool,   default=True)#type=bool,   default=True)
parser.add_argument('--mergeScaled',    help='Applied as the Third  step - merge scaled jSample root files, example --mergeScaled True',      type=str2bool,   default=True)

parser.add_argument('--convertToRoot',  help='Applied as the Zero  step  - find yoda, convert yoda files to root file', type=str2bool,   default=True)

args=parser.parse_args()

inputPath = args.inputPath

jSampleMin = args.jSampleMin
jSampleMax = args.jSampleMax

b_convertToRoot = args.convertToRoot
b_mergeSeeds  = args.mergeSeeds
b_scale       = args.scale
b_mergeScaled = args.mergeScaled

print "args.inputPath:   %s" %(args.inputPath)
print "args.jSampleMin:  %s" %(args.jSampleMin)
print "args.jSampleMax:  %s" %(args.jSampleMax)
print "args.mergeSeeds:  %s" %(args.mergeSeeds)
print "args.scale:       %s" %(args.scale)
print "args.mergeScaled: %s" %(args.mergeScaled)

l_JSAMPLES_INT = list(range(jSampleMin, jSampleMax + 1))
l_JSAMPLES = [str(jSample) for jSample in l_JSAMPLES_INT]

print l_JSAMPLES
#raw_input("STOP")

##########################################
####
####
#if b_convertToRoot:
if b_mergeSeeds == True or b_convertToRoot == True:
	rootdir = inputPath
	
	# recursive loop over directorie to find BatchOutput directory
	for rootdir, dirs, files in os.walk(rootdir):
		for subdir in dirs:
			path = os.path.join(rootdir, subdir)
			# find "BatchOutput" directory
			is_BatchOutput = False
			is_Rucio       = False
			
			if subdir == "BatchOutput":
				is_BatchOutput = True
			
			if subdir == "RUCIO":
				is_Rucio = True
			
			if is_Rucio == True:
				# Get list of all files and directories in RUCIO directory
				l_dirs_all = os.listdir(path)
				l_dirs_all = [path + "/" + item for item in l_dirs_all]
				
				# Loop over JSAMPLES
				for jSAMPLE in l_JSAMPLES:
					l_rootFiles   = list()
					l_iFileMerged = list()
					
					# Get directory of a give JSamples
					str_jSAMPLE = "JZ" + jSAMPLE
					l_dir_yoda = [ item for item in l_dirs_all if str_jSAMPLE in item and os.path.isdir(item) and ( item.endswith("Rivet.yoda") or item.endswith("Rivet.yoda.gz") )] 
					l_dir_root = [ item for item in l_dirs_all if str_jSAMPLE in item and os.path.isdir(item) and item.endswith("Rivet.root")] 
					l_dir_yoda.sort()
					l_dir_root.sort()
					
					# dictionary for root files
					#  as directory and list of rootFiles
					dic_dir_rootFiles = {}
					if b_convertToRoot == False:
						for dir_root in l_dir_root:
							l_rootFiles = os.listdir(dir_root)
							l_rootFiles = [dir_root + "/" + item for item in l_rootFiles]
							l_rootFiles = [item for item in l_rootFiles if os.path.isfile(item) and item.endswith(".root")]
							l_rootFiles.sort()
							dic_dir_rootFiles[dir_root] = l_rootFiles
						
					print "dic_dir_rootFiles"
					print dic_dir_rootFiles
					print "\n\n\n"
					
					#raw_input("STOP BEFORE CONVERT")
					if b_convertToRoot == True:
						
						# Loop over all yoda directories
						for dir in l_dir_yoda:
							
							# Yoda and Root directories
							dir_yoda = dir
							dir_root = dir
							if dir.endswith(".yoda"):
								dir_root = dir_root.replace(".yoda", ".root")
							if dir.endswith(".yoda.gz"):
								dir_root = dir_root.replace(".yoda.gz", ".root")
							
							cmd_mkdir_root = "mkdir -p " + dir_root
							os.system(cmd_mkdir_root)
							
							l_dir_root.append(dir_root)
							
							# List of yoda and root directories
							l_files_yoda = os.listdir(dir_yoda)
							l_files_yoda = [dir_yoda + "/" + item for item in l_files_yoda]
							
							l_files_root = list()
							for yodaFile_i in l_files_yoda:
								rootFile_i = dir_root + "/" + os.path.basename(yodaFile_i)
								
								if rootFile_i.endswith(".yoda"):
									rootFile_i = rootFile_i.replace(".yoda", ".root")
								if rootFile_i.endswith(".yoda.gz"):
									rootFile_i = rootFile_i.replace(".yoda.gz", ".root")
								
								command_yoda2root = "yoda2root %s %s " %( yodaFile_i, rootFile_i )
								print "command for yoda2root"
								print command_yoda2root
								#os.system(command_yoda2root)
								
								
							dic_dir_rootFiles[dir_root] = l_files_root
					
					
					#raw_input("STOP BEFORE  MERGE SEEDS")
					
					
					if b_mergeSeeds == True:
						for dir, l_rootFiles in dic_dir_rootFiles.items():
							l_root_check_xs = list()
							
							# check cross-section directory
							for rootFile_i in l_rootFiles:
								iRootFile=TFile(rootFile_i, "read")
								ANALYSIS = ""
								for key_tdir in iRootFile.GetListOfKeys():
									tdir = key_tdir.ReadObj()
									if tdir.ClassName() == "TDirectoryFile":
										ANALYSIS = tdir.GetName()
										break
								
								if iRootFile and not iRootFile.IsZombie() and iRootFile.Get(ANALYSIS+"/crossSection"):
									iRootFile.Close
									l_root_check_xs.append(rootFile_i)
								else:
									print "Zombie:",iRootFile
							
							# Check if the file is in ProblematicsFiles.txt
							base_path =os.path.dirname(l_rootFiles[-1])
							f_skip_str = base_path + "/ProblematicsFiles.txt"
							
							
							l_files_skip = list()
							isExist = os.path.exists(f_skip_str)
							if isExist == True:
								with open(f_skip_str) as f:
									l_files_skip = f.read().splitlines()
							
							l_rootFiles_clean = [ifile for ifile in l_root_check_xs if ifile not in l_files_skip]
							
							
							# hadd all rootFile in directory_to_yodaFiles
							iFile_merged = dir 
							iFile_merged = iFile_merged.replace(".root", "_MERGED.root")
							
							
							print "\n\n\n f_skip_str: " + f_skip_str
							
							for skipFile in l_files_skip:
								print skipFile
							
							print "\n\n\nClean:"
							for cleanFile in l_rootFiles_clean:
								print cleanFile
							
							print "\n\n\nSkip:"
							print "using skip file: " + f_skip_str
							
							
							for skipFile in l_files_skip:
								print skipFile
							print "\n\n\n"
							
							print "\n\n\n clean files:"
							for iClean in l_rootFiles_clean:
								print iClean
							
							print("\n\n\n")
							#raw_input ("STOP!!!! - Check clean files")
							###
							
							print "\n\n\n iFile_merged:"
							print iFile_merged
							print("\n\n\n")
							#raw_input ("STOP!!!! - iFile_merged")
							
							
							cmd_hadd_iFiles = "hadd -f "  + iFile_merged + " " + " ".join(l_rootFiles_clean)
							
							print "\n\n\n"
							print "cmd_hadd_iFiles:"
							print cmd_hadd_iFiles
							print "\n\n\n"
							print "call hadd command"
							os.system(cmd_hadd_iFiles)
								
							print "\n\n\n"
							print "Check 1st level merging command for RUCIO"
							
							#raw_input("STOP - Check if hadd run - problem expected for JSample2 6Step")
							l_iFileMerged.append(iFile_merged)
							
					
					if is_Rucio == True and l_iFileMerged:
						####################################################
						# hadd all Step files for given JZX
						allFileMerged = l_iFileMerged[0]
						l_tmp = allFileMerged.split("_")
						l_tmp = ['AllStep' if 'Step' in item else item for item in l_tmp]
						allFileMerged = "_".join(l_tmp)
						
						cmd_hadd_iFileMerged = "hadd -f " + allFileMerged + " "  + " ".join(l_iFileMerged)
						
						print "cmd_hadd_iFileMerged"
						print cmd_hadd_iFileMerged
						print "Check 2st level merging command for RUCIO"
						os.system(cmd_hadd_iFileMerged)

#
#if mergeReps:
#    savedir=outdir+"/"+mRepsDir
#    os.system("mkdir -p "+savedir)
#    for run in runs:
#        print "Merging",run
#        for sam in range(jSamMin,jSamMax+1):
#            idir="%s/" % (indir)
#            listFiles=[]
#            for filename in os.listdir(idir):   
#                print "filename: %s" % ( filename )
#                print "run:      %s" % ( run ) 
#                if run in filename :
#                    print "run: %s in %s" % (run, filename)
#                if "JZ"+str(sam) in filename :
#                    print "JZ%s in %s" % (str(sam), filename)
#                if filename.endswith(".yoda"):
#                    print ".yoda in %s" % (filename)
#                print ("\n\n\n")
#                    
#                    
#
#                    
#                if run in filename and "JZ"+str(sam) in filename and filename.endswith(".yoda"):
#
#                #if run+"_JS"+str(sam) in filename and filename.endswith(".root"):
#                    print filename
#                    yodaFile = idir + "/" + filename
#                    rootFile = yodaFile.replace(".yoda", ".root")
#                    command_yoda2root = "yoda2root %s" %( yodaFile )
#                    print command_yoda2root
#                    os.system(command_yoda2root)
#                    
#                    #raw_input("end yoda2root")
#                    
#                    #ifile=TFile(idir+filename,"read")
#                    ifile=TFile(rootFile, "read")
#                    if ifile and not ifile.IsZombie() and ifile.Get(ANALYSIS+"/crossSection"):
#                        listFiles.append(rootFile)
#                        
#                        
#                    else:
#                        print "Zombie:",rootFile
#            if listFiles:
#                command = "hadd -v 0 %s/%s_J%d.root %s" % (savedir,run,sam, ' '.join(listFiles))
#                print command
#                #os.system( command )
#        raw_input("stop")        
#

##########################################
####
####
if b_scale:
	rootdir = inputPath
	
	# recursive loop over all directories to find BatchOutput directory
	for rootdir, dirs, files in os.walk(rootdir):
		for subdir in dirs:
			# find "BatchOutput" directory
			is_BatchOutput = False
			is_Rucio = False
			
			if subdir == "BatchOutput":
				is_BatchOutput = True
				print "is_BatchOutput = True"
			
			if subdir == "RUCIO":
				is_Rucio = True
				print "is_Rucio = True"
			
			if  is_BatchOutput == True or is_Rucio == True:
				path = os.path.join(rootdir, subdir)
				print "BatchOutput or RUCIO directory found at: %s" % (path)
				
				# Get list of all files in BatchOutput directory
				l_files = os.listdir(path)
				
				# loop over JZ
				for jSAMPLE in l_JSAMPLES:
					str_jSAMPLE = "JZ" + jSAMPLE
					
					l_fileToRead = list()
					if is_BatchOutput == True:
						# use only rootFile           - file.endswith(".root")
						# use only merged rootFiles   - "MERGED" in file
						# use only given slice        - "JZ" + jSAMPLE in file
						# use only unscaled rootFiles - "SCALED" not in file
						print "XXXX"
						l_fileToRead = [ file for file in l_files if file.endswith(".root") and "MERGED" in file and str_jSAMPLE in file and "SCALED" not in file ]
					
					if is_Rucio == True:
						# use only rootFile                     - file.endswith(".root")
						# use only merged rootFiles             - "MERGED" in file
						# use only given slice                  - "JZ" + jSAMPLE in file
						# use only Merged Steps for given slice - "AllStep" in file
						# use only unscaled rootFiles           - "SCALED" not in file
						print "YYYY"
						print l_files
						
						l_fileToRead = [ file for file in l_files if file.endswith(".root") and "MERGED" in file and str_jSAMPLE in file and "AllStep" in file and "SCALED" not in file ]
						
					print "Check l_fileToRead"
					print l_fileToRead
					
					#raw_input
					
					for file in l_fileToRead:
						print "Work with rootFile: " + file
						#isRoot    = file.endswith(".root")
						#isMERGED  = "MERGED"    in file
						#isJSAMPLE = str_jSAMPLE in file
						#notSCALE  = "SCALED" not in file
						#
						#
						#if isRoot and isJSAMPLE and isMERGED and notSCALE:
						# open the rootfile to read 
						rootFile = path + "/" + file
						fj=TFile(rootFile, "read")
						
						# prepare container for
						#    TDirector and ListOfHistograms
						#    as python dictionary
						#    TDirectory means       ... string name of TDirectory - it is the same as Rivet Analysis name
						#    ListOfHistograms means ... python list of TH1D histogram, which will be readed from the rootfile and scaled by MC weights
						dic = {}  
						
						# generic loop over all objects in rootfile
						# only TDirectories are expected at the main path
						for key_tdir in fj.GetListOfKeys():
							
							tdir = key_tdir.ReadObj()
							print tdir.GetName()
							print tdir.ClassName()
							print "\n\n"
							
							if tdir.ClassName() == "TDirectoryFile":
								# Ok, it a directory
								#     so, cd
								fj.cd(tdir.GetName())
								
								# init pair of TDirectory and ListOfHistograms
								subdir = tdir.GetName();
								l_hists = list();
								
								# loop over of objects in TDirectory
								for key_obj in tdir.GetListOfKeys():
									obj = key_obj.ReadObj()
									objName = obj.GetName()
									
									print "obj.GetName():   %s" % (obj.GetName())
									print "obj.ClassName(): %s" % (obj.ClassName())
									
									# Load technical Histograms of xs etc. 
									print "\n\n"
									hName_xs         = "/" + tdir.GetName() + "/crossSection"
									hName_nFiles     = "/" + tdir.GetName() + "/nFiles"
									hName_suW        = "/" + tdir.GetName() + "/sumOfWeights"
									hName_nEvents    = "/" + tdir.GetName() + "/nEvents"
									hName_filtEff    = "/" + tdir.GetName() + "/filtEff"
									
									hName_nSelEvents        = "/" + tdir.GetName() + "/nSelectedEvents"
									hName_nSelEvents_jets   = "/" + tdir.GetName() + "/nSelectedEvents_jets"
									hName_nSelEvents_dijets = "/" + tdir.GetName() + "/nSelectedEvents_dijets"

									
									xs         = fj.Get(hName_xs).GetBinContent(1)
									nFiles     = fj.Get(hName_nFiles).GetBinContent(1)
									sumW       = fj.Get(hName_suW).GetBinContent(1)
									nEvents    = fj.Get(hName_nEvents).GetBinContent(1)
									#nSelEvents = fj.Get(hName_nSelEvents).GetBinContent(1)
									filtEff    = fj.Get(hName_filtEff).GetBinContent(1)
									
									# evaluate MC weight
									print "Scale factor sf = %s /  %s  / %s  "     % (xs, nFiles, sumW) 
									sf = xs/nFiles / sumW
									
									scale = xs/ ( nFiles * sumW )
									print "Scale factor sf = %s /  %s  / %s  = %s" % (xs, nFiles, sumW, sf) 
									
									if obj.InheritsFrom("TH1"):
										# Ok, object is a histogram
										hName = objName
										#if hName == "crossSection" or hName == "nFiles" or hName == "sumOfWeights" or hName == "nEvents" or hName == "nSelectedEvents" or hName == "filtEff" :
										#	continue
										#else:
										
										
										# check if object is kinematics histogram
										# it means it is not technical histogram
										#if objName != hName_xs and objName != hName_nFiles and objName != hName_suW and  objName != hName_nEvents and  objName != nSelEvents and objName != filtEff:
										if objName != "crossSection" and objName != "nFiles" and objName != "sumOfWeights" and objName != "nEvents"  and objName != "filtEff" and objName != "nSelectedEvents" and objName != "nSelectedEvents_jets" and objName != "nSelectedEvents_dijets":
											#continue
											
											print "analysis histogram"
											h = TH1D()
											print "fj.GetObject( " + "/" + tdir.GetName() + "/" + hName + ", h)"
											
											fj.GetObject( "/" + tdir.GetName() + "/" + hName, h )
											print "Integrel before Scale: %s" %(h.Integral())
											h_scale = h.Clone()
											
											# scale by evaluated weight
											print  "scale: %s" %(scale)
											
											print "scale by factor: " + str(sf)
											
											h_scale.Scale(sf)
											
											print "Integrel after  Scale: %s" %(h_scale.Integral())
											print "hName:   %s" % (hName)
											print "subdir:  %s" % (subdir)
											print "objName: %s" % (objName)
											h_scale.SetName(objName.replace("/" + subdir +"/", ""))
											
											# save scaled histogram in container
											l_hists.append(h_scale)
										else:
											print "Technical histogram"
											h = TH1D()
											print "fj.GetObject( " + "/" + tdir.GetName() + "/" + hName + ", h)"
											
											fj.GetObject( "/" + tdir.GetName() + "/" + hName, h )
											
											# technical histograms are not weighted
											h.SetName(objName.replace("/" + subdir +"/", ""))
											l_hists.append(h)
									if "cross" in objName:
										print "hName_xs: %s" %(hName_xs)
										#raw_input("stop - cross-section")
								dic[subdir] = l_hists
						
						# save results to new rootFile
						fnew = TFile(rootFile.replace(".root", "_SCALED.root"), "recreate")
						
						# loop over dictionary
						for key, val in dic.items():
							# key as TDirectory
							subdir  = key
							# val as list of new histograms
							l_hists = val
							dd=gDirectory.mkdir(subdir)
							fnew.cd(subdir)
							for h in l_hists:
								print type(h)
								print h.ClassName()
								
								h1 = TH1D()
								h1 = h.Clone()
								print "Write: " + h1.GetName()
								h1.Write()
							fnew.cd("../")
						fnew.Close()
						fj.Close()


#
#if normJsam:
#    savedir=outdir+"/"+mNormDir
#    os.system("rm -rf "+savedir)
#    os.system("mkdir -p "+savedir)
#    for run in runs:
#        for sam in range(jSamMin,jSamMax+1):
#            fileName="%s/%s/%s_J%d.root" %(outdir,mRepsDir,run,sam)
#            fj=TFile(fileName,"read")
#            xs      = fj.Get(ANALYSIS+"/crossSection").GetBinContent(1)
#            nFiles  = fj.Get(ANALYSIS+"/nFiles").GetBinContent(1)
#            sumW    = fj.Get(ANALYSIS+"/sumOfWeights").GetBinContent(1)
#            nEvents = fj.Get(ANALYSIS+"/nEvents").GetBinContent(1)
#            nSelEvents = fj.Get(ANALYSIS+"/nSelectedEvents").GetBinContent(1)
#            filtEff = fj.Get(ANALYSIS+"/filtEff").GetBinContent(1)
#            sf = xs/nFiles / sumW
#            #print nEvents, nSelEvents, sumW
#
#            fileName="%s/%s_J%d.root" % (savedir,run,sam)
#            fnew=TFile(fileName,"recreate")
#            dd=gDirectory.mkdir(ANALYSIS)
#            dd.cd()
#            for hist in hists:
#                histName  = "%s/%s" % (ANALYSIS,hist)
#                htmp=fj.Get( histName)
#                #print run,sam,hist
#                htmp.Scale(sf)
#                htmp.Write(hist)
#            fnew.Close()
#




if b_mergeScaled:
	rootdir = inputPath
	
	# recursive loop over all directories to find BatchOutput directory
	for rootdir, dirs, files in os.walk(rootdir):
		for subdir in dirs:
			# find "BatchOutput" directory
			is_BatchOutput = False
			is_Rucio      = False
			
			if subdir == "BatchOutput":
				is_BatchOutput = True
				print "is_BatchOutput = True"
			
			if subdir == "RUCIO":
				is_Rucio = True
				print "is_Rucio = True"
			
			# find "BatchOutput" directory
			if is_BatchOutput == True or is_Rucio == True:
				path = os.path.join(rootdir, subdir)
				print "BatchOutput or RUCIO directory was found at: %s" % (path)
				
				# Get list of all files in BatchOutput directory
				l_files = os.listdir(path)
				
				# Get list of all files to merge
				l_rootFileToMerge = list()
				if is_BatchOutput == True:
					#    use only rootFile                 - file.endswith(".root")
					#    use only merged rootFiles         - "MERGED" in file
					#    use only rootFile with JZ slices  - "JZ"  in file
					#    use only scaled rootFiles         - "SCALED"  in file
					l_rootFileToMerge = [ path + "/" + file for file in l_files if file.endswith(".root") and "MERGED" in file and "JZ" in file and "SCALED" in file ]
				
				if is_Rucio == True:
					#    use only rootFile                 - file.endswith(".root")
					#    use only merged rootFiles         - "MERGED" in file
					#    use only rootFile with JZ slices  - "JZ"  in file
					#    use only scaled rootFiles         - "SCALED"  in file
					#    use only Merged Steps for given slice - "AllStep" in file
					l_rootFileToMerge = [ path + "/" + file for file in l_files if file.endswith(".root") and "MERGED" in file and "JZ" in file and "AllStep" in file and "SCALED" in file ]
				
				# Set outFile
				rootFileMerged = os.path.basename(l_rootFileToMerge[-1])
				l_tmp = rootFileMerged.split("_")
				l_tmp = [item for item in l_tmp if "Step" not in item and "JZ" not in item and "Event" not in item and "Rivet" not in item]
				rootFileMerged = path + "/"  + "_".join(l_tmp)
				
				#l_tmp = ['AllStep' if 'Step' in item else item for item in l_tmp]
				#rootFileMerged = rootFileMerged[:rootFileMerged.index("JZ")] + rootFileMerged[rootFileMerged.index("JZ") + 4:]
				
				
				
				# Hadd command
				command_hadd_print = "hadd -f \\ \n %s \\ \n %s" %(rootFileMerged, ' \\ \n '.join(l_rootFileToMerge))
				command_hadd = "hadd -f  %s  %s" %(rootFileMerged, ' '.join(l_rootFileToMerge))

				print "command_hadd_print:\n %s" % (command_hadd_print)
				print "\n\n"
				print command_hadd
				os.system(command_hadd)

##########################################
####
####
#if mergeJsam:
#    savedir=outdir+"/"+mJsamDir
#    os.system("rm -rf  "+savedir)
#    os.system("mkdir -p "+savedir)
#    for run in runs:
#        command = "hadd -v 0 %s/%s.root %s/%s/%s_J*.root" %(savedir,run,outdir,mNormDir,run)
#        print command
#        os.system(command)
