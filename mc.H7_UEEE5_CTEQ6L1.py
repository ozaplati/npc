
## Sherpa config with CT10 PDF
## include("MC15JobOptions/Sherpa_Base_Fragment.py")
## Base config for Sherpa
import os,binascii

#include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# iSEED1=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED2=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED3=int(binascii.hexlify(os.urandom(4)), 16) % 1000000
# iSEED4=int(binascii.hexlify(os.urandom(4)), 16) % 1000000

# SEED1 = "%06d" % (iSEED1)
# SEED2 = "%06d" % (iSEED2)
# SEED3 = "%06d" % (iSEED3)
# SEED4 = "%06d" % (iSEED4)

#print "SEEDS = ", SEED1, SEED2, SEED3, SEED4

GEN=""
try:
   GEN=str(os.environ['NPC_GEN'])
except KeyError:
   print "WARNING: using default generator"
   GEN="Pythia8"
print 'GEN =', GEN
#exit()

TUNE=""
try:
   TUNE=str(os.environ['NPC_TUNE'])
except KeyError:
   print "WARNING: using default tune"
   TUNE="4C"
   print "ERROR: Wrong tune set in environment"
   #exit()

print 'TUNE =', TUNE

EFFECT=""
try:
   EFFECT=str(os.environ['NPC_EFFECT'])
except KeyError:
   print "WARNING: using default effect"
   EFFECT="Parton"
   print "ERROR: Wrong effect set in environment"

print 'EFFECT =', EFFECT

# JSAMPLE=0
# try:
#    JSAMPLE=int(os.environ['NPC_JSAMPLE'])
# except KeyError:
#    print "WARNING: using default JSAMPLE"
#    JSAMPLE=0
# print 'JSAMPLE =',JSAMPLE

ecmEnergy=0
try:
   ecmEnergy=runArgs.ecmEnergy
except KeyError:
   print "WARNING: using default ecmEnergy"
   ecmEnergy=13000

print 'ecmEnergy =',ecmEnergy

#SEED1 = "1234"
#SEED2 = "12345"
#SEED3 = "123456"
#SEED4 = "1234567"

#print "SEEDS = ", SEED1, SEED2, SEED3, SEED4

jetPtMin=25
jetPtMax=13000

if runArgs.runNumber==100001:
   jetPtMin=17
   jetPtMax=25
elif runArgs.runNumber==100002:
   jetPtMin=25
   jetPtMax=70
elif runArgs.runNumber==100003:
   jetPtMin=70
   jetPtMax=140
elif runArgs.runNumber==100004:
   jetPtMin=140
   jetPtMax=280
elif runArgs.runNumber==100005:
   jetPtMin=280
   jetPtMax=560
elif runArgs.runNumber==100006:
   jetPtMin=560
   jetPtMax=1120
elif runArgs.runNumber==100007:
   jetPtMin=1120
   jetPtMax=2240
elif runArgs.runNumber==100008:
   jetPtMin=2240
   jetPtMax=3000
elif runArgs.runNumber==100009:
   jetPtMin=3000
   jetPtMax=5500
else:
   jetPtMin=17
   jetPtMax=14000



# if JSAMPLE==0:
#    jetPtMin=25
#    jetPtMax=13000
# if JSAMPLE==1:
#    jetPtMin=17
#    jetPtMax=25
# elif JSAMPLE==2:
#    jetPtMin=25
#    jetPtMax=70
# elif JSAMPLE==3:
#    jetPtMin=70
#    jetPtMax=140
# elif JSAMPLE==4:
#    jetPtMin=140
#    jetPtMax=280
# elif JSAMPLE==5:
#    jetPtMin=280
#    jetPtMax=560
# elif JSAMPLE==6:
#    jetPtMin=560
#    jetPtMax=1120
# elif JSAMPLE==7:
#    jetPtMin=1120
#    jetPtMax=2240
# elif JSAMPLE==8:
#    jetPtMin=2240
#    jetPtMax=3000
# elif JSAMPLE==9:
#    jetPtMin=3000
#    jetPtMax=5500

print "Using jetPtMin=%d, jetPtMax=%d" % (jetPtMin,jetPtMax)

#############################
#### Pythia8 defs
#if runArgs.runNumber==100000: #normal 
if EFFECT=='PartonLVL':
   TYPE = 'NS'

#elif runArgs.runNumber==100001: #UE+Had
elif EFFECT=='ParticleLVL':
   TYPE = 'AH'

#elif runArgs.runNumber==100002: #UE
elif EFFECT=='UEOnly':
   TYPE = 'AS'

#elif runArgs.runNumber==100003: #Hadr
elif EFFECT=='HadOnly':
   TYPE = 'NH'

else:
   print 'ERROR: Wrong EFFECT!'
   exit()


print "Working with ", GEN, TYPE


if GEN=='Pythia8':
   print 'Running on Pythia8'

   evgenConfig.generators += ["Pythia8"]
   evgenConfig.description = "Pythia8 QCD jets"
   evgenConfig.keywords = ["QCD","jets"]
   #evgenConfig.contact  = ["Aliaksei Hrynevich"]
   evgenConfig.contact  = ["Ota Zaplatilek"]
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   ## Base config for Pythia8
   #AtRndmGenSvc.Seeds = ["PYTHIA8 4"+SEED1+" 989"+SEED2,"PYTHIA8_INIT "+SEED3+" 2"+SEED4]
   #AtRndmGenSvc.EventReseeding = True # DEFAULT -> HS IS SAME AT PARTON AND PARTICLE LVL (WARNING: MAKE SURE YOU USE --randomSeed=0 option)
   #AtRndmGenSvc.EventReseeding = False # FIX PYTHIA8_INIT SEEDS IN ALL EVENTS -> HS IS DIFFERENT AT PARTON AND PARTICLE LVL
   AtRndmGenSvc.OutputLevel = INFO
   from Pythia8_i.Pythia8_iConf import Pythia8_i
   genSeq += Pythia8_i("Pythia8",OutputLevel=INFO)
   
   genSeq.Pythia8.Commands += [
      # "Random:setSeed = on", # don't use, doesn't work
      # "Random:seed = 42",    # don't use, doesn't work
   #   "PDF:useLHAPDF=on",
   #   "PDF:LHAPDFset=NNPDF30_nlo_as_0118",
      "Main:timesAllowErrors = 500",
      "6:m0 = 172.5",
      "23:m0 = 91.1876",
      "23:mWidth = 2.4952",
      "24:m0 = 80.399",
      "24:mWidth = 2.085",
      "StandardModel:sin2thetaW = 0.23113",
      "StandardModel:sin2thetaWbar = 0.23146",
      "ParticleDecays:limitTau0 = on",
      "ParticleDecays:tau0Max = 10.0",
      #"Next:numberCount = 100",    #print message every n events
      #"Next:numberShowInfo = 5",   # print event information n times
      "Next:numberShowProcess = 3",# print process record n times
      "Next:numberShowEvent = 3",  # print event record n times

      ]

   
   if TUNE=='4C':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",
         #"PDF:LHAPDFset = cteq6ll.LHpdf"
         #"PDF:pSet = LHAPDF6:cteq6ll"          # PDF:pSet = LHAPDF6:cteq6ll does not exists in database: https://lhapdf.hepforge.org/pdfsets
         #                                      # Possible typo
         #                                      # Ota applies LHAPDF6:cteq6l1 
         "PDF:pSet = LHAPDF6:cteq6l1"
      ]   
   elif TUNE=='AU2CTEQ':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                  # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = cteq6ll.LHpdf",       # Old syntax in Pythia 8.1
         #"PDF:pSet = LHAPDF6:cteq6ll",          # Non-existing pdf LHAPDF6:cteq6ll - probably a typo
         "PDF:pSet = LHAPDF6:cteq6l1",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.00",
         "MultipartonInteractions:pT0Ref = 2.13",
         "MultipartonInteractions:ecmPow = 0.21",
         #"BeamRemnants:reconnectRange = 2.21",  # Old syntax in Pythia 8.1
         "ColourReconnection:range = 2.21",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2CT10':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                  # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = CT10.LHgrid",         # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:CT10",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.10",
         "MultipartonInteractions:pT0Ref = 1.70",
         "MultipartonInteractions:ecmPow = 0.16",
         #"BeamRemnants:reconnectRange = 4.67",  # Old syntax in Pythia 8.1
         "ColourReconnection:range = 4.67",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2MSTW':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                 # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",   # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:MSTW2008lo68cl",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.01",
         "MultipartonInteractions:pT0Ref = 1.87",
         "MultipartonInteractions:ecmPow = 0.28",
         #"BeamRemnants:reconnectRange = 5.32",  # Old syntax in Pythia 8.1
        "ColourReconnection:range = 5.32",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2NNPDF':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                  # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF21_100.LHgrid",  # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF21_100",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.08",
         "MultipartonInteractions:pT0Ref = 1.74",
         "MultipartonInteractions:ecmPow = 0.17",
         #"BeamRemnants:reconnectRange = 8.36",  # Old syntax in Pythia 8.1
        "ColourReconnection:range = 8.36",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='A2CTEQ':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                 # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = cteq6ll.LHpdf",      # Old syntax in Pythia 8.1
         #"PDF:pSet = LHAPDF6:cteq6ll",         # Non-existing pdf LHAPDF6:cteq6ll - probably a typo
         "PDF:pSet = LHAPDF6:cteq6l1",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.06",
         "MultipartonInteractions:pT0Ref = 2.18",
         "MultipartonInteractions:ecmPow = 0.22",
         #"BeamRemnants:reconnectRange = 1.55", # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.55",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='A2MSTW':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                 # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",  # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:MSTW2008lo68cl",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.03",
         "MultipartonInteractions:pT0Ref = 1.90",
         "MultipartonInteractions:ecmPow = 0.30",
         #"BeamRemnants:reconnectRange = 2.28",   # Old syntax in Pythia 8.1
         "ColourReconnection:range = 2.28",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='AU2MSTW2008LO':
      genSeq.Pythia8.Commands += [
         "Tune:pp = 5",
         #"PDF:useLHAPDF = on",                     # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",  # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:MSTW2008lo68cl",
         "MultipartonInteractions:bProfile = 4",
         "MultipartonInteractions:a1 = 0.01",
         "MultipartonInteractions:pT0Ref = 1.87",
         "MultipartonInteractions:ecmPow = 0.28",
         #"BeamRemnants:reconnectRange = 5.32",     # Old syntax in Pythia 8.1
         "ColourReconnection:range = 5.32",
         "SpaceShower:rapidityOrder=0"]
   elif TUNE=='MONASH':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF=on",                       # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"  # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='CMSMONASHSTAR':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:ee = 7",
#	"Tune:pp = 18",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='CMSCUETP8S1CTEQ6L1':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        " Tune:ee = 7",
#	"Tune:pp = 15",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = cteq6ll.LHpdf"]
#    elif TUNE=='CMSCUETP8S1HERAPDF1.5LO':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        " Tune:ee = 7",
#	"Tune:pp = 15",
#	"PDF:useLHAPDF=on",
#	"PDF:LHAPDFset = HERAPDF1.5LO"]
   elif TUNE=='ATLASA14CTEQL1':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7", 
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                  # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = cteq6ll",             # Old syntax in Pythia 8.1
         #"PDF:pSet = LHAPDF6:cteq6ll",          # Non-existing pdf LHAPDF6:cteq6ll - probably a typo
         "PDF:pSet = LHAPDF6:cteq6l1",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.144",
         "SpaceShower:pT0Ref = 1.30",
         "SpaceShower:pTmaxFudge = 0.95",
         "SpaceShower:pTdampFudge = 1.21",
         "SpaceShower:alphaSvalue = 0.125",
         "TimeShower:alphaSvalue = 0.126",
         "BeamRemnants:primordialKThard = 1.72",
         "MultipartonInteractions:pT0Ref = 1.98",
         "MultipartonInteractions:alphaSvalue = 0.118",
         #"BeamRemnants:reconnectRange = 2.08"  # Old syntax in Pythia 8.1
         "ColourReconnection:range = 2.08"]
   elif TUNE=='ATLASA14MRSTWLO':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7", 
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                     # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = MSTW2008lo68cl",         # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:MSTW2008lo68cl",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.62",
         "SpaceShower:pTmaxFudge = 0.92",
         "SpaceShower:pTdampFudge = 1.14",
         "SpaceShower:alphaSvalue = 0.129",
         "TimeShower:alphaSvalue = 0.129",
         "BeamRemnants:primordialKThard = 1.82",
         "MultipartonInteractions:pT0Ref = 2.22",
         "MultipartonInteractions:alphaSvalue = 0.127",
         #"BeamRemnants:reconnectRange = 1.87"      # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.87"]
   elif TUNE=='ATLASA14NNPDF':
      # based on jobFragments from J Ferrando
      print 'Setting Tune= ', TUNE
      evgenConfig.tune = "A14 NNPDF23LO"
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                      # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",  # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"        # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG1p':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                       # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.73",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.131",
         #"BeamRemnants:reconnectRange = 1.71"        # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG1n':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                       # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.69"
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.121",
         #"BeamRemnants:reconnectRange = 1.71"        # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG2p':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                         # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.60",
         "SpaceShower:pTmaxFudge = 1.05",
         "SpaceShower:pTdampFudge = 1.04",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.139",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"           # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG2n':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                         # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",     # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.50",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.08",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.111",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"          # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3ap':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                         # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",     # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.67",
         "SpaceShower:pTmaxFudge = 0.98",
         "SpaceShower:pTdampFudge = 1.36",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.136",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.125",
         #"BeamRemnants:reconnectRange = 1.71"         # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3an':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                        # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",    # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.51",
         "SpaceShower:pTmaxFudge = 0.88",
         "SpaceShower:pTdampFudge = 0.93",
         "SpaceShower:alphaSvalue = 0.127",
         "TimeShower:alphaSvalue = 0.124",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.127",
         #"BeamRemnants:reconnectRange = 1.71"         # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3bp':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                        # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",    # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 1.00",
         "SpaceShower:pTdampFudge = 1.04",
         "SpaceShower:alphaSvalue = 0.129",
         "TimeShower:alphaSvalue = 0.114",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"         # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3bn':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                        # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",    # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.83",
         "SpaceShower:pTdampFudge = 1.07",
         "SpaceShower:alphaSvalue = 0.126",
         "TimeShower:alphaSvalue = 0.138",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"         # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3cp':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                         # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",     # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.140",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"          # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
   elif TUNE=='ATLASA14NNPDFEIG3cn':
      print 'Setting Tune= ', TUNE
      genSeq.Pythia8.Commands += [
         "Tune:ee = 7",
         "Tune:pp = 14",
         #"PDF:useLHAPDF = on",                         # Old syntax in Pythia 8.1
         #"PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",     # Old syntax in Pythia 8.1
         "PDF:pSet = LHAPDF6:NNPDF23_lo_as_0130_qed",
         "SpaceShower:rapidityOrder = on",
         "SigmaProcess:alphaSvalue = 0.140",
         "SpaceShower:pT0Ref = 1.56",
         "SpaceShower:pTmaxFudge = 0.91",
         "SpaceShower:pTdampFudge = 1.05",
         "SpaceShower:alphaSvalue = 0.115",
         "TimeShower:alphaSvalue = 0.127",
         "BeamRemnants:primordialKThard = 1.88",
         "MultipartonInteractions:pT0Ref = 2.09",
         "MultipartonInteractions:alphaSvalue = 0.126",
         #"BeamRemnants:reconnectRange = 1.71"         # Old syntax in Pythia 8.1
         "ColourReconnection:range = 1.71"
         ]
# the below only works for Pythia8 see
# http://home.thep.lu.se/~torbjorn/pythia82html/Welcome.html
#    elif TUNE=='ATLASA14CTEQL1':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 19",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = cteq6ll.LHpdf"]
#    elif TUNE=='ATLASA14MSTW':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 20",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = MSTW2008lo68cl"]
#    elif TUNE=='ATLASA14HERAPDF':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 22",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = HERAPDF1.5LO"]
#    elif TUNE=='ATLASA14NNPDF':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 21",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG1p':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 23",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG1n':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 24",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG2p':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 25",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG2n':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 26",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3ap':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 27",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3an':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 28",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3bp':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 29",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3bn':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 30",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3cp':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 31",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
#    elif TUNE=='ATLASA14NNPDFEIG3cn':
#       print 'Setting Tune= ', TUNE
#       genSeq.Pythia8.Commands += [
#        "Tune:pp = 32",
#        "PDF:useLHAPDF = on",
#        "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed"]
   else:
      print 'Error: Wrong tune ID!', TUNE
      exit()
       
   genSeq.Pythia8.Commands += ["HardQCD:all = on"]
       
   if TYPE=='AH':
      # ParticleLVL 
      pass
        #Pythia8.Commands += [
        #"PartonLevel:MPI = on",
        #"HadronLevel:all = on"
        #]
   elif TYPE=='NH':
      # HadOnly
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         #"HadronLevel:all = on"
         ]
   elif TYPE=='AS':
      # UEOnly
      genSeq.Pythia8.Commands += [
         #"PartonLevel:MPI = on",
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NN':
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "Check:event = off"
         ]
   elif TYPE=='NS':
      # PartonLVL
      genSeq.Pythia8.Commands += [
         "PartonLevel:MPI = off",
         "HadronLevel:all = off",
         "BeamRemnants:primordialKT = off",
         "Check:event = off"
         ]
   else:
      print 'Error: Wrong type ID!'
      exit()
      
      
   genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMin = %d" % jetPtMin]
   genSeq.Pythia8.Commands += ["PhaseSpace:pTHatMax = %d" % jetPtMax]
   
   genSeq.Pythia8.CollisionEnergy = float(runArgs.ecmEnergy)
   #evgenConfig.minevents = runArgs.maxEvents

elif GEN=='Herwigpp':
    pass
#   print 'Running on Herwigpp'
#
#
#   #evgenConfig.generators += ["Herwigpp"]
#   evgenConfig.generators += ["Herwig7"]  
#   #evgenConfig.description = "Herwigpp QCD jets"
#   evgenConfig.description = "Herwig7 QCD jets"
#
#   evgenConfig.keywords = ["QCD","jets"]
#   evgenConfig.contact  = ["Aliaksei Hrynevich"]
#
#   #ServiceMgr.AtRndmGenSvc.Seeds = ["Herwigpp"]
#   ServiceMgr.AtRndmGenSvc.Seeds = ["Herwig7"]
#
#   #ServiceMgr.AtRndmGenSvc.EventReseeding = True
#   
#   #include ( 'MC15JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )
#   
#   #from Herwigpp_i.Herwigpp_iConf import Herwigpp
#   #genSeq += Herwigpp()
#   
#   #from Herwig7_i.Herwig7_iConf import Herwig7
#   #from Herwig7_i import config as hw
#   
#   
#   
#   #from Herwig7_i.Herwig7_iConf import Herwig7
#   #from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME
#   #
#   #genSeq += Herwig7()
#   #Herwig7Config = Hw7ConfigBuiltinME(genSeq, runArgs)
#   # initialize Herwig7 generator configuration for built-in matrix elements
#   include("Herwig7_i/Herwig7_BuiltinME.py")
#
#   #if hasattr(testSeq, "TestHepMC"):
#   #  testSeq.remove(TestHepMC())
#
#
#   cmds = """                                                                                                                              
#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/Particles/t:NominalMass 172.5*GeV
#set /Herwig/Particles/tbar:NominalMass 172.5*GeV
#set /Herwig/Particles/W+:NominalMass 80.399*GeV
#set /Herwig/Particles/W-:NominalMass 80.399*GeV
#set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
#set /Herwig/Particles/W+:Width 2.085*GeV
#set /Herwig/Particles/W-:Width 2.085*GeV
#set /Herwig/Particles/Z0:Width 2.4952*GeV
#set /Herwig/Model:EW/Sin2ThetaW 0.23113                                                                                                          
#""" 
#
#   ## Tunes
#   if TUNE=='LOEE3':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
#          + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % (ecmEnergy))
#   elif TUNE=='LOEE3CT10':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
#          + hw.ue_tune_cmds("LO**-UE-EE-%d-3" % (ecmEnergy))
#   elif TUNE=='CTEQEE3':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
#          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % (ecmEnergy))
#   elif TUNE=='CTEQEE3CT10':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
#          + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % (ecmEnergy))
#   elif TUNE=='LOEE4':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.lo_pdf_cmds("MRSTMCal.LHgrid") \
#          + hw.ue_tune_cmds("UE-EE-4-LO**")
#   elif TUNE=='LOEE4CT10':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.nlo_pdf_cmds("CT10.LHgrid", "MRSTMCal.LHgrid") \
#          + hw.ue_tune_cmds("UE-EE-4-LO**")
#   elif TUNE=='CTEQEE4':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
#          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
#   elif TUNE=='CTEQEE4CT10':
#      cmds += hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#          + hw.nlo_pdf_cmds("CT10.LHgrid", "cteq6ll.LHpdf") \
#          + hw.ue_tune_cmds("UE-EE-4-CTEQ6L1")
#   elif TUNE=='UEEE5CTEQ6L1':
#      #cmds = hw.energy_cmds(ecmEnergy) + hw.base_cmds() \
#      # + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
#      # + hw.ue_tune_cmds("UE-EE-5-CTEQ6L1")
#      #evgenConfig.tune        = "MMHT2014"
#      #Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
#      pass
#      
#   else:
#      print 'Error: Wrong tune ID!'
#      exit()
#      
#   ###We'll turn UE/HAD off as indicated in tune
#   if TYPE=='AH':
#      pass
#   elif TYPE=='AS':
#      cmds += """
#set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#set /Herwig/Analysis/Basics:CheckQuark No
#"""
#   elif TYPE=='NH':
#      cmds += """
#set /Herwig/Shower/ShowerHandler:MPIHandler NULL
#"""
#   elif TYPE=='NS':
#      cmds += """
#set /Herwig/Shower/ShowerHandler:MPIHandler NULL
#set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#set /Herwig/Analysis/Basics:CheckQuark No
#"""
#   else:
#      print 'Error: Wrong type ID!'
#      exit()
# 
#     
#  ### J samples  
#   cmds += """
#set /Herwig/Cuts/JetKtCut:MinKT %d*GeV                               
#set /Herwig/Cuts/JetKtCut:MaxKT %d*GeV                                        
#""" % (jetPtMin,jetPtMax)
#         
#         
#  ###last tweaks
####   cmds += """
####set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
####set /Herwig/Generators/LHCGenerator:MaxErrors 1000000
####"""
#   #cmds += """
##set /Herwig/Generators/LHCGenerator:EventHandler:LuminosityFunction:Energy 13000
##"""
#   #genSeq.Herwigpp.Commands += cmds.splitlines()
#   print "BEFORE Herwig7Config.add_commands(cmds) "
#   Herwig7Config.add_commands(cmds)
#   print "AFTER Herwig7Config.add_commands(cmds) "
#
#   print
#   print cmds
#   print
#   print cmds.splitlines()
#   include("Herwig7_i/Herwig7_EvtGen.py")
#   
#   Herwig7Config.run()
#

elif GEN == 'Sherpa':

   evgenConfig.generators  = ["Sherpa"]
   evgenConfig.auxfiles    = []
   evgenConfig.description = "Sherpa QCD jets"
   evgenConfig.keywords    = ["QCD","jets" ]
   evgenConfig.contact     = [ "aliaksei.hrynevich@cern.ch" ]
   #evgenConfig.minevents   = runArgs.maxEvents

   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   if JSAMPLE==0:
      runArgs.jobConfig = [ "share/sherpa/sherpa17_25.py"]

   if JSAMPLE==1:
      runArgs.jobConfig = [ "share/sherpa/sherpa17_25.py"]
      
   if JSAMPLE==2:
      runArgs.jobConfig = [ "share/sherpa/sherpa25_70.py"]
      
   if JSAMPLE==3:
      runArgs.jobConfig = [ "share/sherpa/sherpa70_140.py"]

   if JSAMPLE==4:
      runArgs.jobConfig = [ "share/sherpa/sherpa140_280.py"]

   if JSAMPLE==5:
      runArgs.jobConfig = [ "share/sherpa/sherpa280_560.py"]

   if JSAMPLE==6:
      runArgs.jobConfig = [ "share/sherpa/sherpa560_1120.py"]
      
   if JSAMPLE==7:
      runArgs.jobConfig = [ "share/sherpa/sherpa1120_2240.py"]

   if JSAMPLE==8:
      runArgs.jobConfig = [ "share/sherpa/sherpa2240_4480.py"]
   
   if JSAMPLE==9:
      runArgs.jobConfig = [ "share/sherpa/sherpa4480_13000.py"]

   if TUNE=="NNPDF30NNLO":
      include("MC15JobOptions/Sherpa_NNPDF30NNLO_Common.py")
   elif TUNE=="CT10":
      #include("MC12JobOption/Sherpa_CT10_Common.py")                                                                                         
      #include("MC15JobOptions/Sherpa_2.2.0_Base_Fragment.py")                                                                                
      #evgenConfig.tune = "CT10"                                                                                                              
      include("MC15JobOptions/Sherpa_CT10_Common.py")
      #fixSeq.FixHepMC.LoopsByBarcode = False                                                                                                 
#      if hasattr(testSeq, "TestHepMC"):                                                                                                      
#         testSeq.remove(TestHepMC())    

   else:
      print 'ERROR: Wrong Tune!'
      exit()

   if TYPE=='AH':
      # ParticleLVL
      pass
   elif TYPE=='NH':
      # HadOnly
      genSeq.Sherpa_i.Parameters += [
         "MI_HANDLER=None"
         ]
   elif TYPE=='AS':
      # UEOnly
      genSeq.Sherpa_i.Parameters += [
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         ]
   elif TYPE=='NN':
      genSeq.Sherpa_i.Parameters += [
         "MI_HANDLER=None",
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         ]
   elif TYPE=='NS':
      # PartonLVL
      genSeq.Sherpa_i.Parameters += [
      #topAlg.Sherpa_i.Parameters += [                                                                                                         
         "MI_HANDLER=None",
         "FRAGMENTATION=Off",
         "DECAYMODEL=Off"
         #"??? BeamRemnants:primordialKT ???? = off",                                                                                          
         ]
   else:
      print 'ERROR: Wrong type ID!'
      exit()

#
#print "ANALYSIS"
#
#ANALYSES = [ 'ATLAS_2018_IJXS_IDXS_Alieksei', 'ATLAS_2022_IJXS_IDXS', 'ATLAS_2018_I1634970' ]
#
#from Rivet_i.Rivet_iConf import Rivet_i
#
#print "RIVET"
#rivet = Rivet_i()
#
#if GEN=='Sherpa':
#   rivet.CrossSection = 987654321 ## Set cross section manually
#
#rivet.AnalysisPath = os.getenv("PWD")  
#rivet.OutputLevel = INFO
#rivet.Analyses = ANALYSES
#
#print "BEFORE genSeq += rivet"
#genSeq += rivet
#
print "AFTER genSeq += rivet"

#include("Herwig7_i/Herwig7EvtGen_H7UE_NNPDF23lo_jetjet.py")
#name = runArgs.jobConfig[0]
#name_info = name.split("_JZ")[1].split(".py")
#slice = int(name_info[0])

#minkT = {0:0,1:0,2:15,3:50,4:150,5:350,6:600,7:950,8:1500,9:2200,10:2800,11:3500,12:4200}
 
# initialize Herwig7 generator configuration for built-in matrix elements
#include("Herwig7_i/Herwig7_BuiltinME.py")

# configure Herwig7
#Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
#Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")

#command = """
#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
#set /Herwig/Cuts/JetKtCut:MinKT 2800*GeV
#"""
#print command
#
#Herwig7Config.add_commands(command)
 
# add EvtGen
#include("Herwig7_i/Herwig7_EvtGen.py")

# run Herwig7
# initialize Herwig7 generator configuration object for built-in/old-style matrix elements

#from Herwig7_i.Herwig7_iConf import Herwig7
#from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

#genSeq += Herwig7()
#Herwig7Config = Hw7ConfigBuiltinME(genSeq, runArgs)
#
#Herwig7Config.run()


## Initialise Herwig7 for run with built-in matrix elements
## Taken from
##    https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/share/common/Herwig7_BuiltinME.py
##    also you can include the setup directly as
##        include("Herwig7_i/Herwig7_BuiltinME.py")

from Herwig7_i.Herwig7_iConf import Herwig7
from Herwig7_i.Herwig7ConfigBuiltinME import Hw7ConfigBuiltinME

genSeq += Herwig7()
Herwig7Config = Hw7ConfigBuiltinME(genSeq, runArgs)

#include("Herwig7_i/Herwig7_BuiltinME.py")
#include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.description = "Herwig7 dijet LO"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact  = [ "ota.zaplatilek@cern.ch" ]
evgenConfig.nEventsPerJob = 1


#include ("GeneratorFilters/FindJets.py")
#CreateJets(prefiltSeq, 0.6 )
#AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)
#include("GeneratorFilters/JetFilter_JZX.py")
#JZSlice(8,filtSeq)

# Aliaksei applied 2010  PDF values but 2019 are imlemented
# very sililar to https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L168
# implemetation in https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwig7_i/python/Herwig7Config.py#L130
#
#command = """

#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#set /Herwig/Particles/t:NominalMass 172.5*GeV
#set /Herwig/Particles/tbar:NominalMass 172.5*GeV
#set /Herwig/Particles/W+:NominalMass 80.399*GeV
#set /Herwig/Particles/W-:NominalMass 80.399*GeV
#set /Herwig/Particles/Z0:NominalMass 91.1876*GeV
#set /Herwig/Particles/W+:Width 2.085*GeV
#set /Herwig/Particles/W-:Width 2.085*GeV
#set /Herwig/Particles/Z0:Width 2.4952*GeV
#set /Herwig/Model:EW/Sin2ThetaW 0.23113                                                                                                          
#""" 

## Tunes
## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
#Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")


# ERROR for insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
#       generate 11:22:10 Error: There was no object named '/Herwig/MatrixElements/SimpleQCD' in the repository.
#       update Matrix lement as
#           insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
# 
# ERROR for
#      set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#      generate 11:27:11 Error: There was no object named '/Herwig/EventHandlers/LHCHandler' in the repository.
#      
#      Other examples from Athena do not work as well:
#          set /Herwig/EventHandlers/LHEHandler:HadronizationHandler NULL
#          set /Herwig/EventHandlers/FxFxLHEHandler:HadronizationHandler NULL
#
# https://herwig.hepforge.org/tutorials/faq/shower.html?highlight=hadronization
# grep --color -R -e "HadronizationHandler" /cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/LCG_88b/MCGenerators/herwig++/7.2.2/x86_64-centos7-gcc62-opt/

# [zaplatilek@ui3 Test_HERWIG]$ less /cvmfs/atlas.cern.ch/repo/sw/software/21.6/sw/lcg/releases/LCG_88b/MCGenerators/herwig++/7.2.2/x86_64-centos7-gcc62-opt/share/Herwig/Matchbox/LO-NoShower.in
# https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/Herwigpp_i/python/config.py#L504

command = ""
if TYPE=='AH':
  # ParticleLVL in means #UE+Had
  pass
elif TYPE=='AS':
  # UEOnly
  command += """
#set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#set /Herwig/EventHandlers/LHEHandler:HadronizationHandler NULL
set /Herwig/EventHandlers/FxFxLHEHandler:HadronizationHandler NULL
set /Herwig/Analysis/Basics:CheckQuark No
"""
elif TYPE=='NH':
  # HadOnly
  command += """
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
"""
elif TYPE=='NS':
  # PartonLVL
  command += """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Shower/ShowerHandler:MPIHandler NULL
# Switch Off Hadronization
set /Herwig/EventHandlers/LHCHandler:HadronizationHandler NULL
#set /Herwig/EventHandlers/LHEHandler:HadronizationHandler NULL
#set /Herwig/EventHandlers/FxFxLHEHandler:HadronizationHandler NULL
#set /Herwig/EventHandlers/EventHandler:HadronizationHandler NULL
#set /Herwig/EventHandlers/theLesHouchesHandler:HadronizationHandler  NULL 

## Create the Handler and Reader
#set LHEReader:FileName
#library LesHouches.so
#create ThePEG::LesHouchesFileReader /Herwig/EventHandlers/LHEReader
#create ThePEG::LesHouchesEventHandler /Herwig/EventHandlers/LHEHandler
#
#set /Herwig/Generators/EventGenerator:EventHandler /Herwig/EventHandlers/LHEHandler
#insert /Herwig/EventHandlers/LHEHandler:LesHouchesReaders 0 /Herwig/EventHandlers/LHEReader
#set    /Herwig/EventHandlers/LHEHandler:HadronizationHandler NULL

set /Herwig/Analysis/Basics:CheckQuark No
"""
else:
  print 'Error: Wrong type ID!'
  exit()

print command

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()


print "END OF JobOption"

