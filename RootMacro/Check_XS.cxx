#include <iostream>
#include <fstream>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <vector>

/// C++17 features - need to link with -lstdc++fs
#include <fstream>
#include <iostream>
#include <filesystem>
namespace fs = std::filesystem;

/// ROOT
#include <TROOT.h>
#include "TROOT.h"
#include "TSystem.h"
#include <TStyle.h>
#include <TFile.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TString.h>

/// ROOT
#include <TCanvas.h>
#include <TString.h>
#include <TLatex.h>
#include <TH1D.h>
#include <TF1.h>

using namespace std;


namespace INIT
{
	const bool    is_DEBUG = true;
	const double  sigma_cut = 5;
	//const TString h_name = "ATLAS_2022_IJXS_IDXS_Rivet_2_2_1/crossSection";
	const TString h_name   = "ATLAS_2023_IJXS_IDXS_Rivet_3_1_8/crossSection";
	
}

void TestFileSystem()
{
	/// Examples for filesystem class
	///    https://en.cppreference.com/w/cpp/filesystem/recursive_directory_iterator
    fs::current_path(fs::temp_directory_path());
    fs::create_directories("sandbox/a/b");
    std::ofstream("sandbox/file1.txt");
    fs::create_symlink("a", "sandbox/syma");
 
    // Iterate over the `std::filesystem::directory_entry` elements explicitly
    for (const fs::directory_entry& dir_entry : 
        fs::recursive_directory_iterator("sandbox"))
    {
        std::cout << dir_entry << '\n';
    }
    std::cout << "-----------------------------\n";
    // Iterate over the `std::filesystem::directory_entry` elements using `auto`
    for (auto const& dir_entry : fs::recursive_directory_iterator("sandbox"))
    {
        std::cout << dir_entry << " ";
        if (fs::is_directory(dir_entry))
        {
			cout << "is directory";
		}
        cout << '\n';
        
    }
 
    fs::remove_all("sandbox");
}


void CheckProblematicRooFiles(TString h_name, double sigma_cut, vector<TString> & v_files)
{
	/// Loop over files and get the cross-section values
	vector<double> v_val;
	for (TString iFile: v_files)
	{
		/// open rootFile
		TFile * f = new TFile(iFile, "OPEN");
		
		/// load histogram
		TH1D * h = new TH1D();
		       h = f->Get<TH1D>(h_name);
		/// cross-section histogram should have one bin only - load the value as cross-section
		if (INIT::is_DEBUG) cout << "\t next file: " << h->GetBinContent(1) << endl;
		double val =  h->GetBinContent(1);
		
		///save it
		v_val.push_back(val);
		
		delete h;
		f->Close();
		delete f;
	}
	
	/// Calculate mean and sigma
	///    so you need sum of all values
	///    and
	///    sum of squeres
	double sum = 0.0;
	       std::for_each(v_val.rbegin(), v_val.rend(), [&](double val) { sum += val; });
	
	double sum_of_squares = 0.0;
	       std::for_each(v_val.rbegin(), v_val.rend(), [&](double val) { sum_of_squares += val*val; });
	
	/// evaluate mean value
	int n =  v_val.size();
	double mean = sum / n;
	
	/// evaluate sigma
	double sigma = std::sqrt(sum_of_squares / n - mean * mean);
	
	if (INIT::is_DEBUG)
	{
		cout << "sum: " << sum << " \t sum_of_squares: " << sum_of_squares << "\t n: " << n << endl;
		cout << "sum_of_squares / n: " << sum_of_squares / n << endl;
		cout << "mean * mean:  " << mean * mean << endl;
		cout << "global mean:  " << mean << endl;
		cout << "global sigma: " << sigma << endl;
	}
	
	/// Loop over root-files again and check the one with large sigma discrepances
	vector<TString> v_files_problem;
	for (TString iFile: v_files)
	{
		TFile * f = new TFile(iFile, "OPEN");
		TH1D * h = new TH1D();
		       h = f->Get<TH1D>(h_name);
		
		/// Check sigma
		double val =  h->GetBinContent(1);
		double iSigma = fabs(mean - val) / sigma;
		
		if (iSigma > sigma_cut)
		{
			cout << "Problematics file: " << iFile << endl;
			cout << "\n\n\n" << endl;
			
			v_files_problem.push_back(iFile);
		}
		delete h;
		f->Close();
		delete f;
	}
	
	TString tmp = v_files.front().Data();
	TString outFile = fs::path( tmp.Data() ).parent_path().string();
	        outFile += "/ProblematicsFiles.txt";
	ofstream f_out;
	f_out.open (outFile.Data());
	for (TString file_problem: v_files_problem)
	{
		f_out <<  file_problem;
		f_out << "\n";
	}
	
	f_out.close();
}

vector<TString> Get_vDirectories(TString _path)
{
	vector<TString> v_dir;
	// Iterate over the `std::filesystem::directory_entry` elements using `auto`
	for ( const fs::directory_entry &  dir_entry :  fs::recursive_directory_iterator(_path.Data()) )
	{
		if (fs::is_directory(dir_entry))
		{
			TString dir = dir_entry.path().string();
			v_dir.push_back(dir);
			
		}
	}
	return (v_dir);
}

vector<TString> Get_vFiles(TString _path)
{
	vector<TString> v_dir;
	// Iterate over the `std::filesystem::directory_entry` elements using `auto`
	for ( const fs::directory_entry &  dir_entry :  fs::recursive_directory_iterator(_path.Data()) )
	{
		if (fs::is_regular_file(dir_entry))
		{
			TString file = dir_entry.path().string();
			v_dir.push_back(file);
			
		}
	}
	return (v_dir);
}


using namespace std;
int main(int nArg, char *arg[])
{
	TString path  = "";
	double sigma_cut;
	TString h_name;
	
	/// CHECK number of arguments
	if (nArg == 1)
	{
		path      = "/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_ATLASA14NNPDF_20221118_jet_dijet__rel19/RUCIO";
		sigma_cut = INIT::sigma_cut;
		h_name    = INIT::h_name;

	}
	else if (nArg == 2)
	{
		path  = arg[1];
		sigma_cut = INIT::sigma_cut;
		h_name    = INIT::h_name;
	}
	else if (nArg == 3)
	{
		path      = arg[1];
		sigma_cut = atof(arg[2]);
		h_name    = INIT::h_name;
	}
	else
	{
		cout << "Error: Wrong number of input parameters!" << endl;
		cout << nArg << " parameters were used" << endl;
		for (int i = 0; i<nArg; i++)
		{
			cout << "arg[" << i << "]: " << arg[i] << endl;
		}
		cout << "End the program" << endl;
		return(-1); 
	}
	
	cout << "TString path      = " << path << endl;
	cout << "TString h_name    = " << h_name << endl;
	cout << "double sigma_cut  = " << sigma_cut << endl;
	
	/// Get all directories in the path
	vector<TString> v_dir = Get_vDirectories(path);
	
	/// Erase the directories which does not include following strings
	///    -  "Rivet.root"        (used in rel19)
	///    - "RivetRootFile.root" (used in rel23)
	///
	v_dir.erase(std::remove_if(	v_dir.begin(),
								v_dir.end(),
								[](TString dir){return !( dir.EndsWith("Rivet.root") || dir.EndsWith("RivetRootFile.root") ) ;}),
								v_dir.end());
	
	/// Debug print
	if (INIT::is_DEBUG)
	{
		cout << "Print dirs:" << endl;
		for (auto dir: v_dir)
		{
			cout << dir << endl;
		}
		int stop; cin >> stop;
	}
	
	/// Loop over Rivet.root or RivetRootFile.root directories
	for (TString dir: v_dir)
	{
		/// Get list of all files in the given Rivet.root directory
		vector<TString> v_files = Get_vFiles(dir);
		
		/// Erase all files from the list, which are not the rootFiles
		v_files.erase(std::remove_if(	v_files.begin(),
										v_files.end(),
										[](TString file){return !file.EndsWith(".root");}),
										v_files.end());
		/// Debug print
		if (INIT::is_DEBUG)
		{
			for (auto file: v_files)
			{
				cout << file << endl;
			}
			cout << "\n\n\n" << endl;
			int stop; cin >> stop;
		}
		/// Check potencial problematic rootFiles
		///    by evaluating mean a variance of cross-section
		CheckProblematicRooFiles(h_name, sigma_cut, v_files);
	}
	
   return(0);
}
