/// C++ and std
#include <iostream>
#include <vector>
/// ROOT
#include <TCanvas.h>
#include <TString.h>
#include <TLatex.h>
#include <TH1D.h>


/// C++ 
#include <tuple>
#include <vector>
#include <map>
#include <type_traits>

/// ROOT
#include <TROOT.h>
#include "TROOT.h"
#include "TSystem.h"
#include <TStyle.h>
#include <TFile.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>

#include "Functions.C"
using namespace std;

using namespace GlobalLatex;

void Function_DrawLatex_v2()
{
	TLatex * latex = new TLatex();
	latex->SetNDC();
	latex->SetTextSize(0.05);
	latex->SetTextFont(72);

	vector<TString> v_text = {GlobalLatex::g_desc, GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange, GlobalLatex::g_tune};

	for (unsigned int iStr=0; iStr < v_text.size(); iStr++)
	{
		latex->DrawLatex(0.4, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
	}
}

void Pad1Function(TH1D * _h)
{
	cout << "Pad1Funtion" << endl;
	
	_h->GetXaxis()->SetTitle("m_{jj} [GeV]");
	_h->GetYaxis()->SetTitle("non-perturbative corr. [-]");
	
	_h->SetMinimum(0.0);
	_h->SetMaximum(4.0);
}

void Pad2Function(TH1D * _h)
{	
	cout << "Pad2Funtion" << endl;
	
	TString hName = _h->GetName();
	TString hTitle = _h->GetTitle();
	
	cout << "\t hName:  " << hName << endl;
	cout << "\t hTitle: " << hTitle << endl;
	
	_h->SetMinimum(0.5);
	_h->SetMaximum(1.5);
}


int main()
{
	/// ////////////////////////////////////////////////////////////////
	cout << "=================================================" << endl;
	cout << " MAIN                                            " << endl;
	cout << "=================================================" << endl;
	
	/// name of HTCondor run
	TString HTCondor_Run_rel19 = "HTCondor_Run_5";
	TString HTCondor_Run_rel21 = "HTCondor_Aliaksei_Run_1";
	
	vector <tuple<TString, TString>> t_gen_tune = {
			{"Pythia8", "ATLASA14NNPDF"}, 
			//{"Pythia8", "ATLASA14CTEQL1"}, 
			//{"Pythia8", "AU2CT10"}, 
			//{"Pythia8", "AU2CTEQ"}
		};
	
	/// name of histograms
	vector<TString> v_hName = 	{"h1_mjj_ystar0",
								"h1_mjj_ystar1",
								"h1_mjj_ystar2",
								"h1_mjj_ystar3",
								"h1_mjj_ystar4",
								"h1_mjj_ystar5",
								"h1_mjj_yboost0",
								"h1_mjj_yboost1",
								"h1_mjj_yboost2",
								"h1_mjj_yboost3",
								"h1_mjj_yboost4",
								"h1_mjj_yboost5",
								};
	
	/// ////////////////////////////////////////////////////////////////
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " MAKE DIRECTORIES                                " << endl;
	cout << "=================================================" << endl;
	
	TString outDir        = "Compare_Releases_19_21";
	TString outDir_png    = outDir + "/png";
	TString outDir_root   = outDir + "/root";
	
	TString cmd_outDir      = "mkdir -p " + outDir;
	TString cmd_outDir_png  = "mkdir -p " + outDir_png;
	TString cmd_outDir_root = "mkdir -p " + outDir_root;
	
	gSystem->Exec(cmd_outDir.Data());
	gSystem->Exec(cmd_outDir_png.Data());
	gSystem->Exec(cmd_outDir_root.Data());
	
	
	/// ////////////////////////////////////////////////////////////////
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " LOOP OVER TUNES                                 " << endl;
	cout << "=================================================" << endl;
	

	for (auto t: t_gen_tune)
	{
		TString generator = get<0>(t);
		TString tune      = get<1>(t);
		
		for (TString hName: v_hName)
		{
			
			TString f_path_rel19 = "HTCondor_Aliaksei_Run1/root/" + hName + "_" + generator + "_" + tune + ".root";
			TString f_path_rel21 = "HTCondor_Run_4/root/"         + hName + "_" + generator + "_" + tune + ".root";
			
			TString h_name  = "npc";
			
			cout << "f_path_rel19: " << f_path_rel19 << endl;
			cout << "f_path_rel21: " << f_path_rel21 << endl;
			cout << "h_name: " << h_name << endl;
			
			TString outputName = hName + "_" + generator + "_"+ tune;
			
			TFile * f_rel19 = new TFile(f_path_rel19, "OPEN");
			TFile * f_rel21 = new TFile(f_path_rel21, "OPEN");
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " LOAD HISTOGRAMS                                 " << endl;
			cout << "=================================================" << endl;
			
			TH1D * h_rel19 = f_rel19->Get<TH1D>(h_name); h_rel19->SetLineColor(kRed);  h_rel19->SetTitle("MCProd, rel. 19, Pythia v8.210");
			TH1D * h_rel21 = f_rel21->Get<TH1D>(h_name); h_rel21->SetLineColor(kBlue); h_rel21->SetTitle("AthGeneration, rel. 21, Pythia v8.306");
			
			cout << "h1->Integral(): " << h_rel19->Integral() << endl;
			cout << "h2->Integral(): " << h_rel21->Integral() << endl;
			
			TString tune_tmp = tune; tune_tmp = tune_tmp.ReplaceAll("ATLAS", "");
			
			vector<TH1D *> v; 
				v.push_back(h_rel19);
				v.push_back(h_rel21); 
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " PREPARE TO DRAW                                 " << endl;
			cout << "=================================================" << endl;
			
			
			bool logX = false;
			bool logY = false;
			TString latex = "HTCondor_Run_1";
			
			/// Set position of legend
			TLegend * l = new TLegend(0.57, 0.35, 0.93, 0.9);
			
			/// Set description for TLatex
			TString text_tmp = hName; text_tmp.ReplaceAll("h1_mjj_", "");
			TString text;
			if (text_tmp.Contains("0")) text = "0.0<XXX<0.5";
			if (text_tmp.Contains("1")) text = "0.5<XXX<1.0";
			if (text_tmp.Contains("2")) text = "1.0<XXX<1.5";
			if (text_tmp.Contains("3")) text = "1.5<XXX<2.0";
			if (text_tmp.Contains("4")) text = "2.0<XXX<2.5";
			if (text_tmp.Contains("5")) text = "2.5<XXX<3.0";
			
			if (text_tmp.Contains("ystar"))
			{
				GlobalFunctions::g_yVarName = "ystar";
				GlobalFunctions::g_yVar     = "y*";
			}
			if (text_tmp.Contains("yboost"))
			{
				GlobalFunctions::g_yVarName = "yboost";
				GlobalFunctions::g_yVar     = "y_{boost}";
			}
			
			text = text.ReplaceAll("XXX", GlobalFunctions::g_yVar);
			GlobalLatex::g_tune = tune;
			

			TString outPNG = outDir_png + "/" + outputName + ".png";
			GlobalLatex::g_yVarRange = text;
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " DRAW FUNCTION                                   " << endl;
			cout << "=================================================" << endl;
			
			/// Draw function
			///     using pointer to a function: Pad1Function - to update all histograms in pad 1
			///                                  Pad2Function - to update all histograms in pad 2
			///                                  Function_DrawLatex - ro draw labels in latex
			Draw (v, outPNG, v.front(), logX, logY, Pad1Function, Pad2Function, latex, l, Function_DrawLatex_v2);
			
			delete f_rel19;
			delete f_rel21;
				
		}
	
		/// ////////////////////////////////////////////////////////////////
		cout << "\n\n\n" << endl;
		cout << "=================================================" << endl;
		cout << " MONTAGE                                         " << endl;
		cout << "=================================================" << endl;
		
		/// merge all 6 png plots (various y* bins) to one pdf file
		TString cmd_montage_ystar =  "montage -mode concatenate -tile 3x2 "  + outDir_png + "/" + "h1_mjj_ystar*" + generator + "_" + tune + "*.png  " + outDir + "/Compare_ystar_" + "_all_" + generator + "_" + tune + ".png";
		cout << cmd_montage_ystar << endl;
		gSystem->Exec(cmd_montage_ystar.Data());
		
		/// merge all 6 png plots (various y_boost bins) to one pdf file
		TString cmd_montage_yboost =  "montage -mode concatenate -tile 3x2 " + outDir_png + "/" + "h1_mjj_yboost*" + generator + "_" + tune + "*.png  " + outDir + "/Compare_yboost_" + "_all_" + generator + "_" + tune + ".png";
		cout << cmd_montage_yboost << endl;
		gSystem->Exec(cmd_montage_yboost.Data());
		}
	return 0;
}
