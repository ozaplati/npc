/// C++17 features - need to link with -lstdc++fs
#include <fstream>
#include <iostream>
#include <filesystem>
#include <iomanip>
namespace fs = std::filesystem;

/// ROOT
#include <TCanvas.h>
#include <TString.h>
#include <TLatex.h>
#include <TH1D.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include <TKDE.h>


/// C++ 
#include <tuple>
#include <vector>
#include <map>
#include <type_traits>

/// ROOT
#include <TROOT.h>
#include "TROOT.h"
#include "TSystem.h"
#include <TStyle.h>
#include <TFile.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>

/// Ota
#include "CONFIG/CONFIG.h"
//#include "CONFIG/CONFIG.cxx"

using namespace std;
namespace Binning
{
	///const std::vector<double> g_vec_binning_new_yStar0 = {240, 270, 300, 340, 380, 420, 460, 500, 540, 580, 630, 680, 730, 780, 830, 890, 950, 1010, 1070, 1140, 1210, 1280, 1350, 1430, 1510, 1590, 1670, 1760, 1850, 1940, 2040, 2140, 2240, 2350, 2460, 2580, 2700, 2820, 2950, 3080, 3220, 3360, 3510, 3660, 3820, 3990, 4160, 4340, 4530, 4730, 4940, 5160, 5390, 5630, 5880, 6140, 6420, 6710, 7020, 7350, 7710, 8100};
	///const std::vector<double> g_vec_binning_new_yStar1 = {280, 320, 360, 400, 450, 500, 550, 600, 650, 700, 760, 820, 880, 940, 1010, 1080, 1150, 1220, 1300, 1380, 1460, 1550, 1640, 1730, 1830, 1930, 2030, 2140, 2250, 2370, 2490, 2620, 2750, 2890, 3030, 3180, 3330, 3490, 3650, 3820, 4000, 4180, 4370, 4570, 4770, 4980, 5200, 5420, 5650, 5890, 6140, 6400, 6670, 6950, 7250, 7570, 7900, 8260};
	///const std::vector<double> g_vec_binning_new_yStar2 = {390, 450, 510, 570, 630, 700, 770, 840, 910, 990, 1070, 1150, 1240, 1330, 1420, 1520, 1620, 1720, 1830, 1940, 2060, 2180, 2310, 2440, 2580, 2730, 2880, 3040, 3210, 3380, 3560, 3750, 3950, 4160, 4380, 4610, 4850, 5100, 5360, 5640, 5930, 6240, 6570, 6910, 7280, 7660, 8080, 8540, 9070, 9650};
	///const std::vector<double> g_vec_binning_new_yStar3 = {600, 690, 790, 890, 990, 1090, 1200, 1310, 1420, 1540, 1660, 1790, 1920, 2060, 2200, 2350, 2500, 2660, 2830, 3010, 3190, 3380, 3580, 3780, 4000, 4230, 4470, 4720, 4980, 5250, 5540, 5840, 6160, 6500, 6870, 7280, 7690, 8070, 8670, 9280}; // add 9280 and re-run all machinery
	///const std::vector<double> g_vec_binning_new_yStar4 = {960, 1110, 1260, 1410, 1560, 1720, 1890, 2060, 2240, 2420, 2610, 2810, 3020, 3240, 3470, 3700, 3950, 4200, 4470, 4750, 5040, 5350, 5650, 5970, 6320, 6720, 7060, 7410, 7950, 8540}; // add 9620
	///const std::vector<double> g_vec_binning_new_yStar5 = {1570, 1830, 2100, 2390, 2680, 3000, 3320, 3640, 3970, 4340, 4710, 5160, 5620, 6070, 6570, 7420, 8680, 9950};
	
	
	
	/// Update from 2024_02_29
	/// Update mjj binning from Standa 2024 - Standa found a bug in cleaning - EventClaning applied
	///                                     - Zdenek procude pQCD calculation with the same binning
	///                                     - EW corrrection requested with the same binning 
	const std::vector<double> g_vec_binning_new_yStar0 = {40, 240, 270, 300, 340, 380, 420, 460, 500, 540, 580, 630, 680, 730, 780, 830, 890, 950, 1010, 1070, 1140, 1210, 1280, 1350, 1430, 1510, 1590, 1670, 1760, 1850, 1940, 2040, 2140, 2240, 2350, 2460, 2580, 2700, 2820, 2950, 3080, 3220, 3360, 3510, 3660, 3820, 3990, 4160, 4340, 4530, 4730, 4940, 5160, 5390, 5630, 5880, 6140, 6420, 6710, 8100};
	const std::vector<double> g_vec_binning_new_yStar1 = {110, 280, 320, 360, 400, 450, 500, 550, 600, 650, 700, 760, 820, 880, 940, 1010, 1080, 1150, 1220, 1300, 1380, 1460, 1550, 1640, 1730, 1830, 1930, 2030, 2140, 2250, 2370, 2490, 2620, 2750, 2890, 3030, 3180, 3330, 3490, 3650, 3820, 4000, 4180, 4370, 4570, 4770, 4980, 5200, 5420, 5650, 5890, 6140, 6400, 6670, 6950, 8260};
	const std::vector<double> g_vec_binning_new_yStar2 = {250, 390, 450, 510, 570, 630, 700, 770, 840, 910, 990, 1070, 1150, 1240, 1330, 1420, 1520, 1620, 1720, 1830, 1940, 2060, 2180, 2310, 2440, 2580, 2730, 2880, 3040, 3210, 3380, 3560, 3750, 3950, 4160, 4380, 4610, 4850, 5100, 5360, 5640, 5930, 6240, 6570, 6910, 7280, 9650};
	const std::vector<double> g_vec_binning_new_yStar3 = {450, 600, 690, 790, 890, 990, 1090, 1200, 1310, 1420, 1540, 1660, 1790, 1920, 2060, 2200, 2350, 2500, 2660, 2830, 3010, 3190, 3380, 3580, 3780, 4000, 4230, 4470, 4720, 4980, 5250, 5540, 5840, 6160, 6500, 6870, 7280, 9280};
	const std::vector<double> g_vec_binning_new_yStar4 = {730, 960, 1110, 1260, 1410, 1560, 1720, 1890, 2060, 2240, 2420, 2610, 2810, 3020, 3240, 3470, 3700, 3950, 4200, 4470, 4750, 5040, 5350, 5650, 5970, 6320, 6720, 7060, 7410, 9740};
	const std::vector<double> g_vec_binning_new_yStar5 = {1220, 1570, 1830, 2100, 2390, 2680, 3000, 3320, 3640, 3970, 4340, 4710, 5160, 5620, 6070, 6570, 7420, 10000};
	
	
	
	///
	/// updated new_yBoost binning with mcEventWeight->at(0) weights
	/// using fine 2D RooUnfildResponse
	/// InputFile: /mnt/nfs19/zaplatilek/IJXS/OUTPUT/mc/IJXSv21EM/NominalPythia_FullRun2__19_05_2022_1DMjj_2DMjjYBoost_fineBinning/y_range_00_30_00_30/MCFullRun2_normalized.root
	/// _RefPutity = 0.5, _RefStability = 0.5, _unc = 0.0
	///const std::vector<double> g_vec_binning_new_yBoost0 = {240, 280, 320, 370, 420, 480, 540, 610, 690, 770, 860, 960, 1070, 1190, 1330, 1480, 1650, 1840, 2040, 2250, 2470, 2700, 2940, 3190, 3450, 3720, 4010, 4310, 4620, 4960, 5300, 5650, 6010, 6400, 6910, 7480, 8250, 9040, 10000};
	///const std::vector<double> g_vec_binning_new_yBoost1 = {240, 280, 320, 370, 420, 470, 530, 590, 660, 740, 830, 930, 1040, 1160, 1290, 1430, 1570, 1710, 1860, 2020, 2190, 2360, 2540, 2730, 2930, 3140, 3360, 3600, 3860, 4130, 4410, 4720, 5030, 5370, 5770, 6260, 6710, 7010}; // Add 7710
	///const std::vector<double> g_vec_binning_new_yBoost2 = {250, 290, 330, 370, 420, 470, 530, 600, 670, 750, 830, 910, 1000, 1090, 1190, 1290, 1390, 1500, 1610, 1730, 1860, 1990, 2130, 2280, 2440, 2610, 2790, 2980, 3190, 3410, 3620, 3930}; // Add 4960
	///const std::vector<double> g_vec_binning_new_yBoost3 = {240, 280, 320, 360, 410, 460, 510, 570, 630, 690, 750, 820, 890, 960, 1040, 1120, 1200, 1290, 1380, 1480, 1580, 1690, 1810, 1940, 2090, 2250, 2470}; // Add 3280
	///const std::vector<double> g_vec_binning_new_yBoost4 = {240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 690, 750, 810, 880, 950, 1020, 1100, 1190, 1290, 1440}; // Add 1810
	///const std::vector<double> g_vec_binning_new_yBoost5 = {240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 700, 760, 830}; // add 1070
	
	///
	/// Update from 2024_02_29
	/// Update mjj binning from Standa 2024 - Standa found a bug in cleaning - EventClaning applied
	///                                     - Zdenek procude pQCD calculation with the same binning
	///                                     - EW corrrection requested with the same binning 
	/// Additionally email thread dicusstion with Zdenek and Standa - "Update for first mjj bins in 1.0 <y_boost < 1.5 possible" from 5.3.2024
	///         Update for 1.0 <y_boost < 1.5, yBoost2
	///         Following bin edges 40, 250, 290, 330, 370, 420, 470 replace with 40, 240, 280, 320, 360, 410, 460 to be more consistent with other mjj binning in yBoosts
	///
	const std::vector<double> g_vec_binning_new_yBoost0 = {40, 240, 280, 320, 370, 420, 480, 540, 610, 690, 770, 860, 960, 1070, 1190, 1330, 1480, 1650, 1840, 2040, 2250, 2470, 2700, 2940, 3190, 3450, 3720, 4010, 4310, 4620, 4960, 5300, 5650, 6010, 6400, 6910, 7480, 10000};
	const std::vector<double> g_vec_binning_new_yBoost1 = {40, 240, 280, 320, 370, 420, 470, 530, 590, 660, 740, 830, 930, 1040, 1160, 1290, 1430, 1570, 1710, 1860, 2020, 2190, 2360, 2540, 2730, 2930, 3140, 3360, 3600, 3860, 4130, 4410, 4720, 5030, 5370, 7550};
	const std::vector<double> g_vec_binning_new_yBoost2 = {40, 240, 280, 320, 360, 410, 460, 530, 600, 670, 750, 830, 910, 1000, 1090, 1190, 1290, 1390, 1500, 1610, 1730, 1860, 1990, 2130, 2280, 2440, 2610, 2790, 2980, 3190, 3410, 3620, 4900};
	const std::vector<double> g_vec_binning_new_yBoost3 = {40, 240, 280, 320, 360, 410, 460, 510, 570, 630, 690, 750, 820, 890, 960, 1040, 1120, 1200, 1290, 1380, 1480, 1580, 1690, 1810, 1940, 2090, 2250, 3280};
	const std::vector<double> g_vec_binning_new_yBoost4 = {40, 240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 690, 750, 810, 880, 950, 1020, 1100, 1190, 1290, 1440, 1800};
	const std::vector<double> g_vec_binning_new_yBoost5 = {40, 240, 280, 320, 360, 400, 440, 490, 540, 590, 640, 700, 760, 830, 1070};
	
	
	
	
	
	/// incl. jet pt binning
	const std::vector<double> g_vec_binning_new_y0 = {75, 88, 102, 118, 134, 150, 166, 183, 201, 219, 237, 257, 277, 297, 320, 342, 366, 390, 415, 442, 469, 497, 526, 558, 590, 623, 657, 693, 730, 770, 811, 851, 895, 940, 986, 1034, 1083, 1135, 1192, 1244, 1301, 1361, 1422, 1486, 1553, 1622, 1694, 1767, 1846, 1927, 2011, 2098, 2189, 2277, 2383, 2487, 2598, 2714, 2837, 2967, 3105, 3252, 3421, 3580, 3771, 3992, 4256, 4599, 4690};
	const std::vector<double> g_vec_binning_new_y1 = {75, 88, 102, 118, 134, 150, 166, 183, 201, 219, 237, 257, 277, 297, 320, 342, 366, 390, 415, 442, 469, 497, 526, 558, 590, 623, 657, 693, 730, 770, 811, 851, 895, 940, 986, 1034, 1083, 1135, 1192, 1244, 1301, 1361, 1422, 1486, 1553, 1622, 1694, 1767, 1846, 1927, 2011, 2098, 2189, 2277, 2383, 2487, 2598, 2714, 2837, 2967, 3105, 3252, 3393, 3580, 3771, 3937};
	const std::vector<double> g_vec_binning_new_y2 = {75, 92, 110, 126, 142, 158, 174, 192, 210, 228, 247, 267, 288, 309, 331, 354, 378, 402, 427, 455, 483, 511, 542, 574, 606, 640, 675, 711, 750, 794, 841, 884, 929, 975, 1022, 1071, 1122, 1182, 1244, 1301, 1361, 1422, 1486, 1553, 1622, 1694, 1767, 1846, 1927, 2011, 2098, 2189, 2277, 2383, 2487, 2598, 2714, 2837, 2967, 3105, 3252, 3337};
	const std::vector<double> g_vec_binning_new_y3 = {75, 88, 102, 118, 134, 150, 166, 183, 201, 219, 237, 257, 277, 297, 320, 342, 366, 390, 415, 442, 469, 497, 526, 558, 590, 627, 666, 706, 750, 794, 841, 895, 952, 1010, 1071, 1135, 1216, 1301, 1391, 1486, 1605, 1736, 1907, 2246};
	const std::vector<double> g_vec_binning_new_y4 = {75, 88, 99, 110, 122, 134, 146, 162, 179, 197, 215, 233, 252, 272, 290, 309, 331, 354, 378, 402, 427, 455, 483, 511, 542, 574, 606, 640, 675, 711, 750, 786, 830, 873, 917, 963, 1010, 1058, 1122, 1182, 1258, 1346, 1605};
	const std::vector<double> g_vec_binning_new_y5 = {75, 88, 102, 118, 134, 150, 166, 183, 201, 219, 237, 257, 277, 300, 326, 354, 384, 415, 443, 476, 511, 550, 590, 640, 693, 760, 851, 1162};
	const std::vector<double> g_vec_binning_new_y6 = {75, 95, 118, 142, 166, 192, 224, 257, 297, 348, 415, 786};
	const std::vector<double> g_vec_binning_new_y7 = {75, 88, 102, 118, 134, 150, 166, 183, 201, 224, 247, 277, 320, 442};
	const std::vector<double> g_vec_binning_new_y8 = {75, 92, 114, 138, 290};
};

namespace GlobalLatex
{
	TString g_desc      = "Non-perturbative corrections";
	TString g_ATLASSim  = "ATLAS Simulation";
	TString g_eng       = "#sqrt{s} = 13 TeV";
	TString g_antKt     = "anti-kt R=0.4";
	TString g_yVarRange = "0.0<y*0.5";
	TString g_tune      = "";
}

enum release {rel19, rel21, rel23};
enum unit {nb, pb};

namespace Rivet
{
	
	enum RivetRoutine{Aliaksei, Ota_rel19, Ota_rel21, Ota_rel23};
	RivetRoutine rivet = RivetRoutine::Ota_rel19;
	
	TString Get_RivetRoutineName( RivetRoutine _rivet)
	{
		TString rivetName;
		switch(_rivet) 
		{
		  case RivetRoutine::Ota_rel19 :
			rivetName = "ATLAS_2022_IJXS_IDXS_Rivet_2_2_1";
			break;
		  case RivetRoutine::Ota_rel21:
			rivetName = "ATLAS_2022_IJXS_IDXS_Rivet_2_3_0";
			break;
		  case RivetRoutine::Ota_rel23:
			rivetName = "ATLAS_2023_IJXS_IDXS_Rivet_3_1_8";
			break;
		  default:
			rivetName = "ATLAS_2022_IJXS_IDXS_Rivet_2_2_1";
		}
		
		return(rivetName);
	}
	
	/// name of histograms
	vector<TString> v_hName_Ota = {
								
								/// dijet mjj yStar
								"h1_mjj_ystar0",
								"h1_mjj_ystar1",
								"h1_mjj_ystar2",
								"h1_mjj_ystar3",
								"h1_mjj_ystar4",
								"h1_mjj_ystar5",
								/// dijet mjj yBoost
								
								"h1_mjj_yboost0",
								
								"h1_mjj_yboost1",
								"h1_mjj_yboost2",
								"h1_mjj_yboost3",
								"h1_mjj_yboost4",
								"h1_mjj_yboost5",
								
								/// incl. jet pt y
								"h1_pt_y0",
								"h1_pt_y1",
								"h1_pt_y2",
								"h1_pt_y3",
								"h1_pt_y4",
								"h1_pt_y5",
								"h1_pt_y6",
								"h1_pt_y7",
								"h1_pt_y8",
								};
	
	vector<TString> v_hName_Aliaksei = 	{
								/// dijet mjj yStar
								"dijet_invmass_ystar0_AKT04",
								"dijet_invmass_ystar1_AKT04",
								"dijet_invmass_ystar2_AKT04",
								"dijet_invmass_ystar3_AKT04",
								"dijet_invmass_ystar4_AKT04",
								"dijet_invmass_ystar5_AKT04",
								/// incl jet pt |y|
								"jet_pt_y0_AKT04",
								"jet_pt_y1_AKT04",
								"jet_pt_y2_AKT04",
								"jet_pt_y3_AKT04",
								"jet_pt_y4_AKT04",
								"jet_pt_y5_AKT04",
								"jet_pt_y6_AKT04",
								"jet_pt_y7_AKT04",
								"jet_pt_y8_AKT04"
								};
	
	vector<TString> Get_vector_hNames(RivetRoutine _rivet)
	{
		vector<TString> v_hNames;
		
		if (_rivet == RivetRoutine::Aliaksei)
		{
			copy(v_hName_Aliaksei.begin(), v_hName_Aliaksei.end(), back_inserter(v_hNames));  
		}
		else
		{
			copy(v_hName_Ota.begin(), v_hName_Ota.end(), back_inserter(v_hNames));  
		}
		
		return(v_hNames);
	}
	
	
	/// name of TDirectory as RivetName
	TString RivetRoutineName           = Rivet::Get_RivetRoutineName( rivet );
	//const vector<TString> v_hName      = Get_vector_hNames( rivet );
} 

namespace GlobalFunctions
{
	TString g_xVarName = "mjj";
	TString g_xVar     = "m_{jj}";
	
	TString g_yVarName = "ystar";
	TString g_yVar     = "y*";
	
	unit xs_unit = pb;
	release rel = rel19;
}


void BinIgnoreAutomatic(TH1D  * _h, double _deltaCut = 3.0)
{
	///
	/// Reset bin content and bin error
	/// If there is a step - abs(val_now  - val_previous)/abs(val_next - val_previous) > _cut
	///
	/// TH1D  * _h       as input histohram to bin modified
	/// double _deltaCut as a parameter for the algorithm
	///
	
	
	/// Loop over bins
	int nBins = _h->GetNbinsX();
	for (Int_t i = 1; i < nBins + 1; i++)
	{
		/// Check as you can eval previous and next bin
		if (i > 0 && i < nBins)
		{
			/// Evaluate previous bin content
			double val_prev = _h->GetBinContent(i - 1);
				/// Make a double-check if you already reset previous bin content in last passege of the loop
				/// If so, then iteratare over all previous bins and find recursively closest non-zero bin content
				if(val_prev == 0.0)
				{
					for (int j = i; j >= 0; j--)
					{
						double val_j = _h->GetBinContent(j    );
						if (val_j != 0.0)
						{
							/// Ok you found it, end the loop with break
							val_prev = val_j;
							break;
						}
					}
				}
			double val_now  = _h->GetBinContent(i    );
			double val_next = _h->GetBinContent(i + 1);
				/// Make a double-check if you already reset next bin
				/// If so, then iteratare over all next bins and find closest non-zero bin content
				if(val_next == 0.0)
				{
					for (int j = i; j <= nBins; j++)
					{
						double val_j = _h->GetBinContent(j    );
						if (val_j != 0.0)
						{
							/// Ok you found it, end the loop with break
							val_next = val_j;
							break;
						}
					}
				}
			double delta      = fabs( val_next - val_prev );
			double delta_prev = fabs( val_next - val_now ) ;
			double delta_next = fabs( val_now  - val_prev );
			
			cout << "\n\n\n" << endl;
			cout << "bin i: " << i << endl;
			cout << "val_prev , val_now , val_next:                     " << val_prev << " , " << val_now << " , " << val_next << endl;
			cout << "delta_prev , delta , delta_next:                   " << delta_prev << " , " << delta << " , " << delta_next << endl;
			cout << "delta_prev/delta , delta/delta , delta_next/delta: " << delta_prev/delta << " , " << delta/delta << " , " << delta_next/delta << endl;
			  
			if ( delta_prev/delta > _deltaCut || delta_next/delta > _deltaCut)
			{
				_h->SetBinContent(i, 0.0);
				_h->SetBinError(i,   0.0);
				
				cout << "Reset value and error in bin i: " << i  << endl;
				int stop; cin >> stop;
			}
		}
	}
}


TH1D * BinIgnoring(TH1D * _h, TString _hName, TString _config_path)
{
	/// Histogram to return
	TH1D * h = new TH1D;
	h = (TH1D *) _h->Clone();
	
	/// Load CONFIG
	CONFIG c(_config_path);
	TString key = "vec_int_BinIgnore__" + _hName;
	
	cout << "key: " << key << endl;
	vector<int> v_bins = c.Load_vector_int(key);
	
	for (auto item: v_bins){cout << item << ", ";}
	
	//int stop; cin >> stop;
	int nBins = h->GetNbinsX();
	for (Int_t i = 1; i < nBins + 1; i++)
	{
		if ( std::find(v_bins.begin(), v_bins.end(), i) != v_bins.end() )
		{
			h->SetBinContent(i, 0.0);
			h->SetBinError(i, 0.0);
		}
	}
	
	/// BinIgnoreAutomatic
	bool    b_BinIgnoreAutomatic = true;
	double  delta_cut = 2.5;
	if (b_BinIgnoreAutomatic == true)
	{
		BinIgnoreAutomatic(h, delta_cut);
	}
	
	return(h);
}

TH1D * Derivative(TH1D * _h)
{
	TH1D * h_derivative = new TH1D();
	h_derivative = (TH1D *) _h->Clone();
	h_derivative->Reset();
	
	for (Int_t i = 1; i < _h->GetNbinsX(); i++)
	{
		double y_i     = _h->GetBinContent(i);
		double y_ii    = _h->GetBinContent(i + 1);
		double y_delta = abs(y_ii - y_i);
		
		double h = ( _h->GetBinWidth(i) + _h->GetBinContent(i + 1) ) / 2.0 ;
		double d = y_delta / h;
		h_derivative->SetBinContent(i, d);
	}
	return(h_derivative);
}

void Function_DrawLatex()
{
	TLatex * latex = new TLatex();
	latex->SetNDC();
	latex->SetTextSize(0.05);
	latex->SetTextFont(72);
	
	//vector<TString> v_text = {GlobalLatex::g_desc, GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	vector<TString> v_text = {GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	
	for (unsigned int iStr=0; iStr < v_text.size(); iStr++)
	{
		latex->DrawLatex(0.4, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
	}
}

void Function_DrawLatex_NPC()
{
	TLatex * latex = new TLatex();
	latex->SetNDC();
	latex->SetTextSize(0.04);
	latex->SetTextFont(72);
	
	//vector<TString> v_text = {GlobalLatex::g_desc, GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	vector<TString> v_text = {GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	
	for (unsigned int iStr=0; iStr < v_text.size(); iStr++)
	{
		//latex->DrawLatex(0.13, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
		latex->DrawLatex(0.2, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
	}
}


vector<TH1D *> RatioHistograms(vector<TH1D *> & _v, TH1D * _h_nominal)
{
	vector<TH1D *> v_ratios;
	for (auto h: _v)
	{
		TH1D * h_ratio = (TH1D *) h->Clone();
		h_ratio->Sumw2();
		h_ratio->Divide(_h_nominal);
		v_ratios.push_back(h_ratio);
	}
	return(v_ratios);
}

TH1D * ApplyLinearInterpolation(TH1D * _h)
{
	TH1D * h = new TH1D;
	h = (TH1D *) _h->Clone();
	
	for (int i = 1; i < _h->GetNbinsX() + 1; i++)
	{
		double val = _h->GetBinContent(i);
		if (val == 0.0)
		{
			/// init
			int    bin_prev = -999;
			int    bin_next = -999;
			double val_prev = -999;
			double val_next = -999;
			
			
			/// find firt non-zero value in front of bin i
			for (int j = i; j > 0; j--)
			{
				double val_j = _h->GetBinContent(j);
				if (val_j != 0.0)
				{
					bin_prev = j;
					val_prev = val_j;
					break;
				}
			}
			
			if (val_prev == -999)
			{
				bin_prev = 1;
				val_prev = _h->GetBinContent(1);
			}
			
			/// find first non-zero value behind bin i
			for (int j = i; j < _h->GetNbinsX() + 1; j++)
			{
				double val_j = _h->GetBinContent(j);
				if (val_j != 0.0)
				{
					bin_next = j;
					val_next = val_j;
					break;
				}
			}
			
			if (val_next == -999)
			{
				bin_next = _h->GetNbinsX();
				val_next = _h->GetBinContent( bin_next );
			}
			
			/// widths
			double width_i = _h->GetBinWidth(i);
			double width_prev_next =_h->GetBinLowEdge( bin_next) +  _h->GetBinWidth( bin_next ) - _h->GetBinLowEdge( bin_prev );
			
			/// linear extrapolation
			double new_val = _h->GetBinContent( bin_prev ) + ( _h->GetBinContent( bin_next ) - _h->GetBinContent( bin_prev ) ) * ( width_i / width_prev_next );
			double new_error = ( _h->GetBinError( bin_prev ) + _h->GetBinError( bin_next ) ) / 2.0;

			h->SetBinContent(i, new_val);
			h->SetBinError(i, new_error);

		}
	}
	
	return(h);
}

void FitFunction(TH1D * _h_npc)
{
	int nBins   = _h_npc->GetNbinsX();
	//double xmin = _h_npc->GetXaxis()->GetBinCenter(1);
	//double xmax = _h_npc->GetXaxis()->GetBinCenter(nBins - 3);
	
	//double xmin = _h_npc->GetXaxis()->GetBinCenter(3);
	//double xmax = _h_npc->GetXaxis()->GetBinCenter(nBins-3);
	
	///TF1 * fit = new TF1("fit_npc", "[0] - [1]*TMath::Exp(-[2]*x) + [3]*TMath::Exp(+[4]*x - [5])", xmin, xmax);
	///      fit->SetParNames("a", "b", "c", "d", "e", "f");
	///      fit->SetParameters(1.0, 0.1, 0.03, 0.05, 0.03, _h_npc->GetXaxis()->GetBinCenter(nBins));
	
	TString h1_name  = _h_npc->GetName();
	TString h1_title = _h_npc->GetTitle();
	cout << "h1_name:  " << h1_name << endl;
	cout << "h1_title: " << h1_title << endl;
	
	TF1 * fit = new TF1();
	//TF1 * fit_exp = new TF1("fit_npc_exp", "[0] - [1]*TMath::Exp(-[2]*x)");
	TF1 * fit_lin = new TF1("fit_npc_lin", "[0] + [1]*x");
	
	/*
	if ( h1_name.Contains("yboost") == false )
	{
	      cout << "exp fit" << endl;
	      fit = fit_exp;
	      fit->SetParNames("a", "b", "c");
	      if (h1_title.Contains("Pythia"))   fit->SetParameters(1.0, 0.1,  0.03);
	      if (h1_title.Contains("Herwigpp")) fit->SetParameters(0.99, 0.098, 0.004);
	      fit->SetLineColor( _h_npc->GetLineColor() );
	  
	}
	else
	{
	*/
	      cout << "linear fit" << endl;
	      fit = fit_lin;
	      //fit = new TF1("fit_npc_lin", "[0] + [1]*x");
	      fit->SetParNames("a", "b");
	      if (h1_title.Contains("Pythia"))   fit->SetParameters(1.0, -0.1);
	      if (h1_title.Contains("Herwigpp")) fit->SetParameters(1.0, +0.1);
	      fit->SetLineColor( _h_npc->GetLineColor() );
	/*
	}
	int stop; cin >> stop;
	*/
	      
	/// “I” Use integral of function in bin instead of value at bin center
	/// “WL” Weighted log likelihood method. To be used when the histogram has been filled with weights different than 1.
	/// “V” Verbose mode (default is between Q and V)
	/// “E” Perform better errors estimation using the Minos technique
	/// “M” Improve fit results, by using the IMPROVE algorithm of TMinuit.
	///_h_npc->Fit(fit->GetName(), "W WL V E M", "", xmin, xmax);
	
	/// Find best fit interval
	bool b_doBest = true;
	double ch2_best = 1E5;
	double xmin_best = _h_npc->GetXaxis()->GetBinCenter(1);
	double xmax_best = _h_npc->GetXaxis()->GetBinCenter(nBins);
	
	if (b_doBest)
	{
		int nBins_begin = 1;
		int nBins_last  = 5;
		
		if (h1_name.Contains("ystar") or h1_name.Contains("yboost") ) nBins_begin = 2;

		vector <int> v_begin; for (int i = 1; i < nBins_begin + 1; i++){ v_begin.push_back(i); }
		vector <int> v_last;  for (int i = nBins; i > nBins - nBins_last; i--){ cout << i << ", "; v_last.push_back(i); }
		cout << endl;
		std::vector< std::pair<int, int> > v_pairs;
		
		for (int begin: v_begin)
		{
			for (int last: v_last)
			{
				if (begin < last)
				{
					v_pairs.push_back( std::pair<int,int>(begin, last) );
				}
			}
		}
		
		for (auto p: v_pairs)
		{
			int begin = p.first;
			int last = p.second;
			cout << "begin, last: " << begin << "\t" << last << endl;
		}
		//int stop; cin >> stop;
		
		for (auto p: v_pairs)
		{
			int begin = p.first;
			int last = p.second;
			
			double xmin = _h_npc->GetBinCenter(begin);
			double xmax = _h_npc->GetBinCenter(last);
			
			/// “I” Use integral of function in bin instead of value at bin center
			/// “WL” Weighted log likelihood method. To be used when the histogram has been filled with weights different than 1.
			/// “V” Verbose mode (default is between Q and V)
			/// “E” Perform better errors estimation using the Minos technique
			/// “M” Improve fit results, by using the IMPROVE algorithm of TMinuit.
			_h_npc->Fit(fit->GetName(), "W WL V E M", "", xmin, xmax);
			double ch2 = fit->GetChisquare();
			double ndf = fit->GetNDF();
			
			cout << "ch2:     " << ch2 << endl;
			cout << "ndf:     " << ndf << endl;
			cout << "ch2/ndf: " << ch2/ndf << endl;
			
			if (ch2/ndf < ch2_best )
			{
				xmin_best = xmin;
				xmax_best = xmax;
				
			}
		}
	}
	/// “I” Use integral of function in bin instead of value at bin center
	/// “WL” Weighted log likelihood method. To be used when the histogram has been filled with weights different than 1.
	/// “V” Verbose mode (default is between Q and V)
	/// “E” Perform better errors estimation using the Minos technique
	/// “M” Improve fit results, by using the IMPROVE algorithm of TMinuit.
	_h_npc->Fit(fit->GetName(), "W WL V E M", "", xmin_best, xmax_best);
	_h_npc->GetListOfFunctions()->Add(fit);
	//int stop; cin >> stop;
	
	//_h_npc->Draw("PSame");
	
	/*
	TF1 * fit_exps = new TF1("fit_npc_exps", "[0] - [1]*TMath::Exp(-[2]*x - [3]) - [4]*TMath::Exp(+[5]*x - [6])", xmin, xmax);
	      fit_exps->SetParNames("const", "A_1", "delta_1", "mu_1", "A_2", "delta_2", "mu_2");
	      fit_exps->SetParameters(1.0, 0.1, 10.0, 0.03, 0.1, -0.03, 1000.0);
	      fit_exps->SetLineColor(_h_npc->GetLineColor() );
	      fit_exps->SetLineStyle(3);
	      
	      _h_npc->Fit(fit_exps->GetName(), "I WL V E M", "", xmin, xmax);
	      _h_npc->GetListOfFunctions()->Add(fit_exps);
	      //_h_npc->SetLineColor(kGreen);
	*/
	
	/*
	vector <double > v_val;
	vector <double> v_err;
	for (int i = 1; i < _h_npc->GetNbinsX()+ 1; i++)
	{
		v_val.push_back(_h_npc->GetBinContent(i));
		v_err.push_back(_h_npc->GetBinError(i));
	}
	
	double rho = 1.0; //default value
	TKDE * kde = new TKDE(v_val.size(), v_val.data(), v_err.data(), _h_npc->GetBinLowEdge(1), _h_npc->GetBinLowEdge( _h_npc->GetNbinsX() ) + _h_npc->GetXaxis()->GetBinWidth( _h_npc->GetNbinsX() ), "", rho);
	//kde->Draw("ConfidenceInterval@0.95 Same");
	kde->Draw("SAME");
	*/

}



TH1D * GaussianKernelSmoothing(TH1D * _histCorr, double _h=1, int _method = 3, bool _debug = false, double _width_a=60, double _width_b=0.08)
{
	/// Smoothed bin contents
	vector <double> newBinContents;
	vector <double> newBinErrors;
	
	/// Loop over bins of the input histogram
	for (int i = 1; i < _histCorr->GetNbinsX() + 1; i++)
	{
		/// Get the bin center and the gaussian kernel width
		double xi    = _histCorr->GetBinCenter(i);
		double width = _h;
		
		if ( _method == 1 )
		{
			/// Use the constant width parameter
			width = _h;
		}
		else if ( _method == 3 )
		{
			/// Use variable bin width (linear function)
			width = ( _width_a + xi * _width_b ) * _h;
		}
		else
		{
			//raise RuntimeError("Undefined smoothing method {}".format(method))
			TString msg = "Undefined smoothing method: ";
			        msg += TString(_method);
			        
			throw std::invalid_argument( msg.Data() );
		}
		
		if (_debug)
		{
			cout << "gaussianKernelSmoothing: bin " <<  i << " : " << width << endl;
		}
		
		/// Initialize the new bin content values
		double weightedSum  = 0;
		double sumOfWeights = 0;
		
		
		// Add by Ota
		double err_weightedSum  = 0;
		double err_sumOfWeights = 0;
		double err_sumOfWeightsSquare = 0;
		
		/// Loop over second time to calculate the weighted sums
		if ( width != 0 )
		{
			for (int j = 1; j < _histCorr->GetNbinsX() + 1; j++)
			{
				double xp = _histCorr->GetBinCenter(j);
				
				/// calculate the bin weight
				double weight = TMath::Exp( -0.5 * TMath::Power( (xp-xi)/width, 2));
				
				if (_histCorr->GetBinContent(j) != 0.0)
				{
					/// Add to the total weighted sum and sum of weights
					weightedSum  += _histCorr->GetBinContent(j) * weight;
					sumOfWeights += weight;
					
					// Add by Ota
					err_weightedSum  += _histCorr->GetBinError(j) * weight;
					err_sumOfWeights += weight;
					
					err_sumOfWeightsSquare += (_histCorr->GetBinError(j) * weight) * (_histCorr->GetBinError(j) * weight);
				}
			}
		}
		
		/// Store the new bin content
		if ( sumOfWeights != 0 )
		{
			newBinContents.push_back( weightedSum / sumOfWeights );
			
			// Add by Ota
			//newBinErrors.push_back( err_weightedSum /  err_sumOfWeights);
			newBinErrors.push_back( sqrt(err_sumOfWeightsSquare)  );
		}
		else
		{
			newBinContents.push_back( _histCorr->GetBinContent(i) );
			
			// Add by Ota
			newBinContents.push_back( _histCorr->GetBinError(i) );
		}
	}
	
	
	TH1D * h_new = new TH1D();
	       h_new = (TH1D *) _histCorr->Clone();
	       h_new->Reset();
	
	/// Update the bin contents
	///for i,newContent in zip( range(1, histCorr.GetNbinsX() + 1), newBinContents ):
	for ( int i = 1; i < h_new->GetNbinsX() + 1; i++ )
	{
		// print(histCorr.GetBinContent(i))
		double val_new = newBinContents.at(i - 1);
		
		// Add by Ota
		double err_new = newBinErrors.at(i - 1);
		
		if (_histCorr->GetBinContent(i) != 0.0)
		{
			h_new->SetBinContent(i, val_new);
			
			// Add by Ota
			h_new->SetBinError(i, err_new);
		}
		// print(histCorr.GetBinContent(i))
	}
	
	return (h_new);
}



//void Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
//TGraphAsymmErrors * Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
std::pair<TCanvas *, TGraphAsymmErrors *> Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
{
	///
	/// One pad Draw function
	///
	bool b_logX = _logX;
	bool b_logY = _logY;
	
	std::vector<TH1D *> v_copy; 
	copy(_v.begin(), _v.end(), back_inserter(v_copy));
	
	/// Canvas
	TCanvas * c = new TCanvas("c", "canvas", 800, 800);
	gROOT->ForceStyle();
	gStyle->SetOptStat(0);
	
	/// Upper plot will be in pad1
	TPad *pad1 = new TPad("pad1", "pad1", 0.0, 0.0, 1.0, 1.0);
	//pad1->SetBottomMargin(0.12); 
	//pad1->SetRightMargin(0.05);
	//pad1->SetTopMargin(0.08);
	pad1->SetBottomMargin(0.12); 
	pad1->SetLeftMargin(0.125);
	pad1->SetRightMargin(0.05);
	pad1->SetTopMargin(0.08);
	
	pad1->SetTicks();
	pad1->SetGridx();
	
	if (b_logX == true) pad1->SetLogx();
	if (b_logY == true) pad1->SetLogy();
	
	pad1->Draw();
	pad1->cd();
	
	/// Legend
	TLegend * legend;
	if (_legend != nullptr) legend = (TLegend *) _legend->Clone();
	else                    legend = new TLegend(0.75, 0.45, 0.88, 0.9);
	legend->SetFillColor(0);
	legend->SetFillStyle(0);
	legend->SetBorderSize(0);
	//legend->SetTextSize(0.0325); // * 6.0/_v.size());
	
	
	/// TGraph - envelope of all tunes
	/// Init vectors for TGraph
	TH1D * h_tmp = v_copy.front();
	
	vector<double> x_val;
	vector<double> x_low;
	vector<double> x_high;
	vector<double> y_val;
	vector<double> y_err_down;
	vector<double> y_err_up;
	
	/// Find Pythia ATLAS14NNPDF for central val. of the envelope
	TH1D * h_envelope_centralVal = nullptr;
	
	for (TH1D * h: v_copy)
	{
		TString hName_tmp = h->GetName();
		TString hTitle_tmp = h->GetTitle();
		if (hName_tmp.Contains("ATLAS14NNPDF") || hTitle_tmp.Contains("ATLAS14NNPDF"))
		{
			h_envelope_centralVal = (TH1D *) h->Clone();
		}
	}
	/// Loop over all bins
	for (int i=1; i < h_tmp->GetNbinsX() + 1; i++)
	{
		/// x-axis values
		x_val.push_back (h_tmp->GetBinCenter(i));
		x_low.push_back (h_tmp->GetBinCenter(i)  - h_tmp->GetBinLowEdge(i));
		x_high.push_back(h_tmp->GetBinLowEdge(i) + h_tmp->GetBinWidth(i) - h_tmp->GetBinCenter(i));
		
		/// y-axis values
		double up   = -999;		/// up unc.
		double down =  999;		/// down unc.
		
		/// Loop over all tunes
		/// find min and max for the envelope
		for (unsigned int ih = 0; ih < v_copy.size(); ih++)
		{
			TH1D * h = v_copy.at(ih);
			double val_i  = h->GetBinContent(i);
			if (val_i > up)   up   = val_i;
			if (val_i < down) down = val_i;
		}
		
		/// value should be approx 1.0, but more save will be use aritmetics mean
		double mean = (up + down)/2.0;
		double centralVal = mean;
		if (h_envelope_centralVal != nullptr)
		{
			centralVal = h_envelope_centralVal->GetBinContent(i);
		}
		
		y_val.push_back(centralVal);
		y_err_down.push_back(centralVal - down);
		y_err_up.push_back(up - centralVal);
	}
	
	//for (int i = 0; i < x_val.size(); i++)
	//{
	//	cout << i << "\t bin, x "  << x_val.at(i) << " (" << x_low.at(i) << ", "  << x_high.at(i) << ") \t y:" << y_val.at(i) << " ( " << y_err_down.at(i) << ", " << y_err_up.at(i) << ")" << endl;
	//}
	//int top; cin >> top;
	
	TGraphAsymmErrors * g_envelope = new TGraphAsymmErrors(	x_val.size(), 
															x_val.data(), 
															y_val.data(),
															x_low.data(),
															x_high.data(),
															y_err_down.data(),
															y_err_up.data() );
	TString gName = "g_npc"; 
	if (_outName.Contains("raw"))           gName += "_raw";
	else if (_outName.Contains("interpol")) gName += "_interpol";
	else if (_outName.Contains("ignore"))   gName += "_ignore";
	else if (_outName.Contains("smooth"))   gName += "_smooth";
	
	gName += "_envelope_"+ GlobalFunctions::g_xVarName + "_" + GlobalFunctions::g_yVarName;
		vector <TString> v_ind = {"0", "1", "2", "3", "4", "5", "6", "7", "8"};
		/// index
		for (TString ind: v_ind)
		{
			TString yVarI = GlobalFunctions::g_yVarName + ind;
			/// test index
			if (_outName.Contains(yVarI))
			{
				gName += ind;
				break;
			}
		}
	
	g_envelope->SetName(gName);
	g_envelope->SetTitle(gName);
	g_envelope->GetXaxis()->SetTitle(GlobalFunctions::g_yVarName);
	g_envelope->GetXaxis()->SetTitle("NPC Envelope [-]");
	
	/// Apply graphics function
	if (pf_pad1_Asym != nullptr) pf_pad1_Asym(g_envelope);
	
	/// Draw envelope at background of the canvas
	g_envelope->Draw("PSame a2 E");
	
	
	/// Loop over histograms
	for (unsigned int i = 0; i < v_copy.size(); i++)
	{	
		TH1D * h = v_copy.at(i);
		
		/// Apply graphics
		h->SetStats(0);
		h->GetXaxis()->SetTitleSize(38);
		h->GetYaxis()->SetTitleSize(38);
		
		h->GetXaxis()->SetTitleFont(43);
		h->GetYaxis()->SetTitleFont(43);
		
		h->GetXaxis()->SetTitleOffset(1.0);
		h->GetYaxis()->SetTitleOffset(1.0);
		
		h->SetMarkerColor( h->GetLineColor() );
		h->SetLineWidth(2);
		h->SetMarkerSize(2);
		
		/// Draw
		TString title = h->GetTitle();
		
		/// Title for legend
		TString l_desc = title;
		for (int i = 0; i < l_desc.Length(); i++)
		{
			char ch = l_desc[i];
			if (ch == '_')
			{
				if (i < l_desc.Length ())
				{
					if (l_desc[ i + 1] == '{')
					{
						continue;
					}
				}
				l_desc.Replace(i, 1, ' ');
			}
		}
		
		/// Call fit function - need to have name of generator in title
		h->Draw("p same E");
		if (_doFit) FitFunction(h);
		
		h->SetTitle("");
		h->Draw("p same E");
		
		if (pf_pad1!= nullptr) pf_pad1(h);
		
		/// Add to legend
		legend->AddEntry(h, l_desc.Data(), "lep");
		
		TLatex * latex = new TLatex();
		latex->SetNDC();
		latex->SetTextSize(0.05);
		latex->SetTextFont(72);
		
		//latex->DrawLatex(0.15, 0.94, "ATLAS, Work in progress");
		latex->DrawLatex(0.15, 0.94, "ATLAS Internal");
		
		if (pf_drawLatex != nullptr) pf_drawLatex();
	
			
		
		
	}
	
	legend->AddEntry(g_envelope, "Sys. unc.", "f");
	
	/// Draw legend
	legend->Draw();
	
	/// Save Plot
	c->SaveAs(_outName);
	
	//return(g_envelope);
	std::pair <TCanvas *, TGraphAsymmErrors *> p_return = std::make_pair(c, g_envelope);
	return( p_return );
}

///


TCanvas * Draw (vector<TH1D *> &_v, TString _outName, TH1D * _h_nominal, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad2) (TH1D*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
{
	bool b_logX = _logX;
	bool b_logY = _logY;
	
	long unsigned int ind_nominal = -1;
	std::vector<TH1D *> v_copy; 
	
	for (long unsigned int i = 0; i < _v.size(); i++)
	{
		TH1D * h = _v.at(i);
		TH1D * h_clone = (TH1D *) h->Clone();
		v_copy.push_back(h_clone);
		
		if (h == _h_nominal) ind_nominal = i;
	}
	/// Canvas
	TCanvas * c = new TCanvas("c", "canvas", 1200, 800);
	gROOT->ForceStyle();
	gStyle->SetOptStat(0);
	
	/// Upper plot will be in pad1
	TPad *pad1 = new TPad("pad1", "pad1", 0.025, 0.5, 1, 1.0);
	pad1->SetBottomMargin(0.005); 
	pad1->SetRightMargin(0.05);
	pad1->SetTopMargin(0.08);
	pad1->SetTicks();
	pad1->SetGridx();
	
	if (b_logX == true) pad1->SetLogx();
	if (b_logY == true) pad1->SetLogy();
	
	pad1->Draw();
	
	/// Lower plot of ratio will be in pad2
	TPad *pad2 = new TPad("pad2", "pad2", 0.025, 0.05, 1, 0.495);
	pad2->SetBottomMargin(0.20);
	pad2->SetTopMargin(0.025);
	pad2->SetRightMargin(0.05);
	
	pad2->SetGridx();
	pad2->SetGridy();
	if (b_logX == true) pad2->SetLogx();
	// if (b_logY == true) pad2->SetLogy(); 
	pad2->Draw();
	
	pad1->cd();
	
	
	/// Legend
	TLegend * legend;
	if (_legend != nullptr) legend = (TLegend *) _legend->Clone();
	else                    legend = new TLegend(0.75, 0.45, 0.88, 0.9);
	legend->SetFillColor(0);
	legend->SetFillStyle(0);
	legend->SetBorderSize(0);
	//legend->SetTextSize(0.05 * 6.0/_v.size());
	legend->SetTextSize(0.05); // * 6.0/_v.size());
	
	
	TH1D * h_nominal = nullptr; //new TH1D();
	if (_h_nominal != nullptr)
	{
		h_nominal = (TH1D *) _h_nominal->Clone();
		h_nominal->SetMarkerStyle(24);
	}
	else
	{
		h_nominal = v_copy.at(0);
		h_nominal->SetMarkerStyle(24);
	}
	
	
	
	vector<TH1D *> v_ratio = RatioHistograms(_v, h_nominal);
	//double yMin_ratio = GetMinYAxis(v_ratio);
	//double yMax_ratio = GetMaxYAxis(v_ratio);
	
	//double yMin = GetMinYAxis(_v);
	//double yMax = GetMaxYAxis(_v);
	
	cout << _v.size();
	/// Loop over defined indBins of combined yStar and yBoost bins
	for (unsigned int i = 0; i < v_copy.size(); i++)
	{	
		TH1D * h = v_copy.at(i);
		
		/// Apply graphics
		h->SetStats(0);
		h->GetYaxis()->SetTitleSize(30);
		h->GetYaxis()->SetTitleFont(43);
		h->GetYaxis()->SetTitleOffset(1.1);
		h->GetYaxis()->SetLabelSize(25);
		h->GetYaxis()->SetLabelFont(44); // Absolute font size in pixel (precision 3)
		
		h->GetXaxis()->SetTitleOffset(3.0);
		h->GetYaxis()->SetTitleOffset(1.3);
		
		h->SetLineWidth(2);
		h->SetMarkerColor( h->GetLineColor());
		
		h->SetLineWidth(2);
		
		//if (yMin <= 0.0 && b_logY == true)
		//{
		//	h->SetMinimum(1.0E-9);
		//}
		//else
		//{
		//	h->SetMinimum(yMin);
		//}
		//h->SetMaximum(yMax);
		
		/// Draw
		pad1->cd();
		TString title = h->GetTitle();
		TString l_desc = title;
		for (Ssiz_t i = 0; i < l_desc.Length(); i++)
		{
			char ch = l_desc[i];
			if (ch == '_')
			{
				if (i < l_desc.Length ())
				{
					if (l_desc[ i + 1] == '{')
					{
						continue;
					}
				}
				l_desc.Replace(i, 1, ' ');
			}
		}
		
		cout << "h_nominal->GetName():  \"" << h_nominal->GetName() << "\"" << endl;
		cout << "h->GetName():          \"" << h->GetName() << "\"" << endl;
		cout << "h_nominal->GetTitle(): \"" << h_nominal->GetTitle() << "\"" << endl;
		cout << "h->GetTitle():         \"" << h->GetTitle() << "\"" << endl;
		
		if (TString(h_nominal->GetName()) == TString(h->GetName()) && TString(h_nominal->GetTitle()) == TString(h->GetTitle()))
		{
			l_desc += " (nominal)";
		}
		cout << "l_desc: " << l_desc << endl;
		
		//TString title = h->GetTitle();
		h->SetTitle("");
		h->Draw("p same E");
		
		if (pf_pad1!= nullptr) pf_pad1(h);
		
		/// Add to legend
		legend->AddEntry(h, l_desc.Data(), "lep");
		
		TLatex * latex = new TLatex();
		latex->SetNDC();
		latex->SetTextSize(0.05);
		latex->SetTextFont(72);
		
		//latex->DrawLatex(0.15, 0.94, "ATLAS, Work in progress");
		latex->DrawLatex(0.15, 0.94, "ATLAS Internal");
		
		if (pf_drawLatex != nullptr) pf_drawLatex();
	
		if (h_nominal != nullptr)
		{	
			pad2->cd();
			if (i == ind_nominal) continue; // skip ratio like nominal/nominal
			
			TH1D * h_ratio = (TH1D *) h->Clone();
			h_ratio->SetTitle(title + " - ratio");
			h_ratio->Sumw2();
			h_ratio->Divide(h_nominal);
				
			h_ratio->SetStats(0);      // No statistics on lower plot
			
			h_ratio->SetMarkerColor( h_ratio->GetLineColor());
			h_ratio->SetLineWidth(2);
			
			h_ratio->GetYaxis()->SetTitleSize(30);
			h_ratio->GetYaxis()->SetTitleFont(43);
			h_ratio->GetYaxis()->SetTitleOffset(1.25);
			h_ratio->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
			h_ratio->GetYaxis()->SetLabelSize(25);
			
			/// x-axis ratio plot settings
			h_ratio->GetXaxis()->SetTitleSize(30);
			h_ratio->GetXaxis()->SetTitleFont(43);
			h_ratio->GetXaxis()->SetTitleOffset(2.3);
			h_ratio->GetXaxis()->SetLabelSize(0.08);
			
			TString y_title = "sample/nominal"; //y_title += h_nominal->GetTitle();
			h_ratio->GetYaxis()->SetTitle(y_title.Data());
			
			h_ratio->SetMinimum(0.0);
			h_ratio->SetMaximum(3.0);
			
			if (pf_pad2!= nullptr) pf_pad2(h_ratio);
			
			h_ratio->SetTitle("");       
			
			/*
			//h_ratio->Fit("pol4");
			TF1 * fit = new TF1("fit", "[0] - [1]*TMath::Exp(-[2]*x)", 100.0, 3000.0);
			//fit->SetParameters(1.0, -1.0, 1.0);
			fit->SetParNames("a", "b", "c");
			fit->SetParameters(1.0, 0.1, 0.03);
			h_ratio->Fit(fit->GetName());
			*/
			
			if (_doFit) FitFunction(h_ratio);
			
			//pad2->SetLogy();
			h_ratio->Draw("p same E");
			
		}
	}
	
	pad1->cd();

	/// Draw legend
	legend->Draw();
	
	/// Save Plot
	c->SaveAs(_outName);
	
	return(c);
}


void Pad1Function(TH1D * _h)
{
	cout << "Pad1Funtion" << endl;
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	if ( GlobalFunctions::xs_unit == unit::pb ) _h->GetYaxis()->SetTitle("d^{2}#sigma/d" + GlobalFunctions::g_xVar + " d" + GlobalFunctions::g_yVar + " [pb/GeV]");
	if ( GlobalFunctions::xs_unit == unit::nb ) _h->GetYaxis()->SetTitle("d^{2}#sigma/d" + GlobalFunctions::g_xVar + " d" + GlobalFunctions::g_yVar + " [nb/GeV]");
	//_h->GetXaxis()->SetRangeUser(200.0, 700.0);
	_h->GetYaxis()->SetRangeUser(1E-9, 1E9);
}


void Pad1Function_NPC(TH1D * _h)
{
	cout << "Pad1Funtion" << endl;
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	_h->GetYaxis()->SetTitle("Non-perturbative correction [-]");
	//_h->GetYaxis()->SetRangeUser(0.9, 1.25);
	_h->GetYaxis()->SetRangeUser(0.8, 1.25);
	
}
void Pad1Function_NPC_TGraph(TGraphAsymmErrors * _h)
{
	cout << "Pad1Function_NPC_TGraph" << endl;
	
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	_h->GetYaxis()->SetTitle("Non-perturbative correction [-]");
	_h->SetTitle("");
	
	//_h->GetYaxis()->SetRangeUser(0.9, 1.25);
	
	//_h->GetYaxis()->SetRangeUser(0.7, 1.4);
	_h->GetYaxis()->SetRangeUser(0.85, 1.3);
	_h->SetFillColorAlpha(411, 0.5);
	
	_h->GetXaxis()->SetTitleSize(38);
	_h->GetYaxis()->SetTitleSize(38);
	_h->GetXaxis()->SetTitleFont(43);
	_h->GetYaxis()->SetTitleFont(43);
	_h->GetXaxis()->SetTitleOffset(1.0);
	_h->GetYaxis()->SetTitleOffset(1.0);

}


void Pad2Function(TH1D * _h)
{	
	cout << "Pad2Funtion" << endl;
	
	TString hName = _h->GetName();
	TString hTitle = _h->GetTitle();
	
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	
	cout << "\t hName:  " << hName << endl;
	cout << "\t hTitle: " << hTitle << endl;
	
	if (hTitle.Contains("Herwig"))
	{
		//_h->SetMinimum(0.0);
		//_h->SetMaximum(1.5);
		
		_h->SetMinimum(0.6);
		_h->SetMaximum(1.2);
		
		
		//_h->SetMinimum(0.9);
		//_h->SetMaximum(1.1);

		
	}
	else
	{
		//_h->SetMinimum(0.8);
		//_h->SetMaximum(1.2);
		
		_h->SetMinimum(0.95);
		_h->SetMaximum(1.05);
	}
	
	//_h->GetXaxis()->SetRangeUser(200.0, 700.0);

}

void Pad2Function_JSamples(TH1D * _h)
{	
	cout << "Pad2Funtion" << endl;
	
	TString hName = _h->GetName();
	TString hTitle = _h->GetTitle();
	
	cout << "\t hName:  " << hName << endl;
	cout << "\t hTitle: " << hTitle << endl;
	
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	
	_h->SetMinimum(1E-4);
	_h->SetMaximum(1.1);
	
	//_h->GetXaxis()->SetRangeUser(200.0, 700.0);
	
}


TH1D * HistogramRebin(TH1D * _h, int _n = 1)
{
	TH1D * h = new TH1D;
	h = (TH1D *) _h->Clone();
	
	TString hName  = h->GetName();
	TString hTitle = h->GetTitle();
	cout << "hName:  " << hName << endl;
	cout << "hTitle: " << hTitle << endl;
	if (hName.Contains("ystar") || hName.Contains("yboost") || hName.Contains("y"))
	{
		vector<double> v_binning;
		if      (hName.Contains("ystar0")) copy(Binning::g_vec_binning_new_yStar0.begin(), Binning::g_vec_binning_new_yStar0.end(), back_inserter(v_binning)); 
		else if (hName.Contains("ystar1")) copy(Binning::g_vec_binning_new_yStar1.begin(), Binning::g_vec_binning_new_yStar1.end(), back_inserter(v_binning)); 
		else if (hName.Contains("ystar2")) copy(Binning::g_vec_binning_new_yStar2.begin(), Binning::g_vec_binning_new_yStar2.end(), back_inserter(v_binning)); 
		else if (hName.Contains("ystar3")) copy(Binning::g_vec_binning_new_yStar3.begin(), Binning::g_vec_binning_new_yStar3.end(), back_inserter(v_binning)); 
		else if (hName.Contains("ystar4")) copy(Binning::g_vec_binning_new_yStar4.begin(), Binning::g_vec_binning_new_yStar4.end(), back_inserter(v_binning)); 
		else if (hName.Contains("ystar5")) copy(Binning::g_vec_binning_new_yStar5.begin(), Binning::g_vec_binning_new_yStar5.end(), back_inserter(v_binning)); 
		
		else if (hName.Contains("yboost0")) copy(Binning::g_vec_binning_new_yBoost0.begin(), Binning::g_vec_binning_new_yBoost0.end(), back_inserter(v_binning)); 
		else if (hName.Contains("yboost1")) copy(Binning::g_vec_binning_new_yBoost1.begin(), Binning::g_vec_binning_new_yBoost1.end(), back_inserter(v_binning)); 
		else if (hName.Contains("yboost2")) copy(Binning::g_vec_binning_new_yBoost2.begin(), Binning::g_vec_binning_new_yBoost2.end(), back_inserter(v_binning)); 
		else if (hName.Contains("yboost3")) copy(Binning::g_vec_binning_new_yBoost3.begin(), Binning::g_vec_binning_new_yBoost3.end(), back_inserter(v_binning)); 
		else if (hName.Contains("yboost4")) copy(Binning::g_vec_binning_new_yBoost4.begin(), Binning::g_vec_binning_new_yBoost4.end(), back_inserter(v_binning)); 
		else if (hName.Contains("yboost5")) copy(Binning::g_vec_binning_new_yBoost5.begin(), Binning::g_vec_binning_new_yBoost5.end(), back_inserter(v_binning)); 
		
		/// Need to setup pt binning
		else if (hName.Contains("y0")) copy(Binning::g_vec_binning_new_y0.begin(), Binning::g_vec_binning_new_y0.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y1")) copy(Binning::g_vec_binning_new_y1.begin(), Binning::g_vec_binning_new_y1.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y2")) copy(Binning::g_vec_binning_new_y2.begin(), Binning::g_vec_binning_new_y2.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y3")) copy(Binning::g_vec_binning_new_y3.begin(), Binning::g_vec_binning_new_y3.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y4")) copy(Binning::g_vec_binning_new_y4.begin(), Binning::g_vec_binning_new_y4.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y5")) copy(Binning::g_vec_binning_new_y5.begin(), Binning::g_vec_binning_new_y5.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y6")) copy(Binning::g_vec_binning_new_y6.begin(), Binning::g_vec_binning_new_y6.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y7")) copy(Binning::g_vec_binning_new_y7.begin(), Binning::g_vec_binning_new_y7.end(), back_inserter(v_binning)); 
		else if (hName.Contains("y8")) copy(Binning::g_vec_binning_new_y8.begin(), Binning::g_vec_binning_new_y8.end(), back_inserter(v_binning)); 
		
		if (v_binning.size() != 0)
		{
			vector<double> v_binning_split;
			if (_n <= 1)
			{
				copy(v_binning.begin(), v_binning.end(), back_inserter(v_binning_split));
			}
			else
			{
				/// Loop ver bin i
				for (long unsigned int i = 0; i < v_binning.size() - 1; i++)
				{
					/// split bin i into _n equidistant parts
					double val = v_binning.at(i);
					double val_next = v_binning.at(i + 1);
					double delta = val_next - val;
					double delta_new = ceil(delta/_n);
					
					/// loop over new bin j
					for (int j = 0; j < _n; j++)
					{
						double val_j = val + delta_new * j;
						v_binning_split.push_back(val_j);
					}
				}
				v_binning_split.push_back(v_binning.back());
			}
			
			h = (TH1D * ) h->Rebin(v_binning_split.size() -1, h->GetName(), v_binning_split.data())->Clone();
			//h->Rebin(3);
			
			double x_low  = v_binning.front();
			double x_high = v_binning.back();
			
			h->GetXaxis()->SetRangeUser(x_low, x_high);
			
			if ( hName.Contains("pt") )
			{
				for (auto val: v_binning)
				{cout << val << ", ";
				}
				cout << endl;
				cout << "check binninng for inl jet with xVar: " << GlobalFunctions::g_xVar << " ,  yVar: " << GlobalFunctions::g_yVar << endl;
				
			}
		}
		cout << "NbinsX: " << h->GetNbinsX() << endl;
		cout << "REBIN" << endl;
		
		
	}
	return(h);
}

#include <TH1D.h>
#include <TString.h>
#include <vector>

TH1D * RemoveFirstBin(TH1D* hist) {
    if (!hist) {
        std::cerr << "Error: Input histogram is null!" << std::endl;
        return nullptr;
    }

    int originalBins = hist->GetNbinsX();
    if (originalBins < 2) {
        std::cerr << "Error: Histogram has fewer than 2 bins!" << std::endl;
		std::cerr << "hist->GetName() : " << hist->GetName() << endl;
		std::cerr << "originalBins :    " << originalBins << endl;

        return nullptr;
    }

    // Retrieve the bin edges
    std::vector<double> binEdges;
    for (int i = 2; i <= originalBins + 1; ++i) { // Skip the first bin
        binEdges.push_back(hist->GetXaxis()->GetBinLowEdge(i));
    }

	
    // Create a new histogram with the updated bin edges
    TString newName = TString(hist->GetName()); // + "_withoutFirstBin";
    TString newTitle = TString(hist->GetTitle()); // + " without First Bin";

	TString backupName = TString(hist->GetName()) + "_old";
    hist->SetName(backupName);

    TH1D* newHist = new TH1D(newName, newTitle, binEdges.size() - 1, binEdges.data());

    // Copy contents from the second bin onward
    for (int i = 2; i <= originalBins; ++i) {
        double content = hist->GetBinContent(i);
        double error = hist->GetBinError(i);
        newHist->SetBinContent(i - 1, content); // Adjust bin index
        newHist->SetBinError(i - 1, error);
    }

    // Copy axis titles
    newHist->GetXaxis()->SetTitle(hist->GetXaxis()->GetTitle());
    newHist->GetYaxis()->SetTitle(hist->GetYaxis()->GetTitle());

	newHist->SetLineColor( hist->GetLineColor() );
	newHist->SetLineStyle( hist->GetLineStyle() );
	newHist->SetMarkerStyle( hist->GetMarkerStyle() );

    return newHist;
}

TH1D * HistogramUpdate(TH1D * _h)
{
	TH1D * h = new TH1D;
	h = (TH1D * ) _h->Clone();
	
	TString hName  = h->GetName();
	TString hTitle = h->GetTitle();
	
	if (hTitle.Contains("Parton"))   h->SetLineColor(kYellow + 2);
	if (hTitle.Contains("Particle")) h->SetLineColor(kGreen + 2);
	
	if (hTitle.Contains("Parton"))   h->SetLineStyle(1);
	if (hTitle.Contains("Particle")) h->SetLineStyle(2);
	
	if (hTitle.Contains("Parton"))   h->SetMarkerStyle(34);
	if (hTitle.Contains("Particle")) h->SetMarkerStyle(24);
	
	/// Check normalization
	/// Scale by bin width?
	/// Scale to nb
	if (GlobalFunctions::rel == release::rel19) 
	{
		double delta_y = 0.5;
		h->Scale(1E-3/delta_y, "width");
		
		cout << "Scale:    h->Scale(1E-3/delta_y, \"width\") " << endl;
	}
	else
	{
		double delta_y = 0.5;
		h->Scale(1/delta_y, "width");
		cout << "Scale:    h->Scale(1/delta_y, \"width\"); " << endl;
	}
	
	return(h);
}


vector<TString> Get_vFiles(TString _path)
{
	vector<TString> v_dir;
	// Iterate over the `std::filesystem::directory_entry` elements using `auto`
	for ( const fs::directory_entry &  dir_entry :  fs::recursive_directory_iterator(_path.Data()) )
	{
		if (fs::is_regular_file(dir_entry))
		{
			TString file = dir_entry.path().string();
			v_dir.push_back(file);
			
		}
	}
	return (v_dir);
}



vector<TString> Get_JSample_RootFiles(TString _path)
{
	vector <TString> v_rootFiles = Get_vFiles(_path);
	 
	cout << "v_rootFiles (in Get_JSample_RootFiles(" << _path << ")):" << endl;
	for (auto item: v_rootFiles){cout << "\t" << item << endl;}
	v_rootFiles.erase(std::remove_if(	v_rootFiles.begin(),
										v_rootFiles.end(),
										[](TString file)
										{ return !( (file.EndsWith("Rivet_MERGED_SCALED.root") || file.EndsWith("RivetRootFile_MERGED_SCALED.root")) && file.Contains("AllStep") && file.Contains("JZ")); }),
										v_rootFiles.end());
	std::sort(v_rootFiles.begin(), v_rootFiles.end());
	
	cout << "after a selection" << endl;
	for (TString rootFile: v_rootFiles)
	{
		cout << rootFile << endl;
	}
	
	return(v_rootFiles);
}

vector<TH1D * > Load_vector_histograms(TString _h_name, vector<TString> _v_rootFiles)
{
	vector<TH1D *> v_hist;
	
	for (TString rootFile: _v_rootFiles)
	{
		cout << "Loading:" << endl;
		cout << "\t rootFile: " << rootFile << endl;
		cout << "\t hName:    " << _h_name << endl;
		
		TFile * f = new TFile(rootFile, "READ");
			cout << "    TFile:ls() :" << endl;
			f->ls();
		
		TH1D *h =f->Get<TH1D>(_h_name);
			/// This is a hack to be able to close the root-File
			///      based on https://root-forum.cern.ch/t/get-histogram-and-close-file/18867/11
			h->SetDirectory(0);
			/// Check the histogram
			cout << "    Check the histogram: " << endl;
			cout << "        f->GetName():  " << f->GetName()  << endl;
			cout << "        h->Integral(): " << h->Integral() << endl;
		
		v_hist.push_back(h);
		
		f->Close();
		delete f;
	}
	
	return(v_hist);
}

void Apply_CutOffCleaning(vector<TH1D *> & _v)
{
	for (TH1D * h: _v)
	{
		bool isCutOff = false;
		double mean = h->GetMean();
		
		/// Loop over all bins
		for (int i = 1; i < h->GetNbinsX() + 1; i++)
		{
			/// find bin edge above mean value
			double low_edge = h->GetBinLowEdge(i);
			if (low_edge > mean)
			{
				/// find first zero bin content after mean value
				double val = h->GetBinContent(i);
				if (val <= 0.0 && isCutOff == false)
				{
					/// this is a first zer bin content
					isCutOff = true;
				}
				
				if (isCutOff == true)
				{
					/// reset all bin contents
					h->SetBinContent(i, 0.0);
					h->SetBinError(i, 0.0);
					
				}
			}
		}
	}
	
	//return(v_hist);
}

void Apply_StatUncCleaning(vector<TH1D *> & _v, double _rel_unc_cut = 0.1)
{
	for (TH1D * h: _v)
	{
		/// Loop over all bins
		for (Int_t i = 1; i < h->GetNbinsX() + 1; i++)
		{
			double val = h->GetBinContent(i);
			double err = h->GetBinError(i);
			double rel_err = err / val;
			
			if (rel_err > _rel_unc_cut)
			{
				h->SetBinContent(i, 0.0);
				h->SetBinError(i, 0.0);
			}
		}
	}
}




TH1D * Add_Histograms(vector<TH1D *> _v)
{
			
	/// vector THX * to return
	TH1D * h_add = new TH1D();
		   h_add->Sumw2();
	
	for (long unsigned int i = 0; i < _v.size(); i++ )
	{
		TH1D * h = _v.at(i);
		
		if (i == 0)
		{
			h_add = (TH1D *) h->Clone();
			TString h_add_name = h->GetName(); 
			h_add_name += "_ADD";
			h_add->SetName(h_add_name);
		}
		else
		{
			cout << "other index" << endl;
			/// Add the histogram
			h_add->Add(h);
		}
	}
	return(h_add);
}

///void WriteToRootFile (TFile * _f, TH1D * _h, TString _path)
///{
///	TDirectory * dir = _f->GetDirectory(_path); 
///	if (!dir)
///	{
///		_f->mkdir(_path);
///	}
///	_f->cd(_path);
///	
///	_h->Write();
///	int n_subDirs = _path.CountChar('/');
///	for (int i = 0; i < n_subDirs; i++)
///	{
///		_f->cd("/..");
///	}
///	
///	/*
///			TFile fff(rootFile_JSamles, "UPDATE");
///			for (int i = 0; i < v_h1_JSamples_particle.size(); i++)
///			{
///				TString path_tmp = v_rootFiles_JSample_particle.at(i);
///				cout << path_tmp << endl;
///				TString tmp = path_tmp(path_tmp.Index("JZ") + 2, 1);
///				cout << path_tmp.First("JZ") + 2 << endl;
///				cout << tmp << endl;
///				TString dir_title = "JZ" + tmp ;
///				cout << dir_title << endl;
///				int top; cin >> top;
///				TDirectory * dir = fff.GetDirectory(dir_title); 
///				if (!dir)
///				{
///					fff.mkdir(dir_title);
///				}
///				
///				fff.cd(dir_title);
///	*/
///}

template<typename MyObject>
void WriteToRootFile (TFile * _f, MyObject _obj, TString _path, bool b_kOverwrite = false)
{
	TDirectory * dir = _f->GetDirectory(_path); 
	if (!dir)
	{
		_f->mkdir(_path);
	}
	_f->cd(_path);
	
	if (b_kOverwrite == true)
	{
		 _obj->Write( _obj->GetName(), TObject::kOverwrite );

	}
	else
	{
		_obj->Write( );
	}
	
	int n_subDirs = _path.CountChar('/');
	for (int i = 0; i < n_subDirs; i++)
	{
		_f->cd("/..");
	}
	
	/*
			TFile fff(rootFile_JSamles, "UPDATE");
			for (int i = 0; i < v_h1_JSamples_particle.size(); i++)
			{
				TString path_tmp = v_rootFiles_JSample_particle.at(i);
				cout << path_tmp << endl;
				TString tmp = path_tmp(path_tmp.Index("JZ") + 2, 1);
				cout << path_tmp.First("JZ") + 2 << endl;
				cout << tmp << endl;
				TString dir_title = "JZ" + tmp ;
				cout << dir_title << endl;
				int top; cin >> top;
				TDirectory * dir = fff.GetDirectory(dir_title); 
				if (!dir)
				{
					fff.mkdir(dir_title);
				}
				
				fff.cd(dir_title);
	*/
}



///void WriteToRootFile (TFile * _f, vector<TH1D *> _v_h1, vector<TString> _v_paths)
///{
///	for (long unsigned int i = 0; i < _v_h1.size(); i++)
///	{
///		WriteToRootFile(_f, _v_h1.at(i), _v_paths.at(i));
///	}
///}

template<typename MyObject>
void WriteToRootFile (TFile * _f, vector< MyObject > _v_obj, vector<TString> _v_paths, bool b_kOverwrite = false)
{
	for (long unsigned int i = 0; i < _v_obj.size(); i++)
	{
		WriteToRootFile(_f, _v_obj.at(i), _v_paths.at(i), b_kOverwrite);
	}
}

TH1D * UpdateJSampleHistogram(TH1D * _h, TString _jSample, TString _generator, TString _level, TString _tune)
{
	TH1D * h = new TH1D();
	h = (TH1D *) _h->Clone();
	
	h->SetTitle( _jSample + " (" + _generator + " - " + _level+ " - " + _tune + ")");
	
	h = HistogramUpdate(h);
	
	/// Not sure why - but I need Scale one more time JSample histograms
	//h->Scale(1.0/1E3);
	
	
	if (_jSample.Contains("JZ2-9") || _jSample.Contains("JS2-9") || _jSample.Contains("JSample2-9")) h->SetLineColor(1);
	else if (_jSample.Contains("JZ2") || _jSample.Contains("JS2") || _jSample.Contains("JSample2"))  h->SetLineColor(2);
	else if (_jSample.Contains("JZ3") || _jSample.Contains("JS3") || _jSample.Contains("JSample3"))  h->SetLineColor(3);
	else if (_jSample.Contains("JZ4") || _jSample.Contains("JS4") || _jSample.Contains("JSample4"))  h->SetLineColor(4);
	else if (_jSample.Contains("JZ5") || _jSample.Contains("JS5") || _jSample.Contains("JSample5"))  h->SetLineColor(5);
	else if (_jSample.Contains("JZ6") || _jSample.Contains("JS6") || _jSample.Contains("JSample6"))  h->SetLineColor(6);
	else if (_jSample.Contains("JZ7") || _jSample.Contains("JS7") || _jSample.Contains("JSample7"))  h->SetLineColor(7);
	else if (_jSample.Contains("JZ8") || _jSample.Contains("JS8") || _jSample.Contains("JSample8"))  h->SetLineColor(8);
	else if (_jSample.Contains("JZ9") || _jSample.Contains("JS9") || _jSample.Contains("JSample9"))  h->SetLineColor(9);
	
	h->SetLineStyle(2);
	
	return h;
}

vector<TH1D *> UpdateJSampleHistograms(vector<TH1D *> & _v, vector<TString> &_v_JSample, TString _generator, TString _level, TString _tune)
{
	vector<TH1D *> v;
	
	for (long unsigned int i = 0; i < _v.size(); i++)
	{
		v.push_back( UpdateJSampleHistogram(_v.at(i), _v_JSample.at(i), _generator, _level, _tune) );
	}
	
	return v;
}


void CreateMontageCommand( const TString & _pathToInputFiles, const TString & _pathToOutputFile, const TString & _montage_script, const int _nRow = 3, const int _nLine = 2, const bool _b_append = true) 
{
    // Construct the montage command
    TString cmd_montage = "montage -mode concatenate -tile " + TString(std::to_string(_nRow)) + "x" + TString(std::to_string(_nLine)) + " " + _pathToInputFiles + " " +  _pathToOutputFile ;

	// Open the montage script file in write mode to recreate it each time
	if (_b_append == true)
	{
		std::ofstream file(_montage_script, std::ios_base::app);

		if (file.is_open()) {
			// Write the command to the file
			file << cmd_montage << "\n";
			file.close();
		} else {
			std::cerr << "Error: Unable to open file " << _montage_script << std::endl;
		}
	}
	else
	{
		std::ofstream file(_montage_script);

		if (file.is_open()) {
			// Write the command to the file
			file << cmd_montage << "\n";
			file.close();
		} else {
			std::cerr << "Error: Unable to open file " << _montage_script << std::endl;
		}
	}
}



int main()
{
	/// ////////////////////////////////////////////////////////////////
	cout << "=================================================" << endl;
	cout << " MAIN                                            " << endl;
	cout << "=================================================" << endl;
	
	const bool  b_LoadFromIgnoreStepDistribution = true;
	const TString inputFile_LoadFromIgnoreStepDistribution = "FOR_STANISLAV/ImprovedIgnoreStepDistributions/NPC_Results__Fixed_and_Updated_IgnoreStepDistributions.root";

	const bool b_rebin = false;		/// true as default
									/// false for Tancredi's request - incl. jet pT with fine binning
	const bool b_dont_eval_NPC = true;

	bool b_eval_NPC_tmp;
	if (b_LoadFromIgnoreStepDistribution == true)	
	{  
		/// If you want to load from the IgnoreStepDistributin, do not run NPC_evaluation
		b_eval_NPC_tmp = false;
	}
	else if (b_dont_eval_NPC == true)
	{
		/// You do not load from the IgnoreStepDistributin
		/// And from same reason you do not want to run NPC_evaluation
		b_eval_NPC_tmp = false;
	}
	else
	{
		/// Otherwise run NPC_evaluation
		b_eval_NPC_tmp = true;
	}
	const bool b_eval_NPC = b_eval_NPC_tmp;

	
	
	/// DESC run
	//TString str_run_desc = "RUCIO_v2_Aliaksei_rel19";
	TString str_run_desc = "RUCIO_v8_rel19_rel23";
	
	/// applied tunes
	///vector<TString> v_tunes = 	{"ATLASA14NNPDF", 
	///							"ATLASA14CTEQL1",
	///							"AU2CT10",
	///							"AU2CTEQ"};
	
	vector <tuple<TString, TString, TString, TString, release, int, int >> t_gen_tune = {
			///{
			///	"GeneratorName", 
			///	"UETuneName", 
			///		"PATH/TO/PATICLE/LEVEL/ROOT/FILE.root",
			///		"PATH/TO/PARTON/LEVEL/ROOT/FILE.root",
			///		release::rel19
			///		1
			///}
			
			{
				"Pythia", 
				"AU2CT10", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_AU2CT10_20221122_jet_dijet__rel19",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_PartonLVL_AU2CT10_20221122_jet_dijet__rel19",
					release::rel19,
					634,
					23
			},
			{
				"Pythia", 
				"AU2CTEQ", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_AU2CTEQ_20221123_jet_dijet__rel19",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_PartonLVL_AU2CTEQ_20221123_jet_dijet__rel19",
					release::rel19,
					802,
					22
			},
			{
				"Pythia", 
				"ATLAS14NNPDF", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_ATLASA14NNPDF_20221118_jet_dijet__rel19",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_PartonLVL_ATLASA14NNPDF_20221118_jet_dijet__rel19",
					//"/mnt/nfs19/zaplatilek/IJXS/Rivet/v3/Pythia8_ParticleLVL_ATLASA14NNPDF_20221128_jet_dijet__rel19",
					//"/mnt/nfs19/zaplatilek/IJXS/Rivet/v3/Pythia8_PartonLVL_ATLASA14NNPDF_20221128_jet_dijet__rel19",
					release::rel19,
					1,
					20
			},
			{
				"Pythia", 
				"ATLASA14CTEQL1", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_ATLASA14CTEQL1_20221124_jet_dijet__rel19",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_PartonLVL_ATLASA14CTEQL1_20221124_jet_dijet__rel19",
					release::rel19,
					4,
					24
			},
			/*
			{
				"Herwigpp", 
				"UEEE5CTEQ6L1", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_ParticleLVL_UEEE5CTEQ6L1__20221120_jet_dijet__rel19/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_PartonLVL_UEEE5CTEQ6L1__20221120_jet_dijet__rel19/",
					release::rel19,
					632,
					21
			},
			{
				"Herwigpp", 
				"UEEE5MSTW2008", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_ParticleLVL_UEEE5MSTW2008_20221119_jet_dijet__rel19/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_PartonLVL_UEEE5MSTW2008_20221119_jet_dijet__rel19/",
					release::rel19,
					804,
					25
			},
			{
				"Herwigpp", 
				"CTEQEE4", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_ParticleLVL_CTEQEE4_20221121_jet_dijet__rel19/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Herwigpp_PartonLVL_CTEQEE4_20221121_jet_dijet__rel19/",
					release::rel19,
					402,
					47
			},
			*/
			{
				"Herwig7", 
				"DefaultNNPDF23", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_ParticleLVL_Default_rel23_withROOT_v5/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_PartonLVL_Default_rel23_withROOT_v5/",
					release::rel23,
					618,
					28
			},
			
			///{
			///	/// Wrong results for v5 SoftTune - it is Default tune! - bug in the jo  script
			///	///                                 seeds are the same for Default, SoftTune and BaryonicReconnectionTune runs -> can not be combined together as Default
			///	///                                 ->  the files can not be combined together as Default
			///	"Herwig7", 
			///	"SoftTuneNNPDF23", 
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v5/",
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v5/",
			///		release::rel23,
			///		598,
			///		20
			///},
			///{
			///	/// Wrong results for v5 BaryonicReconnectionTune - it is Default tune! - bug in the jo  script
			///	///                                                 seeds are the same for Default, SoftTune and BaryonicReconnectionTune runs 
			///	///                                                 ->  the files can not be combined together as Default
			///	"Herwig7", 
			///	"ReconnectionNNPDF23", 
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_ParticleLVL_ReconnectionTune_rel23_withROOT_v5/",
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23/Herwig7_PartonLVL_ReconnectionTune_rel23_withROOT_v5/",
			///		release::rel23,
			///		865,
			///		38
			///},
			///{
			///	/// Wrong results for v7_rel23_reprocessing - stat. dependent events
			///	///       only 25k events per slice
			///	"Herwig7", 
			///	"SoftTuneNNPDF23", 
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/",
			///		"/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7/",
			///		release::rel23,
			///		598,
			///		20
			///},
			
			{
				/// ReconnTune - v8 - 200k events per slice
				/// Look very good
				"Herwig7", 
				"ReconnectionNNPDF23", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_ReconnTune_rel23_withROOT_v8/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_ReconnTune_rel23_withROOT_v8/",
					release::rel23,
					865,
					38
			},
			{
				/// SoftTune - v8_rel23 - 200k events per slice
				/// 
				"Herwig7", 
				"SoftTuneNNPDF23", 
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8/",
					"/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v8/",
					release::rel23,
					598,
					20
			},
			
		};
	
	//TString tDir            = Rivet::RivetRoutineName;
	//vector<TString> v_hName = Rivet::v_hName;
	
	/// ////////////////////////////////////////////////////////////////
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " MAKE DIRECTORIES                                " << endl;
	cout << "=================================================" << endl;
	
	TString outDir_RUCIO;
	outDir_RUCIO = "RUCIO_Run";

	//TString outDir_Routine = outDir_RUCIO + "/" + Rivet::RivetRoutineName;
	TString outDir_Routine = outDir_RUCIO + "/" + "Routine";
	TString outDir_png     = outDir_Routine + "/png";
	TString outDir_eps     = outDir_Routine + "/eps";
	TString outDir_root    = outDir_Routine + "/root";
	TString outDir_macro   = outDir_Routine + "/macro";
	
	TString outDir_png_JSamples = outDir_Routine + "/png/JSamples";
	TString outDir_eps_JSamples = outDir_Routine + "/eps/Jsamples";
	TString outDir_macro_JSamples = outDir_Routine + "/macro/Jsamples";
	
	TString outDir_png_npc = outDir_Routine   + "/png/npc";
	TString outDir_eps_npc = outDir_Routine   + "/eps/npc";
	TString outDir_macro_npc = outDir_Routine + "/macro/npc";
	
	TString outDir_png_JSamples_Particle   = outDir_Routine + "/png/JSamples/Particle";
	TString outDir_eps_JSamples_Particle   = outDir_Routine + "/eps/Jsamples/Particle";
	TString outDir_macro_JSamples_Particle = outDir_Routine + "/macro/Jsamples/Particle";
	
	TString outDir_png_JSamples_Parton   = outDir_Routine + "/png/JSamples/Parton";
	TString outDir_eps_JSamples_Parton   = outDir_Routine + "/eps/Jsamples/Parton";
	TString outDir_macro_JSamples_Parton = outDir_Routine + "/macro/Jsamples/Parton";
	
	TString cmd_outDir_RUCIO  = "mkdir -p " + outDir_RUCIO;
	TString cmd_outDir_RivetRoutine = "mkdir -p " + outDir_Routine;
	TString cmd_outDir_png    = "mkdir -p " + outDir_png;
	TString cmd_outDir_eps    = "mkdir -p " + outDir_eps;
	TString cmd_outDir_root   = "mkdir -p " + outDir_root;
	TString cmd_outDir_macro  = "mkdir -p " + outDir_macro;
	
	TString cmd_outDir_png_JSamples    = "mkdir -p " + outDir_png_JSamples;
	TString cmd_outDir_eps_JSamples    = "mkdir -p " + outDir_eps_JSamples;
	TString cmd_outDir_macro_JSamples  = "mkdir -p " + outDir_macro_JSamples;
	
	TString cmd_outDir_png_npc    = "mkdir -p " + outDir_png_npc;
	TString cmd_outDir_eps_npc    = "mkdir -p " + outDir_eps_npc;
	TString cmd_outDir_macro_npc  = "mkdir -p " + outDir_macro_npc;
	
	TString cmd_outDir_png_JSamples_Particle    = "mkdir -p " + outDir_png_JSamples_Particle;
	TString cmd_outDir_eps_JSamples_Particle    = "mkdir -p " + outDir_eps_JSamples_Particle;
	TString cmd_outDir_macro_JSamples_Particle  = "mkdir -p " + outDir_macro_JSamples_Particle;
	
	TString cmd_outDir_png_JSamples_Parton    = "mkdir -p " + outDir_png_JSamples_Parton;
	TString cmd_outDir_eps_JSamples_Parton    = "mkdir -p " + outDir_eps_JSamples_Parton;
	TString cmd_outDir_macro_JSamples_Parton  = "mkdir -p " + outDir_macro_JSamples_Parton;
	
	gSystem->Exec(cmd_outDir_RUCIO.Data());
	gSystem->Exec(cmd_outDir_RivetRoutine.Data());
	gSystem->Exec(cmd_outDir_png.Data());
	gSystem->Exec(cmd_outDir_eps.Data());
	gSystem->Exec(cmd_outDir_root.Data());
	gSystem->Exec(cmd_outDir_macro.Data());
	
	gSystem->Exec(cmd_outDir_png_JSamples.Data());
	gSystem->Exec(cmd_outDir_eps_JSamples.Data());
	gSystem->Exec(cmd_outDir_macro_JSamples.Data());
	
	gSystem->Exec(cmd_outDir_png_npc.Data());
	gSystem->Exec(cmd_outDir_eps_npc.Data());
	gSystem->Exec(cmd_outDir_macro_npc.Data());
	
	gSystem->Exec(cmd_outDir_png_JSamples_Particle.Data());
	gSystem->Exec(cmd_outDir_eps_JSamples_Particle.Data());
	gSystem->Exec(cmd_outDir_macro_JSamples_Particle.Data());
	
	gSystem->Exec(cmd_outDir_png_JSamples_Parton.Data());
	gSystem->Exec(cmd_outDir_eps_JSamples_Parton.Data());
	gSystem->Exec(cmd_outDir_macro_JSamples_Parton.Data());
	
	/// ////////////////////////////////////////////////////////////////
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " LOOP OVER TUNES                                 " << endl;
	cout << "=================================================" << endl;
	
	
	vector<TString> v_pathsNPC;
	TString rootFile_JSamles = outDir_root + "/" + "NPC_RUCIO.root";
	
	if (b_eval_NPC == true)
	{

		/// ////////////////////////////////////////////////////////////////
		cout << "\n\n\n" << endl;
		cout << "=================================================" << endl;
		cout << " EVALUATE NPC                                    " << endl;
		cout << "=================================================" << endl;
	
		TFile fff(rootFile_JSamles, "RECREATE");
		fff.Close();
		
		//for (TString tune: v_tunes)
		for (auto t: t_gen_tune)
		{
			TString generator = get<0>(t);
			TString tune      = get<1>(t);
			TString f1_path   = get<2>(t);
			TString f2_path   = get<3>(t);
			
			cout << "f1_path: " << f1_path << endl;
			vector<TString> v_rootFiles_JSample_particle = Get_JSample_RootFiles(f1_path);
			vector<TString> v_rootFiles_JSample_parton   = Get_JSample_RootFiles(f2_path);
			
			cout << "v_rootFiles_JSample_particle:" << endl;
			for (TString item: v_rootFiles_JSample_particle){cout << "\t" << item << endl;}
			cout << "v_rootFiles_JSample_parton:" << endl;
			for (TString item: v_rootFiles_JSample_parton){cout << "\t" << item << endl;}
			
			int tttstop; cin >> tttstop;
			
			release rel = get<4>(t);
			
			/// TDirectory structure
			TFile * f_NPC = new TFile(rootFile_JSamles, "UPDATE");
				TString tdir_GeneratorTune = generator + "_"+ tune;							f_NPC->mkdir(tdir_GeneratorTune);
				TString tdir_Particle = tdir_GeneratorTune + "/Particle";					f_NPC->mkdir(tdir_Particle);
				TString tdir_Parton   = tdir_GeneratorTune + "/Parton";						f_NPC->mkdir(tdir_Parton);
				TString tdir_NPC      = tdir_GeneratorTune + "/NPC";						f_NPC->mkdir(tdir_NPC);
				TString tdir_NPC_Fine = tdir_GeneratorTune + "/NPC/Fine";					f_NPC->mkdir(tdir_NPC_Fine);
				
				TString tdir_Particle_Clean   = tdir_GeneratorTune + "/Particle/Clean";		f_NPC->mkdir(tdir_Particle_Clean);
				TString tdir_Particle_NoClean = tdir_GeneratorTune + "/Particle/NoClean";	f_NPC->mkdir(tdir_Particle_NoClean);
				TString tdir_Particle_Fine    = tdir_GeneratorTune + "/Particle/Fine";		f_NPC->mkdir(tdir_Particle_Fine);

				TString tdir_Parton_Clean     = tdir_GeneratorTune + "/Parton/Clean";		f_NPC->mkdir(tdir_Parton_Clean);
				TString tdir_Parton_NoClean   = tdir_GeneratorTune + "/Parton/NoClean";		f_NPC->mkdir(tdir_Parton_NoClean);
				TString tdir_Parton_Fine      = tdir_GeneratorTune + "/Parton/Fine";		f_NPC->mkdir(tdir_Parton_Fine);
				
				TString tdir_JSampleRatio   = tdir_GeneratorTune + "/JSample/ratio";		f_NPC->mkdir(tdir_JSampleRatio);
				

			f_NPC->Close();
			
			/// Identification of JZ sample from rootFile name
				vector<TString> v_JSamples;
				for (long unsigned int i = 0; i < v_rootFiles_JSample_particle.size(); i++)
				{
					TString path_tmp = v_rootFiles_JSample_particle.at(i);
					TString tmp = path_tmp(path_tmp.Index("JZ") + 2, 1);
					cout << path_tmp.First("JZ") + 2 << endl;
					TString dir_title = "JZ" + tmp ;
					v_JSamples.push_back(dir_title);
					
				}
				std::sort(v_JSamples.begin(), v_JSamples.end());
			
			Rivet::RivetRoutine routine;
			if      (rel == release::rel19)	routine = Rivet::RivetRoutine::Ota_rel19;
			else if (rel == release::rel21)	routine = Rivet::RivetRoutine::Ota_rel21;
			else if (rel == release::rel23)	routine = Rivet::RivetRoutine::Ota_rel23;
			
			vector<TString> v_hName = Rivet::Get_vector_hNames( routine );
			
			/// Loop over histograms
			for (TString hName: v_hName)
			{
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " LOAD JSAMPLE HISTOGRAMS                         " << endl;
				cout << "=================================================" << endl;
				
				TString h_name = Rivet::Get_RivetRoutineName( routine ) + "/"+ hName; //"ATLAS_2022_IJXS_IDXS_Rivet_2_2_1/" + hName;
				
				
				/// Load histograms for each JSample
				vector<TH1D * > v_h1_JSamples_Particle = Load_vector_histograms(h_name, v_rootFiles_JSample_particle);
				vector<TH1D * > v_h1_JSamples_Parton   = Load_vector_histograms(h_name, v_rootFiles_JSample_parton);
					
					
				cout << "h_name: " << h_name << endl;
				for (auto i: v_rootFiles_JSample_particle){cout << i << endl; };
				
				
				cout << "Check integrals:" << endl;
				for (auto h: v_h1_JSamples_Particle){cout << h->GetName() << " ,   Integral: " << h->Integral(); };
				//cout << "Stop" << endl;
				//int stop; cin >> stop;
				
				/// 
				/// fine 1 GeV histograms
				///
					// ///
					/// //
					// ///
					// Hadd JZX together for particle level with fine 1 GeV binning
					TH1D * h1_AllJSamples_Particle_fine = Add_Histograms(v_h1_JSamples_Particle);
					// Hadd JZX together fot parton level with fine 1 GeV binning
					TH1D * h1_AllJSamples_Parton_fine   = Add_Histograms(v_h1_JSamples_Parton);
					// NPC ratio with fine 1 GeV biniing
					TH1D * h1_npc_fine = (TH1D * ) h1_AllJSamples_Particle_fine->Clone();
						   h1_npc_fine->Sumw2();
						   h1_npc_fine->Divide(h1_AllJSamples_Parton_fine);
						   
						   
						TString fine_name_new = "h_ratio_particle_over_parton_fine___"; 
								fine_name_new += h1_AllJSamples_Particle_fine->GetName();
								fine_name_new += "__";
								fine_name_new += h1_AllJSamples_Parton_fine->GetName();
						TString fine_title_new = "particle over parton - fine 1 GeV bining";
								h1_npc_fine->SetName(fine_name_new);
								h1_npc_fine->SetTitle(fine_title_new);
								//v_ratia.push_back(h_ratio_i);
								
						TString h1_npc_fine_path = tdir_NPC_Fine;
								//h1_npc_fine_path += "/";
								//h1_npc_fine_path +=v_JSamples.at(i);
								//v_ratio_paths.push_back(h_ratio_path);

					// Save it to the root file
					vector<TH1D * > v_fine_particle = {h1_AllJSamples_Particle_fine}; vector<TString> v_dir_fine_particle = {tdir_Particle_Fine};
					vector<TH1D * > v_fine_parton   = {h1_AllJSamples_Parton_fine};   vector<TString> v_dir_fine_parton   = {tdir_Parton_Fine};
					vector<TH1D * > v_fine_pnc      = {h1_npc_fine};                  vector<TString> v_dir_fine_npc      = {tdir_NPC_Fine};
					
					TFile * f_NPC_update_Fine = new TFile(rootFile_JSamles, "UPDATE");
					WriteToRootFile(f_NPC_update_Fine, v_fine_particle, v_dir_fine_particle);
					WriteToRootFile(f_NPC_update_Fine, v_fine_parton,   v_dir_fine_parton);
					WriteToRootFile(f_NPC_update_Fine, v_fine_pnc,      v_dir_fine_npc);
					f_NPC_update_Fine->Close(); 
					delete f_NPC_update_Fine;
					// ///
					/// //
					// ///
				
				
				
				
				/// Rebin histograms for each JSample
				///    Apply split binning
				vector<TH1D *> v_h1_JSamples_Particle_SplitRebin;
				vector<TH1D *> v_h1_JSamples_Parton_SplitRebin;
				
				/// HERE apply change for tancredi - fine binning
				// Do not apply rebinning for Tancredi jet pt - implemente switcher b_rebin == false
				
				if (b_rebin == true)
				{
					for_each (v_h1_JSamples_Particle.begin(), v_h1_JSamples_Particle.end(), [& v_h1_JSamples_Particle_SplitRebin](TH1D  * h) { v_h1_JSamples_Particle_SplitRebin.push_back(HistogramRebin(h, 2)); });
					for_each (v_h1_JSamples_Parton.begin(), v_h1_JSamples_Parton.end(),     [& v_h1_JSamples_Parton_SplitRebin  ](TH1D  * h) { v_h1_JSamples_Parton_SplitRebin.push_back  (HistogramRebin(h, 2)); });
				}
				else
				{
					for_each (v_h1_JSamples_Particle.begin(), v_h1_JSamples_Particle.end(), [& v_h1_JSamples_Particle_SplitRebin](TH1D  * h) { v_h1_JSamples_Particle_SplitRebin.push_back(h); });
					for_each (v_h1_JSamples_Parton.begin(), v_h1_JSamples_Parton.end(),     [& v_h1_JSamples_Parton_SplitRebin  ](TH1D  * h) { v_h1_JSamples_Parton_SplitRebin.push_back  (h); });
				}
				
				/// Write to RootFile - NoClean
				vector<TString> v_dir_Jsample_Particle_NoClean, v_dir_Jsample_Parton_NoClean;
				for (long unsigned int i = 0; i < v_JSamples.size(); i++) { v_dir_Jsample_Particle_NoClean.push_back(tdir_Particle_NoClean + "/" + v_JSamples.at(i));}
				for (long unsigned int i = 0; i < v_JSamples.size(); i++) { v_dir_Jsample_Parton_NoClean.push_back(tdir_Parton_NoClean + "/" + v_JSamples.at(i));}
				
				TFile * f_NPC_update_NoClean = new TFile(rootFile_JSamles, "UPDATE");
				WriteToRootFile(f_NPC_update_NoClean, v_h1_JSamples_Particle_SplitRebin, v_dir_Jsample_Particle_NoClean);
				WriteToRootFile(f_NPC_update_NoClean, v_h1_JSamples_Parton_SplitRebin,   v_dir_Jsample_Parton_NoClean);
				f_NPC_update_NoClean->Close(); 
				delete f_NPC_update_NoClean;
				
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " APPLY CUTOFF CLEANING                           " << endl;
				cout << "=================================================" << endl;
				
				///Apply Cut off Cleaning
				if (b_rebin == true)
				{
					Apply_CutOffCleaning(v_h1_JSamples_Particle_SplitRebin);
					Apply_CutOffCleaning(v_h1_JSamples_Parton_SplitRebin);
				}
				
				///Reset bins with high stat unc.
				///    relative stat unc. above 10%
				if (b_rebin == true)
				{
					Apply_StatUncCleaning(v_h1_JSamples_Particle_SplitRebin);
					Apply_StatUncCleaning(v_h1_JSamples_Parton_SplitRebin);
				}
				
				/// Write to RootFile - Clean using CutOffCleaning
				vector<TString> v_dir_Jsample_Particle_Clean, v_dir_Jsample_Parton_Clean;
				for (long unsigned int i = 0; i < v_JSamples.size(); i++) { v_dir_Jsample_Particle_Clean.push_back(tdir_Particle_Clean + "/" + v_JSamples.at(i));}
				for (long unsigned int i = 0; i < v_JSamples.size(); i++) { v_dir_Jsample_Parton_Clean.push_back(tdir_Parton_Clean + "/" + v_JSamples.at(i));}
				
				TFile * f_NPC_update_Clean = new TFile(rootFile_JSamles, "UPDATE");
				WriteToRootFile(f_NPC_update_Clean, v_h1_JSamples_Particle_SplitRebin, v_dir_Jsample_Particle_Clean);
				WriteToRootFile(f_NPC_update_Clean, v_h1_JSamples_Parton_SplitRebin,   v_dir_Jsample_Parton_Clean);
				f_NPC_update_Clean->Close();
				delete f_NPC_update_Clean;
				
				/// Rebin one more time
				vector<TH1D *> v_h1_JSamples_Particle_Rebin;
				vector<TH1D *> v_h1_JSamples_Parton_Rebin;
				
				/// HERE apply change for tancredi - fine binning
				// Do not apply rebinning for Tancredi jet pt - implemente switcher b_rebin == false
				
				if (b_rebin == true)
				{
					for_each (v_h1_JSamples_Particle_SplitRebin.begin(), v_h1_JSamples_Particle_SplitRebin.end(), [& v_h1_JSamples_Particle_Rebin](TH1D  * h) { v_h1_JSamples_Particle_Rebin.push_back(HistogramRebin(h, 1)); });
					for_each (v_h1_JSamples_Parton_SplitRebin.begin(), v_h1_JSamples_Parton_SplitRebin.end(),     [& v_h1_JSamples_Parton_Rebin](TH1D  * h) { v_h1_JSamples_Parton_Rebin.push_back  (HistogramRebin(h, 1)); });
				}
				else
				{
					for_each (v_h1_JSamples_Particle_SplitRebin.begin(), v_h1_JSamples_Particle_SplitRebin.end(), [& v_h1_JSamples_Particle_Rebin](TH1D  * h) { v_h1_JSamples_Particle_Rebin.push_back (h); });
					for_each (v_h1_JSamples_Parton_SplitRebin.begin(), v_h1_JSamples_Parton_SplitRebin.end(),     [& v_h1_JSamples_Parton_Rebin](TH1D  * h) { v_h1_JSamples_Parton_Rebin.push_back     (h); });
				}
				
				//////// UPDATE HERE
				vector<TH1D *>  v_ratia;
				vector<TString> v_ratio_paths;
				for (long unsigned int i = 0; i < v_h1_JSamples_Particle_SplitRebin.size(); i++)
				{
					TH1D * h_particle_i = (TH1D *) v_h1_JSamples_Particle_SplitRebin.at(i)->Clone();
					TH1D * h_parton_i   = (TH1D *) v_h1_JSamples_Parton_SplitRebin.at(i)->Clone();
					TH1D * h_ratio_i = (TH1D *) h_particle_i->Clone();
						   h_ratio_i->Sumw2();
					
					TString name_new = "h_ratio_particle_over_parton___"; 
					        name_new += h_particle_i->GetName();
					        name_new += "__";
					        name_new += h_parton_i->GetName();
					TString title_new = "particle over parton - ";
					        title_new += v_JSamples.at(i);
					        h_ratio_i->SetName(name_new);
					        h_ratio_i->SetTitle(title_new);
					        
					        h_ratio_i->Divide(h_parton_i);
					        v_ratia.push_back(h_ratio_i);
					        
					TString h_ratio_path = tdir_JSampleRatio;
					        h_ratio_path += "/";
					        h_ratio_path +=v_JSamples.at(i);
					        v_ratio_paths.push_back(h_ratio_path);
				}
				TFile * f_NPC_update_Jsample_ratio = new TFile(rootFile_JSamles, "UPDATE");
				WriteToRootFile(f_NPC_update_Jsample_ratio, v_ratia, v_ratio_paths);
				f_NPC_update_Jsample_ratio->Close(); 
				delete f_NPC_update_Jsample_ratio;
				
				
				
				TString outputName = hName + "_" + generator + "_"+ tune;
				
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " ADD JSAMPLE HISTOGRAMS                          " << endl;
				cout << "=================================================" << endl;
				
				
				TH1D * h1 = Add_Histograms(v_h1_JSamples_Particle_Rebin);
				TH1D * h2 = Add_Histograms(v_h1_JSamples_Parton_Rebin);
				
				cout << "h1->Integral(): " << h1->Integral() << endl;
				cout << "h2->Integral(): " << h2->Integral() << endl;
				
				TString tune_tmp = tune; tune_tmp = tune_tmp.ReplaceAll("ATLAS", "");
				
				h1->SetTitle( generator + " - Particle level - " + tune_tmp);
				h2->SetTitle( generator + " - Parton level   - " + tune_tmp);
				
				/// Set Global variables
				/// Set description for TLatex
				TString text_tmp = hName; 
					text_tmp.ReplaceAll("h1_mjj_", ""); 
					text_tmp.ReplaceAll("h1_pt_", "");
					text_tmp.ReplaceAll("h1_pt_", "");
					text_tmp.ReplaceAll("dijet_invmass_", "");
					text_tmp.ReplaceAll("jet_pt_", "");
					text_tmp.ReplaceAll("_AKT04", "");
				
				TString text;
				if (text_tmp.Contains("0")) text = "0.0<XXX<0.5";
				if (text_tmp.Contains("1")) text = "0.5<XXX<1.0";
				if (text_tmp.Contains("2")) text = "1.0<XXX<1.5";
				if (text_tmp.Contains("3")) text = "1.5<XXX<2.0";
				if (text_tmp.Contains("4")) text = "2.0<XXX<2.5";
				if (text_tmp.Contains("5")) text = "2.5<XXX<3.0";
				if (text_tmp.Contains("6")) text = "3.0<XXX<3.5";
				if (text_tmp.Contains("7")) text = "3.5<XXX<4.0";
				if (text_tmp.Contains("8")) text = "4.0<XXX<4.5";
				
				
				if (hName.Contains("mjj") || hName.Contains("mass"))
				{
					GlobalFunctions::g_xVarName = "mjj";
					GlobalFunctions::g_xVar= "m_{jj}";
				}
				
				if (hName.Contains("pt"))
				{
					GlobalFunctions::g_xVarName = "pt";
					GlobalFunctions::g_xVar= "p_{T}";
				}
				
				if (text_tmp.Contains("ystar"))
				{
					GlobalFunctions::g_yVarName = "ystar";
					GlobalFunctions::g_yVar     = "y*";
				}
				else if (text_tmp.Contains("yboost"))
				{
					GlobalFunctions::g_yVarName = "yboost";
					GlobalFunctions::g_yVar     = "y_{boost}";
				}
				else if (text_tmp.Contains("y") and !text.Contains("ystar") and !text_tmp.Contains("yboost"))
				{
					GlobalFunctions::g_yVarName = "y";
					GlobalFunctions::g_yVar     = "|y|";
				}
				
				TString latex = "RUCIO_Run_rel19";
				GlobalFunctions::rel = rel;
				if (rel == release::rel19) 
				{
					GlobalFunctions::xs_unit = unit::nb;
				}
				else
				{
					GlobalFunctions::xs_unit = unit::pb;
				}
				
				text = text.ReplaceAll("XXX", GlobalFunctions::g_yVar);
				
				///
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " SAVE TO ROOTFILE                                " << endl;
				cout << "=================================================" << endl;
				
				/*
				TString rootFile = outDir_root + "/" + outputName + ".root";
				TFile f ( rootFile, "RECREATE" );
				TH1D * h1_tmp  = (TH1D *) h1->Clone(); h1_tmp->SetName(TString(h1_tmp->GetName()) + "_ParticleLVL");
				TH1D * h2_tmp  = (TH1D *) h2->Clone(); h2_tmp->SetName(TString(h2_tmp->GetName()) + "_PartonLVL");
				TH1D * h_ratio = (TH1D *) h2->Clone();
				h_ratio->Divide(h1);
				h_ratio->SetName("npc");
				h_ratio->SetTitle("ratio of " + TString(h1_tmp->GetName()) + " over " + TString(h2_tmp->GetName()));
				FitFunction(h_ratio);
				
				h1_tmp->Write();
				h2_tmp->Write();
				h_ratio->Write();
				f.Close();
				*/
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " PREPARE TO DRAW                                 " << endl;
				cout << "=================================================" << endl;


				bool logX = false;
				bool logY = true;
				
				/// Set position of legend
				TLegend * l = new TLegend(0.57, 0.55, 0.93, 0.9);
				
				
				//TString outPNG = outDir_png + "/" + outputName + ".png";
				TString outEPS = outDir_eps   + "/" + outputName + ".eps";
				TString outPNG = outDir_png   + "/" + outputName + ".png";
				TString outC   = outDir_macro + "/" + outputName + ".C";
				
				
				GlobalLatex::g_yVarRange = text;
				
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " DRAW Slices - Particle                          " << endl;
				cout << "=================================================" << endl;
				
				vector<TH1D *> 	v_h1_JSamples_Particle_Rebin_Draw_tmp;
					TH1D * h1_clone = (TH1D *) h1->Clone();
					v_h1_JSamples_Particle_Rebin_Draw_tmp.push_back(h1_clone);
					copy(v_h1_JSamples_Particle_Rebin.begin(), v_h1_JSamples_Particle_Rebin.end(), back_inserter(v_h1_JSamples_Particle_Rebin_Draw_tmp)); 
				
				vector<TString> v_JSamples_Draw;
					v_JSamples_Draw.push_back("JZ2-9");
					copy(v_JSamples.begin(), v_JSamples.end(), back_inserter(v_JSamples_Draw)); 
				
				vector<TH1D *> v_h1_JSamaples_Particle_Draw = UpdateJSampleHistograms(v_h1_JSamples_Particle_Rebin_Draw_tmp, v_JSamples_Draw, generator, "Particle", tune);
				
				TString outPNG_JSamples_Particle = outDir_png_JSamples_Particle + "/" + outputName + ".png";
				TString outEPS_JSamples_Particle = outDir_eps_JSamples_Particle + "/" + outputName + ".eps";
				TString outC_JSamples_Particle   = outDir_macro_JSamples_Particle + "/" + outputName + ".C";
				
				TCanvas * c_JSamples_Particle = Draw (v_h1_JSamaples_Particle_Draw, outPNG_JSamples_Particle, v_h1_JSamaples_Particle_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				          c_JSamples_Particle->SaveAs(outPNG_JSamples_Particle);
				          c_JSamples_Particle->SaveAs(outEPS_JSamples_Particle);
				          c_JSamples_Particle->SaveAs(outC_JSamples_Particle);
				          
				//Draw (v_h1_JSamaples_Particle_Draw, outEPS_JSamples_Particle, v_h1_JSamaples_Particle_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				//Draw (v_h1_JSamaples_Particle_Draw, outC_JSamples_Particle,   v_h1_JSamaples_Particle_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " DRAW Slices - Parton                            " << endl;
				cout << "=================================================" << endl;
				
				vector<TH1D *> 	v_h1_JSamples_Parton_Rebin_Draw_tmp;
					TH1D * h2_clone = (TH1D *) h2->Clone();
					v_h1_JSamples_Parton_Rebin_Draw_tmp.push_back(h2_clone);
					copy(v_h1_JSamples_Parton_Rebin.begin(), v_h1_JSamples_Parton_Rebin.end(), back_inserter(v_h1_JSamples_Parton_Rebin_Draw_tmp)); 
				
				vector<TH1D *> v_h1_JSamaples_Parton_Draw = UpdateJSampleHistograms(v_h1_JSamples_Parton_Rebin_Draw_tmp, v_JSamples_Draw, generator, "Parton", tune);
				
				TString outPNG_JSamples_Parton = outDir_png_JSamples_Parton + "/" + outputName + ".png";
				TString outEPS_JSamples_Parton = outDir_eps_JSamples_Parton + "/" + outputName + ".eps";
				TString outC_JSamples_Parton   = outDir_macro_JSamples_Parton + "/" + outputName + ".C";
				
				TCanvas * c_JSamples_Parton = Draw (v_h1_JSamaples_Parton_Draw, outPNG_JSamples_Parton, v_h1_JSamaples_Parton_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				          c_JSamples_Parton->SaveAs(outPNG_JSamples_Parton);
				          c_JSamples_Parton->SaveAs(outEPS_JSamples_Parton);
				          c_JSamples_Parton->SaveAs(outC_JSamples_Parton);
				          
				//Draw (v_h1_JSamaples_Parton_Draw, outEPS_JSamples_Parton, v_h1_JSamaples_Parton_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				//Draw (v_h1_JSamaples_Parton_Draw, outC_JSamples_Parton,   v_h1_JSamaples_Parton_Draw.front(), logX, logY, Pad1Function, Pad2Function_JSamples, latex, l, Function_DrawLatex);
				
				
				/// ////////////////////////////////////////////////////////////////
				cout << "\n\n\n" << endl;
				cout << "=================================================" << endl;
				cout << " DRAW RUCIO                                      " << endl;
				cout << "=================================================" << endl;
				
				h1 = HistogramUpdate(h1);
				h2 = HistogramUpdate(h2);
				vector<TH1D *> v; 
					v.push_back(h2);
					v.push_back(h1); 
				
				
				/// Draw function
				///     using pointer to a function: Pad1Function - to update all histograms in pad 1
				///                                  Pad2Function - to update all histograms in pad 2
				///                                  Function_DrawLatex - ro draw labels in latex
				bool doFit = false;
				TCanvas * c_RUCIO = Draw (v, outEPS, v.front(), logX, logY, Pad1Function, Pad2Function, latex, l, Function_DrawLatex, doFit);
				          c_RUCIO->SaveAs(outEPS);
				          c_RUCIO->SaveAs(outPNG);
				          c_RUCIO->SaveAs(outC);
				          
				//Draw (v, outPNG, v.front(), logX, logY, Pad1Function, Pad2Function, latex, l, Function_DrawLatex, doFit);
				//Draw (v, outC,   v.front(), logX, logY, Pad1Function, Pad2Function, latex, l, Function_DrawLatex, doFit);
				
				TH1D * h_npc = (TH1D * ) h1->Clone();
					   h_npc->Divide(h2);
					   h_npc->GetYaxis()->SetTitle("NPC [-]");
					   TString title_npc = generator + " - " + tune;
					   h_npc->SetTitle(title_npc);
					   
					   //double h_factor = 1;
					   //int    method  = 3;
					   //bool   debug   = false;
					   //double width_a = 60;
					   //double width_b = 0.08;
					   //h_npc = GaussianKernelSmoothing(h_npc, h_factor, method, debug, width_a, width_b);
				
				TFile * f_NPC_update_npc = new TFile(rootFile_JSamles, "UPDATE");
				WriteToRootFile (f_NPC_update_npc, h_npc, tdir_NPC);
				f_NPC_update_npc->Close(); 
				delete f_NPC_update_npc;
				
				v_pathsNPC.push_back(tdir_NPC + "/" + h_npc->GetName());
				
				/// ////////////////////////////////////////////////////////////////
			}
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " MONTAGE                                         " << endl;
			cout << "=================================================" << endl;
			
			if ( Rivet::rivet == Rivet::RivetRoutine::Ota_rel19 ||
			     Rivet::rivet == Rivet::RivetRoutine::Ota_rel21 ||
			     Rivet::rivet == Rivet::RivetRoutine::Ota_rel23   )
			{
				/// merge JSamples - Particle - all 6 png plots (various y* bins) to one pdf file
				TString cmd_montage_JSample_Particle_ystar = "montage -mode concatenate -tile 3x2 "  + outDir_png_JSamples_Particle + "/" + "h1_mjj_ystar*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Particle_ystar_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Particle_ystar << endl;
				gSystem->Exec(cmd_montage_JSample_Particle_ystar.Data());
				
				/// merge JSamples - Parton - all 6 png plots (various y* bins) to one pdf file
				TString cmd_montage_JSample_Parton_ystar = "montage -mode concatenate -tile 3x2 "  + outDir_png_JSamples_Parton + "/" + "h1_mjj_ystar*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Parton_ystar_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Parton_ystar << endl;
				gSystem->Exec(cmd_montage_JSample_Parton_ystar.Data());
				
				/// merge JSamples - Particle - all 6 png plots (various y* bins) to one pdf file
				TString cmd_montage_JSample_Particle_yboost = "montage -mode concatenate -tile 3x2 "  + outDir_png_JSamples_Particle + "/" + "h1_mjj_yboost*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Particle_yboost_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Particle_yboost << endl;
				gSystem->Exec(cmd_montage_JSample_Particle_yboost.Data());
				
				/// merge JSamples - Parton - all 6 png plots (various y* bins) to one pdf file
				TString cmd_montage_JSample_Parton_yboost = "montage -mode concatenate -tile 3x2 "  + outDir_png_JSamples_Parton + "/" + "h1_mjj_yboost*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Parton_yboost_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Parton_yboost << endl;
				gSystem->Exec(cmd_montage_JSample_Parton_yboost.Data());
				
				/// merge JSamples - Particle - all 9 png plots (various y bins) to one pdf file
				TString cmd_montage_JSample_Particle_y = "montage -mode concatenate -tile 3x3 "  + outDir_png_JSamples_Particle + "/" + "h1_pt_y*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Particle_y_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Particle_y << endl;
				gSystem->Exec(cmd_montage_JSample_Particle_y.Data());
				
				/// merge JSamples - Parton - all 9 png plots (various y bins) to one pdf file
				TString cmd_montage_JSample_Parton_y = "montage -mode concatenate -tile 3x3 "  + outDir_png_JSamples_Parton + "/" + "h1_pt_y*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_JSamples_Parton_y_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_JSample_Parton_y << endl;
				gSystem->Exec(cmd_montage_JSample_Parton_y.Data());
				
				
				/// merge all 6 png plots (various y* bins) to one pdf file
				TString cmd_montage_ystar = "montage -mode concatenate -tile 3x2 "  + outDir_png + "/" + "h1_mjj_ystar*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_ystar_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_ystar << endl;
				gSystem->Exec(cmd_montage_ystar.Data());
				
				
				/// merge all 6 png plots (various y_boost bins) to one pdf file
				TString cmd_montage_yboost =  "montage -mode concatenate -tile 3x2 " + outDir_png + "/" + "h1_mjj_yboost*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_yboost_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_yboost << endl;
				gSystem->Exec(cmd_montage_yboost.Data());
				
				/// merge all 9 y bins
				TString cmd_montage_y = "montage -mode concatenate -tile 3x3 " + outDir_png + "/" + "h1_pt_y*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_y_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_y << endl;
				gSystem->Exec(cmd_montage_y.Data());
				
				
			}
			if (Rivet::rivet == Rivet::RivetRoutine::Aliaksei)
			{
				/// merge 6 png plots (various y bins) to one pdf file
				TString cmd_montage_ystar = "montage -mode concatenate -tile 3x2 "  + outDir_png + "/" + "dijet_invmass_ystar*_AKT04*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_ystar_AK04_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_ystar << endl;
				gSystem->Exec(cmd_montage_ystar.Data());
				
				/// merge 9 png plots (various y bins) to one pdf file
				TString cmd_montage_y = "montage -mode concatenate -tile 3x3 " + outDir_png + "/" + "jet_pt_y*_AKT04*" + generator + "_" + tune + "*.png  " + outDir_Routine + "/Compare_y_AK04_" + "_all_" + generator + "_" + tune + ".png";
				cout << cmd_montage_y << endl;
				gSystem->Exec(cmd_montage_y.Data());
			}
			
		}
		/// ////////////////////////////////////////////////////////////////
		cout << "\n\n\n" << endl;
		cout << "=================================================" << endl;
		cout << " Merge All png plots to one large pdf document   " << endl;
		cout << "=================================================" << endl;
		TString cmd_merge = "convert " + outDir_png + "/*.png " + outDir_Routine + "/Merge_all_plots_in_png_directory.pdf ";
		cout << cmd_merge << endl;
		gSystem->Exec(cmd_merge);
	}
	
	
	/// ////////////////////////////////////////////////////////////////
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " LOAD f_NPC_load and                             " << endl;
	cout << " f_NPC_Results_Load_fromIgnoreStepDistribution   " << endl;
	cout << "=================================================" << endl;

	TFile * f_NPC_load ; //= new TFile(rootFile_JSamles, "OPEN");
	//TFile * f_NPC_Results_Load_fromIgnoreStepDistribution = nullptr;
	if (b_LoadFromIgnoreStepDistribution == true)
	{
		f_NPC_load = new TFile(inputFile_LoadFromIgnoreStepDistribution, "OPEN");	
	}
	else
	{
		f_NPC_load = new TFile(rootFile_JSamles, "OPEN");	
	}

	vector<TString> v_tdirs;
	
	vector <TH1D * > v_h1_binwidths;
	
	int ind_hName = 0;
	
	
	vector<TString> v_hName = Rivet::Get_vector_hNames( Rivet::RivetRoutine::Ota_rel19 );
	for (TString hName: v_hName)
	{
		ind_hName += 1;
		vector <TH1D *> v_npc_raw;          vector<TString> v_npc_raw_paths;
		vector <TH1D *> v_npc_ignore;       vector<TString> v_npc_ignore_paths;
		vector <TH1D *> v_npc_interpol;     vector<TString> v_npc_interpol_paths;
		vector <TH1D *> v_npc_smooth;       vector<TString> v_npc_smooth_paths;
		
		vector <TH1D *> v_derivative1;
		vector <TH1D *> v_derivative2;
		
		for (auto t: t_gen_tune)
		{
			TString generator = get<0>(t);
			TString tune      = get<1>(t);
			//release rel       = get<4>(t);
			int color         = get<5>(t);
			int style         = get<6>(t);
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " Load NPC Raw   " << endl;
			cout << "=================================================" << endl;

			TH1D * h = new TH1D;
			if (b_LoadFromIgnoreStepDistribution == true)
			{
				cout << "Load /NPC/NPC_Raw from new" << endl;
				TString h_path_raw = generator + "_" + tune + "/NPC/NPC_Raw/" + hName + "_ADD";
				cout << "h_path_raw: " << h_path_raw << endl;
				
				//TH1D * h_tmp = f_NPC_load->Get<TH1D>(h_path_raw);
				//h = RemoveFirstBin(h_tmp);
				
				h = f_NPC_load->Get<TH1D>(h_path_raw);
			}
			else
			{
				cout << "Load /NPC/NPC_Raw from old" << endl;

				TString h_path = generator + "_" + tune + "/NPC/" + hName + "_ADD";
				cout << "h_path: " << h_path << endl;
				
				//TH1D * h_tmp = f_NPC_load->Get<TH1D>(h_path);
				//h = RemoveFirstBin(h_tmp);

				h = f_NPC_load->Get<TH1D>(h_path);
				
			}
			h->SetLineColor(color);
			h->SetMarkerStyle(color);
			h->SetMarkerStyle(style);
			
			/// Algo for Reset bin contents
			///
			/*
			TH1D * h_derivative1 = Derivative( h );
			TH1D * h_derivative2 = Derivative( h_derivative1 );
			
			v_derivative1.push_back( h_derivative1 );
			v_derivative2.push_back( h_derivative2 );
			
			
			cout << "\nfirst derivative for i/i+1" << endl;
			for (int i = 1; i < h_derivative1->GetNbinsX(); i++)
			{
				cout << i << " \t "<< h->GetBinCenter(i) - h->GetBinWidth(i)/2.0 << " to " << h->GetBinCenter(i) + h->GetBinWidth(i)/2.0 << "\t " << h_derivative1->GetBinContent(i) / h_derivative1->GetBinContent(i + 1) << endl;
			}
			
			cout << "\nsecond derivative - i/i+1" << endl;
			for (int i = 1; i < h_derivative2->GetNbinsX(); i++)
			{
				//cout <<  h_derivative2->GetBinContent(i) / h_derivative2->GetBinContent(i + 1) << " ";
				cout << i << " \t "<< h->GetBinCenter(i) - h->GetBinWidth(i)/2.0 << " to " << h->GetBinCenter(i) + h->GetBinWidth(i)/2.0 << "\t " << h_derivative2->GetBinContent(i) / h_derivative2->GetBinContent(i + 1) << endl;
			}
			
			int stop; cin >> stop;
			
			double d2_cut = 10.0;
			double d1_cut = 10.0;
			double new_val = 1.05;
			for ( int i = 1 + 1; i < h->GetNbinsX() +1 -1 -1; i++)
			{
				
				double val_prev = -999;
				double val_now  = -999;
				double val_next = -999;
				if ( i - 1 <= h->GetNbinsX() ) val_prev = h->GetBinContent(i - 1);
				if ( i     <= h->GetNbinsX() ) val_now  = h->GetBinContent(i);
				if ( i + 1 <= h->GetNbinsX() ) val_next = h->GetBinContent(i + 1);
				
				double derivative1_prev = -999;
				double derivative1_now  = -999;
				double derivative1_next = -999;
				if ( i - 1 <= h_derivative1->GetNbinsX() ) derivative1_prev = h_derivative1->GetBinContent(i - 1);
				if ( i     <= h_derivative1->GetNbinsX() ) derivative1_now  = h_derivative1->GetBinContent(i);
				if ( i + 1 <= h_derivative1->GetNbinsX() ) derivative1_next = h_derivative1->GetBinContent(i + 1);
				
				double derivative2_prev = -999;
				double derivative2_now  = -999;
				double derivative2_next = -999;
				if ( i - 1 <= h_derivative2->GetNbinsX() ) derivative2_prev = h_derivative2->GetBinContent(i - 1);
				if ( i     <= h_derivative2->GetNbinsX() ) derivative2_now  = h_derivative2->GetBinContent(i);
				if ( i + 1 <= h_derivative2->GetNbinsX() ) derivative2_next = h_derivative2->GetBinContent(i + 1);
				
				int j_prev = i - 1;
				int j_next = i + 1;
				
				if ( std::abs( derivative2_now  / derivative2_prev )  >  d2_cut ||
				     std::abs( derivative2_prev / derivative2_now  )  >  d2_cut ||
				     std::abs( derivative1_now  / derivative1_prev )  >  d1_cut ||
				     std::abs( derivative1_prev / derivative1_now  )  >  d1_cut )
				
				{
					/// find first previous non-zero value
					for ( int j = i; j > 0; j--)
					{
						double val_j = h->GetBinContent(j);
						double derivative1_j = h_derivative1->GetBinContent(j);
						double derivative2_j = h_derivative2->GetBinContent(j);
						if ( std::abs(derivative2_j    / derivative2_prev)  < d2_cut && 
						     std::abs(derivative2_prev / derivative2_j )    < d2_cut && 
						     std::abs(derivative1_j    / derivative1_prev ) < d1_cut &&
						     std::abs(derivative1_prev / derivative1_j )    < d1_cut
						     )
						{
							j_prev = j;
							val_prev = val_j;
							derivative1_prev = h_derivative1->GetBinContent(j);
							derivative2_prev = h_derivative2->GetBinContent(j);
							break;
						}
					} 
					
					/// find first next non-zero value
					for ( int j = i; j < h_derivative2->GetNbinsX() + 1; j++)
					{
						double derivative1_j = h_derivative1->GetBinContent(j);
						double derivative2_j = h_derivative2->GetBinContent(j);
						
						if ( std::abs(derivative2_j    / derivative2_prev)  < d2_cut && 
						     std::abs(derivative2_prev / derivative2_j )    < d2_cut && 
						     std::abs(derivative1_j    / derivative1_prev ) < d1_cut &&
						     std::abs(derivative1_prev / derivative1_j )    < d1_cut
						     )
						{
							j_next = j;
							val_next         = h->GetBinContent(j);;
							derivative1_next = h_derivative1->GetBinContent(j);
							derivative2_next = h_derivative2->GetBinContent(j);
							break;
						}
					}
				}
				
				cout << "i                                                : " << h->GetBinCenter(i) - h->GetBinWidth(i)/2.0 << ", " << h->GetBinCenter(i) + h->GetBinWidth(i)/2.0 << endl;
				cout << "j_prev, i, j_next                                : " << j_prev << "\t" << i << "\t" << j_next << endl;
				cout << "val_prev,  val, val_next                         : " << setprecision(2) << val_prev << "\t" << val_now << "\t" << val_next << endl;
				cout << "derivative1_prev,  derivative1, derivative1_next : " << setprecision(2) << derivative1_prev << "\t" << derivative1_now << "\t" << derivative1_next << setprecision(4) << "\t prew/now, now/prew : " << derivative1_prev / derivative1_now << ", " << derivative1_now / derivative1_prev << "\t now/next, next/now: " << derivative1_now / derivative1_next << ", " << derivative1_next / derivative1_now << endl;
				cout << "derivative2_prev,  derivative2, derivative2_next : " << setprecision(2) << derivative2_prev << "\t" << derivative2_now << "\t" << derivative2_next << setprecision(4) << "\t prew/now, now/prew : " << derivative2_prev / derivative2_now << ", " << derivative2_now / derivative2_prev << "\t now/next, next/now: " << derivative2_now / derivative2_next << ", " << derivative2_next / derivative2_now << endl;
				
				if ( derivative2_prev != -999 && derivative2_now  != -999  && derivative2_now != -999 &&
				     derivative1_prev != -999 && derivative1_now  != -999  && derivative1_now != -999 &&
				     derivative2_prev !=  0.0  && derivative2_now !=  0.0  && derivative2_now !=  0.0 &&
				     derivative1_prev !=  0.0  && derivative1_now !=  0.0  && derivative1_now !=  0.0 )
				{
					if ( 
					     ///Prev
					     std::abs( derivative2_now  / derivative2_prev ) > d2_cut || 
					     std::abs( derivative2_prev / derivative2_now  ) > d2_cut ||
					     std::abs( derivative1_now  / derivative1_prev ) > d1_cut ||
					     std::abs( derivative1_prev / derivative1_now  ) > d1_cut ||
					     /// Next
					     std::abs( derivative2_now  / derivative2_next ) > d2_cut || 
					     std::abs( derivative2_next / derivative2_now  ) > d2_cut ||
					     std::abs( derivative1_now  / derivative1_next ) > d1_cut ||
					     std::abs( derivative1_next / derivative1_now  ) > d1_cut
					     )
					{
						h->SetBinContent(i, new_val);
						cout << "redefine val: " << "SetBinContent(i, " << (val_prev + val_next)/2.0  << ") " << endl;
						cout << "\n\n" << endl;
						//h->SetBinContent(i, (val_prev + val_next)/2.0 );
					}
				}
				
				int stop; cin >> stop;
			}
			*/
			
			v_npc_raw.push_back( h );
			
			/// ////////////////////////////////////////////////////////////////
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " Load NPC Ignore   " << endl;
			cout << "=================================================" << endl;

			TH1D * h_npc_ignore = new TH1D;
			if (b_LoadFromIgnoreStepDistribution == true)
			{
				TString h_path_ignore = generator + "_" + tune + "/NPC/NPC_Ignore/" + hName + "_ADD";
				cout << "h_path_ignore: " << h_path_ignore << endl;
				
				h_npc_ignore = f_NPC_load->Get<TH1D>(h_path_ignore);
				
				cout << "h_npc_ignore->GetNbinsX(): " << h_npc_ignore->GetNbinsX() << " HERE" << endl;

				//h_npc_ignore = RemoveFirstBin(h_npc_ignore_tmp);

				h_npc_ignore->SetLineColor(color);
				h_npc_ignore->SetMarkerStyle(color);
				h_npc_ignore->SetMarkerStyle(style);
				
				v_npc_ignore.push_back( h_npc_ignore );
			}
			else
			{
				/// ////////////////////////////////////////////////////////
				/// Bin Ignore
				bool b_debug_ignore = true;
				TString config_path = "CONFIG_IGNORE_BINNING/" + generator + "_" + tune + ".env";
				
				/// Debug print
				if (b_debug_ignore)
				{
					for (Int_t i = 1; i < h->GetNbinsX() + 1; i++)
					{
						cout << "i :" << i << "\t" << h->GetBinCenter(i) - h->GetBinWidth(i)/2.0 << ", " << h->GetBinCenter(i) + h->GetBinWidth(i)/2.0 << "\t" << h->GetBinContent(i) << endl;
					}
				}
				int stop1; cin >> stop1;
				
				/// Apply BinIgnore Function
				///    Configuration only for Herwigpp
				//TH1D * h_npc_ignore;
				h_npc_ignore = BinIgnoring(h, hName, config_path);
				v_npc_ignore.push_back( h_npc_ignore );
			}

			/// ////////////////////////////////////////////////////////
			/// Linear Interpolation
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " Linear Interpolation   " << endl;
			cout << "=================================================" << endl;

			//cout << "h_npc_ignore->GetNbinsX(): " << h_npc_ignore->GetNbinsX() << endl;
			//TH1D * h_npc_interpol_tmp = ApplyLinearInterpolation( h_npc_ignore );
			//cout << "h_npc_interpol_tmp->GetNbinsX(): " << h_npc_interpol_tmp->GetNbinsX() << endl;
			//TH1D * h_npc_interpol = RemoveFirstBin(h_npc_interpol_tmp);
			//cout << "h_npc_interpol->GetNbinsX(): " << h_npc_interpol->GetNbinsX() << endl;

			TH1D * h_npc_interpol_tmp = ApplyLinearInterpolation( h_npc_ignore );
			TH1D * h_npc_interpol = RemoveFirstBin( h_npc_interpol_tmp );
			
			//TH1D * h_npc_interpol = ApplyLinearInterpolation( h_npc_ignore );

			v_npc_interpol.push_back( h_npc_interpol );
			
			/// ////////////////////////////////////////////////////////
			/// Prepare Smoothing
			///
			
			/// Find width_a and width_b for GaussianKernelSmoothing
			///   using lineat fit
			TH1D * h_bin_widths = (TH1D *) h_npc_interpol->Clone();
			       h_bin_widths->Reset();
			       h_bin_widths->SetName("h_bin_widths__" + hName);
			       h_bin_widths->SetTitle("h_bin_widths__" + hName);
			       h_bin_widths->GetXaxis()->SetTitle( h->GetXaxis()->GetTitle() );
			       h_bin_widths->GetYaxis()->SetTitle("bin width");
			       
			for (Int_t j = 1; j < h->GetNbinsX() + 1; j++)
			{
				h_bin_widths->SetBinContent( j, h->GetBinWidth(j) );
			}
			
			TF1 * f_linear = new TF1("f_linear", "[0] + [1]*x", h_bin_widths->GetBinCenter(1), h_bin_widths->GetBinCenter( h_bin_widths->GetNbinsX() - 1) );
			      f_linear->SetParNames("a", "b");
			      f_linear->SetParameters(60, 0.08);
			      
			      h_bin_widths->Fit(f_linear->GetName(), "I WL V E M", "");
			      h_bin_widths->GetListOfFunctions()->Add(f_linear);
			
			v_h1_binwidths.push_back(h_bin_widths);
			//int top; cin >> top;
			
			/// ////////////////////////////////////////////////////////
			/// Gaussian Kernel Smoothing
			cout << "\n\n\n" << endl;
			cout << "=================================================" << endl;
			cout << " Gaussian Kernel Smoothing                       " << endl;
			cout << "=================================================" << endl;

			double h_factor = 1;
			int    method  = 3;
			bool   debug   = false;
			double width_a = f_linear->GetParameter(0);
			double width_b = f_linear->GetParameter(1);
			
			//TH1D * h_npc_interpol_tmp = RemoveFirstBin(h_npc_interpol);
			TH1D * h_npc_smooth = GaussianKernelSmoothing(h_npc_interpol, h_factor, method, debug, width_a, width_b);
			v_npc_smooth.push_back(h_npc_smooth);
			
			TString tdir_GeneratorTune = generator + "_"+ tune;							if (ind_hName == 1) v_tdirs.push_back(tdir_GeneratorTune); //f_NPC->mkdir(tdir_GeneratorTune);
			TString tdir_NPC_Raw       = tdir_GeneratorTune + "/NPC/NPC_Raw";			if (ind_hName == 1) v_tdirs.push_back(tdir_GeneratorTune); //f_NPC->mkdir(tdir_NPC_Raw);
			TString tdir_NPC_Ignore    = tdir_GeneratorTune + "/NPC/NPC_Ignore";		if (ind_hName == 1) v_tdirs.push_back(tdir_GeneratorTune); //f_NPC->mkdir(tdir_NPC_Ignore);
			TString tdir_NPC_Interpol  = tdir_GeneratorTune + "/NPC/NPC_Interpol";		if (ind_hName == 1) v_tdirs.push_back(tdir_GeneratorTune); //f_NPC->mkdir(tdir_NPC_Interpol);
			TString tdir_NPC_Smooth    = tdir_GeneratorTune + "/NPC/NPC_Smooth";		if (ind_hName == 1) v_tdirs.push_back(tdir_GeneratorTune); //f_NPC->mkdir(tdir_NPC_Smooth);
			
			TString h_npc_raw_path = tdir_NPC_Raw;
					//h_npc_raw_path += "/";
					//h_npc_raw_path += hName;
					v_npc_raw_paths.push_back(h_npc_raw_path);
			
			TString h_npc_ignore_path = tdir_NPC_Ignore;
					//h_npc_ignore_path += "/";
					//h_npc_ignore_path += hName;
					v_npc_ignore_paths.push_back(h_npc_ignore_path);
			
			TString h_npc_interpol_path = tdir_NPC_Interpol;
					//h_npc_interpol_path += "/";
					//h_npc_interpol_path += hName;
					v_npc_interpol_paths.push_back(h_npc_interpol_path);
			
			TString h_npc_smooth_path = tdir_NPC_Smooth;
					//h_npc_smooth_path += "/";
					//h_npc_smooth_path += hName;
					v_npc_smooth_paths.push_back(h_npc_smooth_path);
		}
		
		TString rootFile_Results_path = outDir_root + "/" + "NPC_Results.root";
		TFile * f_NPC_Results;
		if (ind_hName == 1)
		{
			if (b_LoadFromIgnoreStepDistribution == true)
			{
				TString cmd_copy = "cp " + inputFile_LoadFromIgnoreStepDistribution + " " + rootFile_Results_path;
				gSystem->Exec( cmd_copy.Data() );
				f_NPC_Results = new TFile(rootFile_Results_path, "UPDATE");
			}
			else
			{
				f_NPC_Results = new TFile(rootFile_Results_path, "RECREATE");
				
				for (TString tdir: v_tdirs)
				{
					f_NPC_Results->mkdir(tdir);
				}
			}

		}
		else
		{
			f_NPC_Results = new TFile(rootFile_Results_path, "UPDATE");
		}
		

		bool b_overWrite = false;
		if (b_LoadFromIgnoreStepDistribution == true)
		{ 
			b_overWrite = true;
		}

		if (b_LoadFromIgnoreStepDistribution == false)
		{
			WriteToRootFile(f_NPC_Results, v_npc_raw,      v_npc_raw_paths, b_overWrite);
			WriteToRootFile(f_NPC_Results, v_npc_ignore,   v_npc_ignore_paths, b_overWrite);
		}
		WriteToRootFile(f_NPC_Results, v_npc_interpol, v_npc_interpol_paths, b_overWrite);
		WriteToRootFile(f_NPC_Results, v_npc_smooth,   v_npc_smooth_paths, b_overWrite);
		f_NPC_Results->Close(); 
		delete f_NPC_Results;
		
		
		TFile f_tmp("f_tmp.root", "RECREATE");
		for (TH1D * h_binWidth: v_h1_binwidths)
		{
			h_binWidth->Write();
		}
		f_tmp.Close();
		
		if (hName.Contains("mjj") || hName.Contains("mass"))
		{
			GlobalFunctions::g_xVarName = "mjj";
			GlobalFunctions::g_xVar= "m_{jj}";
		}
		
		if (hName.Contains("pt"))
		{
			GlobalFunctions::g_xVarName = "pt";
			GlobalFunctions::g_xVar= "p_{T}";
		}
		
		TString text_tmp = hName; 
			text_tmp.ReplaceAll("h1_mjj_", ""); 
			text_tmp.ReplaceAll("h1_pt_", "");
			text_tmp.ReplaceAll("h1_pt_", "");
			text_tmp.ReplaceAll("dijet_invmass_", "");
			text_tmp.ReplaceAll("jet_pt_", "");
			text_tmp.ReplaceAll("_AKT04", "");
		
		TString text;
		if (text_tmp.Contains("0")) text = "0.0<XXX<0.5";
		if (text_tmp.Contains("1")) text = "0.5<XXX<1.0";
		if (text_tmp.Contains("2")) text = "1.0<XXX<1.5";
		if (text_tmp.Contains("3")) text = "1.5<XXX<2.0";
		if (text_tmp.Contains("4")) text = "2.0<XXX<2.5";
		if (text_tmp.Contains("5")) text = "2.5<XXX<3.0";
		if (text_tmp.Contains("6")) text = "3.0<XXX<3.5";
		if (text_tmp.Contains("7")) text = "3.5<XXX<4.0";
		if (text_tmp.Contains("8")) text = "4.0<XXX<4.5";
		
		if (text_tmp.Contains("ystar"))
		{
			GlobalFunctions::g_yVarName = "ystar";
			GlobalFunctions::g_yVar     = "y*";
		}
		else if (text_tmp.Contains("yboost"))
		{
			GlobalFunctions::g_yVarName = "yboost";
			GlobalFunctions::g_yVar     = "y_{boost}";
		}
		else if (text_tmp.Contains("y") and !text.Contains("ystar") and !text_tmp.Contains("yboost"))
		{
			GlobalFunctions::g_yVarName = "y";
			GlobalFunctions::g_yVar     = "|y|";
		}
		
		TString latex = "RUCIO_Run_rel19";
		release rel = release::rel19;
		
		if (rel == release::rel19) 
		{
			GlobalFunctions::xs_unit = unit::nb;
		}
		else
		{
			GlobalFunctions::xs_unit = unit::pb;
		}
		
		text = text.ReplaceAll("XXX", GlobalFunctions::g_yVar);
		
		bool logX = true;
		bool logY = false;
		
		/// Set position of legend
		TLegend * l = new TLegend(0.55, 0.55, 0.93, 0.9);
		GlobalLatex::g_yVarRange = text;
		
		//bool b_doFit = true;
		bool b_doFit = false;
		TString outputName_raw = "npc_raw_" + hName;
		TString outPNG_npc = outDir_png_npc   + "/" + outputName_raw + ".png";
		TString outEPS_npc = outDir_eps_npc   + "/" + outputName_raw + ".eps";
		TString outC_npc   = outDir_macro_npc + "/" + outputName_raw + ".C";
		
		//
		//
		//

		std::pair<TCanvas *, TGraphAsymmErrors *> p_npc_raw = Draw (v_npc_raw, outPNG_npc, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		TCanvas           * c_npc_raw = p_npc_raw.first;
		                    c_npc_raw->SaveAs(outPNG_npc);
		                    c_npc_raw->SaveAs(outEPS_npc);
		                    c_npc_raw->SaveAs(outC_npc);
		TGraphAsymmErrors * g_npc_raw = p_npc_raw.second;
		
		//TGraphAsymmErrors * g_npc_raw_png = Draw (v_npc_raw, outPNG_npc, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_raw_eps = Draw (v_npc_raw, outEPS_npc, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_raw_C   = Draw (v_npc_raw, outC_npc,   logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		
		TString outputName_ignore = "npc_ignore_" + hName;
		TString outPNG_npc_ignore = outDir_png_npc   + "/" + outputName_ignore + ".png";
		TString outEPS_npc_ignore = outDir_eps_npc   + "/" + outputName_ignore + ".eps";
		TString outC_npc_ignore   = outDir_macro_npc + "/" + outputName_ignore + ".C";
		
		std::pair<TCanvas *, TGraphAsymmErrors *> p_npc_ignore = Draw (v_npc_ignore, outPNG_npc_ignore, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		TCanvas           * c_npc_ignore = p_npc_ignore.first;
		                    c_npc_ignore->SaveAs(outPNG_npc_ignore);
		                    c_npc_ignore->SaveAs(outEPS_npc_ignore);
		                    c_npc_ignore->SaveAs(outC_npc_ignore);
		TGraphAsymmErrors * g_npc_ignore = p_npc_ignore.second;
		
		//TGraphAsymmErrors * g_npc_ignore_png = Draw (v_npc_ignore, outPNG_npc_ignore, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_ignore_eps = Draw (v_npc_ignore, outEPS_npc_ignore, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_ignore_C   = Draw (v_npc_ignore, outC_npc_ignore,   logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		
		TString outputName_interpol = "npc_interpol_" + hName;
		TString outPNG_npc_interpol = outDir_png_npc + "/" + outputName_interpol + ".png";
		TString outEPS_npc_interpol = outDir_eps_npc + "/" + outputName_interpol + ".eps";
		TString outC_npc_interpol   = outDir_macro_npc + "/" + outputName_interpol + ".C";
		
		std::pair<TCanvas *, TGraphAsymmErrors *> p_npc_interpol =Draw (v_npc_interpol, outPNG_npc_interpol, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		TCanvas           * c_npc_interpol = p_npc_interpol.first;
		                    c_npc_interpol->SaveAs(outPNG_npc_interpol);
		                    c_npc_interpol->SaveAs(outEPS_npc_interpol);
		                    c_npc_interpol->SaveAs(outC_npc_interpol);
		TGraphAsymmErrors * g_npc_interpol = p_npc_interpol.second;
		
		//TGraphAsymmErrors * g_npc_interpol_png = Draw (v_npc_interpol, outPNG_npc_interpol, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_interpol_eps = Draw (v_npc_interpol, outEPS_npc_interpol, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_interpol_C   = Draw (v_npc_interpol, outC_npc_interpol,   logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		
		TString outputName_smooth = "npc_smooth_" + hName;
		TString outPNG_smooth = outDir_png_npc   + "/" + outputName_smooth + ".png";
		TString outEPS_smooth = outDir_eps_npc   + "/" + outputName_smooth + ".eps";
		TString outC_smooth   = outDir_macro_npc + "/" + outputName_smooth + ".C";
		
		std::pair<TCanvas *, TGraphAsymmErrors *> p_npc_smooth = Draw (v_npc_smooth, outEPS_smooth, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		TCanvas           * c_npc_smooth = p_npc_smooth.first;
		                    c_npc_smooth->SaveAs(outPNG_smooth);
		                    c_npc_smooth->SaveAs(outEPS_smooth);
		                    c_npc_smooth->SaveAs(outC_smooth);
		TGraphAsymmErrors * g_npc_smooth = p_npc_smooth.second;
		
		//TGraphAsymmErrors * g_npc_smooth_eps = Draw (v_npc_smooth, outEPS_smooth, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_smooth_png = Draw (v_npc_smooth, outPNG_smooth, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		//TGraphAsymmErrors * g_npc_smooth_C   = Draw (v_npc_smooth, outC_smooth,   logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		
		/// ////////////////////////////////////////////////////////
		/// Save TGraphAsymmError of the envelope to rootFile
		cout << "\n\n\n" << endl;
		cout << "=================================================" << endl;
		cout << " Save TGraphAsymmError of the envelope to rootFile " << endl;
		cout << "=================================================" << endl;

		TFile * f_NPC_Results_ReOpen = new TFile(rootFile_Results_path, "UPDATE");
		WriteToRootFile(f_NPC_Results_ReOpen, g_npc_raw,      "Envelope/NPC_Raw", b_overWrite);
		WriteToRootFile(f_NPC_Results_ReOpen, g_npc_ignore,   "Envelope/NPC_Ignore", b_overWrite);
		WriteToRootFile(f_NPC_Results_ReOpen, g_npc_interpol, "Envelope/NPC_Interpol", b_overWrite);
		WriteToRootFile(f_NPC_Results_ReOpen, g_npc_smooth,   "Envelope/NPC_Smooth", b_overWrite);
		
		f_NPC_Results_ReOpen->Close(); 
		delete f_NPC_Results_ReOpen;
	
	}
	
	f_NPC_load->Close();
	delete f_NPC_load;
	
	/// ////////////////////////////////////////////////////////
	/// Montage
	cout << "\n\n\n" << endl;
	cout << "=================================================" << endl;
	cout << " Montage                                         " << endl;
	cout << "=================================================" << endl;

	std::map<TString, TString> m_npc_types= { {"01", "raw"},
		                                      {"02", "ignore"},
		                                      {"03", "interpol"},
		                                      {"04", "smooth"}
		                                    };
	const TString pathToMontageScript = outDir_Routine + "/";
	const TString montageScript       = pathToMontageScript + "montage.sh";
	bool  monatageScriptAppend = false;

	for (std::pair<TString, TString> npc_type: m_npc_types)
	{
		TString key = npc_type.first;
		TString val = npc_type.second;
		
		/// y-bins
		TString pathToInputFiles_npc_y  = outDir_png_npc + "/" + "npc_" + val + "_h1_pt_y*.png";
		        pathToInputFiles_npc_y.ReplaceAll( pathToMontageScript, "" );
		TString pathToOutputFiles_npc_y = key + "_Compare_npc_" + val + "_y_all.png";

		CreateMontageCommand( pathToInputFiles_npc_y, pathToOutputFiles_npc_y, montageScript, 3, 3, monatageScriptAppend); 

		if (monatageScriptAppend == false)
		{	
			/// In the very first CreateMontageCommand recriate the script
			/// then only append to the end of the script
			monatageScriptAppend = true;
		}
		
		/// ystar-bins
		TString pathToInputFiles_npc_ystar  = outDir_png_npc + "/" + "npc_" + val + "_h1_mjj_ystar*.png";
		        pathToInputFiles_npc_ystar.ReplaceAll( pathToMontageScript, "" );
		TString pathToOutputFiles_npc_ystar = key + "_Compare_npc_" + val + "_ystar_all.png";

		CreateMontageCommand( pathToInputFiles_npc_ystar, pathToOutputFiles_npc_ystar, montageScript, 3, 2, monatageScriptAppend); 

				
		/// yboost-bins
		TString pathToInputFiles_npc_yboost  = outDir_png_npc + "/" + "npc_" + val + "_h1_mjj_yboost*.png";
		        pathToInputFiles_npc_yboost.ReplaceAll( pathToMontageScript, "" );
		TString pathToOutputFiles_npc_yboost = key + "_Compare_npc_" + val + "_yboost_all.png";

		CreateMontageCommand( pathToInputFiles_npc_yboost, pathToOutputFiles_npc_yboost, montageScript, 3, 2, monatageScriptAppend); 

	}
	
	return 0;
}

