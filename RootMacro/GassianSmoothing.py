
def gaussianKernelSmoothing(histCorr, h=1, method = 3, debug = False, width_a=60, width_b=0.08):
	# smoothed bin contents
	newBinContents = []
	
	# loop over bins of the input histogram
	for i in range(1, histCorr.GetNbinsX() + 1):
		# get the bin center and the gaussian kernel width
		xi = histCorr.GetBinCenter(i)
		
		if method==1:
			# use the constant width parameter
			width = h
		elif method == 3:
			# use variable bin width (linear function)
			width = (width_a + xi * width_b) * h
		else:
			raise RuntimeError("Undefined smoothing method {}".format(method))
		
		if debug:
			print("gaussianKernelSmoothing: bin ", i, ": ", width)
			
		# initialize the new bin content values
		weightedSum = 0
		sumOfWeights = 0
		
		# loop over second time to calculate the weighted sums
		if width != 0:
			for j in range(1, histCorr.GetNbinsX()+1):
				xp = histCorr.GetBinCenter(j)
				
				# calculate the bin weight
				weight = exp( -0.5 * ((xp-xi)/width)**2 )
				
				# add to the total weighted sum and sum of weights
				weightedSum  += histCorr.GetBinContent(j) * weight
				sumOfWeights += weight
		
		# store the new bin content
		if sumOfWeights!=0:
			newBinContents += [ weightedSum / sumOfWeights ]
		else: 
			newBinContents += [ histCorr.GetBinContent(i) ]
	
	# update the bin contents
	for i,newContent in zip( range(1, histCorr.GetNbinsX() + 1), newBinContents ):
		# print(histCorr.GetBinContent(i))
		histCorr.SetBinContent(i, newContent)
		
		# print(histCorr.GetBinContent(i))
	return histCorr
	
