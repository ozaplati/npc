#!/bin/bash
ROOTFLAGS=$(root-config --cflags)
ROOTLD=$(root-config --libs)

###########################
#     COMPILE CLASSES     #
###########################

echo ""
echo "compile -main:"
g++ -o draw.o -c 		draw.C 			-I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file
g++ -o Draw_Rucio.o -c 	Draw_Rucio.cxx 	-I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file
g++ -o draw_slices.o -c draw_slices.C 	-I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file

g++ -o Check_XS.o -c Check_XS.cxx 		-I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file


g++ -o compare_Releases_19_21.o -c compare_Releases_19_21.C -I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file
g++ -o Plot.o -c Plot.C -I  $ROOTFLAGS $FJFLAGS 		# compile main cxx file



######################################
#   CREATE EXECUTABLE FILE OF MAIN   #
######################################
echo "create executable "
g++ -o draw 		draw.o 						$ROOTLD -I  $ROOTFLAGS 		# create executable file
g++ -o Draw_Rucio 	Draw_Rucio.o 	-lstdc++fs	$ROOTLD -I  $ROOTFLAGS 		# create executable file
g++ -o draw_slices 	draw_slices.o 				$ROOTLD -I  $ROOTFLAGS 		# create executable file

g++ -o Check_XS Check_XS.o 			-lstdc++fs 	$ROOTLD -I  $ROOTFLAGS 		# create executable file


g++ -o compare_Releases_19_21 compare_Releases_19_21.o $ROOTLD -I  $ROOTFLAGS 				# create executable file
g++ -o Plot Plot.o $ROOTLD -I  $ROOTFLAGS 				# create executable file


echo ""
echo "DONE!"
