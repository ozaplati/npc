#include <TFile.h>
#include <TH1D.h>
#include <TKey.h>
#include <TDirectory.h>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;
bool g_DEBUG = false;

void TransformHistograms(const char* _inRoot, const char* _outRoot) {
    // Open the input ROOT file in read mode
    TFile* inputFile = TFile::Open(_inRoot, "READ");
    if (!inputFile || inputFile->IsZombie()) {
        std::cerr << "Error: Unable to open input file " << _inRoot << std::endl;
        return;
    }

    // Create the output ROOT file in write mode
    TFile* outputFile = TFile::Open(_outRoot, "RECREATE");
    if (!outputFile || outputFile->IsZombie()) {
        std::cerr << "Error: Unable to create output file " << _outRoot << std::endl;
        inputFile->Close();
        return;
    }

    // Define possible ystar and yboost tags
    std::vector<std::string> yTags = {"ystar0", "ystar1", "ystar2", "ystar3", "ystar4", "ystar5",
                                      "yboost0", "yboost1", "yboost2", "yboost3", "yboost4", "yboost5"};

    // Loop over all keys in the main directory of the input file
    TIter nextKey(inputFile->GetListOfKeys());
    TKey* key;
    while ((key = (TKey*)nextKey())) {
        // Check if the object is a TH1D
        if (std::string(key->GetClassName()) == "TH1D") {
            // Read the histogram
            TH1D* hist = (TH1D*)key->ReadObj();
            if (!hist) continue;

            // Get the histogram's name
            std::string histName = key->GetName(); //hist->GetName();
            
            if (g_DEBUG) 
            { 
                cout << "key:       " << key << endl;
                cout << "histName:  " << histName << endl;
                cout << "histTitle: " << hist->GetTitle() << endl;
            }

            // Extract the prefix from histName (before the second underscore)
            size_t firstUnderscore = histName.find('_');
            size_t secondUnderscore = histName.find('_', firstUnderscore + 1);
            std::string prefix = (secondUnderscore != std::string::npos) ? histName.substr(0, secondUnderscore) : histName;

            // Define the desired directory structure
            std::string dirPath = prefix + "/NPC/NPC_Smooth_FourSteps";

            // Create or retrieve the required directory structure
            TDirectory* baseDir = outputFile->GetDirectory(prefix.c_str());
            if (!baseDir) baseDir = outputFile->mkdir(prefix.c_str());

            TDirectory* npcDir = baseDir->GetDirectory("NPC");
            if (!npcDir) npcDir = baseDir->mkdir("NPC");

            TDirectory* finalDir = npcDir->GetDirectory("NPC_Smooth_FourSteps");
            if (!finalDir) finalDir = npcDir->mkdir("NPC_Smooth_FourSteps");

            // Change to the final directory
            finalDir->cd();

            // Find the relevant yTag in histName
            std::string foundTag;
            for (const auto& tag : yTags) 
            {
                if (g_DEBUG) 
                { 
                    cout << "histName: " << histName << endl;
                    cout << "tag:      " << tag << endl;
                    cout << "histName.find(tag): " << histName.find(tag) << endl;
                    cout << "std::string::npos:  " << std::string::npos << endl;
                }

                if (histName.find(tag) != std::string::npos) {
                    foundTag = tag;
                    break;
                }
            }
            

            // Create the new histogram name based on the found tag
            std::string newHistName = "h1_mjj_" + (foundTag.empty() ? "unknown" : foundTag) + "_ADD";
            
            if (g_DEBUG) 
            { 
                cout << "foundTag:    " << foundTag << endl;
                cout << "newHistName: " << newHistName << endl;
                int stop; cin >> stop; 
            }

            hist->SetName(newHistName.c_str());
            hist->Write();

            // Go back to the root of the output file
            outputFile->cd();

            // Clean up the histogram
            delete hist;
        }
    }

    // Close both files
    inputFile->Close();
    outputFile->Close();

    std::cout << "Histograms successfully transformed and saved in " << _outRoot << std::endl;
}

int main ()
{
    TString inRoot  = "FROM_STANISLAV/NPC_Results_smooth.root";
    TString outRoot = "FROM_STANISLAV/NPC_Results_smooth_transformed.root";

    TransformHistograms(inRoot.Data(), outRoot.Data());

    return (0);
}