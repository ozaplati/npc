#include <iostream>
#include "CONFIG.h"

using namespace std;

/// //////////////////////////////////////////////////////////////// ///
///                    CONFIG - CONSTRUCTOR                          ///
/// //////////////////////////////////////////////////////////////// ///
CONFIG::CONFIG(TString _config)
{
	m_config = _config;
	this->Init_map();
}

CONFIG::CONFIG(string _config)
{
	m_config = TString(_config.c_str());
	this->Init_map();

}

CONFIG::CONFIG(const char * _config)
{
	m_config = TString(_config);
	this->Init_map();
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - DESTRUCTOR                          ///
/// //////////////////////////////////////////////////////////////// ///
CONFIG::~CONFIG()
{
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PRIVATE METHODS                     ///
/// //////////////////////////////////////////////////////////////// ///
TString CONFIG::RemoveWhiteSpace(TString _tString)
{
	string str(_tString.Data());
	
	int ind_start = 0;
	for (std::size_t i = 0; i < str.length(); ++i)
	{
		char ch = str[i];
		if (ch == ' ' || ch == '\t')
		{
			ind_start += 1;
		}
		else
		{
			break;
		}
	}
	
	string str_return = str.substr(ind_start, str.length() - ind_start);
	return(TString(str_return));
}

TString CONFIG::IngnoreComments(TString _line)
{
	/// copy input line std::string
	string line_tmp(_line.Data());
	//cout << "line_tmp: " << line_tmp << endl;
	
	/// as return variable
	TString line_return = "";
	if ( _line.Contains(m_comment) == false)
	{
		return(_line);
	}
	else
	{
		for (std::size_t i = 0; i < line_tmp.length(); i++)
		{
			char ch = line_tmp[i];
			line_return.Append(ch);
			
			/// comments located after # symbol
			if (TString(ch) == m_comment)
			{
				if (i == 0)
				{
					/// this is a comment at the beginning of the line
					line_return = "";
					break;
				}
				else if (i > 0)
				{
					/// check previous char
					char ch_previous = line_tmp[i-1];
					
					/// \\# do not consider as comment
					if (ch_previous == '\\')
					{
						/// # is part of the string
						/// this sequence of \# you want to save
						continue;
					}
					else
					{
						/// ok, there is a comment
							/// save only fist i chars
						line_return = TString(line_tmp.substr(0, i));
						break;
					}
				}
			} 
		}
		return line_return;
	}
}

void CONFIG::Init_map()
{
	vector<TString> lines = this->Read_line_by_line();
	
	/// Loop over lines in config
	for (int i = 0; i < lines.size(); i++)
	{
		TString line = this->IngnoreComments(lines.at(i));

		/// (condition) ? expressionTrue : expressionFalse;
		bool skip_line = (line == "") ? true : false;
		if (skip_line == true) continue;
		
		int index_dots = line.Index(':');
		if (index_dots > -1)
		{
			/// there is specific string of ':'
			///     before : is located key
			///     after  : is located value
			TString key = line;
			key.ReplaceAll(" ", "");
			int n_key = key.Length() - index_dots;
			key.Remove(index_dots, n_key);
			
						
			/// /////////////////////////////////////////////////////////
			/// evaluate val - cut after ':'
			TString tmp_val(line(index_dots + 1, line.Length() - (index_dots + 1)));
			
			
			/// ////////////////////////////////////////////////////////
			/// Look on value
			
			/// multi-line value is implemented via 2xBacksSlashes
			///     in line_end variable
			///
			/// is it multiple line command?
			///    check how many lines you need to consider for this command 
			int n_MultiLine = 0;
			
			/// is multiple-line value ?
			if (tmp_val.Contains(m_newLine))
			{
				/// loop over following lines
				for (int j = i + 1; j < lines.size(); j++)
				{
					/// look on lines
					TString nxt_line = lines.at(j);
					TString prev_line = (j != i) ? lines.at(j-1) : m_newLine;
					
					/// if previus line included line_end string
					/// then add the line to value
					if (prev_line.Contains(m_newLine) == true)
					{
						n_MultiLine += 1;
						
						/// Ignore comments via #
						TString nxt_line_noComments = this->IngnoreComments(nxt_line);
						/// Add to value
						tmp_val += nxt_line_noComments;
					}
					else
					{
						/// no more lines to add to value
						/// so, break loop over j
						break;
					}
				}
			}
			
			/// remove white spaces at beginning of the value		
			TString value = this->RemoveWhiteSpace(tmp_val);
			
			/// replace \comment symbol
			value = value.ReplaceAll(TString("\\" + m_comment), m_comment);
			
			/// remove neLine string in value
			value = value.ReplaceAll(m_newLine, "");
			
			m_map[key] = value;
			
		}
		
	}
	
}


bool CONFIG::IsFound (TString _key) const
{
	bool found = false;
	for (std::pair<TString, TString> element : this->m_map) {
		TString key = element.first;
		
		if (_key == key)
		{
			return(true);
		}
	}
	if (found == false)
	{
		cout << "\n\n!!! ERROR !!!" << endl;
		cout << "\t CONFIG::IsFound (TString _key)" << endl;
		cout << "\t no such key: " << "\""<< _key << "\""  << endl;
		cout << "\t in config file: " << this->m_config << endl;
		cout << endl; 
		cout << "\t There are only following kies: " << endl;
		this->Print_keys();
		throw("END OF PROGRAM");
		//int stop; cin >> stop;	
	}
	return(found);
}

bool CONFIG::To_bool(TString _key, TString _val) const
{
	if (_val == "true" || _val == "True" || _val == "TRUE")
	{
		return(true);
	}
	else if (_val == "false" || _val == "False" || _val == "FALSE")
	{
		return(false);
	}
	else
	{
		cout << "\n\n!!! ATTANTION !!!" << endl;
		cout << "bool CONFIG::Load_bool(TString _key) const" << endl;
		cout << "_key: " << "\"" <<_key << "\"" << endl;
		cout << "_configFile: " << this->m_config << endl;
		cout << "val: " << "\"" << _val << "\"" << " can not be convert to boolen!!!" << endl;
		cout << endl; 
		throw("END OF PROGRAM");
		//int stop; cin >> stop;	
	}
}


vector<TString> CONFIG::Read_line_by_line() const
{
	
	std::ifstream ifs(this->m_config);
	
	std::vector<TString> lines;
	
	if (!ifs) 
	{
		std::cerr << "Cannot open file: " << this->m_config << std::endl;
	}
	else
	{
		for (std::string line; std::getline(ifs, line); /**/) 
		{
			TString s(line);
			lines.push_back(s);
		}
		
	}
	
	return lines;
}


vector<TString> CONFIG::SplitMultiString(TString input, TString splitString)
{
	/****************************************************************************
	 * HelpFunction - split long string to multiple smaller substrings			*
	 *																			*
	 * substrings are divided in two if is it contans the splitString as a key	*
	 * in my case, it is used something like this:								*
	 * "[1,2,3,4][5,6,7][8,9,10]" as input										*
	 * using splitingString = "][" and removing the remaing "[", "]"			*
	 * you can get "1,2,3,4"		"5,6,7"		"8,9,10"						*
	 ****************************************************************************/
	
	int dim = input.CountChar('[');
	
	vector<TString> vStr;
	TString s_temp1 = "";
	TString s_temp = input;
	input.ReplaceAll("\n", "");
	int index = 0;
	while (s_temp.Contains(splitString))
	{
		int ind = s_temp.Index(splitString);
		s_temp.Replace(ind, 2, "");
		int ind_last = s_temp.Length();
		s_temp1 = s_temp(0, ind);
		
		//cout << "\t\t s_temp1 before: " << s_temp1 << endl;
		if (splitString == "][")
		{
			s_temp1.ReplaceAll("[", "");
			s_temp1.ReplaceAll("]", "");
		}
		//cout << "\t\t s_temp1 after: " << s_temp1 << endl;
		vStr.push_back(s_temp1);
		
		s_temp.Replace(0, ind, "");
	
	}

	//cout << "\t s_temp before: " << s_temp1 << endl;
	
	if (splitString == "][")
	{
		s_temp.ReplaceAll("[", "");
		s_temp.ReplaceAll("]", "");
	}
	//cout << "\t s_temp after: " << s_temp1 << endl;

	
	vStr.push_back(s_temp);
	
	return(vStr);
}

TString CONFIG::Get_val(TString _key) const
{
	this->IsFound(_key);
	
	for (std::pair<TString, TString> element : this->m_map) {
		TString key = element.first;
		TString val = element.second;
		
		if (key == _key)
		{
			return(val);
		}
	}
	return(TString(""));
}


TString CONFIG::SystemPath(TString _str) const
{
	if (_str.Contains("$"))
	{
		if (_str.Contains("${") && _str.Contains("}"))
		{
			TString system_var_before(_str(0, _str.Index('$')));
			TString system_var(_str(_str.Index("$") + 2, _str.Index("}") - 2));
			TString system_var_after(_str(_str.Index("}") + 1, _str.Length()));
			
			cout << "_str:              " << _str << endl;
			cout << "system_var_before: " << system_var_before << endl;
			cout << "system_var:        " << system_var << endl;
			cout << "system_var_after:  " << system_var_after << endl;

			if(const char* env_p = std::getenv(system_var.Data()))
			{
				TString s_return = system_var_before + TString(env_p) + system_var_after;
				//cout << "s_return:      " << s_return << endl;
				return (s_return);
			}
			else
			{
				cout << "ATTENTION - TString  CONFIG::SystemPath(TString _str) const" << endl;
				cout << "\t no system variable like: " << system_var << endl;
				cout << "\t inside the _str: " << _str << endl;
				throw("END OF PROGRAM");

			}
		}
		else
		{
			cout << "ATTENTION - TString  CONFIG::SystemPath(TString _key) const" << endl;
			cout << "\t the system variable have to include symbols of  \"$" << "\{" << "\" and \"" << "\"" << "}\" " << endl;
			cout << "\t your _key: " << _str << endl;
			throw("END OF PROGRAM");
		}
	}
	return(_str);
}



/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                             PRINTS                               ///
/// //////////////////////////////////////////////////////////////// ///

void CONFIG::Print_config() const
{
	vector<TString> v = this->Read_line_by_line();
	for_each(
		v.cbegin(), 
		v.cend(),
		[](TString s){cout << s << endl;}
	);
}

void CONFIG::Print_keys() const
{
	for (std::pair<TString, TString> element : this->m_map) {
		TString key = element.first;
		
		std::cout << "\t" << "\"" << key << "\"" << endl;
	}
}

void CONFIG::Print() const
{
	for (std::pair<TString, TString> element : this->m_map) {
		TString key = element.first;
		TString val = element.second;
		
		std::cout << "\"" << key << "\": " << "\t" << "\"" << val << "\"" << std::endl;
	}
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC GET METHODS                  ///
/// //////////////////////////////////////////////////////////////// ///

TString CONFIG::Get_config() const
{
	return(m_config);
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD STRING                            ///
/// //////////////////////////////////////////////////////////////// ///
TString CONFIG::Load_string(TString _key) const
{
	TString val = this->Get_val(_key);
	
	return(val);
}

string CONFIG::Load_string(string _key) const
{
	TString key(_key.c_str());
	TString val = this->Get_val(key);
	
	string str(val.Data());
	return(str);
}
char * CONFIG::Load_string(const char * _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	char * ch = new char[val.Length() + 1];
	strcpy(ch, val.Data());
	
	cout << "\t\t in method ch: " << ch << endl;

	return(ch);
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD PATH                              ///
/// //////////////////////////////////////////////////////////////// ///

TString  CONFIG::Load_path(TString _key, bool _check_if_exists) const
{
	TString str = this->Load_string(_key);
	TString str_return = this->SystemPath(str);
	
	if (_check_if_exists == true)
	{
		/// fs::exists(const char *) in C++ 17 - updated header
		///    return true  if file or path exists
		///    return false if file or path does not exist	
		///	
		///    compile with g++ -lstdc++fs	
		//if (fs::exists(str_return.Data()) == false) 
        if (this->Path_Exists(str_return) == false)
        {
			cout << "\n\n\n" << endl;
			cout << "!!! ATTENTION !!!" << endl;
			cout << "\t TString  CONFIG::Load_path(TString _key, bool _check_if_exists = false) const" << endl;
			cout << "\t TString _key:             " << _key << endl;
			cout << "\t bool    _check_if_exists: " << _check_if_exists << endl;
			cout << "\t TString path: " << str_return << endl;
			cout << "\n" << endl;
			cout << "\t ERROR: file " << _key << " does not exists" << endl;
			cout << "\t EXIT PROGRAM" << endl;
			///exit (EXIT_FAILURE);
			throw("file does not exist");
		}	 
	}
	return(str_return);
}

string CONFIG::Load_path(string _key, bool _check_if_exists)  const
{
	TString str = this->Load_string(TString(_key));
	TString str_path = this->SystemPath(str);
	string str_return(str_path.Data());

	if (_check_if_exists == true)
	{
		/// fs::exists(const char *) in C++ 17 - updated header
		///    return true  if file or path exists
		///    return false if file or path does not exist	
		///	
		///    compile with g++ -lstdc++fs	

		//if (fs::exists(str_path.Data()) == false) 
        if (this->Path_Exists(str_return) == false)
        {
			cout << "\n\n\n" << endl;
			cout << "!!! ATTENTION !!!" << endl;
			cout << "\t string  CONFIG::Load_path(string _key, bool _check_if_exists = false) const" << endl;
			cout << "\t string _key:              " << _key << endl;
			cout << "\t bool    _check_if_exists: " << _check_if_exists << endl;
			cout << "\t TString path: " << str_path << endl;
			cout << "\n" << endl;
			cout << "\t ERROR: file " << _key << " does not exists" << endl;
			cout << "\t EXIT PROGRAM" << endl;
			///exit (EXIT_FAILURE);
			throw("file does not exist");
		}	 
	}
	
	return(str_return);
}
char *   CONFIG::Load_path(const char * _key, bool _check_if_exists)  const
{
	TString str = this->Load_string(TString(_key));
	TString str_path = this->SystemPath(str);
	
	if (_check_if_exists == true)
	{
		/// fs::exists(const char *) in C++ 17 - updated header
		///    return true  if file or path exists
		///    return false if file or path does not exist	
		///	
		///    compile with g++ -lstdc++fs
		
		//if (fs::exists(str_path.Data()) == false) 
		if (this->Path_Exists(str_path) == false)
        {
			cout << "\n\n\n" << endl;
			cout << "!!! ATTENTION !!!" << endl;
			cout << "\t char *  CONFIG::Load_path(const char * _key, bool _check_if_exists = false) const" << endl;
			cout << "\t const char * _key:      " << _key << endl;
			cout << "\t bool  _check_if_exists: " << _check_if_exists << endl;
			cout << "\t TString path: " << str_path << endl;
			cout << "\n" << endl;
			cout << "\t ERROR: file " << _key << " does not exists" << endl;
			cout << "\t EXIT PROGRAM" << endl;
			///exit (EXIT_FAILURE);
			throw("file does not exist");
		}	 
	}
	
	/// convert to char *
	char * ch = new char[str_path.Length() + 1];
	strcpy(ch, str_path.Data());
	
	return(ch);
}

	
/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD BOOL                              ///
/// //////////////////////////////////////////////////////////////// ///

bool CONFIG::Load_bool(TString _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	bool b = this->To_bool(key, val);
	return(b);
}

bool CONFIG::Load_bool(string _key) const
{
	
	TString key(_key.c_str());
	TString val = this->Get_val(key);
	
	bool b = this->To_bool(key, val);
	return(b);
}

bool CONFIG::Load_bool(const char * _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	bool b = this->To_bool(key, val);
	return(b);
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD INT                               ///
/// //////////////////////////////////////////////////////////////// ///

int CONFIG::Load_int(TString _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	int int_return = val.Atoi();
	return(int_return);
}

int CONFIG::Load_int(string _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	int int_return = val.Atoi();
	return(int_return);
}

int CONFIG::Load_int(const char * _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	int int_return = val.Atoi();
	return(int_return);
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD FLOAT                            ///
/// //////////////////////////////////////////////////////////////// ///

float CONFIG::Load_float(TString _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	float f_return = val.Atof();
	return(f_return);
}

float CONFIG::Load_float(string _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	float f_return = val.Atof();
	return(f_return);
}

float CONFIG::Load_float(const char * _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	float f_return = val.Atof();
	return(f_return);
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                           LOAD DOUBLE                            ///
/// //////////////////////////////////////////////////////////////// ///

double CONFIG::Load_double(TString _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	double double_return = val.Atof();
	return(double_return);
}

double CONFIG::Load_double(string _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	double double_return = val.Atof();
	return(double_return);
}

double CONFIG::Load_double(const char * _key) const
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	double double_return = val.Atof();
	return(double_return);
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                      LOAD VECTOR OF STRINGS                      ///
/// //////////////////////////////////////////////////////////////// ///

vector<TString> CONFIG::Load_vector_str(TString _key)
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	val.ReplaceAll(" ", "");
	val.ReplaceAll("\t", "");
	
	vector<TString> vec_str = this->SplitMultiString(val, "][");
	return(vec_str);
}

vector<string> CONFIG::Load_vector_str(string _key)
{
	TString key(_key);
	vector <TString> vec_TStr = this->Load_vector_str(key);
	vector <string> vec_Str;
	for (int i = 0; i < vec_TStr.size(); i++)
	{
		TString t =  vec_TStr.at(i);
	
		string str(t.Data());
		vec_Str.push_back(str);
	}
	
	return(vec_Str);
}

vector<char *> CONFIG::Load_vector_str(const char * _key)
{
	TString key(_key);
	vector <TString> vec_TStr = this->Load_vector_str(key);
	vector < char * > vec_ch;
	for (int i = 0; i < vec_TStr.size(); i++)
	{
		TString t =  vec_TStr.at(i);
		
		char * ch = new char[t.Length() + 1];
		strcpy(ch, t.Data());
		vec_ch.push_back(ch);
	}
	
	return(vec_ch);
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                      LOAD VECTOR OF INT                          ///
/// //////////////////////////////////////////////////////////////// ///

vector<int> CONFIG::Load_vector_int(TString _key)
{
	TString key(_key);
	vector <TString> vec_TStr = this->Load_vector_str(key);
	vector <int> vec_int;
	for (int i = 0; i < vec_TStr.size(); i++)
	{
		TString t =  vec_TStr.at(i);
		int i_int = t.Atoi();
		vec_int.push_back(i_int);
	}
	
	return(vec_int);
}

vector<int> CONFIG::Load_vector_int(string _key)
{
	TString key(_key);
	vector <int> v = this->Load_vector_int(key);
	
	return(v);
}

vector<int> CONFIG::Load_vector_int(const char * _key)
{
	TString key(_key);
	vector <int> v = this->Load_vector_int(key);
	
	return(v);
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                      LOAD VECTOR OF DOUBLE                       ///
/// //////////////////////////////////////////////////////////////// ///

vector<double> CONFIG::Load_vector_double(TString _key)
{
	/// Load vector <double> from config-file
	
	TString key(_key);
	vector <TString> vec_TStr = this->Load_vector_str(key);
	vector <double> vec_double;
	for (int i = 0; i < vec_TStr.size(); i++)
	{
		TString t =  vec_TStr.at(i);
		double i_int = t.Atof();
		vec_double.push_back(i_int);
	}
	
	return(vec_double);
}


vector<double> CONFIG::Load_vector_double(string _key)
{
	/// Load vector <double> from config-file
	TString key(_key);
	vector <double> v = this->Load_vector_double(key);
	
	return(v);
}

vector<double> CONFIG::Load_vector_double(const char * _key)
{
	/// Load vector <double> from config-file
	TString key(_key);
	vector <double> v = this->Load_vector_double(key);
	
	return(v);
}


/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                 LOAD VECTOR OF VECTOR OF STRING                  ///
/// //////////////////////////////////////////////////////////////// ///

/// Load vector vector of string 
vector< vector< TString> > CONFIG::Load_vector_vector_str(TString _key)
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	val.ReplaceAll(" ", "");
	val.ReplaceAll("\t", "");
	
	
	vector<TString> vector_one = this->SplitMultiString(val, "]][[");
	
	vector <vector <TString> > vector_return;
	
	for (TString i_item: vector_one)
	{
		vector<TString> v_str = this->SplitMultiString(i_item, "][");
		vector_return.push_back(v_str);
	}
	return(vector_return);
}


vector< vector< string> > CONFIG::Load_vector_vector_str(string _key)
{
	TString key(_key);
	vector < vector <TString> > vec_vec_t = this->Load_vector_vector_str(key);
	
	vector <vector <string> > vec_vec_return;
	
	for (vector<TString> vec: vec_vec_t)
	{
		vector <string> v;
		for (TString t: vec)
		{
			string str(t.Data());
			v.push_back(str);
		}
		vec_vec_return.push_back(v);
	}
	return(vec_vec_return);
}

vector< vector< char *> > CONFIG::Load_vector_vector_str(const char * _key)
{
	TString key(_key);
	vector < vector <TString> > vec_vec_t = this->Load_vector_vector_str(key);
	
	vector <vector <char *> > vec_vec_return;
	
	for (vector<TString> vec: vec_vec_t)
	{
		vector <char *> v;
		for (TString t: vec)
		{
			char * ch = new char[t.Length() + 1];
			strcpy(ch, t.Data());
			
			v.push_back(ch);
		}
		vec_vec_return.push_back(v);
	}
	return(vec_vec_return);
	

}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                   LOAD VECTOR OF VECTOR OF INT                   ///
/// //////////////////////////////////////////////////////////////// ///

vector< vector< int> > CONFIG::Load_vector_vector_int(TString _key)
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	vector<TString> vector_one = this->SplitMultiString(val, "]][[");
	
	vector <vector <int> > vector_return;
	
	for (TString i_item: vector_one)
	{
		vector<TString> v_str = this->SplitMultiString(i_item, "][");
		vector<int> v_int;
		
		for (int i = 0; i < v_str.size(); i++)
		{
			int i_int = v_str.at(i).Atoi();
			v_int.push_back(i_int);
		}
		
		vector_return.push_back(v_int);
	}
	return(vector_return);
}

vector< vector< int> > CONFIG::Load_vector_vector_int(string _key)
{
	TString key(_key);
	vector< vector<int> > v = this->Load_vector_vector_int(key);
	
	return(v);
}

vector< vector< int> > CONFIG::Load_vector_vector_int(const char * _key)
{
	TString key(_key);
	vector< vector<int> > v = this->Load_vector_vector_int(key);
	
	return(v);
}

/// //////////////////////////////////////////////////////////////// ///
///                     CONFIG - PUBLIC METHODS                      ///
///                 LOAD VECTOR OF VECTOR OF DOUBLE                  ///
/// //////////////////////////////////////////////////////////////// ///


vector< vector< double > > CONFIG::Load_vector_vector_double(TString _key)
{
	TString key(_key);
	TString val = this->Get_val(key);
	
	vector<TString> vector_one = this->SplitMultiString(val, "]][[");
	
	vector <vector <double> > vector_return;
	
	for (TString i_item: vector_one)
	{
		vector<TString> v_str = this->SplitMultiString(i_item, "][");
		vector<double> v_int;
		
		for (int i = 0; i < v_str.size(); i++)
		{
			int i_int = v_str.at(i).Atof();
			v_int.push_back(i_int);
		}
		
		vector_return.push_back(v_int);
	}
	return(vector_return);
}

vector< vector<double> > CONFIG::Load_vector_vector_double(string _key)
{
	TString key(_key);
	vector< vector<double> > v = this->Load_vector_vector_double(key);
	
	return(v);
}

vector< vector<double> > CONFIG::Load_vector_vector_double(const char * _key)
{
	TString key(_key);
	vector< vector<double> > v = this->Load_vector_vector_double(key);
	
	return(v);
}




bool CONFIG::Dir_Exists(TString _dir) const
{
	/// return true if directory if exists
	/// id _dir id file then return false

	const char* pzPath = _dir.Data();
	
    if ( pzPath == NULL) return false;

    DIR *pDir;
    bool bExists = false;

    pDir = opendir (pzPath);

    if (pDir != NULL)
    {
        bExists = true;    
        (void) closedir (pDir);
    }

    return (bExists);
}

bool CONFIG::Path_Exists( TString _path) const
{
	/// path can be file or direcory
	/// return true for file resp. directory if exists
    ifstream f(_path.Data());
    return f.good();
}
