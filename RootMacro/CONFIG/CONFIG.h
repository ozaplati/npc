#ifndef CONFIG_h
#define CONFIG_h

/// std classes
#include <vector>
#include <map>
#include <algorithm>

/// ioput ouput classes
#include <iostream>
#include <fstream>
#include <sstream>

/// string classes
#include <stdio.h>
#include <stdlib.h>
#include <string> 

/// ROOT TString class
#include "TString.h"

#include <dirent.h>


//#include <filesystem>

/*
#ifndef __has_include
  static_assert(false, "__has_include not supported");
#else
#  if __has_include(<filesystem>)
#    include <filesystem>
     namespace fs = std::filesystem;
#  elif __has_include(<experimental/filesystem>)
#    include <experimental/filesystem>
     namespace fs = std::experimental::filesystem;
#  elif __has_include(<boost/filesystem.hpp>)
#    include <boost/filesystem.hpp>
     namespace fs = boost::filesystem;
#  endif
#endif
*/

//#include <filesystem>

using namespace std;

class CONFIG
{
	/*******************************************************************
	 *                        CONFIG class                             *
	 *                       Ota Zaplatilek                            *
	 *                                                                 *
	 *    CLASS FOR LOADING OF CONFIGURATION .env FILE IN C++ CODE     * 
	 *                                                                 *
	 *        This class reads an .env file line by line and           *
	 *    save its content into a privite std::map <TString, TString>  *
	 *                                                                 *
	 *      The .env file have line structure using key and value      *
	 *      separated by double-dot. The .env file looks like this     *
	 *                                                                 *
	 *             # comments                                          *
	 *             key_1 : value_1                                     *
	 *             key_2 : value_2                                     *
	 *                                                                 *
	 * one line comments are implemeted by symbol: '#'                 *
	 * if you want to use '#' in a value/key then substitute:          *
	 *                    '#'   --->   '\#'                            *
	 *                                                                 *
	 *  User can load any value from the privite std::map using        *
	 *  an appropiate key and suitable loading method. The kies of     *
	 *  char *, string, TString  are consider for each loading methods *                               *
	 *                                                                 *
	 * User can read following C++ types within CONFIG class:          *
	 *        int                          char *                      *
	 *        double                       std::string                 *
	 *        float                        TString                     *
	 *        double                                                   *
	 *                                                                 *
	 *             std::vector of all above                            *
	 *             std::vector of std::vector of all above             *
	 *                                                                 *
	 ******************************************************************/
	
	private:
		TString m_config = "";
		map<TString, TString> m_map;
		
		bool Dir_Exists(TString _dir) const;
		bool Path_Exists(TString _dir_or_file) const;
		
		///
		TString m_newLine = "\\\\";
		TString m_comment = "#";
		TString IngnoreComments(TString);	
		TString RemoveWhiteSpace(TString _val);
	
	protected:
		void Init_map ();
		
		TString Get_val(TString _key) const;
		
		bool IsFound (TString _key) const;
		bool To_bool (TString _key, TString _val) const;
		
		vector<TString> SplitMultiString(TString input, TString splitString);
		
		vector<TString> Read_line_by_line() const;
		
		TString SystemPath(TString _str) const;
		
	public:
		/// Constructor
		CONFIG(string  _config);
		CONFIG(TString _config);
		CONFIG(const char * _config);
		
		/// Destructor
		virtual ~CONFIG();
		
		/// Public methods
		
		void Print() const;
		void Print_keys() const;
		void Print_config() const;
		
		TString Get_config() const;
		
		/// Load string variable from config-file
		TString      Load_string(TString _key) const;
		string       Load_string(string _key)  const;
		char * Load_string(const char * _key)  const;

		/// Load system path from string variable in config-file
		TString      Load_path(TString _key, bool _check_if_exists = false) const;
		string       Load_path(string _key, bool _check_if_exists  = false)  const;
		char *       Load_path(const char * _key, bool _check_if_exists = false)  const;
				
		/// Load bool variable from config-file
		bool Load_bool(TString _key) const;
		bool Load_bool(string _key)  const;
		bool Load_bool(const char * _key) const;
		
		/// Load int
		int Load_int(TString _key) const;
		int Load_int(string _key)  const;
		int Load_int(const char * _key) const;
		
		/// Load float
		float Load_float(TString _key) const;
		float Load_float(string _key)  const;
		float Load_float(const char * _key) const;
		
		/// Load double
		double Load_double(TString _key) const;
		double Load_double(string _key)  const;
		double Load_double(const char * _key) const;
		
		/// Load vector of string
		vector<TString> Load_vector_str(TString _key);
		vector<string>  Load_vector_str(string _key);
		vector<char *>  Load_vector_str(const char * _key);
		
		/// Load vector of int
		vector<int> Load_vector_int(TString _key);
		vector<int> Load_vector_int(string _key);
		vector<int> Load_vector_int(const char * _key);
		
		/// Load vector of double
		vector<double> Load_vector_double(TString _key);
		vector<double> Load_vector_double(string _key);
		vector<double> Load_vector_double(const char * _key);
		
		/// Load vector vector of string 
		vector< vector< TString> >  Load_vector_vector_str(TString _key);
		vector< vector< string> >   Load_vector_vector_str(string _key);
		vector< vector< char *> >   Load_vector_vector_str(const char * _key);
		
		/// Load vector vector of int 
		vector< vector< int > > Load_vector_vector_int(TString _key);
		vector< vector< int > > Load_vector_vector_int(string _key);
		vector< vector< int > > Load_vector_vector_int(const char * _key);
		
		/// Load vector vector of double 
		vector< vector< double > > Load_vector_vector_double(TString _key);
		vector< vector< double > > Load_vector_vector_double(string _key);
		vector< vector< double > > Load_vector_vector_double(const char * _key);
};

#endif
