## Setup enviroment
- When you do not work in ATLAS -c conteiner
```
source setup_root.sh
```
- When you  work in ATLAS -c conteiner
```
source setup_root_in_container.sh
```


## Compilation
- Compilation within make and `Makefile`
- To compile all
```
make
```
- To compile only given executable
```
make Check_XS
make Draw_Rucio
mke TransformStanislavsRootFile
make NPC_FinalPlot
mke TransformStanislavsRootFile
```
## Cross-checks
- Code of `Check_XS.cxx` to identifie problematic root-files (with extremly huge MC weight) from large grid production
## Main code for NPC studies
- Code of `Draw_Rucio.cxx` is responsible for most of the NPC studies
- This code computes `NPC` from given `JSAMPLE` histograms, applies last bin cleaning, applies rebinning, computes `NPC`
- Since `NPC_Raw` (mainly in dijets) suffers from many non-smooth problems due to MC weights, several additional procedures are need to apply:
  - `NPC_Ignore` - try to remove problematic bins by agorithm or by `CONGIG.env` file, later it was developed interactive application for bin removal by simple clicking on the problematic bins in `TCanvas` - the interactive application requires more steps explaned later
  - `NPC_Interpolate` - linear interpolation for the removed bins from `NPC_Ignore` step, bin errors are set as average (arithmetic mean) from first abailable bin in front and behind the removed bins
  - `NPC_Smooth` - Gaussian Kernel Smoothing applied on `NPC_Interpolate` step, `fit` procedure tested but not satisfying, best performace found for 4-step Gaussian Kernel Smoothing from Stanislav - such cooperation requirese more steps explaned later
- At the begnning of `main` function are defined the `NPC` inputs as `std::vector< std::tupple>`, each `std::tupple` per one MC generator with given setup:
```
    vector <tuple<TString,   TString, TString, TString, release >> t_gen_tune = {
        {
            "NameOfTheGenerator", 
            "NameOfTheTune", 
                "/PATH/TO/MERGED/SCALED/PARTICLE/LEVEL/ROOT/FILE.root",
                "/PATH/TO/MERGED/SCALED/PARTON/LEVEL/ROOT/FILE.root",
                release::rel19,
                lineColor,
                markerStyle
        },
        ...
        ...
        ...
    }
```
- Demostrative `std::tupple` example for `Pythia_AU2CT10` as:
```
{
    "Pythia", 
    "AU2CT10", 
        "/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_ParticleLVL_AU2CT10_20221122_jet_dijet__rel19",
        "/mnt/nfs19/zaplatilek/IJXS/Rivet/v2/Pythia8_PartonLVL_AU2CT10_20221122_jet_dijet__rel19",
        release::rel19,
        634,
        23
},
```	
- Note several `AthGeneration` releases used for the MC production, each release have some specifics, so define the release as well to pick up proper setup:
  - `release::rel19` for the Aliaksei's rel 19 Rivet routine - used for `Pythia` and `Herwigpp`
  - `release::rel21` for the Ota's rel 21 Rivet routine - used for test with `Pythia`
  - `release::rel23` for the Ota's rel 23 Rivet routine - used for `Herwig7`
- On the top of `main` are several key setup variables additionaly
	const TString inputFile_LoadFromIgnoreStepDistribution = "FOR_STANISLAV/ImprovedIgnoreStepDistributions/NPC_Results__Fixed_and_Updated_IgnoreStepDistributions.root";

	const bool b_rebin = false;		/// true as default
									/// false for Tancredi's request - incl. jet pT with fine binning
	const bool b_dont_eval_NPC = true;

  - `bool b_eval_NPC` - `true` to evaluate `NPC_Raw` from the `JSAMPLES`, this step may take long time; `false` to skip this step
  - `bool b_eval_NPC` - this boolen is also defined using boolen decistion of several more boolen variables, to avoid crash and miss-configuration
  - `const bool b_rebin` - `false` to evaluate `NPC_Raw` in fine 1 GeV binning (typically used for Tancredi and inclusive jets); `true` for dijets 
  - `const bool  b_LoadFromIgnoreStepDistribution` - `false` when you run first-time and you want to evalue the `NPC_Raw` from the `JSAMPLES`; later Ota started to colaborate with Stanislav on 4-Step Gaussina Kernel Smoothing - in this case Ota wants to read the distributions from the `Ignore` step and avoid the `NPC_Raw` evaluation - for this case `true`;
  - `const TString inputFile_LoadFromIgnoreStepDistribution` - if you apply `const bool  b_LoadFromIgnoreStepDistribution = true` then you need to define input root-file in the variable of `NPC_Results__Fixed_and_Updated_IgnoreStepDistributions`, this root-file will be copied and updated (the `Interpolate`, `Smooth` and `Envelope` will be overwited in the copied root-file)
  - To run the code just compile it and run as
```
make  Draw_Rucio
./Draw_Rucio
```
- When the run is finished, then you may download the results via `scp` and merge final plots using genrated `montage.sh` script
```
scp -r $ui3:/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/Fork/npc/RootMacro/RUCIO_Run .
cd RUCIO_Run/Routine
source montage.sh
```
## Interactive bin removal application
- Interactive bin removal application is implemented in `InteractiveBinRemoval.C`
- To run it you need `x` windows - thus it will not run on `ATLAS -c` container, so run it locally or within the `ATLAS -c` container
- You should apply the modifications with `InteractiveBinRemoval.C` code on the `Ignore` step histograms only!
```
# Load a root-file
root -l NPC_Results.root

# Load the code
.L InteractiveBinRemoval.C

# Copy the current root file NPC_Results.root to NPC_Results_copy.root
CopyRootFile()

# Open TBrowser and navigate to you histogram in TBrowser

new TBrowser

# Now you should looking on your histogram, which you want to modify, in the TBrowser
# Now you can run following commad to create new TCanvas for InteractiveBinRemoval
InteractiveBinRemoval( (TH1*)gPad->GetPrimitive("h1_mjj_ystar_ADD") ); 

# Where "h1_mjj_ystar_ADD" is a name of the histogram, which you want to modify and simutainously, which you are looking on it the the TBroeser - When you make a typo it returns an error - it is done due for safety to avoid typo and missleading modification of wrong histograms ;-)

# Now clinking on the bin contents you remove can remove the problematic bins
# When you done finish the interaction removing mode type "save"
save

# Now the updated histogram is saved to the copied rootfile of NPC_Results_copy.root and the InteractiveRemoval mode is ended
```
- With the toolkit of `InteractiveBinRemoval.C` you can also performe several more check and updates:
  - Have a double-check of `BinContents` of all the histogram in the current `TDirectory` opened in your `TBrowser` in the original root-file of `NPC_Results.root` using the functions:
```
void CheckBinContent(int _iBin = 1);
void CheckBinContentLast();
```
  - You can also modify `BinContents` and `BinErrors` in the histogram and save it into te copied root-file of `NPC_Results_copy.root` using the functions:
```
void RedefineLastBinContent(TH1* hist, double _lastBinContent = 1.0, double _lastBinError = 0.1) 
void RedefineBinContent    (TH1* hist, int _iBin, double _lastBinContent = 1.0, double _lastBinError = 0.1) 
```
  - Apply the `Redefine` functions again in savety way to avoid of typos, demostrative example:
```
RedefineLastBinContent( (TH1*)gPad->GetPrimitive("h1_mjj_ystar_ADD"), 1.0, 0.1 ); 
RedefineBinContent    ( (TH1*)gPad->GetPrimitive("h1_mjj_ystar_ADD"), 1, 1.0, 0.1 ); 
```
- Typically when you finish the manual modification of `Ignore` step histogram, you can run the `Draw_Rucio.cxx` code on the ipdated root-file - Thus update the `main` section in `Draw_Rucio.cxx`:
```
const bool b_LoadFromIgnoreStepDistribution = true
const TString inputFile_LoadFromIgnoreStepDistribution = path/to/my/new/updated/root/file/of/NPC_Results_copy.root
```



