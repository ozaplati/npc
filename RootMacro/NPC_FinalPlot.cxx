/// C++17 features - need to link with -lstdc++fs
#include <fstream>
#include <iostream>
#include <filesystem>
#include <iomanip>
namespace fs = std::filesystem;

/// ROOT
#include <TCanvas.h>
#include <TString.h>
#include <TLatex.h>
#include <TH1D.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include <TKDE.h>


/// C++ 
#include <tuple>
#include <vector>
#include <map>
#include <type_traits>

/// ROOT
#include <TROOT.h>
#include "TROOT.h"
#include "TSystem.h"
#include <TStyle.h>
#include <TFile.h>
#include <TPaveStats.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>

/// Ota
//#include "CONFIG/CONFIG.h"
//#include "CONFIG/CONFIG.cxx"
using namespace std;



namespace GlobalLatex
{
	TString g_desc      = "Non-perturbative corrections";
	TString g_ATLASSim  = "ATLAS Simulation";
	TString g_eng       = "#sqrt{s} = 13 TeV";
	TString g_antKt     = "anti-kt R=0.4";
	TString g_yVarRange = "0.0<y*0.5";
	TString g_tune      = "";
}

namespace GlobalFunctions
{
	TString g_xVarName = "mjj";
	TString g_xVar     = "m_{jj}";
	
	TString g_yVarName = "ystar";
	TString g_yVar     = "y*";
}

void CreateMontageCommand( const std::string& _pathToInputFiles, const std::string& _pathToOutputFile, const std::string& _montage_script) 
{
    // Construct the montage command
    std::string cmd_montage = "montage -mode concatenate -tile 3x2 " + _pathToInputFiles + " " +  _pathToOutputFile ;

	// Open the montage script file in write mode to recreate it each time
    std::ofstream file(_montage_script);

    if (file.is_open()) {
        // Write the command to the file
        file << cmd_montage << "\n";
        file.close();
    } else {
        std::cerr << "Error: Unable to open file " << _montage_script << std::endl;
    }
}

template<typename MyObject>
void WriteToRootFile (TFile * _f, MyObject _obj, TString _path)
{
	TDirectory * dir = _f->GetDirectory(_path); 
	if (!dir)
	{
		_f->mkdir(_path);
	}
	_f->cd(_path);
	
	_obj->Write();
	int n_subDirs = _path.CountChar('/');
	for (int i = 0; i < n_subDirs; i++)
	{
		_f->cd("/..");
	}
}

void SetupGlobalVariables(TString hName)
{
    if (hName.Contains("mjj") || hName.Contains("mass"))
    {
        GlobalFunctions::g_xVarName = "mjj";
        GlobalFunctions::g_xVar= "m_{jj}";
    }
    
    if (hName.Contains("pt"))
    {
        GlobalFunctions::g_xVarName = "pt";
        GlobalFunctions::g_xVar= "p_{T}";
    }
    
    TString text_tmp = hName; 
        text_tmp.ReplaceAll("h1_mjj_", ""); 
        text_tmp.ReplaceAll("h1_pt_", "");
        text_tmp.ReplaceAll("h1_pt_", "");
        text_tmp.ReplaceAll("dijet_invmass_", "");
        text_tmp.ReplaceAll("jet_pt_", "");
        text_tmp.ReplaceAll("_AKT04", "");
    
    TString text;
    if (text_tmp.Contains("0")) text = "0.0<XXX<0.5";
    if (text_tmp.Contains("1")) text = "0.5<XXX<1.0";
    if (text_tmp.Contains("2")) text = "1.0<XXX<1.5";
    if (text_tmp.Contains("3")) text = "1.5<XXX<2.0";
    if (text_tmp.Contains("4")) text = "2.0<XXX<2.5";
    if (text_tmp.Contains("5")) text = "2.5<XXX<3.0";
    if (text_tmp.Contains("6")) text = "3.0<XXX<3.5";
    if (text_tmp.Contains("7")) text = "3.5<XXX<4.0";
    if (text_tmp.Contains("8")) text = "4.0<XXX<4.5";
    
    if (text_tmp.Contains("ystar"))
    {
        GlobalFunctions::g_yVarName = "ystar";
        GlobalFunctions::g_yVar     = "y*";
    }
    else if (text_tmp.Contains("yboost"))
    {
        GlobalFunctions::g_yVarName = "yboost";
        GlobalFunctions::g_yVar     = "y_{boost}";
    }
    else if (text_tmp.Contains("y") and !text.Contains("ystar") and !text_tmp.Contains("yboost"))
    {
        GlobalFunctions::g_yVarName = "y";
        GlobalFunctions::g_yVar     = "|y|";
    }
    
    
    text = text.ReplaceAll("XXX", GlobalFunctions::g_yVar);
    GlobalLatex::g_yVarRange = text;
}

TGraphAsymmErrors * RemoveUnderFlowBin(TGraphAsymmErrors * _g)
{
    int n = _g->GetN();  // Get the number of points in the original graph
    TGraphAsymmErrors *g_new = new TGraphAsymmErrors(n - 1);  // Create a new graph with one less point

    for (int i = 1; i < n; i++) {  // Start from the second point (i = 1)
        double x, y;
        _g->GetPoint(i, x, y);  // Get point from the original graph
        g_new->SetPoint(i - 1, x, y);  // Set point in the new graph

        // Set the asymmetric errors for the new graph
        g_new->SetPointError(
            i - 1,
            _g->GetErrorXlow(i), _g->GetErrorXhigh(i),
            _g->GetErrorYlow(i), _g->GetErrorYhigh(i)
        );
    }
    return (g_new);
}
//void Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
//TGraphAsymmErrors * Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
std::pair<TCanvas *, TGraphAsymmErrors *> Draw (vector<TH1D *> &_v, TString _outName, bool _logX, bool _logY, void (*pf_pad1) (TH1D*), void (*pf_pad1_Asym) (TGraphAsymmErrors*), TString _latexText, TLegend * _legend, void (*pf_drawLatex)(), bool _doFit = false)
{
	///
	/// One pad Draw function
	///
	bool b_logX = _logX;
	bool b_logY = _logY;
	
	std::vector<TH1D *> v_copy; 
	copy(_v.begin(), _v.end(), back_inserter(v_copy));
	
	/// Canvas
	TCanvas * c = new TCanvas("c", "canvas", 800, 800);
	gROOT->ForceStyle();
	gStyle->SetOptStat(0);
	
	/// Upper plot will be in pad1
	TPad *pad1 = new TPad("pad1", "pad1", 0.0, 0.0, 1.0, 1.0);
	//pad1->SetBottomMargin(0.12); 
	//pad1->SetRightMargin(0.05);
	//pad1->SetTopMargin(0.08);
	pad1->SetBottomMargin(0.12); 
	pad1->SetLeftMargin(0.125);
	pad1->SetRightMargin(0.05);
	pad1->SetTopMargin(0.08);
	
	pad1->SetTicks();
	pad1->SetGridx();
	
	if (b_logX == true) pad1->SetLogx();
	if (b_logY == true) pad1->SetLogy();
	
	pad1->Draw();
	pad1->cd();
	
	/// Legend
	TLegend * legend;
	if (_legend != nullptr) legend = (TLegend *) _legend->Clone();
	else                    legend = new TLegend(0.75, 0.45, 0.88, 0.9);
	legend->SetFillColor(0);
	legend->SetFillStyle(0);
	legend->SetBorderSize(0);
	//legend->SetTextSize(0.0325); // * 6.0/_v.size());
	
	
	/// TGraph - envelope of all tunes
	/// Init vectors for TGraph
	TH1D * h_tmp = v_copy.front();
	
	vector<double> x_val;
	vector<double> x_low;
	vector<double> x_high;
	vector<double> y_val;
	vector<double> y_err_down;
	vector<double> y_err_up;
	
	/// Find Pythia ATLAS14NNPDF for central val. of the envelope
	TH1D * h_envelope_centralVal = nullptr;
	
	for (TH1D * h: v_copy)
	{
		TString hName_tmp = h->GetName();
		TString hTitle_tmp = h->GetTitle();
		if (hName_tmp.Contains("ATLAS14NNPDF") || hTitle_tmp.Contains("ATLAS14NNPDF"))
		{
			h_envelope_centralVal = (TH1D *) h->Clone();
		}
	}
	/// Loop over all bins
	for (int i=1; i < h_tmp->GetNbinsX() + 1; i++)
	{
		/// x-axis values
		x_val.push_back (h_tmp->GetBinCenter(i));
		x_low.push_back (h_tmp->GetBinCenter(i)  - h_tmp->GetBinLowEdge(i));
		x_high.push_back(h_tmp->GetBinLowEdge(i) + h_tmp->GetBinWidth(i) - h_tmp->GetBinCenter(i));
		
		/// y-axis values
		double up   = -999;		/// up unc.
		double down =  999;		/// down unc.
		
		/// Loop over all tunes
		/// find min and max for the envelope
		for (unsigned int ih = 0; ih < v_copy.size(); ih++)
		{
			TH1D * h = v_copy.at(ih);
			double val_i  = h->GetBinContent(i);
			if (val_i > up)   up   = val_i;
			if (val_i < down) down = val_i;
		}
		
		/// value should be approx 1.0, but more save will be use aritmetics mean
		double mean = (up + down)/2.0;
		double centralVal = mean;
		if (h_envelope_centralVal != nullptr)
		{
			centralVal = h_envelope_centralVal->GetBinContent(i);
		}
		
		y_val.push_back(centralVal);
		y_err_down.push_back(centralVal - down);
		y_err_up.push_back(up - centralVal);
	}
	
	//for (int i = 0; i < x_val.size(); i++)
	//{
	//	cout << i << "\t bin, x "  << x_val.at(i) << " (" << x_low.at(i) << ", "  << x_high.at(i) << ") \t y:" << y_val.at(i) << " ( " << y_err_down.at(i) << ", " << y_err_up.at(i) << ")" << endl;
	//}
	//int top; cin >> top;
	
	TGraphAsymmErrors * g_envelope = new TGraphAsymmErrors(	x_val.size(), 
															x_val.data(), 
															y_val.data(),
															x_low.data(),
															x_high.data(),
															y_err_down.data(),
															y_err_up.data() );
	TString gName = "g_npc"; 
	if (_outName.Contains("raw"))           gName += "_raw";
	else if (_outName.Contains("interpol")) gName += "_interpol";
	else if (_outName.Contains("ignore"))   gName += "_ignore";
	else if (_outName.Contains("smooth"))   gName += "_smooth";
	
	gName += "_envelope_"+ GlobalFunctions::g_xVarName + "_" + GlobalFunctions::g_yVarName;
		vector <TString> v_ind = {"0", "1", "2", "3", "4", "5", "6", "7", "8"};
		/// index
		for (TString ind: v_ind)
		{
			TString yVarI = GlobalFunctions::g_yVarName + ind;
			/// test index
			if (_outName.Contains(yVarI))
			{
				gName += ind;
				break;
			}
		}
	
	g_envelope->SetName(gName);
	g_envelope->SetTitle(gName);
	g_envelope->GetXaxis()->SetTitle(GlobalFunctions::g_yVarName);
	g_envelope->GetXaxis()->SetTitle("NPC Envelope [-]");
	
	/// Apply graphics function
	if (pf_pad1_Asym != nullptr) pf_pad1_Asym(g_envelope);
	
	/// Draw envelope at background of the canvas
    //g_envelope->GetXaxis()->SetRange(2, g_envelope->GetPointX()); 
    TGraphAsymmErrors * g_envelope2 = RemoveUnderFlowBin(g_envelope);
    if (pf_pad1_Asym != nullptr) pf_pad1_Asym(g_envelope2);

	g_envelope2->Draw("PSame a2 E");
	
	
	/// Loop over histograms
	for (unsigned int i = 0; i < v_copy.size(); i++)
	{	
		TH1D * h = v_copy.at(i);
		
		/// Apply graphics
		h->SetStats(0);
		h->GetXaxis()->SetTitleSize(38);
		h->GetYaxis()->SetTitleSize(38);
		
		h->GetXaxis()->SetTitleFont(43);
		h->GetYaxis()->SetTitleFont(43);
		
		h->GetXaxis()->SetTitleOffset(1.0);
		h->GetYaxis()->SetTitleOffset(1.0);
		
		h->SetMarkerColor( h->GetLineColor() );
		h->SetLineWidth(2);
		h->SetMarkerSize(2);
		
		/// Draw
		TString title = h->GetTitle();
		
		/// Title for legend
		TString l_desc = title;
		for (int i = 0; i < l_desc.Length(); i++)
		{
			char ch = l_desc[i];
			if (ch == '_')
			{
				if (i < l_desc.Length ())
				{
					if (l_desc[ i + 1] == '{')
					{
						continue;
					}
				}
				l_desc.Replace(i, 1, ' ');
			}
		}
		
		/// Call fit function - need to have name of generator in title
        
        /// to exclude underflow and overflow bins in the plot
        h->GetXaxis()->SetRange(2, h->GetNbinsX()); 
		h->Draw("p same E");
		//if (_doFit) FitFunction(h);
		
		h->SetTitle("");
		h->Draw("p same E");
		
		if (pf_pad1!= nullptr) pf_pad1(h);
		
		/// Add to legend
		legend->AddEntry(h, l_desc.Data(), "lep");
		
		TLatex * latex = new TLatex();
		latex->SetNDC();
		latex->SetTextSize(0.05);
		latex->SetTextFont(72);
		
		//latex->DrawLatex(0.15, 0.94, "ATLAS, Work in progress");
		latex->DrawLatex(0.15, 0.94, "ATLAS Internal");
		
		if (pf_drawLatex != nullptr) pf_drawLatex();
	
			
		
		
	}
	
	legend->AddEntry(g_envelope, "Sys. unc.", "f");
	
	/// Draw legend
	legend->Draw();
	
	/// Save Plot
	c->SaveAs(_outName);
	
	//return(g_envelope);
	std::pair <TCanvas *, TGraphAsymmErrors *> p_return = std::make_pair(c, g_envelope);
	return( p_return );
}

///

void Pad1Function_NPC(TH1D * _h)
{
	cout << "Pad1Funtion" << endl;
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	_h->GetYaxis()->SetTitle("Non-perturbative correction [-]");
	//_h->GetYaxis()->SetRangeUser(0.9, 1.25);
	_h->GetYaxis()->SetRangeUser(0.8, 1.25);
	
}
void Pad1Function_NPC_TGraph(TGraphAsymmErrors * _h)
{
	cout << "Pad1Function_NPC_TGraph" << endl;
	
	_h->GetXaxis()->SetTitle(GlobalFunctions::g_xVar + " [GeV]");
	_h->GetYaxis()->SetTitle("Non-perturbative correction [-]");
	_h->SetTitle("");
	
	//_h->GetYaxis()->SetRangeUser(0.9, 1.25);
	
	//_h->GetYaxis()->SetRangeUser(0.7, 1.4);
	_h->GetYaxis()->SetRangeUser(0.85, 1.3);
	_h->SetFillColorAlpha(411, 0.5);
	
	_h->GetXaxis()->SetTitleSize(38);
	_h->GetYaxis()->SetTitleSize(38);
	_h->GetXaxis()->SetTitleFont(43);
	_h->GetYaxis()->SetTitleFont(43);
	_h->GetXaxis()->SetTitleOffset(1.0);
	_h->GetYaxis()->SetTitleOffset(1.0);

}

void Function_DrawLatex_NPC()
{
	TLatex * latex = new TLatex();
	latex->SetNDC();
	latex->SetTextSize(0.04);
	latex->SetTextFont(72);
	
	//vector<TString> v_text = {GlobalLatex::g_desc, GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	vector<TString> v_text = {GlobalLatex::g_ATLASSim, GlobalLatex::g_eng, GlobalLatex::g_antKt, GlobalLatex::g_yVarRange};
	
	for (unsigned int iStr=0; iStr < v_text.size(); iStr++)
	{
		//latex->DrawLatex(0.13, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
		latex->DrawLatex(0.2, 0.85 - iStr * 0.05 * 1.25, v_text.at(iStr).Data());
	}
}



int main()
{
	/// ////////////////////////////////////////////////////////////////
	std::cout << "=================================================" << std::endl;
	std::cout << " MAIN                                            " << std::endl;
	std::cout << "=================================================" << std::endl;
    
	const TString inRootFile = "FROM_STANISLAV/NPC_Results_smooth_transformed.root";
	
    /// const tupple means const all items inside
	const std::vector < std::tuple< TString, TString, TString, TString, int, int >> t_inputs = {
			///{
			///	"GeneratorName", 
			///	"TuneName", 
			///		"PATH/TO/THE/TDirectory",
			///		"DESCRIPTION",
			///		color
            ///     marker
			///}
			
			{
				"Pythia", 
				"AU2CT10", 
					"Pythia_AU2CT10/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					634,
					23
			},
			{
				"Pythia", 
				"AU2CTEQ", 
					"Pythia_AU2CTEQ/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					802,
					22
			},
			{
				"Pythia", 
				"ATLAS14NNPDF", 
                    "Pythia_ATLAS14NNPDF/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
                    1,
					20
			},
			{
				"Pythia", 
				"ATLASA14CTEQL1", 
                	"Pythia_ATLASA14CTEQL1/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
                	4,
					24
			},
            
			{
				"Herwigpp", 
				"UEEE5CTEQ6L1", 
                    "Herwigpp_UEEE5CTEQ6L1/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					632,
					21
			},
			{
				"Herwigpp", 
				"UEEE5MSTW2008", 
					"Herwigpp_UEEE5MSTW2008/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
                    804,
					25
			},
			{
				"Herwigpp", 
				"CTEQEE4", 
					"Herwigpp_CTEQEE4/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
                    402,
					47
			},
            
			{
				"Herwig7", 
				"DefaultNNPDF23", 
					"Herwig7_DefaultNNPDF23/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					618,
					28
			},
			{
				/// ReconnTune - v8 - 200k events per slice
				/// Look very good
				"Herwig7", 
				"ReconnectionNNPDF23", 
					"Herwig7_ReconnectionNNPDF23/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					865,
					38
			},
			{
				/// SoftTune - v8_rel23 - 200k events per slice
				/// 
				"Herwig7", 
				"SoftTuneNNPDF23", 
					"Herwig7_SoftTuneNNPDF23/NPC/NPC_Smooth_FourSteps/",
                    "Smooth - Stanislav",
					598,
					20
			},
			
		};
    
    const vector<TString> v_hNames = {
                            /// dijet mjj yStar
                            "h1_mjj_ystar0_ADD",
                            "h1_mjj_ystar1_ADD",
                            "h1_mjj_ystar2_ADD",
                            "h1_mjj_ystar3_ADD",
                            "h1_mjj_ystar4_ADD",
                            "h1_mjj_ystar5_ADD",
                            /// dijet mjj yBoost
                            "h1_mjj_yboost0_ADD",
                            "h1_mjj_yboost1_ADD",
                            "h1_mjj_yboost2_ADD",
                            "h1_mjj_yboost3_ADD",
                            "h1_mjj_yboost4_ADD",
                            "h1_mjj_yboost5_ADD",
        };

    /// ////////////////////////////////////////////////////////////////
	std::cout << "\n\n\n" << endl;
	std::cout << "=================================================" << std::endl;
	std::cout << " MAKE DIRECTORIES                                " << std::endl;
	std::cout << "=================================================" << std::endl;
	
	TString outDir_RUCIO = "FROM_STANISLAV/RUCIO_Run";

	//TString outDir_Routine = outDir_RUCIO + "/" + Rivet::RivetRoutineName;
	TString outDir_Routine = outDir_RUCIO   + "/Routine";
	TString outDir_png     = outDir_Routine + "/png";
	TString outDir_eps     = outDir_Routine + "/eps";
	TString outDir_root    = outDir_Routine + "/root";
	TString outDir_macro   = outDir_Routine + "/macro";
	
	//TString outDir_png_JSamples = outDir_Routine + "/png/JSamples";
	//TString outDir_eps_JSamples = outDir_Routine + "/eps/Jsamples";
	//TString outDir_macro_JSamples = outDir_Routine + "/macro/Jsamples";
	
	TString outDir_png_npc = outDir_Routine   + "/png/npc";
	TString outDir_eps_npc = outDir_Routine   + "/eps/npc";
	TString outDir_macro_npc = outDir_Routine + "/macro/npc";
	
	//TString outDir_png_JSamples_Particle   = outDir_Routine + "/png/JSamples/Particle";
	//TString outDir_eps_JSamples_Particle   = outDir_Routine + "/eps/Jsamples/Particle";
	//TString outDir_macro_JSamples_Particle = outDir_Routine + "/macro/Jsamples/Particle";
	
	//TString outDir_png_JSamples_Parton   = outDir_Routine + "/png/JSamples/Parton";
	//TString outDir_eps_JSamples_Parton   = outDir_Routine + "/eps/Jsamples/Parton";
	//TString outDir_macro_JSamples_Parton = outDir_Routine + "/macro/Jsamples/Parton";
	
	TString cmd_outDir_RUCIO  = "mkdir -p " + outDir_RUCIO;
	TString cmd_outDir_RivetRoutine = "mkdir -p " + outDir_Routine;
	TString cmd_outDir_png    = "mkdir -p " + outDir_png;
	TString cmd_outDir_eps    = "mkdir -p " + outDir_eps;
	TString cmd_outDir_root   = "mkdir -p " + outDir_root;
	TString cmd_outDir_macro  = "mkdir -p " + outDir_macro;
	
	//TString cmd_outDir_png_JSamples    = "mkdir -p " + outDir_png_JSamples;
	//TString cmd_outDir_eps_JSamples    = "mkdir -p " + outDir_eps_JSamples;
	//TString cmd_outDir_macro_JSamples  = "mkdir -p " + outDir_macro_JSamples;
	
	TString cmd_outDir_png_npc    = "mkdir -p " + outDir_png_npc;
	TString cmd_outDir_eps_npc    = "mkdir -p " + outDir_eps_npc;
	TString cmd_outDir_macro_npc  = "mkdir -p " + outDir_macro_npc;
	
	//TString cmd_outDir_png_JSamples_Particle    = "mkdir -p " + outDir_png_JSamples_Particle;
	//TString cmd_outDir_eps_JSamples_Particle    = "mkdir -p " + outDir_eps_JSamples_Particle;
	//TString cmd_outDir_macro_JSamples_Particle  = "mkdir -p " + outDir_macro_JSamples_Particle;
	
	//TString cmd_outDir_png_JSamples_Parton    = "mkdir -p " + outDir_png_JSamples_Parton;
	//TString cmd_outDir_eps_JSamples_Parton    = "mkdir -p " + outDir_eps_JSamples_Parton;
	//TString cmd_outDir_macro_JSamples_Parton  = "mkdir -p " + outDir_macro_JSamples_Parton;
	
	gSystem->Exec(cmd_outDir_RUCIO.Data());
	gSystem->Exec(cmd_outDir_RivetRoutine.Data());
	gSystem->Exec(cmd_outDir_png.Data());
	gSystem->Exec(cmd_outDir_eps.Data());
	gSystem->Exec(cmd_outDir_root.Data());
	gSystem->Exec(cmd_outDir_macro.Data());
	
	//gSystem->Exec(cmd_outDir_png_JSamples.Data());
	//gSystem->Exec(cmd_outDir_eps_JSamples.Data());
	//gSystem->Exec(cmd_outDir_macro_JSamples.Data());
	
	gSystem->Exec(cmd_outDir_png_npc.Data());
	gSystem->Exec(cmd_outDir_eps_npc.Data());
	gSystem->Exec(cmd_outDir_macro_npc.Data());
	
	//gSystem->Exec(cmd_outDir_png_JSamples_Particle.Data());
	//gSystem->Exec(cmd_outDir_eps_JSamples_Particle.Data());
	//gSystem->Exec(cmd_outDir_macro_JSamples_Particle.Data());
	
	//gSystem->Exec(cmd_outDir_png_JSamples_Parton.Data());
	//gSystem->Exec(cmd_outDir_eps_JSamples_Parton.Data());
	//gSystem->Exec(cmd_outDir_macro_JSamples_Parton.Data());

	TString outRoot_envelope = outDir_root + "/" + "NPC_Results_Smooth_FourSteps_Envelope.root";
	TFile * f_NPC_Results_Envelope = new TFile(outRoot_envelope, "RECREATE");

    for (auto hName: v_hNames)
    {
        /// Per one yStar  bin
        /// Per one yBoost bin resp.

        vector<TH1D *> v_hist;

        for (auto input: t_inputs)
        {
            /// Load values from tuple
            const TString hGenerator = std::get<0>(input); // "Herwig7";
			const TString hTune      = std::get<1>(input); // "SoftTuneNNPDF23", 
            const TString hPath      = std::get<2>(input);
            const TString hDesc      = std::get<3>(input);
            const int hColor         = std::get<4>(input);
            const int hMarker        = std::get<5>(input);

            /// Paths to histogram
            const TString hName_tPath = hPath + "/" + hName;
            const TString rootFile    = inRootFile;

            /// Load the histogram
            cout << "Loading:" << endl;
            cout << "\t rootFile: " << rootFile << endl;
            cout << "\t hName:    " << hName_tPath << endl;
            
            TFile * f = new TFile(rootFile, "READ");
                cout << "    TFile:ls() :" << endl;
                f->ls();
            
            TH1D *h =f->Get<TH1D>(hName_tPath);
                /// This is a hack to be able to close the root-File
                ///      based on https://root-forum.cern.ch/t/get-histogram-and-close-file/18867/11
                h->SetDirectory(0);
                /// Check the histogram
                cout << "    Check the histogram: " << endl;
                cout << "        f->GetName():  " << f->GetName()  << endl;
                cout << "        h->Integral(): " << h->Integral() << endl;
            
            /// Update the histogram
            h->SetLineColor(hColor);
            h->SetMarkerStyle(hMarker);
            TString title = hGenerator + " " + hTune + " " + hDesc;
            h->SetTitle(title);

            /// Store the histogram in vector
            v_hist.push_back(h);
            
            f->Close();
            delete f;
        }

        /// Prepare to plot
        SetupGlobalVariables(hName);

		bool logX = true;
		bool logY = false;
		
		/// Set position of legend
		TLegend * l = new TLegend(0.55, 0.55, 0.93, 0.9);
		
		//bool b_doFit = true;
		bool b_doFit = false;

        TString outputName = "npc_smooth_stanislav" + hName;
		TString outPNG = outDir_png_npc   + "/" + outputName + ".png";
		TString outEPS = outDir_eps_npc   + "/" + outputName + ".eps";
		TString outC   = outDir_macro_npc + "/" + outputName + ".C";
		
		TString latex = "RUCIO_Run_rel19";

        std::pair<TCanvas *, TGraphAsymmErrors *> p_npc = Draw (v_hist, outEPS, logX, logY, Pad1Function_NPC, Pad1Function_NPC_TGraph, latex, l, Function_DrawLatex_NPC, b_doFit);
		TCanvas           * c_npc = p_npc.first;
		                    c_npc->SaveAs(outPNG);
		                    c_npc->SaveAs(outEPS);
		                    c_npc->SaveAs(outC);
		TGraphAsymmErrors * g_npc = p_npc.second;

		WriteToRootFile(f_NPC_Results_Envelope, g_npc,      "Envelope/NPC_Smooth_FourSteps");

		
    }
    
	f_NPC_Results_Envelope->Close();
	delete f_NPC_Results_Envelope;

	std::string montage_inFiles_ystar  = "png/npc/npc_smooth_stanislavh1_mjj_ystar*_ADD.png";
	std::string montage_inFiles_yboost = "png/npc/npc_smooth_stanislavh1_mjj_yboost*_ADD.png";

	std::string montage_outFiles_ystar  = "npc_smooth_stanislav_ystar.png";
	std::string montage_outFiles_yboost = "npc_smooth_stanislav_yboost.png";

	std::string montage_script_ystar  = "FROM_STANISLAV/RUCIO_Run/Routine/montage_ystar.sh";
	std::string montage_script_yboost = "FROM_STANISLAV/RUCIO_Run/Routine/montage_yboost.sh";

	CreateMontageCommand(montage_inFiles_ystar,  montage_outFiles_ystar,  montage_script_ystar);
	CreateMontageCommand(montage_inFiles_yboost, montage_outFiles_yboost, montage_script_yboost);

    return (0);
}
