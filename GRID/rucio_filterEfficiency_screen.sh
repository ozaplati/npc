#!bin/bash

#
# Setup part
#    ATTANTION - Be carefull on the lenght of SCREEN_NAME string, 
#                if the value of SCREEN_NAME is too long, then the screen command do not execute a process!
#
#PATH_RUCIO=/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7/RUCIO_XXXSteps
#SCREEN_NAME="H7_SoftTune_Partonv7_XXXSteps"
#
#PATH_RUCIO=/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8/RUCIO
#SCREEN_NAME=H7_SoftTune_v8_FILTER_EFF
#
PATH_RUCIO=/mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v8/RUCIO
SCREEN_NAME=H7_SoftTune_Parton_v8_EFF
ARR_JZX=(1 2 3 4 5 6 7 8 9)
#ARR_JZX=(1)
IS_LOG_GENERATE="TRUE"
#BOOL_UNZIP="TRUE"
BOOL_UNZIP="FALSE"
BOOL_NFILES_ROOT="TRUE"



#
# Main part of the script - no changes below please
#
echo ""
echo ""
echo "Rucio directories of download log files:"
CURRENT_PATH=`pwd`
for iJXZ in ${ARR_JZX[*]}
do
	# Evaluate JZ<X>
	JZX="JZ"${iJXZ}
	
	# Debug print
	echo "Working on:"
	echo "    JZX:        "${JZX}
	echo "    PATH_RUCIO: "${PATH_RUCIO}
	
	# Prepare for screen section
	SCREEN_SECTION_NAME=grep_${SCREEN_NAME}_${JZX}
	SCREEN_SECTION_LOG=${PATH_RUCIO}/log.${SCREEN_SECTION_NAME}
	echo "    "
	echo "    SCREEN_SECTION_NAME: "${SCREEN_SECTION_NAME}
	echo "    SCREEN_SECTION_LOG:  "${SCREEN_SECTION_LOG}
	
	# Create screen section
	echo "source ${CURRENT_PATH}/grep_filterEfficiency_fromRucioLogs.sh ${PATH_RUCIO} ${JZX} ${IS_LOG_GENERATE} ${BOOL_UNZIP} ${BOOL_NFILES_ROOT} 2>&1 | tee ${SCREEN_SECTION_LOG}"
	screen -S ${SCREEN_SECTION_NAME} -d -m bash -c "source ${CURRENT_PATH}/grep_filterEfficiency_fromRucioLogs.sh ${PATH_RUCIO} ${JZX} ${IS_LOG_GENERATE} ${BOOL_UNZIP} ${BOOL_NFILES_ROOT} 2>&1 | tee ${SCREEN_SECTION_LOG}"
	echo ""
	echo ""
done

echo ""
echo "DONE"
echo ""
