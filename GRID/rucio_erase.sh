#!bin/bash
# syntax:
#     source rucio_erase.sh
#
# run without arguments to erase your HUGE EVNT DIDSs from SCRATCH_DISK
# the EVNT are defined in INFILE_DIDS file (one line per one DIDS), use explicita name of DIDS, erasse method do not use a wild cards (symbol *)

INFILE_DIDS=DIDS/ERASE/Pythia8_ParticleLVL_ATLASA14NNPDF_JZX_10MEvents_XStep_20221018__rel21_EVNT.txt

########################################################################
# DO NOT MODIFY FOLLOWING MAIN PART OF THE SCRIPT
########################################################################

echo ""
echo "rucio_erase.sh"
echo ""
echo "INFILE_DIDS: " ${INFILE_DIDS}
echo ""
echo ""
echo ""

# Read DIDS from a given file
ARR_DIDS=($(cat ${INFILE_DIDS}))

# Setup proxy
echo "Setup proxy:"
voms-proxy-init -voms atlas

# Setup rucio
setupATLAS
lsetup rucio

# Loop over DIDS
for DIDS in ${ARR_DIDS[@]}; do
	echo "Working on DIDS: "${DIDS}
	
	# Test if the DIDS includes EVNT.pool.root
	if [[ ${DIDS} == *"EVNT.pool.root"* ]]; then
		echo "run rucio erase " ${DIDS}
		rucio erase ${DIDS}
		echo ""
	fi
done
