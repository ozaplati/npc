#!bin/bash
# Syntax:
#     source rucio_delete_rules.sh
#  To remove large files on SCRATCHDISKs from your quata
#  User quota is 200 TB on SCRATCHDISKs per account 
#
# Read input file INFILE_DIDS_RULES_SCRATCHDISK line by line
# Each line includes one RULE_ID, lines includeng '#' will be ingnored as a comment
# Remove rule_id for each rule in the input file
#
#
# The input file can be extracted using an similar commands, just play a little bit with greps
#    rucio list-rules --account ozaplati | grep --color -e "SCRATCHDISK" | grep --color -e "EVNT.pool.root" -e "Rivet.yoda" -e "prod.log" | grep --color -e "user.ozaplati.Herwig7"  | grep --color -e "OK" | grep -e "TB" > tmp_rule_id
# Then copy the table to  LibreOffice, to extract only the firt column with the RULE_IDs
# And save in as a .txt file to `GDIDS_RULE_ID_TO_REMOVE` directory
#
#

#INFILE_DIDS_RULES_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/guids_IJXSv33EM_NoSmooth_clean.txt
#INFILE_DIDS_RULES_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/guids_NPC_v7_prod____stat_dependent_events_clean.txt
#INFILE_DIDS_RULES_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/guids_NPC_v5____bug_no_other_tunes_clean.txt
INFILE_DIDS_RULES_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/guids_NPC_v8_rel23_Herwig7_ReconnnTune_Particle.txt


########################################################################
# DO NOT MODIFY FOLLOWING MAIN PART OF THE SCRIPT
########################################################################

echo ""
echo "rucio_delete_rules.sh"
echo ""
echo "INFILE_DIDS_RULES_SCRATCHDISK: " ${INFILE_DIDS_RULES_SCRATCHDISK}
echo ""
echo ""
echo ""

#
# Read RULE_IDs from a given file
#
ARR_RULE_ID=()
while IFS= read -r line; do
	echo "Text read from file: $line"
	if [[ ${line} != *"#"* ]]; then
		ARR_RULE_ID+=( "$line" )
	fi
done < ${INFILE_DIDS_RULES_SCRATCHDISK}

#
# Setup proxy
#
#echo "Setup proxy:"
#voms-proxy-init -voms atlas --valid 96:00 --vomslife 96:00

#
# Setup rucio
#
setupATLAS
lsetup rucio

#
# Loop over RULE_ID 
#
for RULE_ID in ${ARR_RULE_ID[@]}; do
	#
	# Delete the RULE_ID
	#
	echo "rucio delete-rule " ${RULE_ID}
	rucio delete-rule ${RULE_ID}
done
