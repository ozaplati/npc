	#!bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################

# Generator - e.q.: 
#             GEN=Herwigpp
#             GEN=Pythia8
GEN=Pythia8

# Description - arbitrary description of the produduction e.q.: 
#               DESC="" or DESC="__test1"
DESC="__rel19"

# OTHER SETUP NO NEED TO CHANGE
if     [[ ${GEN} =  "Herwigpp" ]]; then
	ARR_PDF=(UEEE5MSTW2008)
elif   [[ ${GEN} =  "Pythia8"  ]]; then
	#ARR_PDF=(AU2CT10 AU2CTEQ ATLASA14NNPDF ATLASA14CTEQL1)
	ARR_PDF=(ATLASA14NNPDF)

else
	echo "Wrong setup of GEN variable: "${GEN}
	echo "\t apply one of the following:"
	echo "\t\t GEN=Pythia8"
	echo "\t\t GEN=Herwigpp"
	exit
fi

JOB_OPTION=GenerateJetsJO_PY8_EIG.py
ARR_EFFECTS=(PartonLVL ParticleLVL)
ARR_JSAMPLE=(2 3 4 5 6 7 8 9)
#ARR_JSAMPLE=(4)

#N_STEPS=1
#ARR_STEP=$(seq 1 $N_STEPS)
#ARR_STEP=(1)
ARR_STEP=(1 2 3 4 5 6 7 8 9 10)
#ARR_STEP=(11 12 13 14 15 16 17 18 19 20)

####################################
# number of events per one job
#     - use int not string
#N_EVENTS_PER_JOB=1000000
N_EVENTS_PER_JOB=100000

####################################
# number of jobs
#     - use int not string
N_JOBS=100

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################


####################################
# Data tag in format YYYYMMDD
DATE_TAG=`date +"%Y%m%d"`



#####################################################
# total number of events = N_JOBS * N_EVENTS_PER_JOB
#     - do not change
N_EVENTS_TOT="$(( ${N_JOBS}*${N_EVENTS_PER_JOB} ))"

# Do I need to set runNumber ???


#######################################
# description
#     - for a name of output container
#     - for a Woking subDir name
#GRID_DESC="ParticleGun_piplus_eta02_03_${N_EVENTS_TOT}Evt_140520"
M_EVENTS=$((N_EVENTS_TOT/1000000))

#######################################
# paths inicialization
BASE_DIR=`pwd`
WORK_DIR=${BASE_DIR}"/Work"

#######################################
# make Work directories
mkdir -p ${WORK_DIR}

#######################################
# Setup for all jobs
setupATLAS
#voms-proxy-init -voms atlas
lsetup panda  
lsetup pbook                                                           # to run pathena

for EFFECT in ${ARR_EFFECTS[@]}; do                                      # LOOP over EFFECT
	for PDF in ${ARR_PDF[@]}; do                                         # LOOP over PDF
		for JSAMPLE in ${ARR_JSAMPLE[@]}; do                             # LOOP over ptMin ptMax ranges using JSAMPLE
			for STEP in ${ARR_STEP[@]}; do
				
				# name of working directory and name of pathena sample
				DIR_NAME=${GEN}_${EFFECT}_${PDF}_JZ${JSAMPLE}_${M_EVENTS}MEvents_${STEP}Step_${DATE_TAG}${DESC}
				
				WORK_DIR_I=${WORK_DIR}"/"${DIR_NAME}
				
				mkdir -p ${WORK_DIR_I}
				
				cd ${WORK_DIR_I}
				
				# copy all needed files
				cp ${NON_PERTURBATIVE_CORRECTIONS}/Fork/npc/*.so  .
				cp ${NON_PERTURBATIVE_CORRECTIONS}/Fork/npc/${JOB_OPTION}  .
				
				so_files=$(ls *.so)
				echo $so_files

				oldstr=".so "
				newstr=".so,"
				so_files_separated=$(echo $so_files | sed "s/$oldstr/$newstr/g")
				
				# zip all needed files to inTarBall
				MY_JOB_CONTENTS=myjobcontents.tgz
				tar -zcvf ${MY_JOB_CONTENTS} *
				
				################################################################
				# setup AthGeneration for Gen_tf.py
				#asetup 21.6.94,AthGeneration,here
				asetup 19.2.4.12.1,MCProd,64,here
				
				# Rivet
				# source setupRivet.sh
				export RIVET_ANALYSIS_PATH=$PWD
				echo $RIVET_ANALYSIS_PATH
				
				let RUN=${JSAMPLE}+100000
				let SEED=${JSAMPLE}*100000+${STEP}*10000
				
				
				################################################################
				# RUN with pathena
				echo -e "\n\n"
				echo "RUN with pathena"
				echo -e "\n\n"
				
				# prun
				prun --exec "Generate_tf.py \
	--ecmEnergy=13000 \
	--firstEvent=1 \
	--randomSeed=$SEED \
	--jobConfig=${JOB_OPTION} \
	--runNumber=$RUN \
	--maxEvents=$N_EVENTS_PER_JOB \
	--outputEVNTFile=EVNT.pool.root \
	--outputYODAFile Rivet.yoda \
	--env NPC_JSAMPLE=$JSAMPLE NPC_TUNE=$PDF NPC_GEN=$GEN NPC_EFFECT=$EFFECT" \
	--extFile ${JOB_OPTION},${so_files_separated}  \
	--outDS user.ozaplati.${DIR_NAME} \
	--useAthenaPackage \
	--nJobs ${N_JOBS} \
	--output Rivet.yoda,EVNT.pool.root
				
				cd ${BASE_DIR}
				echo ""
				echo ""
				echo ""
			done
		done
	done
done

#--outDS user.ozaplati.${DIR_NAME}_${DATE_TAG} \

#optGridOutputSampleName

#--outputYODAFile=Rivet.yoda \

# --jobConfig=$PWD 
# --jobConfig=mc.Py8_ATLASA14NNPDF.py \
# --inDS RivetATLAS_2018_IJXS_IDXS_Alieksei.so RivetATLAS_2022_IJXS_IDXS.so mc.Py8_ATLASA14NNPDF.py 
# --inTarBall # transef also .so files and mc.py...
# PWD=/srv/workDir
# /usr/bin/env

