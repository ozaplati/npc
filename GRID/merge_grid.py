#!bin/python
import os
PATH         ="/mnt/nfs19/zaplatilek/IJXS/Rivet/Pythia8_PartonLVL_ATLASA14NNPDF_20221018__rel21/"
ARR_JSAMPLES = ["JZ2", "JZ3", "JZ4", "JZ5", "JZ6", "JZ7", "JZ8", "JZ9"]



# input and output paths
#   no need to modify - just check if directory of MERGE_SUBJOUBS exists
subPath_subJobs  = "MERGED_SUBJOBS"
subPath_JSamples = "MERGED_JSAMPLES"

path_subJobs  = PATH + "/" + subPath_subJobs
path_JSamples = PATH + "/" + subPath_JSamples

# mkdir
mkdir_cmd = "mkdir -p " + path_JSamples
os.system(mkdir_cmd)

# loop over JSAMPLES
for JSAMPLE in ARR_JSAMPLES:
	# get list of yoda files
	l_files = [f for f in os.listdir(path_subJobs) if f.endswith(".yoda") and JSAMPLE in f]
	
	# get string of yoda files
	#    using absolute path
	str_files = " ".join([path_subJobs + "/" + str(f) for f in l_files])
	
	# get name of new yoda file
	#     use the same name of the files as very first element of l_files,
	#     but omit XStep information
	tmp_str = l_files[0]
	l_tmp_str = [s for s in tmp_str.split("_") if "Step" not in s]
	merged_yoda = "_".join([str(s) for s in l_tmp_str])
	
	# get string of new yoda file
	#    using absolute path
	merged_yoda = path_JSamples + "/" + merged_yoda
	
	# yoda merge command
	cmd_merge = "yodamerge -o " + merged_yoda + " " + str_files
	os.system (cmd_merge)
	
