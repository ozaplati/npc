#!bin/bash
# syntax:
#     source rucio_download.sh
#
# run without arguments to download your DIDSs, which are defined in INFILE_DIDS file (one line per one DIDS)
# and download the DIDSs into you diskspace. The downloaded DIDs are saved at OUTPUT_PATH directory (absolute path is recomended)
#
# Each DIDS is downloaded using screen section, you can check screen sections via:
#     screen -list
#     screen -r ID
# you can check also log file of the screen section at ${OUTPUT_PATH}
#

 
#RivetRootFiles 
INFILE_DIDS=DIDS/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8.txt
OUTPUT_PATH=${NON_PERTURBATIVE_CORRECTIONS_RIVET}/v8_rel23/

#RivetRootFiles - MissingJZX
#INFILE_DIDS=DIDS/v6_rel23/TMP/Herwig7_PartonLVL_Default_rel23_withROOT_v5_MissingJZX.txt
#OUTPUT_PATH=${NON_PERTURBATIVE_CORRECTIONS_RIVET}/v5_rel23_missingJZX/


########################################################################
# DO NOT MODIFY FOLLOWING MAIN PART OF THE SCRIPT
########################################################################


########################################################################
FILE_NAME=`basename ${INFILE_DIDS} .txt`
OUTPUT_PATH=${OUTPUT_PATH}/${FILE_NAME}/RUCIO

CURRENT_PATH=`pwd`
#CURRENT_PATH=$(realpath $0) # THIS DOES NOT WORK!

########################################################################
echo ""
echo "rucio_download.sh"
echo ""
echo "INFILE_DIDS: " ${INFILE_DIDS}
echo "FILE_NAME:   " ${FILE_NAME}
echo "OUTPUT_PATH: " ${OUTPUT_PATH}
echo ""
echo ""
echo ""
mkdir -p ${OUTPUT_PATH}

# Read DIDS from a given file
ARR_DIDS=($(cat ${INFILE_DIDS}))

# Setup proxy
echo "Setup proxy:"
#voms-proxy-init -voms atlas
#voms-proxy-init --vomslife 96:0 --valid 96:0 -voms atlas

# Loop over DIDS
ITER=0
for DIDS in ${ARR_DIDS[@]}; do
	# download DIDS from rucio using screen
	echo "Working on DIDS: "${DIDS}
	ITER=$((ITER+1))
	SCREEN_NAME=${FILE_NAME}_${ITER}
	SCREEN_LOG=${OUTPUT_PATH}/log.${SCREEN_NAME}
	
	screen -S ${SCREEN_NAME} -d -m bash -c "source ${CURRENT_PATH}/rucio_screen.sh ${OUTPUT_PATH} ${DIDS} 2>&1 | tee ${SCREEN_LOG}"
done
