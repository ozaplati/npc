#!bin/bash
#
# source rucio_screen.sh OUTPUT_PATH DIDS
#
# ARGUMENT 1 ... OUTPUT PATH WHERE YOU WANT TO STORE YOUR DIDS
# ARGUMENT 2 ... NAME OF YOUT DIDS, WHICH YOU WANT DO DOWNLOAD FROM RUCIO DATABASE
#

OUTPUT_PATH=${1}
DIDS=${2}

########################################################################
# GO TO OUTPUT PATH
cd ${OUTPUT_PATH}

# PRINT YOUR ARGUMENTS
echo "Run rucio_screen.sh"            
echo "OUTPUT_PATH: " ${OUTPUT_PATH}   
echo "DIDS:        " ${DIDS}           
echo ""                               
echo "run setupATLAS"                 

# SETUP ATLAS ENVIROMENT
# NOTE, export AND alis COMMANDS DO NOT WORK
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

# SETUP RUCIO - YOU NEED TO HAVE VALID PROXY
echo "run lsetup rucio"               
lsetup rucio  

# DOWNLOAD YOUR DIDS FROM RUCIO                        
echo "run rucio list-dids"            
rucio list-dids ${DIDS}               
echo "run rucio -v download ${DIDS}"  
rucio -v download ${DIDS}             
