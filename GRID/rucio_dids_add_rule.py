import os, sys

# All my .evnt file
# [zaplatilek@ui3 JXS]$ rucio list-dids user.ozaplati:user.ozaplati.*EVNT.pool.root

pathToListOf_DIDsBaseName = "DIDS_Grid_BaseNames/v8_rel23/Herwig7_ParticleLVL_ReconnectionTune_v8.txt"
NDays                     = 365
Comment                   = "ATLAS IJXS - Herwig7 - Reconn - Particle level - v8 - steps 5,6"

dic_outContainers = {
        "EVNT"         : "_EVNT.pool.root",
        "RootFile"     : "_RivetRootFile.root",
        "log.generate" : "_log.generate",
        "log"          : ".log",
        "yoda"         : "_Rivet.yoda",
}



##################################################################################
## DO NOT MODIFY
##################################################################################

def Rucio_add_rule( _did, _time_seconds, _comment, _RES = "PRAGUELCG2_LOCALGROUPDISK" ):
	cmd =  " rucio --account ozaplati  add-rule " + " "
	cmd += " --lifetime " + str(_time_seconds)    + " "
	cmd += " --comment "  + _comment              + " "
	cmd +=  _did                                  + " "
	cmd += " 1 "                                  + " "
	cmd += _RES                                   + " "
	
	#print ("\t\t" + cmd)
	os.system(cmd)
	

def ReadFileToList(_pathToFile) :
	lines = []
	
	# check file exists
	isOk = os.path.isfile(_pathToFile)
	
	if isOk == True:
		with open(_pathToFile) as file:
			for line in file: 
				line = line.strip()
				lines.append(line) 
		return (lines)
	else:
		mgs  = "input file: " + _pathToFile + "does not exist" + "\n"
		msg += "Exit program"
		print (msg)
		exit(msg)

l_DIDsBaseNames = ReadFileToList( pathToListOf_DIDsBaseName )

for DIDBaseName in l_DIDsBaseNames:
	print ("\n\n")
	print ("Working on DIDBaseName: " + DIDBaseName)
	
	for key in dic_outContainers:
		container = key
		container_ends_with = dic_outContainers[key]
		
		DIDFullName = DIDBaseName + container_ends_with
		if DIDBaseName == "":
			print ("\t\t skip " + DIDBaseName)
		elif DIDBaseName[0] == '#':
			print ("\t\t skip " + DIDBaseName)
		else:
			print ("\t\t add-rule for " + DIDFullName)
			seconds = 1 * 3600 * 24 * NDays
			comment = "\'" + Comment + "\'" 
			Rucio_add_rule(DIDFullName,  seconds, comment)
