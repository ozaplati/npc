import sys, os
import argparse

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('true', '1'):
        return True
    elif v.lower() in ('false', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

#####################################################
# Args define
parser = argparse.ArgumentParser()
parser.add_argument('--inputPath',      help='Input to RUCIO directory,  example: --inputPath  /mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO',           type=str, default="/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO")
parser.add_argument('--jSampleMin',     help='Minimal jSample applied for MC generation, example: --jSampleMin 1', type=int,      default=1)
parser.add_argument('--jSampleMax',     help='Maximal jSample applied for MC generation, example: --jSampleMax 9', type=int,      default=9)
parser.add_argument('--outputPath',     help='Output path,               example: --outputPath  /mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO_XXXSteps', type=str, default="/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO_XXXSteps")
parser.add_argument('--debug',          help='Debug mode, do not call os.sytem commands, example: --debug True',   type=str2bool, default=True)
parser.add_argument('--toBeReplacedByXXXSteps', help='String a Step directory to be replace by XXXSteps, example: --toBeReplacedByXXXSteps 1Step',   type=str, default="1Step")



#####################################################
# Args
args=parser.parse_args()
inputPath  = args.inputPath
jSampleMin = args.jSampleMin
jSampleMax = args.jSampleMax
outputPath = args.outputPath
debug      = args.debug
toBeReplacedByXXXSteps = args.toBeReplacedByXXXSteps


print "\n\n\n"
print "Arguments:"
print "args.inputPath:  " + args.inputPath
print "args.jSampleMin: " + str(args.jSampleMin)
print "args.jSampleMax: " + str(args.jSampleMax)
print "args.outputPath: " + args.outputPath
print "args.debug:      " + str(args.debug)

#####################################################
# List of all JZX
v_iJZX = list(range(jSampleMin, jSampleMax+1))
v_JZX  = ["JZ" + str(i) for i in v_iJZX]
print "\n\n\n"
print "Following JZX applied:"
for JZX in v_JZX:
	print "\t" + JZX

#####################################################
# key directories to copy
v_dirKey = [ "RivetRootFile.root", 
             "log.generate",
           ]
print "\n\n\n"
print "Following dirKey applied:"
for dirKey in v_dirKey:
	print "\t" + dirKey
	
	
#####################################################
# Make outputPath directory
cmd_mkdir_path_output = "mkdir -p " + outputPath
if debug == False:
	os.system(cmd_mkdir_path_output)

#####################################################
# Loop over all dirKeys
for dirKey in v_dirKey:
	
	#################################################
	# Loop over all JZX
	for JZX in v_JZX:
		
		#############################################
		# Get list of all directories at path_input,
		#    which also include following strings:
		#    JZX
		#    dirKey
		#    and which do not include XXXSteps
		#
		v_list_dirs = [os.path.join(inputPath, o) for o in os.listdir(inputPath) if os.path.isdir(os.path.join(inputPath, o)) and JZX in o and dirKey in o and "XXXSteps" not in o]
		v_list_dirs.sort()
		
		#############################################
		# Debug print
		print ("\n\n\n")
		print ("Working on: " + JZX + ", " + dirKey)
		for item in v_list_dirs:
			print (item)
		
		#############################################
		# New directory
		newDirBaseName = os.path.basename(v_list_dirs[0])
		newDirBaseName = newDirBaseName.replace(toBeReplacedByXXXSteps, "XXXSteps")
		newDir = outputPath + "/" + newDirBaseName
		
		cmd_mkdir_newDir = "mkdir -p " + newDir
		if debug == False:
			os.system(cmd_mkdir_newDir)
		
		#############################################
		# Move all the files to new directory
		for idir in v_list_dirs:
			copyFrom = idir + "/*"
			copyTo   = newDir + "/."
			cmd_copy = "cp " + copyFrom + " " + copyTo
			print cmd_copy
			if debug == False:
				os.system(cmd_copy)


