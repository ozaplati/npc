#!bin/bash

PATH_RUCIO=/mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23_test/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v5/RUCIO
JZX="JZ2"

# Get array of all log directories from rucio download
arr_rucio=($(find ${PATH_RUCIO} -name "*${JZX}*.log" -type d -regex "^.*$"))

echo ""
echo ""
echo "Rucio directories of download log files:"
for rucioDirLog in ${arr_rucio[*]}
do
    echo ${rucioDirLog}
done
echo ""
echo ""

# Loop over downloaded log directories from rucio
for rucioDirLog in ${arr_rucio[*]}
do
	#rucioDirLog=/mnt/nfs19/zaplatilek/eos/NonPerturbativeCorrections/TMP/TEST_GRID/v5_rel23_test_copy/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v5__logs/RUCIO/user.ozaplati.Herwig7_ParticleLVL_SoftTune_JZ3_10MEvents_1Step_20231107_jet_dijet__rel23_withROOT_v5.log
	
	# Get an array of all .log.tgz files
	arr=($(find ${rucioDirLog} -name "*.log.tgz" -type f -regex "^.*$"))
	arr_grep=()
	
	# Loop over files
	for file in ${arr[*]}
	do
		# DEGUG PRINT
		echo ""
		echo "Working on:"
		echo "   $file"
		
		# Get File name fro full path
		fileName="$(basename ${file})"
			oldstr=".log.tgz"
			newstr=".log"
		
		# Remove .tgz from fileName
		dirName=$(echo $fileName | sed "s/$oldstr/$newstr/g")
		
		# Output directory of the extracted tgz file
		outDir=${rucioDirLog}/${dirName}
		mkdir -p ${outDir}
		
		# Extract the tgz file to new directory
		tar -xf ${file} -C ${outDir}
		
		# Find the log.generate in the new directory of the extracted files
		logFile=$(find ${outDir} -name "log.generate")
		
		# Grep the log.generate file
		#    to extract Filter Efficiency and Weighted Filter Efficiecy
		outFileGrep=${outDir}/log.Grep_Filter_Efficiency
		
		# Remove if already exists
		if [ -f "$outFileGrep" ] ; then
			rm ${outFileGrep}
		fi
		
		touch ${outFileGrep}
		grep --color -e "INFO Events passed" -e "INFO Events passed" -e "INFO Total efficiency" -e "INFO Filter Expression" -e "INFO Filter Efficiency" -e "INFO Weighted Filter Efficiency" ${logFile} >> ${outFileGrep}
		 
		echo ""
		echo ""
		
		# Append the outFileGrep to array
		arr_grep=(${arr_grep[@]} ${outFileGrep})
	done
	
	# Merge all grep files together
	echo ""
	echo ""
	echo "Merge All grep files for give slice"
	echo ""
	echo ""
	
	outFile_All=${rucioDirLog}/log.Grep_Filter_Efficiency_All
	outFile_Eff=${rucioDirLog}/log.Grep_Filter_Efficiency_Only
	outFile_WEff=${rucioDirLog}/log.Grep_Weighted_Filter_Efficiency_Only
	echo ${outFile_All}
	echo ${outFile_Eff}
	echo ${outFile_WEff}
	
	# Remove outFiles if alredy exists from previus runs
	if [ -f "$outFile_All" ] ; then
		rm ${outFile_All}
	fi
	if [ -f "$outFile_Eff" ] ; then
		rm ${outFile_Eff}
	fi
	if [ -f "$outFile_WEff" ] ; then
		rm ${outFile_WEff}
	fi
	
	# Create tje outFiles
	touch ${outFile_All}
	touch ${outFile_Eff}
	touch ${outFile_WEff}
	
	for grepFile in ${arr_grep[*]}
	do
		cat ${grepFile} >> ${outFile_All}
		grep --color -e "INFO Filter Efficiency"          ${grepFile} >> ${outFile_Eff}
		grep --color -e "INFO Weighted Filter Efficiency" ${grepFile} >> ${outFile_WEff}
	done
done

