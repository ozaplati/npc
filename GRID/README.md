### Grid production README
There are two working scripts to submit grid jobs, the scripts are called `prun_<DESCRIPTION>.sh`:
 - `prun_aliaksei_test.sh`     ... to submit code from Athena rel. 19 to produce `Rivet.yoda` files
 - `prun_rel23_withROOT.sh`    ... to submit code from Athena rel. 23 to produce `Rivet.yoda` and `RivetRoot.root` files


#### Before the grid submition
Before the grid submition compile your Rivet routine and perfrom local test for event generation, job-option validation and running of rivet analysis.
Then you can edit grid submition scripts.
Choose which release you want to submit and have a look on the `prun_<DESCRIPTION>.sh` script listed above.
Edit `prun_<DESCRIPTION>.sh` script to:
 - setup `GEN` variable         - to setup MC
 - setup `ARR_TUNE` variable    - to setup array of your TUNES
 - setup `DESC` variable        - to setup description key encoded in the name of output grid container 
 - setup `ARR_EFFECTS` variable - to setup array of your prediction level, usually Parton and Paticle level as `ARR_EFFECTS=(PartonLVL ParticleLVL)`
 - setup `ARR_JSAMPLE` variable - to setup array of JZX slices, usually `ARR_JSAMPLE=(1 2 3 4 5 6 7 8 9)`
 - setup `ARR_STEP` variable    - to setup array of steps for more statistic - which updates seed factors, usually `ARR_STEP=(1 2)`
 - setup `N_JOBS`               - to setup number of grid sub-jobs
 - setup `N_EVENTS_PER_JOB`     - to setup number of generated events in per one sub-job, for N_EVENTS_PER_JOB=25000 optimazed for Herwig7 rel23 production - to be able to use 2 GB core machines
 

### Grid job submition:
```
open new terminal
voms-proxy-init -voms atlas --vomslife 96:00 --valid 96:00
screen -S Grid_submit_NPC
setupATLAS_NPC
cd GRID
source prun_<DESCRIPTION>.sh
CTRL+A+D
```

Finally, have a look on myBigPanda web page monitoring: 
```
https://bigpanda.cern.ch/user/
```

#### Before downloading
Make a copy the needed files to Prague (`PRAGUELCG2_LOCALGROUPDISK`) using rucio
You need to create new rucio rule ether with:
1) rucio web page: `https://rucio-ui.cern.ch`
   - to login you need to have imported grid certificate in your web browser
   - follow `Data Transfer` tab, `New rule` tab, and fill all items for new rucio rule
2) rucio add-rule command as alternative to 1)
   - alternatively you can use a script `GRID/rucio_dids_add_rule.py`, which sends the rucio requests from the command line
   - before running have a look on the script and define following variables:
```
   pathToListOf_DIDsBaseName = "Path/To/Input/File/List/With/Base/Name/Of/The/DIDS"
   NDays                     = 365 
   Comment                   = "ATLAS IJXS - Herwig7 - SoftTune - Particle level - v7 as a comment"
```
   to define input DIDs, time and any comment for the rucio rule. 
   Note, the `pathToListOf_DIDsBaseName` includes list of input DIDs line by line, empty line are possible, lines starting with slash # will be ignored in the script
   Each line in the `pathToListOf_DIDsBaseName` should not include the name of output container (like: `_EVNT.pool.root`, `RivetRootFile.root`, `_log.generate`, `.log`, `_Rivet.yoda` ), such names of output containers are defined in `dic_outContainers` dictionary
   - run the script in screen section as follows
```
   screen -S screen_section_name
   setupATLAS
   voms-proxy-init -voms atlas --vomslife 96:00 --valid 96:00
   lsetup rucio
   python rucio_dids_add_rule.py 2>&1 | tee log.<LOG_NAME>
```
3) Monitor your rucio add-rules on the web page
   - https://rucio-ui.cern.ch/r2d2
4) There is an user quata `200 TB` per account on `SCRATCHDISK`s. If you reach this quata, then you need to clear the `SCRATCHDISK`s
  - Check your quatas
```
rucio whoami
rucio list-account-usage <ozaplati>
```
  - Identify `rule_id`s on `SCRATCHDISK`s to be removed, `rule_is`s are in the first column - play a little bit with grep to sort the correct `rule_id`
```
rucio list-rules --account ozaplati | grep --color -e "SCRATCHDISK" | grep --color -e "EVNT.pool.root" -e "Rivet.yoda" -e "prod.log" | grep --color -e "user.ozaplati.Herwig7"  | grep --color -e "OK" | grep -e "TB" -e "GB" | grep -e "Parton" > tmp_rule_id 
```
  - Remove the `rule_id`
```
rucio delete-rule <rule_id>
```
   


#### Download containers from rucio
```
cd GRID/DIDS
```
  - create `.txt` file of your yoda DIDS and save it in the GRID/DIDS directory
    - use one DID per line
    - you can use `*` symbol
  - check `rucio_download.sh` script 
    - setup `INFILE_DIDS` variable with your `.txt` file
  - setup proxy with `vomslife` and `valid` flags
```
  voms-proxy-init -voms atlas --vomslife 96:00 --valid 96:00
```
  - run the script - the script creates N screen section, 1 section for one line in your `.txt` file to download the DIDS
  
```
  source rucio_download.sh
```



#### After downloading containers from rucio - Double check completness of the files
 - Double-check number of downloaded files
 - Here is an example for `Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7`
```
cd $NON_PERTURBATIVE_CORRECTIONS_RIVET
cd v7_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7
grep --color -A4 -B4 -e "Total files" RUCIO/log.Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7_* > CheckDownloadedFiles.txt
```
 - Read the `CheckDownloadedFiles.txt`, values for `Total files (DID):`, `Total files (filtered):`, and `Downloaded files:` shoulb be the same. In ideal case `4000 files` of `4000 subjob` (but some jobs may failed, be exhausted or be removed from the grid type before the downloading)
 - Double-check size of the files, 1 RivetROOTFile ... approx. 336 KB, 1 log.generate ... approx. 448 KB
 - You may expect for `4000 subjobs`, `4 steps` and `9 JZX slices`: `(336 + 448) * 4000 * 4 * 9 / (1024 * 1024)` `GB` output 
 - Compare this number with `du -sh RUCIO` command

#### After downloading containers from rucio - Copy file to XXXSteps directory
 - Used for large grid production with multiple of `Step`s applied
 - Run sctipt of `mv_dir_after_rucio_download.py`
```
# Try rto run in debug mode - to see, which files you works
# with --debug True, no os.system commands are applied
python mv_dir_after_rucio_download.py --help
python mv_dir_after_rucio_download.py --inputPath </mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO>  --outputPath </mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO_XXXSteps> --jSampleMin 1 --jSampleMax 9 --debug True
# If OK, the --debug False, to copy all the files with os.system commands
python mv_dir_after_rucio_download.py --inputPath </mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO>  --outputPath </mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23_reprocessing/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v7/RUCIO_XXXSteps> --jSampleMin 1 --jSampleMax 9 --debug False


# v8_prod :
python mv_dir_after_rucio_download.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v8/RUCIO  --outputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v8/RUCIO_XXXSteps --jSampleMin 1 --jSampleMax 9 --debug True --toBeReplacedByXXXSteps 5Step
python mv_dir_after_rucio_download.py --inputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8/RUCIO  --outputPath /mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8/RUCIO_XXXSteps --jSampleMin 1 --jSampleMax 9 --debug True --toBeReplacedByXXXSteps 5Step
```
 - finally rename the RUCIO directiries - if you do not apply thi step then `merge_python3.py` may fail with errors
```
cd </mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_ParticleLVL_SoftTune_rel23_withROOT_v8>
mv RUCIO RUCIO_DOWNLOADED
mv RUCIO_XXXSteps RUCIO

cd </mnt/nfs19/zaplatilek/IJXS/Rivet/v8_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v8>
mv RUCIO RUCIO_DOWNLOADED
mv RUCIO_XXXSteps RUCIO

``` 


#### Grep filter efficiency and cross-section
 - Before you merge your RIVETRootFiles.root extract filter efficiancy and cross-section first from `log.generate` file
 - Edit setup part of `rucio_filterEfficiency_screen.sh` script:
 ```
PATH_RUCIO=/mnt/nfs19/zaplatilek/IJXS/Rivet/v7_rel23/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v7/RUCIO
SCREEN_NAME="Herwig7_PartonLVL_Default_rel23_withROOT_v7"
ARR_JZX=(1 2 3 4 5 6 7 8 9)
IS_LOG_GENERATE="TRUE"
BOOL_UNZIP="FALSE"
 ```
 - For rel23 production you should change only `PATH_RUCIO` and `SCREEN_NAME` variabels
 - The script call another script `grep_filterEfficiency_fromRucioLogs.sh` anf run it in a screen section
 - The script `grep_filterEfficiency_fromRucioLogs.sh` loops over all already downloaded files from rucio, identifies `log.generate` file and greps a filter efficiency and a cross-section from it, such numbers will be needed in `merge_python3.py` in the next steps for normalization of MC samples
 - If you do not have `log.generate` container from the grid, you can use `.log` container instead. However this is a depricated approach (applied before `v6` production), in such cases setup `IS_LOG_GENERATE=FALSE` and `BOOL_UNZIP=TRUE` to identyfile `.log` container, apply `tar` on it, indentify `log.generate` and finally extract the values. This approach is very slow for too many log files - several hours event in screen sections
```
source rucio_filterEfficiency_screen.sh
```
