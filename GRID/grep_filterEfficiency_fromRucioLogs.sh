#!bin/bash

# ARGUMENT 1 ... INPUT PATH WHERE YOU HAVE THE RUCIO .log DIRECTORIES DOWNLOADED,
#                USUALLY, SOMETHINK LIKE THIS:
#                    /mnt/nfs19/zaplatilek/IJXS/Rivet/v5_rel23_test/Herwig7_PartonLVL_SoftTune_rel23_withROOT_v5/RUCIO
# ARGUMENT 2 ... STRING OF JZ<X> slice, WHERE <X> IS INTEGER NUMBER 1 TO 9,
#                FOR EXAMPLE: JZ6
# ARGUMENT 3 ... BOOL TRUE OR FALSE TO UNZIP .tgz FILES, TRUE AS DEFAULT
#                TRUE, True, true TO UNZIP
#
# ARGUMENT 5 ... BOOL TRUE OR FALSE TO ALSO PRINT A NUMBER OF RIVETROOT FILES in FIVEN DIRECTORY
#                THIS IS NEEDED IF YOU APPLIED mv_dir_after_rucio_download.py SCRIPT
#                TRUE, True, true TO 
#
#
# Setup part of the script
#
INPUT_PATH=${1}
JZX=${2}
IS_LOG_GENERATE=${3}
IS_LOG_TGZ=""

if [[ "${IS_LOG_GENERATE}" == "true" || "${IS_LOG_GENERATE}" == "True" || "${IS_LOG_GENERATE}" == "TRUE" ]] ; then
	IS_LOG_TGZ="FALSE"
else
	IS_LOG_TGZ="TRUE"
fi

BOOL_UNZIP=${4}
BOOL_NFILES_ROOT=${5}



#
# Main part of the script - no changes below please
#
PATH_RUCIO=${INPUT_PATH}

echo ""
echo ""
echo "RUNNING SCRIPT: grep_filterEfficiency_fromRucioLogs.sh"
echo "   ARG 1 = PATH_RUCIO       = "${1}
echo "   ARG 2 = JZX              = "${2}
echo "   ARG 3 = IS_LOG_GENERATE  = "${3}
echo "   ARG 4 = BOOL_UNZIP       = "${4}
echo "   ARG 5 = BOOL_NFILES_ROOT = "${5}
echo ""
echo ""

# Get array of all log directories from rucio download
#    This find command works recursively
#    If you run the script already, then the unzip directies can be included, such unziped directores additionly inslude substring *.00*
#    Test each potential log directory on *.00* string afterwards

KEY_LOG_DIR=""
if [[ "${IS_LOG_GENERATE}" == "TRUE" ]] ; then
	KEY_LOG_DIR="log.generate"
fi

if [[ "${IS_LOG_TGZ}" == "TRUE" ]] ; then
	KEY_LOG_DIR=".log"
fi






arr_rucio_tmp=($(find ${PATH_RUCIO} -name "*${JZX}*${KEY_LOG_DIR}" -type d -regex "^.*$"))
# Test each potential log directory on *.00* string 
arr_rucio=()
for rucioDirLog_tmp in ${arr_rucio_tmp[*]}
do
	if [[ ${rucioDirLog_tmp} != *".00"* ]];then
		# rucioDirLog_tmp does not contain .00 substring
		arr_rucio=(${arr_rucio[@]} ${rucioDirLog_tmp})
	fi
done

# Print identified directories
#    the rucio directories should look like this
#        $INPUT_PATH/user.ozaplati.<Herwig7<_<Particle>LVL_<Default>_JZ<1>_10MEvents_<1>Step_<20231107>_<jet_dijet__rel23_withROOT_v5>.log
#    counting only one rucio directory per one JZX
echo ""
echo ""
echo "Rucio directories of download log files:"
for rucioDirLog in ${arr_rucio[*]}
do
    echo ${rucioDirLog}
done
echo ""
echo ""

arr_grep=()



# Loop over downloaded log directories from rucio
for rucioDirLog in ${arr_rucio[*]}
do
	echo ""
	echo ""
	echo ""
	echo "IS_LOG_TGZ:      " ${IS_LOG_TGZ}
	echo "IS_LOG_GENERATE: " ${IS_LOG_GENERATE} 
	
	if [[ "${IS_LOG_TGZ}" == "TRUE" ]] ; then
		#####################################
		# It means
		# IS_LOG_TGZ == TRUE and IS_LOG_GENERATE == FALSE
	
		# Get an array of all .log.tgz files
		arr=($(find ${rucioDirLog} -name "*.log.tgz" -type f -regex "^.*$"))
		
		# Loop over files
		for file in ${arr[*]}
		do
			# DEGUG PRINT
			echo ""
			echo "Working on:"
			echo "    $file"
			echo ""
			
			# Get File name fro full path
			fileName="$(basename ${file})"
				oldstr=".log.tgz"
				newstr=".log"
			
			# Remove .tgz from fileName
			dirName=$(echo $fileName | sed "s/$oldstr/$newstr/g")
			
			# Output directory of the extracted tgz file
			outDir=${rucioDirLog}/${dirName}
			mkdir -p ${outDir}
			
			# Extract the tgz file to new directory
			if [[ "${BOOL_UNZIP}" == "true" || "${BOOL_UNZIP}" == "True" || "${BOOL_UNZIP}" == "TRUE" ]] ; then
				tar -xvf ${file} -C ${outDir}
			fi
			
			# Find the log.generate in the new directory of the extracted files
			logFile=$(find ${outDir} -name "log.generate")
			
			# Grep the log.generate file
			#    to extract Filter Efficiency and Weighted Filter Efficiecy
			outFileGrep=${outDir}/log.Grep_logGenerate
			
			# Remove if already exists
			if [ -f "$outFileGrep" ] ; then
				rm ${outFileGrep}
			fi
			
			# Check if log.generate file exist
			#     If so, then use grep command
			if [ -f "$logFile" ] ; then
				touch ${outFileGrep}
				grep --color -e "INFO Events passed" -e "INFO Events passed" -e "INFO Total efficiency" -e "INFO Filter Expression" -e "INFO Filter Efficiency" -e "INFO Weighted Filter Efficiency" -e "INFO MetaData: cross-section" -e "crossSection()/picobarn():" -e "crossSection()/picobarn):" ${logFile} >> ${outFileGrep}
			else
				echo "    log.generate does not exist"
			fi 
			echo ""
			echo ""
			
			# Append the outFileGrep to array
			if [ -f "$outFileGrep" ] ; then
				arr_grep=(${arr_grep[@]} ${outFileGrep})
			fi
			
		done
	else
		#####################################
		# It means
		# IS_LOG_GENERATE == TRUE and IS_LOG_TGZ == FALSE
		#
		outFileGrep_tmp=${rucioDirLog}/log.Grep_logGenerate_tmp
		outFileGrep=${rucioDirLog}/log.Grep_logGenerate
		
		# Remove potencially existing files
		if [ -f "$outFileGrep_tmp" ] ; then
			rm ${outFileGrep_tmp}
		fi
		
		if [ -f "$outFileGrep" ] ; then
			rm ${outFileGrep}
		fi
		
		
		
		arr_logFiles=($(find ${rucioDirLog} -name "*00*.log.generate" -type f -regex "^.*$"))
		for logFile in ${arr_logFiles[*]}
		do
			echo "Check: " ${logFile}
		done
		
		for logFile in ${arr_logFiles[*]}
		do
			# Grep the key words
			grep --color -e "INFO Events passed =" -e "INFO Events passed ="                                                                              ${logFile} >> ${outFileGrep_tmp}
			grep --color -e "INFO Total efficiency =" -e "INFO Filter Expression =" -e "INFO Filter Efficiency =" -e "INFO Weighted Filter Efficiency ="  ${logFile} >> ${outFileGrep_tmp}
			grep --color -e "INFO MetaData: cross-section" -e "crossSection()/picobarn():" -e "crossSection()/picobarn):"                                 ${logFile} >> ${outFileGrep_tmp}
		done
		# Some lines are duplicated with additional "line:" substring
		#    Try to read outFileGrep_tmp line by line in while loop
		#    And after thet remove unwanted lines in for loop
		ARR_GREP_LINES=()
		IFS=$'\n' # set the Internal Field Separator to newline
		while read -r line; do
			ARR_GREP_LINES+=( "$line" )
		done < ${outFileGrep_tmp}
		
		STR_UNWANTED1="line:"
		STR_UNWANTED2="_keyString"
		for GREP_LINE in ${ARR_GREP_LINES[@]}; do
			if [[ "${GREP_LINE}" != *"$STR_UNWANTED1"* && "${GREP_LINE}" != *"$STR_UNWANTED2"* ]]; then
				echo ${GREP_LINE} >> ${outFileGrep}
			fi
		done
		
		# Remove temporary file
		if [ -f "$outFileGrep_tmp" ] ; then
			rm ${outFileGrep_tmp}
		fi
		
		# Append the outFileGrep to array
		if [ -f "$outFileGrep" ] ; then
			arr_grep=(${arr_grep[@]} ${outFileGrep})
		fi
	fi
	
	
	if [[ "${BOOL_NFILES_ROOT}" == "TRUE" ]] ; then
		
		# Get path to RootRivet directory from rucoDirLog variables
		pathTo_ROOTRivetFiles=${rucioDirLog}
			oldstr="_log.generate"
			newstr="_RivetRootFile.root"
			pathTo_ROOTRivetFiles=$(echo $pathTo_ROOTRivetFiles | sed "s/$oldstr/$newstr/g")
		
		# Number of *.RivetRootFile.root files in ${pathTo_ROOTRivetFiles} directory
		NFiles=$(find ${pathTo_ROOTRivetFiles} -name '*.RivetRootFile.root' | wc -l)
		
		outFile_NFiles=${rucioDirLog}/log.RootRivet_NFiles
		echo "NFiles = " ${NFiles}
		echo "NFiles = " ${NFiles} > ${outFile_NFiles}
	fi
	
	# Merge all grep files together
	echo ""
	echo ""
	echo "Merge All grep files for give slice"
	echo ""
	echo ""

	outFile_All=${rucioDirLog}/log.Grep_All
	outFile_Eff=${rucioDirLog}/log.Grep_Filter_Efficiency_Only
	outFile_WEff=${rucioDirLog}/log.Grep_Weighted_Filter_Efficiency_Only
	outFile_XS_nb=${rucioDirLog}/log.Grep_XS_nb_Only
	outFile_XS_pb=${rucioDirLog}/log.Grep_XS_pb_Only

	echo ${outFile_All}
	echo ${outFile_Eff}
	echo ${outFile_WEff}
	echo ${outFile_XS_nb}
	echo ${outFile_XS_pb}

	# Remove outFiles if alredy exists from previus runs
	if [ -f "$outFile_All" ] ; then
		rm ${outFile_All}
	fi
	if [ -f "$outFile_Eff" ] ; then
		rm ${outFile_Eff}
	fi
	if [ -f "$outFile_WEff" ] ; then
		rm ${outFile_WEff}
	fi
	if [ -f "$outFile_XS_nb" ] ; then
		rm ${outFile_XS_nb}
	fi
	if [ -f "$outFile_XS_pb" ] ; then
		rm ${outFile_XS_pb}
	fi

	# Create tje outFiles
	touch ${outFile_All}
	touch ${outFile_Eff}
	touch ${outFile_WEff}
	touch ${outFile_XS_nb}
	touch ${outFile_XS_pb}

	# List of all grep files
	echo ""
	echo ""
	echo ""
	echo "List of grep files:"
	for grepFile in ${arr_grep[*]}
	do
		echo "    "${grepFile}
	done
	
	
	for grepFile in ${arr_grep[*]}
	do
		# All
		cat ${grepFile} >> ${outFile_All}
		
		# FilterEfficiency only
		grep --color -e "INFO Filter Efficiency"          ${grepFile} >> ${outFile_Eff}
		grep --color -e "INFO Weighted Filter Efficiency" ${grepFile} >> ${outFile_WEff}
		
		# Cross-section only
		grep --color -e "INFO MetaData: cross-section"    ${grepFile} >> ${outFile_XS_nb}
		grep --color -e "crossSection()/picobarn():" -e "crossSection()/picobarn):"      ${grepFile} >> ${outFile_XS_pb}
	done
	
done

