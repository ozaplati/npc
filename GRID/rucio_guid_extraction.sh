#!bin/bash
# Syntax:
#     source rucio_guid_extraction.sh
#  To get guids on SCRATCHDISKs 
#
# Read input file INFILE_DIDS_SCRATCHDISK line by line
# Each line includes one DIDS, lines includeng '#' will be ingnored as a comment
#
#

INFILE_DIDS_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/dids_NPC_v8_rel23_Herwig7_ReconnnTune_Particle.txt
OUTFILE_GUIDS_SCRATCHDISK=DIDS_RULE_ID_TO_REMOVE/guids_NPC_v8_rel23_Herwig7_ReconnnTune_Particle.txt


########################################################################
# DO NOT MODIFY FOLLOWING MAIN PART OF THE SCRIPT
########################################################################

echo ""
echo "rucio_guid_extraction.sh"
echo ""
echo "INFILE_DIDS_SCRATCHDISK: " ${INFILE_DIDS_SCRATCHDISK}
echo ""
echo ""
echo ""

#
# Read DIDS from a given file to an array
#
ARR_DIDS=()
while IFS= read -r line; do
	echo "Text read from file: $line"
	if [[ ${line} != *"#"* ]]; then
		ARR_DIDS+=( "$line" )
	fi
done < ${INFILE_DIDS_SCRATCHDISK}

#
# Setup proxy
#
#echo "Setup proxy:"
#voms-proxy-init -voms atlas --valid 96:00 --vomslife 96:00

#
# Setup rucio
#
#setupATLAS
#lsetup rucio

FILE_LIST_OF_ALL_RULES=DIDS_RULE_ID_TO_REMOVE/All_rules_tmp.txt
rucio list-rules --account ozaplati > ${FILE_LIST_OF_ALL_RULES}

rm ${OUTFILE_GUIDS_SCRATCHDISK}

#
# Loop over DIDS 
#
for DIDS in ${ARR_DIDS[@]}; do
	#
	# Get GUID for given DIDS
	#
	# echo "rucio list-rules --account ozaplati | grep --color -e " ${DIDS}
	#rucio list-rules --account ozaplati | 
	grep --color -e ${DIDS} ${FILE_LIST_OF_ALL_RULES} 2>&1 | tee --append ${OUTFILE_GUIDS_SCRATCHDISK}
done

rm ${FILE_LIST_OF_ALL_RULES}
