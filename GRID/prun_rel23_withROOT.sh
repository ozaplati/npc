#!bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################


# Generator - e.q.: 
#             GEN=Herwig7
GEN=Herwig7

# Description - arbitrary description of the produduction e.q.: 
#               DESC="" or DESC="__test1"
#DESC="_jet_dijet__rel23_withROOT_v8_prod"
DESC="myTinyTest2"

# OTHER SETUP NO NEED TO CHANGE
if     [[ ${GEN} =  "Herwig7" ]]; then
	#ARR_TUNE=(Default)
	ARR_TUNE=(SoftTune)
	#ARR_TUNE=(BaryonicReconnectionTune)
else
	echo "Wrong setup of GEN variable: "${GEN}
	echo "\t apply one of the following:"
	echo "\t\t GEN=Herwig7"
	exit
fi

#ARR_EFFECTS=(PartonLVL ParticleLVL)
ARR_EFFECTS=(ParticleLVL)
#ARR_EFFECTS=(PartonLVL)


## FOR TESTING
#ARR_JSAMPLE=(9)
#ARR_STEP=(1 2)
#ARR_STEP=(1)

## FOR v0
#ARR_JSAMPLE=(2)
ARR_JSAMPLE=(1 2 3 4 5 6 7 8 9)
#ARR_JSAMPLE=(4 6 7 8)

#ARR_STEP=(5 6)
ARR_STEP=(7)
#ARR_STEP=(2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)
#ARR_STEP=(2 3)
#ARR_STEP=(1 2 3 4 5 6 7 8 9 10)

## FOR v0 and v1
#ARR_JSAMPLE=(3 4 5)
#ARR_STEP=(11 12 13 14 15 16 17 18 19 20)

## FOR v0 v1 and v2
#ARR_JSAMPLE=(3 4 5)
#ARR_STEP=($(seq 21 1 60))

## FOR v0 v1 and v2
#ARR_JSAMPLE=(6 7)
#ARR_STEP=($(seq 11 1 30))

echo "Check value of STEP in ARR_STEP:"
for STEP in ${ARR_STEP[@]}; do echo ${STEP} ; done


####################################
# number of events per one job
#     - use int not string
#N_EVENTS_PER_JOB=1000000
#N_EVENTS_PER_JOB=100000
#N_EVENTS_PER_JOB=25000
N_EVENTS_PER_JOB=250


####################################
# number of jobs
#     - use int not string
#N_JOBS=4000
#N_JOBS=400
N_JOBS=1

#N_JOBS=2

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################


####################################
# Data tag in format YYYYMMDD
DATE_TAG=`date +"%Y%m%d"`



####vo#################################################
# total number of events = N_JOBS * N_EVENTS_PER_JOB
#     - do not change
N_EVENTS_TOT="$(( ${N_JOBS}*${N_EVENTS_PER_JOB} ))"

# Do I need to set runNumber ???


#######################################
# description
#     - for a name of output container
#     - for a Woking subDir name
#GRID_DESC="ParticleGun_piplus_eta02_03_${N_EVENTS_TOT}Evt_140520"
M_EVENTS=$((N_EVENTS_TOT/1000000))

#######################################
# paths inicialization
BASE_DIR=`pwd`
WORK_DIR=${BASE_DIR}"/Work"
source ~/.bash_profile_container


#######################################
# make Work directories
mkdir -p ${WORK_DIR}

#######################################
# Setup for all jobs
#setupATLAS
#voms-proxy-init -voms atlas
#lsetup panda  
#lsetup pbook                                                           # to run pathena

for EFFECT in ${ARR_EFFECTS[@]}; do                                      # LOOP over EFFECT
	for TUNE in ${ARR_TUNE[@]}; do                                       # LOOP over TUNES
		export MY_TUNE=${TUNE}
		for JSAMPLE in ${ARR_JSAMPLE[@]}; do                             # LOOP over ptMin ptMax ranges using JSAMPLE
			
			# Set JOB_OPTION
			JOB_OPTION=""
			if [ ${JSAMPLE} == 1 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ1.py
			elif [ ${JSAMPLE} == 2 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ2.py
			elif [ ${JSAMPLE} == 3 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ3.py
			elif [ ${JSAMPLE} == 4 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ4.py
			elif [ ${JSAMPLE} == 5 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ5.py
			elif [ ${JSAMPLE} == 6 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ6.py
			elif [ ${JSAMPLE} == 7 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ7.py
			elif [ ${JSAMPLE} == 8 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ8.py
			elif [ ${JSAMPLE} == 9 ]
				then
				JOB_OPTION=mc.H7EG_jetjet_72_Cluster_JZ9plus.py
			fi
			
			for STEP in ${ARR_STEP[@]}; do
				
				# name of working directory and name of pathena sample
				# BaryonicReconnectionTune
				
				# Tune name update in DIR_NAME
				# BaryonicReconnectionTune -> ReconnectionTune
				TUNE_IN_DIRNAME_TMP=${TUNE}
				STR_OLD="BaryonicReconnectionTune"
				#STR_NEW="ReconnectionTune"
				STR_NEW="ReconnTune"
				TUNE_IN_DIRNAME_TMP=$(echo $TUNE_IN_DIRNAME_TMP | sed "s/$STR_OLD/$STR_NEW/g")
				DIR_NAME=${GEN}_${EFFECT}_${TUNE_IN_DIRNAME_TMP}_JZ${JSAMPLE}_${M_EVENTS}MEvents_${STEP}Step_${DATE_TAG}${DESC}
				
				WORK_DIR_I=${WORK_DIR}"/"${DIR_NAME}
				
				mkdir -p ${WORK_DIR_I}
				
				cd ${WORK_DIR_I}
				
				# copy all needed files
				RIVET_OBJECT=RivetATLAS_2023_IJXS_IDXS_Rivet_3_1_8_ROOT.so
				cp ${NON_PERTURBATIVE_CORRECTIONS}/Fork/npc/${RIVET_OBJECT}  .
				cp ${NON_PERTURBATIVE_CORRECTIONS}/Fork/npc/${JOB_OPTION}    .
				
				so_files=$(ls *.so)
				echo $so_files
				
				oldstr=".so "
				newstr=".so,"
				so_files_separated=$(echo $so_files | sed "s/$oldstr/$newstr/g")
				
				# zip all needed files to inTarBall
				MY_JOB_CONTENTS=myjobcontents.tgz
				tar -zcvf ${MY_JOB_CONTENTS} *
				
				################################################################
				# setup AthGeneration for Gen_tf.py
				asetup 23.6.18,AthGeneration,here
				
				
				# Rivet
				# source setupRivet.sh
				source setupRivet
				
				export RIVET_ANALYSIS_PATH=$PWD
				echo $RIVET_ANALYSIS_PATH
				
				
				
				let RUN=${JSAMPLE}+100000
				################################################################
				# Seed!
				#Old seed - before 2023_11_08
				#let SEED=${JSAMPLE}*1000000+${STEP}*${N_EVENTS_PER_JOB}
				
				#New seed from 2023_11_09
				# One seed per one subjob
				#                   10 000M + Step*100M*2
				#                   up to 50 Steps
				#                   Start with Step 2
				#let SEED=${JSAMPLE}*10000000000+${STEP}*100000000*2
				
				#New seed from 2023_11_09
				# One seed per one subjob
				#                   10M + Step*100k*2
				#                   up to 50 Steps
				#                   Start with Step 2
				let SEED=${JSAMPLE}*10000000+${STEP}*100000*2
				
				################################################################
				# RUN with pathena
				lsetup panda
				echo -e "\n\n"
				echo "RUN with pathena"
				echo -e "\n\n"
				
				# prun
				prun --exec "Gen_tf.py \
	--ecmEnergy=13000 \
	--firstEvent=1 \
	--randomSeed=%RNDM:$SEED \
	--jobConfig=. \
	--runNumber=$RUN \
	--maxEvents=$N_EVENTS_PER_JOB \
	--outputEVNTFile=EVNT.pool.root \
	--rivetAnas=ATLAS_2023_IJXS_IDXS_Rivet_3_1_8 \
	--outputYODAFile Rivet.yoda \
	--env NPC_JSAMPLE=$JSAMPLE NPC_TUNE=$TUNE NPC_GEN=$GEN NPC_EFFECT=$EFFECT" \
	--extFile ${JOB_OPTION},${so_files_separated}  \
	--outDS user.ozaplati.${DIR_NAME} \
	--useAthenaPackage \
	--nJobs ${N_JOBS} \
	--outputs Rivet.yoda,RivetRootFile.root,log.generate,EVNT.pool.root 
				
				cd ${BASE_DIR}
				echo ""
				echo ""
				echo ""
			done
		done
	done
done

# 	--jobConfig=${WORK_DIR_I} \ 
#	--jobConfig=${JOB_OPTION} \ 
# 5 GB of memory via --memory 5120 
# 	--mergeOutput
# 	--outputs Rivet.yoda,RivetRootFile.root,log.generate
