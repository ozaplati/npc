theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = [ 'pool.root' ]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

#rivet.Analyses += ['MC_JETS', 'ATLAS_2023_IJXS_IDXS_Rivet_3_1_8', 'MC_GENERIC']
rivet.Analyses += ['ATLAS_2023_IJXS_IDXS_Rivet_3_1_8']
rivet.RunName = ''
#rivet.HistoFile = 'MyOutput_local.yoda'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
#rivet.SkipWeights=True
job += rivet
