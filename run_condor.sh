#!bin/bash

########################################################################
#                           FUNCTIONS                                  #
########################################################################

FUNCTION_SUBMIT () {
	# SUBMIT FUNCTION TO CREATE DIRECTORY STRUCTURE FOR HTCONDOR
	# COPY ALL NECESERRY FILE
	# AND SUBMIT THE JOBS
	
	# SYNTAX:
	# FUNCTION_SUBMIT <GENERATOR> <TUNE> <EFFECT> <JOB_OPTION> <WORKING_PATH>
	
	local _GEN=${1}
	
	# ATLAS TUNE OF MULTIPRATON INTERACTIONS 
	#    ATLASA14NNPDF BY DEFAULT
	local _TUNE=${2}
	
	# EFFECT OF PartonLVL, ParticleLVL, UEOnly, HadOnly 
	#    PartonLVL BY DEFAULT
	local _EFFECT=${3}
	
	# JobOption
	local _JO=${4}
	
	# PATH TO NEW WORKING DIRECTORY WHICH WILL BE CREATED
	local _WORKING_PATH_INPUT=${5}
	
	local _N_STEPS=${6}
	
	# MAKE DIRECTORY STRUCTURE
	local WORKING_PATH=${_WORKING_PATH_INPUT}/${_GEN}_${_EFFECT}_${_TUNE}
	mkdir -p ${WORKING_PATH}
	
	local OUTPUT_DIR=BatchOutput
	
	mkdir -p  ${WORKING_PATH}/${OUTPUT_DIR}
	mkdir -p  ${WORKING_PATH}/logs
	
	# COPY NECESSERY FILES
	cp *.so  ${WORKING_PATH}/.
	cp *.so  ${WORKING_PATH}/.
	cp ${JO} ${WORKING_PATH}/.
	cp HTCrunBatch.sh ${WORKING_PATH}/.
	
	cp myjob.submit ${WORKING_PATH}/.
	
	local JOBSUBMIT_FILE_NAME=myjob.submit
	local JOBSUBMIT_FILE=${WORKING_PATH}/${JOBSUBMIT_FILE_NAME}
	
	# UPDATE myjob.submit FILE
	declare -A arr_CHANGES
	
	arr_CHANGES=( 
			["EIG = ATLASA14NNPDF"]="EIG = ${_TUNE}" 
			["PartonLVL"]="${_EFFECT}" 
			["Pythia8"]="${_GEN}"
			["mc.Py8_ATLASA14NNPDF.py"]="${_JO}"
		)
	
	# LOOP OVER ALL CHANGES
	for key in "${!arr_CHANGES[@]}"; do
		echo "     UPDATE myjob.submit file: "
		echo "         Find         \"${key}\""
		echo "         Replace with \"${arr_CHANGES[$key]}\""
		echo "         in ${JOBSUBMIT_FILE}"
		KEY=${key}
		VAL=${arr_CHANGES[$key]}
		
		# seed command need to be double quotation marsks
		#     single quotation marks do not work
		sed -i "s/${KEY}/${VAL}/g" ${JOBSUBMIT_FILE} 
	done

	for i in $(seq 2 $_N_STEPS); do 
		echo " "                                         >> ${JOBSUBMIT_FILE}
		echo "Step    = ${i}"                            >> ${JOBSUBMIT_FILE}
		echo "queue \$(NREP) JSAMPLE in 2 3 4 5 6 7 8 9" >> ${JOBSUBMIT_FILE}
		echo " "                                         >> ${JOBSUBMIT_FILE}
	 done

	# SUBMIT THE JOBS
	cd ${WORKING_PATH}
	echo "submit the jobs" 
	condor_submit ${JOBSUBMIT_FILE_NAME}
	

}

########################################################################
#                             MAIN                                     #
########################################################################

# NEW WORKING DIRECTORY
# NEED TO BE SET
WORKING_DIR=HTCondor_Run_5
CURRENT_PATH=${PWD}
WORKING_PATH=${CURRENT_PATH}/${WORKING_DIR}

N_STEPS=100

GEN="Pythia8"
#GEN="Herwig7"

arr_EFFECTS=( "PartonLVL" "ParticleLVL" )
#arr_EFFECTS=( "PartonLVL" "ParticleLVL" "UEOnly" "HadOnly" )

########################################################################
# SETUP TUNES AND EFFECTS
if   [ ${GEN} =  "Pythia8" ]; then
	arr_TUNES=( "ATLASA14NNPDF" )
	#arr_TUNES=( "AU2CT10" "AU2CTEQ" "ATLASA14NNPDF" "ATLASA14CTEQL1" )
	JO=mc.Py8_ATLASA14NNPDF.py
	
	#arr_EFFECTS=( "PartonLVL" "ParticleLVL" "UEOnly" "HadOnly" )
elif [ ${GEN} =  "Herwig7" ]; then
	# H7.1-Default as Default tune
	# MMHT2014 as default pdf
	arr_TUNES=( "DEFAULT" )
	JO=mc.H7_DEFAULT.py
else
	echo "ERROR - run_condor.sh - Invalid value of GEN=$GEN} - Setup GEN=Pythia8 or GEN=Herwig7"
	echo "return 1"
	return 1
fi


# LOOP OVER TUNES
for TUNE in "${arr_TUNES[@]}"; do
	# LOOP OVER EFFECT
	for EFFECT in "${arr_EFFECTS[@]}"; do
		printf "\n\n\n"
		printf "WORKING ON: ${TUNE}, ${EFFECT} \n" 
		FUNCTION_SUBMIT ${GEN} ${TUNE} ${EFFECT} ${JO} ${WORKING_PATH} ${N_STEPS}
		cd ${CURRENT_PATH}
	done
done
